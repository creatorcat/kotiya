::
:: Copyright 2022 - Knolle the Creator Cat (creatorcat.de) and project contributors
::
:: Licensed under the Apache License, Version 2.0 (the "License").
:: You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
::

:: This is a wrapper for the real gradle wrapper launcher script.
:: It runs gradlew with the JRE/JDK of our choice.
:: This is the best way to define gradle JRE because:
:: * gradle.properties JRE path is not recognized when calling "gradlew --version"
::   and thus leads to confusion
:: * gradle.properties in main project dir does not apply to included builds (i.e. build-src)
@echo off
setlocal
  set JAVA_HOME=%JDK11_HOME%
  :: We need to find a proper path to run the script when called from anywhere,
  :: so we extract it from this script's call command.
  :: However, we want to keep the working dir of the caller to support running directly in a sub-project dir.
  call "%~dp0\gradle\wrapper\gradlew.bat" %*
  exit /B %ERRORLEVEL%
endlocal
