/*
 * Copyright 2022 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.buildlogic.project

/**
 * Pre-compiled script plugin: Use Common Gradle Wrapper
 *
 * Makes this project use the common wrapper i.e. the wrapper of generic-build-logic.
 * Calling this project's wrapper task will:
 * * Not execute the original task at all (disabled)
 * * Instead delegate to the generic-build-logic wrapper task
 * * Copy the generic wrapper properties file into this project's canonical wrapper dir to satisfy IDEs
 */

val genericBuildLogic = gradle.includedBuild("generic-build-logic")
val wrapperDirPart = "gradle/wrapper"

val copyProps = tasks.register<Copy>("wrapperCopyPropertiesToDefaultLocation") {
    from(genericBuildLogic.projectDir.resolve("$wrapperDirPart/gradle-wrapper.properties"))
    into(wrapperDirPart)
    dependsOn(genericBuildLogic.task(":wrapper"))
}

tasks {
    named<Wrapper>("wrapper") {
        enabled = false
        dependsOn(copyProps)
    }
}
