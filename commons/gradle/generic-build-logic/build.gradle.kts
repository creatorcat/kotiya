/*
 * Copyright 2021 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

plugins {
    `kotlin-dsl`
    idea
}

description = "Generic build logic to be used in all main projects"

///////////////////////////////
// Kotlin
///////////////////////////////

repositories {
    mavenCentral()
}

///////////////////////////////
// Gradle Wrapper
///////////////////////////////

val GRADLE_VERSION: String by project

tasks {
    wrapper {
        distributionType = Wrapper.DistributionType.ALL
        gradleVersion = GRADLE_VERSION
        // put script files in wrapper dir - we have own wrapper scripts
        scriptFile = jarFile.resolveSibling("gradlew")
    }
}

////////////////////////////
// IDEA
////////////////////////////

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
        excludeDirs.add(file("gradle/wrapper"))
    }
}
