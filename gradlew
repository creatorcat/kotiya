#!/usr/bin/env sh
#
# Copyright 2022 - Knolle the Creator Cat (creatorcat.de) and project contributors
#
# Licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
#

# This is just a delegate to our generic-build-logic gradle wrapper launcher script.

# We need to find a proper path to run the script when called from anywhere,
# so we extract it from this script's call command.
# However, we want to keep the working dir of the caller to support running directly in a sub-project dir.
currentScriptDir=$(dirname "$0")
"$currentScriptDir/commons/gradle/generic-build-logic/gradlew" $*
exit $?
