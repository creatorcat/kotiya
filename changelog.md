# Kotiya Release Notes

For an explanation of the versioning scheme see [the main readme](readme.md#versioning-scheme).

## He-4

Git-Tag: [He-4](https://gitlab.com/creatorcat/kotiya/tags/He-4)

### Changes

#### Dependency Updates

* Kotlin 1.3.21 -> 1.4.10

### Bug-Fixes

Documentation:
* Fix changelog to readme link (see top of this file)

### For Developers

#### Development Environment

Eclipse support:
* Add Eclipse product to support UMLet UML diagrams
* Add Eclipse configuration for all projects (via Gradle)
* Document Eclipse IDE support in [devdoc](doc/devdoc.md#eclipse-configuration)

Project documentation:
* At the moment UMLet *and* PlantUML are used for adding diagrams to the documentation.
  In the future this might change to PlantUML only.
* Add PlantUML template: modules overview package diagram
* Add UMLet UML template: repository content overview
* Document doc-md file generation
* Move markdown doc sources into specific folder to align them with UMLet sources
* Mark generated `.md` files as generated in IDEs

IntelliJ settings:
* Improve IDE inspections: check for problematic whitespace
* Improve IDE inspections: ignore unresolved access warning in Gradle files
* Fix marking of generated files concerning Gradle wrapper
* Adjust markdown navigator settings to plugin update

#### Test Dependency Updates

* JUnit 5.1.1 -> 5.5.2
* mockito-kotlin 2.0.0 -> 2.2.0

#### Build

Introduction of build-src project:
* Add `gradle/build-src` as included Kotlin-JVM project for code supporting the build itself
* Add and use GitIgnoreReader as first real code in build-src project

Introduction of `basics.gradle`:
* Defines basic properties, repositories and utilities for all builds (main & build-src)
* Replaces `gradle.properties`

Kotlin Build:
* Remove custom MPP dependency adjustment since new Kotlin Plugin does this now

Dokka Documentation generation:
* Add new configuration property `cccInternalDocDeps` to define internal dependencies for doc generation
* Adjust Dokka build tasks after plugin update and improve them where possible
* Add more Gradle properties and functions to simplify link creation

General Build improvements:
* Add property `gradleScriptsDir` for script plugins folder
* Add special IDE Gradle script to configure Eclipse and IntelliJ

#### Build Dependency Updates

* Gradle 5.2 -> 6.7
* Dokka 0.9.17 -> 0.9.18

## He-3 - New Gradle and Collection Utilities

Git-Tag: [He-3](https://gitlab.com/creatorcat/kotiya/tags/He-3)

### core - 0.9

Collections:
* Added map extensions [merge](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-mutable-map/merge.html)
  and [mergeAll](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-mutable-map/merge-all.html)
* Added collection extension
  [addAll(vararg elements)](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-mutable-collection/add-all.html)

Text:
* Added the [TextAsTextSerializer](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.text/-text-as-text-serializer) that can write and read text lists

### test-core - 0.9

*internal changes only*

### matchers - 0.11

Common matchers:
* Added new collection content matchers [containsAll](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers.common/contains-all.html)
  and [containsOnly](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers.common/contains-only.html)

### gradle - 0.8

Task utilities:
* Added task extensions to print java-style logs:
  [logJavaStyleError](https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle/de.creatorcat.kotiya.gradle/org.gradle.api.-task/log-java-style-error.html)
  and [logJavaStyleWarning](https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle/de.creatorcat.kotiya.gradle/org.gradle.api.-task/log-java-style-warning.html)
* Added task dependency support predicate: [isDirectTaskDependency](https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle/de.creatorcat.kotiya.gradle/is-direct-task-dependency.html)
* Added the [TaskGroup](https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle/de.creatorcat.kotiya.gradle/-task-group) object to easily access all standard Gradle task group names
* **Deprecated** `Project.taskOfType` as there are better alternatives using standard Gradle methods

### test-gradle - 0.8

Restructured [FunctionalGradleTestBase](https://creatorcat.gitlab.io/kotiya/docs/test-gradle/html/kotiya-test-gradle/de.creatorcat.kotiya.test.gradle/-functional-gradle-test-base):
* Improved mechanism for applying plugins
* More fine grained control options of the generated script content through new
  (overridable) methods and properties

### For Developers

Improved $-token dokka link generation for markdown documentation:
* url-style is not necessary anymore for classes, functions and values (no need to escape capital letters anymore)
* new macro `$dokkaMd` generates a full link: `[lastArg](dokka-url)`
* improved TODO-Tag removal

Build script improvements:
* `base`-plugin is now already applied in root project so clean works there too
* automatic publishing configuration for gradle plugin projects is now **disabled** because it interferes with
  our own publishing
* new method for module-projects: `excludeTestResource(path)` to exclude generated test resources from IDEA and Gradle

## H-3 - More utilities and better documentation

Git-Tag: [H-3](https://gitlab.com/creatorcat/kotiya/tags/H-3)

### core - 0.8

Data structures:
* Added [PropertyBasedDataDefinition](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-property-based-data-definition)
  and [combinedHashCodeOf](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/combined-hash-code-of.html) general object utilities
* Added [Wrap<T>](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-wrap) class: a simple wrapper for anything

Text utilities:
* Added common [SYSTEM_LINE_SEPARATOR](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.text/-s-y-s-t-e-m_-l-i-n-e_-s-e-p-a-r-a-t-o-r.html) constant
  to make the system line separator available even in common code
* Added [String.sloshToSlash()](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.text/kotlin.-string/slosh-to-slash.html) extension
  to convert `\` to `/`
* Added [Appendable.appendln(text)](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.text/java.lang.-appendable/appendln.html) extension
  to append with a newline

Reflection:
* Added [Any?.asNullOrInstanceOf(KClass)](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.reflect/kotlin.-any/as-null-or-instance-of.html)
  safe cast utility

### test-core - 0.8

*internal changes only*

### matchers - 0.10

* Added ad-hoc matcher creation method [matches(predicate)](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers.operators/matches.html)

### gradle - 0.7

* Improved Groovy `Closure` support: Replace `withClosureOf` by
  [T.apply(Closure)](https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle/de.creatorcat.kotiya.gradle/apply.html) which is a much clearer approach

### test-gradle - 0.7

*internal changes only*

### Dependency Updates

* Kotlin 1.3.20 -> 1.3.21
* Gradle 5.1.1 -> 5.2

### For Developers

Introduced markdown documentation file generation with $-token replacement:
* All `readme.md`, `module.md` and similar files are now generated from respective files in the new project
  `:doc-src` located in `doc/src`. This way it is possible to apply a $-token replacement mechanism
  that enables the usage of a kind of *macros* in the md-sources.
  This is very useful for example to generate links to the generated Dokka-Html.
* With the new mechanism the final location of the changelog was moved to the GitLab standard location:
  the project root dir.
* All relevant code references in the docs have been adjusted to be actual Dokka-Html-Links with the help
  of the new macro functionality

Build:
* Excluded test resource dirs with gradle test generated stuff from resource processing to allow
  the respective task to be up-to-date.
* Make the gitlab-ci script use the Gradle Daemon and split the build into several parts to make it fail faster.

Version Scheme:
* Changed version scheme to 100-day-stable nuclides instead of 10-year-stable
* Dependencies from one MPP project to another will now lead to proper target platform specific
  artifact dependencies in generated POM files (Kotlin does not support this by itself, yet)

### Build Dependency Updates

* GeneVer 0.2 -> 0.3

## H-2 - Improved Gradle Plugin and unsigned Int support

Git-Tag: [H-2](https://gitlab.com/creatorcat/kotiya/tags/H-2)

Includes the following modules with separate versions:

### core - 0.7

* Added collection extension [setAll](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-mutable-collection/set-all.html)
  which replaces all elements
* Added unsigned indexed access operators [get](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-list/get.html)
  and [set](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-mutable-list/set.html) to list
  and [unsigned size](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/kotlin.collections.-collection/u-size.html) to collections

### test-core - 0.7

* Added value extracting matcher assertion methods in [test.core package](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core)

### matchers - 0.9

* Added precondition methods [check](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/check.html)
  and [require](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/require.html) with value extraction support
* Added matcher for positive ints that extracts an unsigned int on success

### gradle - 0.6

* Added method `withClosureOf` to simplify Groovy script `Closure` DSL block usage in Kotlin

### test-gradle - 0.6

* Added [DummyClosure](https://creatorcat.gitlab.io/kotiya/docs/test-gradle/html/kotiya-test-gradle/de.creatorcat.kotiya.test.gradle/-dummy-closure)
  implementation for Gradle Groovy DSL function testing
* [FunctionalGradleTestBase](https://creatorcat.gitlab.io/kotiya/docs/test-gradle/html/kotiya-test-gradle/de.creatorcat.kotiya.test.gradle/-functional-gradle-test-base):
  * Changed default additional args of runAndAssertTask to "--stacktrace" instead of "clean"
  * Added support to print tagged values during Gradle functional testing and later assert them

### Dependency Updates

* Kotlin 1.3.20

### For Developers

* Explained nuclide versioning scheme
* Applied Genever plugin to generate the version and added respective launchers to publish releases
* Added main `test` task for MPP projects that runs tests for all platforms -->
  global `test` now runs all tests

## H-1 - First public availability

Git-Tag: [H-1](https://gitlab.com/creatorcat/kotiya/tags/H-1)

Includes the following modules with separate versions:

### core, test-core - 0.6

* First public availability

### gradle, test-gradle - 0.5

* First public availability

### matchers - 0.8

* First public availability
