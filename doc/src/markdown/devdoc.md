# Developer Documentation

## Repository Structure

![Repository Content](src/umlet/repo-contents.png "Repository Contents")

`.idea/`: IntelliJ IDEA project settings - see [IDEA configuration](#intellij-idea-configuration)

[//]: # (TODO fill)
`commons`:

`config/`: misc tool configuration data
* `dictionaries/`: custom dictionaries to be used in the IDE
* `eclipse/`: Eclipse product definition - see [Eclipse configuration](#eclipse-configuration)
* `gitlab/`: [GitLab](https://gitlab.com) CI configuration

`doc/`: general project documentation
* The documentation is generated from the sources in the sub folder `src` -
  see [Documentation Generation](#documentation-generation)

`gradle/`:
* `build-src`: Gradle buildSrc project containing code to support the build - see [build-src project](#build-src-project) 
* `scripts/`: various `.gradle` files that define specific parts of the build - see [scripts](#scripts)
* `wrapper/`: the gradle wrapper
* `basics.gradle`: Gradle script for most basic properties and functions - see [scripts](#scripts)

`modules/`: all modules of this project with their sources and resources
* The source code is mainly written in [Kotlin](https://kotlinlang.org/).
* The directory contains a sub-directory for each module.
* See [modules-readme](../modules/readme.md) for details.

## Tools

The project is configured and build using [Gradle](https://docs.gradle.org/current/userguide/userguide.html).

The recommended IDE for Kotlin development is [IntelliJ IDEA](https://www.jetbrains.com/idea/).
However, some parts (UML-Diagrams) are better handled using [Eclipse](https://www.eclipse.org/).
Therefore, both IDEs are supported as explained below.

### Gradle configuration

JDK used by the build:
* The gradle build uses a JDK-9.
* The JDK path must be specified in an environment variable called `JDK9_HOME`.
* The path variable is used directly in the wrapper batch/shell file to specify the `JAVA_HOME` dir for the gradle build.
  This is the only way (I know) of using environment variables for the JDK path and
  still make it possible to use a different JDK in each project.

Local CreatorCat repository:
* To simplify testing of and dependencies between snapshot builds of CreatorCat projects,
  the gradle scripts support a local repository for dependencies and publishing.
* If the property `CCAT_LOCAL_REPO` is defined, it will be used as the local repository path.
* `CCAT_LOCAL_REPO` should be defined in the `gradle.properties` file in `GRADLE_USER_HOME`.

Bintray publishing:
* To enable publishing to [Bintray](https://bintray.com/) the properties `BINTRAY_USER`
  and `BINTRAY_API_KEY` need to be defined in the `gradle.properties` file in `GRADLE_USER_HOME`.
* They may be left out if publishing is not intended.

See [Build and Configuration](#build-and-configuration) for details about the Gradle projects and tasks.

### IntelliJ IDEA configuration

Nearly all relevant project settings are stored within this repository, so the project can be opened right away.
* Open the project (*do not use import!*).
* Import the main gradle module e.g. via *"Project Structure.../ Modules/ '+'-button/ import..."*
  (see [IntelliJ doc-page](https://www.jetbrains.com/help/idea/gradle.html#import_gradle_module)).
* The remaining modules should be loaded automatically, then.

The following (not pre-installed) plugins are also recommended and supported by the settings:
* [Save Actions](https://github.com/dubreuia/intellij-plugin-save-actions)
* [Statistic](https://plugins.jetbrains.com/plugin/4509-statistic)

Spell Checking:
* At `config/dictionaries` there are two word list files:
  * `creatorcat.dic` for general and CreatorCat specific words (should only be changed in the template repository)
  * `project.dic` for project specific words, which is meant to be extended for the project
  * Both files are sorted in alphabetical order but case sensitive (lower case first)
* To make IntelliJ aware of those dictionaries, add the path as custom dictionary path in the project settings.
  Unfortunately, this setting cannot be stored in the provided project properties.

Project file templates:
* There are special templates for new files which must be enabled to be used.
* Set *"Editor/ File and Code templates/ Scheme"* to *"Project"*.

### Eclipse configuration

#### Installation & Setup

To install the proper eclipse for the project, including all plugins, follow the instructions:

1. Download and run the [Eclipse Installer](https://www.eclipse.org/downloads/).
2. Add the setup file (*config/eclipse/ccat.setup*) for this project as custom user product (via the **+** button).
3. Choose and install this product to some directory.
   An *eclipse* folder will be created in the chosen directory, which contains the executable.
4. (*recommended*) After installation, open Eclipse in a workspace of your choice and
   perform some basic configuration in the preferences dialog:
    * Disable automatic updates - this will be covered by Oomph based on the product file 
    and the standard Eclipse mechanism will only interfere.
    * Enable the preference recorder (red button)
5. Just Import the root project as "Existing Gradle project"

About the product definition file:

* The main eclipse packages are retrieved from a release specific update site, so version 
  constraints are not necessary for those.
* Version constraints are only necessary on packages not available from the official site
  or not up-to-date enough there (Buildship).

#### Included Plugins

The custom Eclipse product for this project is based on the Eclipse for Java Developers package,
which includes a reasonable Java, Git and Gradle support.
It also contains the [PDE feature](https://www.eclipse.org/pde/).
Furthermore, the following plugins are included.

* **FluentMark**:  
A Markdown editor: <https://github.com/grosenberg/fluentmark>  
**_Selection criteria:_**
Seems to be the most up-to-date Markdown editor for eclipse and
has proven quite usable so far.

* **UMLet**:  
A lightweight graphical UML diagram editor: <http://www.umlet.com/>  
**_Selection criteria:_**
This tool enables creation of UML 2.5 compatible diagrams without the need of developing
a complete data model behind the scenes.

* **AutoDeriv**:  
Enables configuration or derived projects files via *.derived* files: <http://nodj.github.io/AutoDeriv/>  
**_Selection criteria:_**
No known alternative.

* **Kotlin Eclipse**:  
Eclipse Kotlin support: <https://github.com/JetBrains/kotlin-eclipse>  
**_Selection criteria:_**
No known alternative.

## Build and Configuration

The main `modules`-project Gradle file:
* configures only little parts of the module projects directly.
* Instead, it loads the [topic specific scripts](#scripts) and provides the `applyCCC` method,
  that applies the configuration handlers (`cccHandlers`) to each module.

The module projects themselves:
* need to define some `ccc`-properties and then call `applyCCC`.
* add specific dependencies etc. as necessary.

### Scripts

The `gradle/basics.gradle` script contains basic properties and utilities for the build:  
* it is applied at the very first moment to the main build and the [build-src project](#build-src-project)
* thus, it allows sharing common stuff in both main and build-src
* such common stuff is e.g. version constants, repository definitions and some basic utility functions

The `gradle/scripts` folder contains so called *Gradle script plugins*, that each:
* deal with a specific topic of the build e.g. Kotlin compilation, Dokka generation or publishing.
* may perform some general configuration in the `src`-project level.
* add a *configuration handler* to the `cccHandlers` list that will apply the build configuration
  to each module project on calling `applyCCC`.

So called `ccc`-properties adjust the actual outcome of the configuration via a handler:
* The supported properties are listed at the top of each script file.
* Each module project must define the proper `ccc`-properties before calling `applyCCC` in its `build.gradle` file.

### build-src project

The `gradle/build-src` folder contains an included project that supplies functionality for the build itself.
As it may contain arbitrary Kotlin code and tests, it is much superior to the script plugins.
Thus, in the future, it will replace more and more of the scripts content. 

### Documentation Generation

Documentation sources are located in `doc/src` which also is a Gradle project.
Via the task `generateMarkdownDocumentation` the documentation files (Markdown) will be generated and deployed.
This includes the following steps:

1. Copy the source files defined as keys in `cccMdDocGenerationMap` to their associated targets
2. Replace all $$-Tokes in the target files (see the `doc/src/build.gradle` for details)
3. Remove all HTML-style TODO tags in the target files

## Open Issues

[//]: # (TEMPLATE_TODO switch to JDK 10 or higher once dokka-javadoc works there see https://github.com/Kotlin/dokka/issues/294)
[//]: # (TEMPLATE_TODO .md files are not auto-formatted on save because the hard line wrap cannot be disabled)
