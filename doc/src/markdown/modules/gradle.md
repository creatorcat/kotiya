# Module kotiya-gradle

Classes and utilities to support [Gradle](https://docs.gradle.org/current/userguide/userguide.html) plugin
and build script development.

State: *early Alpha*
* Only contains minimal content so far 
* API may change any time

Dependencies:
* Gradle API

# Package de.creatorcat.kotiya.gradle

Contains utilities for general gradle development.
