# Module kotiya-test-core

This module provides general purpose test utilities.

Features:
* The key concept is the use of [kotiya-matchers]($dokka(matchers)) via the
  $dokkaMd(test-core,test.core,assertThat.) or
  [T.assert]($dokka(test-core,test.core,assert.)) method
* Test exceptions using $dokkaMd(test-core,test.core,assertThrows.)
* Stub and observe methods or properties in common code
  where [kotlin-mockito](https://github.com/nhaarman/mockito-kotlin/wiki) is not available
  via [the stubbing package]($dokka(test-core,test.core.stubbing))
* Validate that implementations of standard methods like `equals` or `compare` comply to the general contract
  via reflective function assertion methods like
  $dokkaMd(test-core,test.core.reflect,assertEqualsContractFor.)

State: *Beta*
* The collection of code is rather miscellaneous
* API might change in details

Dependencies:
* [kotiya-core]($dokka(core))
* Basic test assertion functionality is based on
  [kotlin.test](https://kotlinlang.org/api/latest/kotlin.test/index.html)
  and [JUnit5](https://junit.org/junit5/)
* Advanced test assertion functionality is mostly based on [kotiya-matchers]($dokka(matchers))

# Package de.creatorcat.kotiya.test.core

Contains basic test utilities for general usage.

# Package de.creatorcat.kotiya.test.core.reflect

Contains methods to test general properties of functions like symmetry or consistency.
These can be combined to the general contract of standard methods like `equals` or `compare`.

# Package de.creatorcat.kotiya.test.core.stubbing

Contains methods to stub implementations of test dummies like
$dokkaMd(test-core,test.core.stubbing,mustNotBeCalled.)
and to observe function or property calls like
$dokkaMd(test-core,test.core.stubbing,withCallObserver.).

# Package de.creatorcat.kotiya.test.core.io

This package contains test utilities concerning files and IO.
