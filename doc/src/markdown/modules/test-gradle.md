# Module kotiya-test-gradle

Classes and utilities to support [Gradle](https://docs.gradle.org/current/userguide/userguide.html) plugin testing.

State: *early Alpha*
* Only contains minimal content so far 
* API may change any time

Dependencies:
* Functional test support is based on the [Gradle TestKit](https://guides.gradle.org/testing-gradle-plugins/)
* Test assertion functionality is mostly based on [kotiya-matchers]($dokka(matchers))

# Package de.creatorcat.kotiya.test.gradle

General utilities to test gradle builds with custom plugins:
* Base class $dokkaMd(test-gradle,test.gradle,FunctionalGradleTestBase)
  to simplify and standardize functional tests
* Build result matchers: `assertThat(result, hasTask("build") which hasOutcome(SUCCESS))`
* Groovy `Closure` usage simulation via $dokkaMd(test-gradle,test.gradle,DummyClosure)
