# Module kotiya-matchers

Framework of self-explaining condition matchers.

A matcher generally:
* checks if a value satisfies certain conditions or is in a specific state
* enables generation of human readable descriptions of what is expected of a value to match
  and why a certain value did not match

[//]: # (TODO detailed feature doc)

[Kotiya matchers]($dokka(matchers,matchers,Matcher)) furthermore:
* may be constructed hierarchically by combining multiple matchers via logic operations
  ($dokkaMd(matchers,matchers,and.), $dokkaMd(matchers,matchers,or.),
  $dokkaMd(matchers,matchers,not.), $dokkaMd(matchers,matchers.operators,each.),
  $dokkaMd(matchers,matchers.operators,isAnyOf.) and more)
  which generates a tree-like structure with each matcher being one node 
* provide their [expectation description]($dokka(matchers,matchers,Matcher,generateExpectationTree.)) as a tree,
  matching the hierarchical structure of the combined matcher,
  and thus enable generation of proper texts even for complex matcher combinations
  (see $dokkaMd(matchers,matchers,describeExpectations.))
* enable tracing of the [matcher's evaluation path]($dokka(matchers,matchers,MatcherEvaluationNode))
  through the matcher tree and identifying those child matcher's that actually contribute to the match result
  (i.e. which constraint was actually violated - see $dokkaMd(matchers,matchers,describeWhy.))
* allow extraction of type-cast values from successful matching (e.g. `isInstanceOf<Int>() which isLessThan(17)`)
* are also usable as simple $dokkaMd(core,core.predicates,Predicate.) functions (e.g. `if (isNull(x))`)
* are implemented as Kotlin common module and thus platform independent

There are of course alternative matcher libraries, but none of them has all of the above listed features. 

State: *late Beta*
* Concepts and type hierarchy have undergone several evolution and evaluation steps
* API might only change in details

Dependencies:
* `kotiya-core`

# Package de.creatorcat.kotiya.matchers

Contains the [matcher interface]($dokka(matchers,matchers,Matcher)) and all associated basic types. 

# Package de.creatorcat.kotiya.matchers.common

Contains matcher definitions for common use cases.

# Package de.creatorcat.kotiya.matchers.introspection

Contains matchers meant for testing a matcher itself.

# Package de.creatorcat.kotiya.matchers.operators

Contains matcher operators for logical or structural combination.
