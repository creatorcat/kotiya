# Module kotiya-core

The core module of Kotiya.
It provides general purpose utility functions and types.

State: *Beta*
* The collection of code is rather miscellaneous
* API might change in details 

Dependencies:
* `kotlin-reflect` on JVM

# Package de.creatorcat.kotiya.core.math

Basic mathematical utilities.

Integer maths:
* A [pow operator]($dokka(core,core.math,kotlin.Int,pow.html)) for several integer types
* [Extensions of the IntRange]($dokka(core,core.math,kotlin.ranges.IntRange)) to generate all ordered pairs 

# Package de.creatorcat.kotiya.core.predicates

Introduces the [Predicate<T>]($dokka(core,core.predicates,Predicate.)) type
which actually is an alias for a `(T) -> Boolean` function.
The type is used to define some extension functions e.g. to check if a predicate
$dokkaMd(core,core.predicates,kotlin.Function1,appliesToAll.) arguments.

# Package de.creatorcat.kotiya.core.reflect

Utilities concerning reflection.

Delegation:
* The [PropertyDelegate]($dokka(core,core.reflect,otherProperty.)) useful for value or variable delegation:
  `var x by otherProperty(::y)`

# Package de.creatorcat.kotiya.core.structures

Structural data types and associated utilities.

Data structures:
* $dokkaMd(core,core.structures,Queue) - a First-In-First-Out (FIFO) collection
* Tree representation via $dokkaMd(core,core.structures,TreeNode)
* [Wrap<T>]($dokka(core,core.structures,Wrap)) - simply wrap any object e.g. to get indirect write access
* `data class` like standard method implementation support via
  $dokkaMd(core,core.structures,PropertyBasedDataDefinition)

Functionality:
* Some extensions to standard collections and iterables
* Tree walking based on `TreeNode`

# Package de.creatorcat.kotiya.core.text

This package is concerned with strings and other text representing objects.
