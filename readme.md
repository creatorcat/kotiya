# Kotiya - Multipurpose Kotlin Library

This [Kotiya is a leopard](https://en.wikipedia.org/wiki/Sri_Lankan_leopard) of code.
It was originally born in [Ceylon](https://www.ceylon-lang.org/)
but it soon moved to [Kotlin](https://kotlinlang.org/) where it feels more at home.

Kotiya is a set of kotlin libraries aiming to make developer's life easier or at least more comfortable ;-)

## User Guide

For release notes see the [changelog](changelog.md).

### Modules

Kotiya consists of the following modules with dependencies:

```plantuml
package "Kotiya" {
    package core <<mpp: jvm>> #ff8f8f/7eff7e {
    }
    package matchers <<mpp: jvm>> #ff8f8f/7eff7e {
    }
    package gradle <<jvm>> #ffaaaa {
    }
    package test-core as testCore <<mpp: jvm>> #ff8f8f/7eff7e {
    }
    package test-gradle as testGradle <<jvm>> #ffaaaa {
    }
}

package org.jetbrains.kotlin as kotlin {
    package kotlin-reflect as kotRef <<jvm>> #cacaca {
    }
    package kotlin-test-common as kotTestCom <<mpp: common>> #cacaca {
    }
    package kotlin-test as kotTest <<jvm>> #cacaca {
    }
    package kotlin-test-junit5 as kotTestJunit5 <<jvm>> #cacaca {
    }
}

package org.gradle as orgGradle {
    package gradleAPI <<jvm>> #cacaca {
    }
    package gradleTestKit <<jvm>> #cacaca {
    }
}

package org.junit.jupiter as orgJunit {
    package "junit-jupiter-api" as junitJupiterApi <<jvm>> #cacaca {
    }
}

gradle ..> core : <<api*>>
gradle ..> gradleAPI : <<api*>>
matchers ..> core : <<api*>>
testCore ..> core : <<api*>>
testCore ..> matchers : <<api*>>
testCore ..> kotRef : <<implementation>>
testCore ..> junitJupiterApi : <<api>>
testCore ..> kotTestCom : <<implementation>>
testCore ..> kotTest : <<implementation>>
testCore ..> kotTestJunit5 : <<implementation>>
testGradle ..> core : <<api*>>
testGradle ..> matchers : <<api*>>
testGradle ..> testCore : <<api*>>
testGradle ..> gradle : <<api*>>
testGradle ..> gradleAPI : <<api*>>
testGradle ..> gradleTestKit : <<api*>>
```
Diagram hints:
* The dependency types correspond to those defined in the respective Gradle modules
(see [Kotlin dependency types](https://kotlinlang.org/docs/reference/using-gradle.html#dependency-types)). <br>
* `api*` = dependency should be `api` but due to a Kotlin MPP bug they are defined as `implementation` at the moment.
* Kotlin and Java standard library dependencies are omitted as they are essential defaults on the respective platform.

#### List of modules with versions and documentation links:

Module                                                                                  | Version | Supported Platforms
:-------------------------------------------------------------------------------------- | :-----: | :------------------
[kotiya-core][core-d] ([javadoc][core-j]) - general purpose functions and types         | 0.9     | common, jvm
[kotiya-test-core][test-core-d] ([javadoc][test-core-j]) - general purpose test utilities | 0.9   | common, jvm
[kotiya-matchers][matchers-d] ([javadoc][matchers-j]) - self-explaining condition matchers | 0.11  | common, jvm
[kotiya-gradle][gradle-d] ([javadoc][gradle-j]) - gradle plugin and script development support | 0.8 | jvm
[kotiya-test-gradle][test-gradle-d] ([javadoc][test-gradle-j]) - gradle plugin test support | 0.8 | jvm

[core-d]: https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core
[core-j]: https://creatorcat.gitlab.io/kotiya/docs/core/javadoc
[test-core-d]: https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core
[test-core-j]: https://creatorcat.gitlab.io/kotiya/docs/test-core/javadoc
[matchers-d]: https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers
[matchers-j]: https://creatorcat.gitlab.io/kotiya/docs/matchers/javadoc
[gradle-d]: https://creatorcat.gitlab.io/kotiya/docs/gradle/html/kotiya-gradle
[gradle-j]: https://creatorcat.gitlab.io/kotiya/docs/gradle/javadoc
[test-gradle-d]: https://creatorcat.gitlab.io/kotiya/docs/test-gradle/html/kotiya-test-gradle
[test-gradle-j]: https://creatorcat.gitlab.io/kotiya/docs/test-gradle/javadoc

All modules are available via the [CreatorCat bintray repository](https://bintray.com/knolle/creatorcat).

### Gradle

Add a Kotiya module as dependency in your Gradle build:

    repositories {
        maven {
            url = "https://dl.bintray.com/knolle/creatorcat"
        }
    }

    dependencies {
        // for your common-module
        implementation("de.creatorcat.kotiya:kotiya-core-metadata:0.9")
        // for your jvm-module
        implementation("de.creatorcat.kotiya:kotiya-core-jvm:0.9")
    }

## Versioning Scheme

Since the Kotiya modules are developed each in its own speed, they have their own specific version numbers
that follow the general [SemVer](https://semver.org/) approach in their base components (`major.minor.patch`).

However, as can be seen in the [changelog](changelog.md),
subsequent releases of all modules are bundled into one Kotiya main release.
The version string of these main releases is based on 100-days-stable [nuclides](https://en.wikipedia.org/wiki/Nuclide):
* The initial version was the named after the nuclide of the first element in the periodic table
  with the lowest mass number: `H-1` aka Hydrogen-1
* The version is incremented to the next nuclide of the current element (by mass number)
  which has a half-life of at least 100 days ... unstable versions should be avoided :-) :
  `H-1` --> `H-2` --> `H-3`
* If there is no next 100-days-stable nuclide for the current element,
  the version is incremented to the lowest (by mass number) 100-days-stable nuclide
  of the next element in the periodic table: `H-3` --> `He-3`

See the [table of nuclides](https://en.wikipedia.org/wiki/Table_of_nuclides) to find out which version is next.

## Developer Documentation

See the [detailed developer documentation](doc/devdoc.md).

## License

The [Apache 2 license](http://www.apache.org/licenses/LICENSE-2.0) (given in full in [LICENSE.txt](LICENSE.txt))
applies to all sources in this repository unless explicitly marked otherwise.
