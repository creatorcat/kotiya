/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.require
import groovy.lang.Closure
import kotlin.reflect.KClass

/**
 * A dummy implementation of [Closure] that simply executes the [codeBlock] on [call]
 * with the block's receiver being the closure's [delegate].
 * Enables testing of Gradle Groovy script methods that deal with closures.
 *
 * @param R closure and [codeBlock] return type
 * @param T delegate class
 */
class DummyClosure<R, T : Any>(
    /** Class of the closure's delegate object. */
    val delegateClass: KClass<T>,
    /** Closure content block that is to be executed on [call] with the [delegate] as receiver. */
    val codeBlock: T.() -> R
) : Closure<R>(null) {

    /**
     * Executes the [codeBlock] with the block's receiver being the closure's [delegate].
     *
     * @return the result of the [codeBlock]
     */
    override fun call(): R =
        require("delegate", delegate, isInstanceOf(delegateClass)).codeBlock()

    companion object {

        /**
         * Convenience method to create a [DummyClosure].
         *
         * @param R closure and [codeBlock] return type
         * @param T delegate class
         * @param codeBlock Closure content block that is to be executed on [call] with the [delegate] as receiver.
         */
        inline fun <R, reified T : Any> of(noinline codeBlock: T.() -> R): DummyClosure<R, T> =
            DummyClosure(T::class, codeBlock)
    }
}
