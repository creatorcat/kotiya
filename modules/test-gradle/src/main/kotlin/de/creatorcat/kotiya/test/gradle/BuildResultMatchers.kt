/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.core.text.withPrefix
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.BuildTask
import org.gradle.testkit.runner.TaskOutcome

/**
 * Creates a matcher that checks if a [BuildResult] has the given [task].
 * If so, the task is extracted into the result so this matcher may be combined for task assertions.
 *
 * Example:
 *
 *     assertThat(result, hasTask("build") which hasOutcome(SUCCESS))
 *
 * @param task gradle task name or full path (a name will be converted to a root-project task path)
 */
fun hasTask(task: String): ValueExtractingAtomicMatcher<BuildResult, BuildTask> =
    ValueExtractingAtomicMatcher.of(
        descriptionOfAtomicCondition(
            "contain the task '$task'", "contains the task '$task'"
        )
    ) {
        val taskPath = task.withPrefix(":")
        val buildTask = it.task(taskPath)
        Pair(buildTask, "has ${if (buildTask != null) "a" else "no"} task with path '$taskPath'")
    }

/**
 * Creates a matcher that checks if a [BuildTask] has the given [outcome].
 */
fun hasOutcome(outcome: TaskOutcome): AtomicMatcher<BuildTask> =
    Matcher.of(
        descriptionOfAtomicCondition(
            "have outcome $outcome", "has outcome $outcome"
        )
    ) {
        Pair(it.outcome == outcome, "has outcome ${it.outcome}")
    }
