/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import java.io.File
import java.nio.file.Paths

/**
 * Base class for functional gradle plugin tests.
 *
 * Usage (for each test class or even each test case):
 * 1. define gradle environment (like [projectDir], [buildDirPath], [pluginsToApply])
 * 2. create build script for the test via [writeBuildScript]
 * 3. use the [runner] or one of the `run*` methods to run the test build
 * 4. evaluate the results
 */
abstract class FunctionalGradleTestBase(
    /**
     * Plugins to be applied in the [generated build script][generateBuildScriptPluginsPart] of each test case.
     * Each plugin is identified by a pair of strings: `pluginId` and `version`,
     * where `version` may be `null` for default plugins.
     *
     * @see add
     */
    protected val pluginsToApply: MutableList<Pair<String, String?>>
) : FunctionalTestOutputVerifier {

    /**
     * Indicates whether to apply the Gradle base plugin.
     * If set, the base plugin is applied before all other plugins.
     * Defaults to `true`.
     *
     * @see generateBuildScriptPluginsPart
     */
    protected var applyBasePlugin: Boolean = true

    /**
     * Creates the object with the given [defaultPluginsToApply] identified by their plugin ids.
     * These will define the initial value of [pluginsToApply][FunctionalGradleTestBase.pluginsToApply].
     */
    constructor(vararg defaultPluginsToApply: String) :
        this(defaultPluginsToApply.mapTo(mutableListOf<Pair<String, String?>>()) { it to null })

    /**
     * Convenience method to add a default plugin identified by [pluginId] to the [pluginsToApply].
     * Default plugins need no version when applied.
     * Use the other variant of this method to apply plugins that need a version.
     *
     * @see generateBuildScriptPluginsPart
     */
    protected fun MutableList<Pair<String, String?>>.add(pluginId: String) {
        add(pluginId to null)
    }

    /**
     * Convenience method to add a plugin identified by [pluginId] and [version] to the [pluginsToApply].
     * To add default plugins that need no version use the other variant of this method.
     *
     * @see generateBuildScriptPluginsPart
     */
    protected fun MutableList<Pair<String, String?>>.add(pluginId: String, version: String) {
        add(pluginId to version)
    }

    /**
     * The gradle project directory, defaults to `"src/test/resources/projectDir"`.
     *
     * @see getProjectFile
     */
    protected var projectDir: File = File("src/test/resources/projectDir")

    /** Get a file with [path] relative to the [projectDir]. */
    protected fun getProjectFile(path: String): File = File(projectDir, path)

    /**
     * The gradle runner, lazily created
     * and initially configured with [projectDir] and plugin classpath.
     */
    protected open val runner: GradleRunner by lazy {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .forwardOutput()
    }

    /**
     * The projects build directory path, relative to the [projectDir] or as absolute path.
     * Defaults to `"build"`.
     *
     * @see getBuildFile
     * @see getBuildPath
     */
    protected var buildDirPath: String = "build"

    /** Get a path string inside the [buildDirPath] by extending it with the given [path]. */
    protected fun getBuildPath(path: String): String = "$buildDirPath/$path"

    /**
     * Get a file with [path] relative to the [buildDirPath].
     * Note, that the [buildDirPath] may be relative to the [projectDir]
     * and thus the file may depend on [projectDir] as well.
     */
    protected fun getBuildFile(path: String): File {
        val buildFilePath = Paths.get(getBuildPath(path))
        return if (buildFilePath.isAbsolute)
            buildFilePath.toFile()
        else getProjectFile(buildFilePath.toString())
    }

    /**
     * Generates the content of the *plugins* block of the build script for the test.
     * Applies the following plugins in the given order:
     * * the gradle *base*-plugin if [applyBasePlugin] is `true`
     * * all [pluginsToApply]
     */
    protected open fun generateBuildScriptPluginsPart(): String =
        with(StringBuilder()) {
            if (applyBasePlugin) applyPlugin("base")
            for ((id, version) in pluginsToApply) {
                if (version != null)
                    applyPlugin(id, version)
                else
                    applyPlugin(id)
            }
            toString()
        }

    private fun Appendable.applyPlugin(id: String) =
        appendln("id \"$id\"")

    private fun Appendable.applyPlugin(id: String, version: String) =
        appendln("id \"$id\" version \"$version\"")

    /**
     * Generates the text of the build script file with the following contents:
     * * the "plugins"-block with content generated via [generateBuildScriptPluginsPart]
     * * the definition of the build directory using [buildDirPath]
     * * the [mainContent]
     */
    protected open fun generateBuildFileContent(mainContent: String): String = """
plugins {
${generateBuildScriptPluginsPart()}}

buildDir = file("$buildDirPath")

$mainContent
"""

    /**
     * Writes the build script file using [generateBuildFileContent] with the given [mainContent].
     */
    protected fun writeBuildScript(mainContent: String) {
        getProjectFile("build.gradle").writeText(generateBuildFileContent(mainContent))
    }

    /**
     * Uses the [runner] to run the given [task] and asserts that the [task] has the [expectedOutcome].
     *
     * @param additionalArguments additional arguments for the [gradle runner call][GradleRunner.withArguments]
     *   that will be prepended before the [task];
     *   defaults to `{"--stacktrace"}`
     * @param expectedOutcome defaults to [SUCCESS]
     * @param expectBuildSuccess indicates whether the build is expected to run successful; defaults to `true`
     */
    protected fun runAndAssertTask(
        task: String,
        vararg additionalArguments: String = arrayOf("--stacktrace"),
        expectedOutcome: TaskOutcome = SUCCESS,
        expectBuildSuccess: Boolean = true
    ): BuildResult {
        val buildRunner = runner.withArguments(*additionalArguments, task)
        val result = if (expectBuildSuccess) buildRunner.build() else buildRunner.buildAndFail()
        assertThat(result, hasTask(task) which hasOutcome(expectedOutcome))
        return result
    }
}
