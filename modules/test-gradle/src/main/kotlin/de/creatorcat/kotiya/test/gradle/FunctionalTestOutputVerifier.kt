/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.testkit.runner.BuildResult

/**
 * Extension interface for functional gradle tests that provides methods
 * to verify build result output.
 */
interface FunctionalTestOutputVerifier {

    /**
     * Method to be used **inside** the test build script text when calling
     * [writeBuildScript][FunctionalGradleTestBase.writeBuildScript]
     * to make the build produce *tag = value* outputs that may later be asserted via [assertOutputValues].
     * Example:
     *
     *     writeBuildScript(
     *         """
     *         def myValue = 5
     *         ${printAssertableValue("myValue")}
     *         ${printAssertableValue("myValue - 1", "lowerValue")}
     *         """
     *     )
     *     val result = runAndAssertTask("myTask")
     *     result.assertOutputValues(
     *         "myValue" to 5, "lowerValue" to 4
     *     )
     *
     * @param valueCode script code that will produce the value when the build runs
     * @param tag tag to identify the value when asserting the output
     */
    fun printAssertableValue(valueCode: String, tag: String = valueCode): String =
        """logger.lifecycle("$ASSERTION_START_TAG $tag = {} $ASSERTION_END_TAG", $valueCode)"""

    /**
     * Method to assert *tag = value* outputs that where produced by the build run
     * by using [printAssertableValue] in the script.
     * The method will try to find each of the [tagValuePairs] in this result's [output][BuildResult.getOutput]
     * (with the value part converted to string).
     * If any pair is missing, the assertion fails.
     * Example:
     *
     *     writeBuildScript(
     *         """
     *         def myValue = 5
     *         ${printAssertableValue("myValue")}
     *         ${printAssertableValue("myValue - 1", "lowerValue")}
     *         """
     *     )
     *     val result = runAndAssertTask("myTask")
     *     result.assertOutputValues(
     *         "myValue" to 5, "lowerValue" to 4
     *     )
     */
    fun BuildResult.assertOutputValues(vararg tagValuePairs: Pair<String, Any?>) {
        for ((tag, value) in tagValuePairs)
            assertThat(output, contains("$ASSERTION_START_TAG $tag = $value $ASSERTION_END_TAG"))
    }

    companion object {
        internal const val ASSERTION_START_TAG = "VALUE-ASSERTION:"
        internal const val ASSERTION_END_TAG = ":END-ASSERTION"
    }
}
