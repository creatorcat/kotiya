/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.core.structures.Wrap
import groovy.lang.Closure
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class GroovyTestUtilTest {

    @Test fun `closure - usage`() {
        val receiver = "Groovy"
        val closure = DummyClosure<String, String>(String::class) {
            "$this is supported!"
        }
        closure.delegate = receiver

        assertEquals(String::class, closure.delegateClass)
        assertEquals("Groovy is supported!", closure.call())
    }

    @Test fun `closure - usage with unit result`() {
        val wrap = Wrap(0)
        val closure: Closure<Unit> = DummyClosure.of<Unit, Wrap<Int>> {
            data++
        }
        closure.delegate = wrap

        closure.call()
        assertEquals(1, wrap.data)
    }

    @Test fun `closure - static creator`() {
        val block: String.() -> String = { this }
        val closure = DummyClosure.of<String, String>(block)
        assertEquals(String::class, closure.delegateClass)
        assertSame(block, closure.codeBlock)
    }
}
