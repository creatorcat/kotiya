/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import de.creatorcat.kotiya.matchers.InvertibleValueStateDescription
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.BuildTask
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import kotlin.test.Test

class BuildResultMatchersTest {

    @Test fun `hasTask - description`() {
        (hasTask("dummy").expectedState as InvertibleValueStateDescription).assert(
            hasContents(
                "contain the task 'dummy'", "contains the task 'dummy'",
                "not contain the task 'dummy'", "does not contain the task 'dummy'"
            )
        )
    }

    @Test fun `hasTask - task path resolution`() {
        val testResult: BuildResult = mock()

        hasTask("dummy").evaluate(testResult)
        verify(testResult).task(eq(":dummy"))

        hasTask(":sub:dummy").evaluate(testResult)
        verify(testResult).task(eq(":sub:dummy"))
    }

    @Test fun `hasTask - evaluation`() {
        val expectedTask: BuildTask = mock()
        val testResult: BuildResult = mock {
            on { task(eq(":validTask")) } doReturn expectedTask
        }

        val successfulMatcher = hasTask(":validTask")
        successfulMatcher.evaluate(testResult).assert(
            hasContents(
                true,
                successfulMatcher,
                "has a task with path ':validTask'",
                expectedTask
            )
        )

        val failingMatcher = hasTask(":unknown")
        failingMatcher.evaluate(testResult).assert(
            hasContents<BuildTask>(
                false,
                failingMatcher,
                "has no task with path ':unknown'",
                null
            )
        )
    }

    @Test fun `hasOutcome - description`() {
        (hasOutcome(SUCCESS).expectedState as InvertibleValueStateDescription).assert(
            hasContents(
                "have outcome $SUCCESS", "has outcome $SUCCESS",
                "not have outcome $SUCCESS", "does not have outcome $SUCCESS"
            )
        )
    }

    @Test fun `hasOutcome - evaluation`() {
        val testTask: BuildTask = mock {
            on { outcome } doReturn SUCCESS
        }

        val successfulMatcher = hasOutcome(SUCCESS)
        successfulMatcher.evaluate(testTask).assert(
            hasContents(
                true,
                successfulMatcher,
                "has outcome $SUCCESS"
            )
        )

        val failingMatcher = hasOutcome(FAILED)
        failingMatcher.evaluate(testTask).assert(
            hasContents(
                false,
                failingMatcher,
                "has outcome $SUCCESS"
            )
        )
    }
}
