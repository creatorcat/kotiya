/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.gradle.FunctionalTestOutputVerifier.Companion.ASSERTION_END_TAG
import de.creatorcat.kotiya.test.gradle.FunctionalTestOutputVerifier.Companion.ASSERTION_START_TAG
import org.gradle.testkit.runner.BuildResult
import kotlin.test.Test
import kotlin.test.assertEquals

class FunctionalTestOutputVerifierTest : FunctionalTestOutputVerifier {

    @Test fun `printAssertableValue - generated code`() {
        assertEquals(
            """logger.lifecycle("$ASSERTION_START_TAG TAG = {} $ASSERTION_END_TAG", this.is(some) { code })""",
            printAssertableValue("this.is(some) { code }", "TAG")
        )

        assertEquals(
            """logger.lifecycle("$ASSERTION_START_TAG someValue = {} $ASSERTION_END_TAG", someValue)""",
            printAssertableValue("someValue")
        )
    }

    @Test fun `assertOutputValues - finds pairs`() {
        val result = mock<BuildResult> {
            on { output } doReturn """
                some output...
                $ASSERTION_START_TAG x = 23 $ASSERTION_END_TAG 
                some more output...
                $ASSERTION_START_TAG y = hello $ASSERTION_END_TAG 
                even more output...
                """
        }
        result.assertOutputValues("x" to 23, "y" to "hello")
    }

    @Test fun `assertOutputValues - throws on missing pair`() {
        val result = mock<BuildResult> {
            on { output } doReturn """
                some output...
                $ASSERTION_START_TAG x = 23 $ASSERTION_END_TAG 
                some more output...
                $ASSERTION_START_TAG y = hello $ASSERTION_END_TAG 
                even more output...
                """
        }
        assertThrows<AssertionError> {
            result.assertOutputValues("x" to 23, "y" to "hello world")
        }
    }
}
