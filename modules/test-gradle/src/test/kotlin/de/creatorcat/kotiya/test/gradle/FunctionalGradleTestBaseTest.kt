/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.endsWith
import de.creatorcat.kotiya.matchers.common.exists
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.io.assertRecursiveDeletionIfExists
import java.io.File
import java.nio.file.Paths
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FunctionalGradleTestBaseTest : FunctionalGradleTestBase("plugin-1", "plugin-2") {

    @Test fun `plugins to apply - initial values`() {
        assertThat(pluginsToApply, hasElementsEqual<Pair<String, String?>>("plugin-1" to null, "plugin-2" to null))
        assertTrue(applyBasePlugin)
    }

    @Test fun `plugins to apply - convenience adders`() {
        pluginsToApply.add("plugin-3", "1.0")
        pluginsToApply.add("plugin-4")
        assertThat(
            pluginsToApply,
            hasElementsEqual(
                "plugin-1" to null, "plugin-2" to null, "plugin-3" to "1.0", "plugin-4" to null
            )
        )
    }

    @Test fun `generateBuildScriptPluginsPart - applies constructor plugins`() {
        assertThat(
            generateBuildScriptPluginsPart(),
            contains("id \"plugin-1\"") and contains("id \"plugin-2\"")
        )
    }

    @Test fun `generateBuildScriptPluginsPart - applies additional plugins`() {
        pluginsToApply.add("plugin-3", "1.2.3")
        assertThat(
            generateBuildScriptPluginsPart(),
            contains("id \"plugin-1\"") and contains("id \"plugin-2\"")
                and contains("id \"plugin-3\" version \"1.2.3\"")
        )
    }

    @Test fun `generateBuildScriptPluginsPart - base plugin appliance`() {
        assertThat(
            generateBuildScriptPluginsPart(),
            contains("id \"base\"")
        )
        applyBasePlugin = false
        assertThat(
            generateBuildScriptPluginsPart(),
            !contains("id \"base\"")
        )
    }

    @Test fun `buildDirPath - default value`() {
        assertEquals("build", buildDirPath)
    }

    @Test fun `getBuildPath - extends build dir path`() {
        buildDirPath = "some/dummy/dir"
        assertEquals("$buildDirPath/subDir/file", getBuildPath("subDir/file"))
    }

    @Test fun `getBuildFile - with absolute build dir`() {
        val absolutePath = Paths.get("dummy-build-dir").toAbsolutePath()
        buildDirPath = absolutePath.toString()
        assertEquals(absolutePath.resolve("sub").toFile(), getBuildFile("sub"))
    }

    @Test fun `getBuildFile - with relative build dir`() {
        assertEquals(File("$projectDir/$buildDirPath/sub"), getBuildFile("sub"))
    }

    @Test fun `projectDir - default value`() {
        assertEquals(File("src/test/resources/projectDir"), projectDir)
    }

    @Test fun `getProjectFile - uses project dir`() {
        projectDir = File("some/dummy/dir")
        assertEquals(File(projectDir, "subDir/file"), getProjectFile("subDir/file"))
    }

    @Test fun `writeBuildScript - creates build file in project dir`() {
        val buildFile = deleteAndGetBuildFile()
        writeBuildScript("")
        assertThat(buildFile, exists)
    }

    @Test fun `writeBuildScript - script contains generated content`() {
        val buildFile = deleteAndGetBuildFile()
        val expectedContent = generateBuildFileContent("MAIN")
        writeBuildScript("MAIN")
        assertEquals(expectedContent, buildFile.readText())
    }

    @Test fun `generateBuildFileContent - contains proper plugins part`() {
        val buildScriptPluginsPart = generateBuildScriptPluginsPart()
        assertThat(
            generateBuildFileContent(""),
            contains(
                Regex("""plugins \{[^}]*$buildScriptPluginsPart[^}]*}""")
            )
        )
    }

    @Test fun `generateBuildFileContent - contains build dir initialization`() {
        buildDirPath = "BUILD_FILE"
        assertThat(
            generateBuildFileContent(""),
            contains("buildDir = file(\"$buildDirPath\")")
        )
    }

    @Test fun `generateBuildFileContent - ends with main content and newline`() {
        val mainContent = "MAIN CONTENT"
        assertThat(
            generateBuildFileContent(mainContent),
            endsWith("$mainContent\n")
        )
    }

    private fun deleteAndGetBuildFile(): File {
        val buildFile = getProjectFile("build.gradle")
        assertRecursiveDeletionIfExists(buildFile)
        return buildFile
    }
}
