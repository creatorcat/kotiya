/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import kotlin.test.Test

class FunctionalTestOutputVerifierFuncTest : FunctionalGradleTestBase(FunctionalTestDummyPlugin.ID) {

    @Test fun `print and assert some values`() {
        writeBuildScript(
            """
            def myValue = 5
            ${printAssertableValue("myValue")}
            ${printAssertableValue("myValue - 1", "lowerValue")}
            """
        )
        val result = runAndAssertTask("projects")
        result.assertOutputValues("myValue" to 5, "lowerValue" to 4)
    }
}
