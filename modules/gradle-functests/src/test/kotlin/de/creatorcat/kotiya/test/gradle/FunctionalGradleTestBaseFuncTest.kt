/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.operators.isAllOf
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.gradle.FunctionalTestDummyPlugin.Companion.DUMMY_TASK_NAME
import de.creatorcat.kotiya.test.gradle.FunctionalTestDummyPlugin.Companion.FAILING_TASK_NAME
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import kotlin.test.Test

class FunctionalGradleTestBaseFuncTest : FunctionalGradleTestBase(FunctionalTestDummyPlugin.ID) {

    @Test fun `default plugin is actually applied`() {
        writeBuildScript("")
        val result = runAndAssertTask("tasks")
        assertThat(result.output, contains("${DUMMY_TASK_NAME} - "))
    }

    @Test fun `versioned plugin is actually applied`() {
        pluginsToApply.add("org.jetbrains.kotlin.jvm", KotlinVersion.CURRENT.toString())
        writeBuildScript("")
        val result = runAndAssertTask("tasks")
        assertThat(result.output, contains("classes - "))
    }

    @Test fun `base plugin is actually applied`() {
        writeBuildScript("")
        val result = runAndAssertTask("tasks")
        assertThat(
            result.output, isAllOf(
                contains("clean - "), contains("check - "), contains("assemble - "), contains("build - ")
            )
        )
    }

    @Test fun `runner runs in project dir`() {
        writeBuildScript("""task printDir { doFirst { println "P-DIR: ${'$'}projectDir" } }""")
        val result = runner.withArguments("printDir").build()
        assertThat(result, hasTask("printDir") which hasOutcome(SUCCESS))
        assertThat(result.output, contains("P-DIR: ${projectDir.absolutePath}"))
    }

    @Test fun `build dir is actually set`() {
        writeBuildScript("""task printDir { doFirst { println "B-DIR: ${'$'}buildDir" } }""")
        val result = runAndAssertTask("printDir")
        assertThat(result.output, contains("B-DIR: ${getBuildFile("").absolutePath}"))
    }

    @Test fun `runAndAssertTask - runs all tasks`() {
        writeBuildScript("")
        assertThat(
            runAndAssertTask(DUMMY_TASK_NAME, "projects", "tasks"),
            hasTask(DUMMY_TASK_NAME) and hasTask("projects") and hasTask("tasks")
        )
    }

    @Test fun `runAndAssertTask - default args`() {
        writeBuildScript("")
        val result = runAndAssertTask(FAILING_TASK_NAME, expectedOutcome = FAILED, expectBuildSuccess = false)
        assertThat(
            result.output,
            contains("Caused by: java.lang.ArrayIndexOutOfBoundsException:")
        )
    }

    @Test fun `runAndAssertTask - with failing main task`() {
        writeBuildScript("")
        assertThat(
            runAndAssertTask(FAILING_TASK_NAME, expectedOutcome = FAILED, expectBuildSuccess = false),
            hasTask(FAILING_TASK_NAME) which hasOutcome(FAILED)
        )
    }

    @Test fun `runAndAssertTask - with failing additional task`() {
        writeBuildScript("")
        assertThat(
            runAndAssertTask(DUMMY_TASK_NAME, FAILING_TASK_NAME, "--continue", expectBuildSuccess = false),
            hasTask(FAILING_TASK_NAME) which hasOutcome(FAILED) and
                (hasTask(DUMMY_TASK_NAME) which hasOutcome(SUCCESS))
        )
    }

    @Test fun `runAndAssertTask - throws on bad outcome`() {
        writeBuildScript("")
        assertThrows<AssertionError> { runAndAssertTask(DUMMY_TASK_NAME, expectedOutcome = UP_TO_DATE) }
    }

    @Test fun `runAndAssertTask - throws on build failure`() {
        writeBuildScript("")
        assertThrows<Throwable> { runAndAssertTask("failure", expectedOutcome = FAILED) }
    }
}
