/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.gradle

import de.creatorcat.kotiya.core.text.CREATOR_CAT_CLI_HELLO
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.HelpTasksPlugin

/**
 * Dummy plugin to be used in functional tests.
 */
class FunctionalTestDummyPlugin : Plugin<Project> {

    /**
     * Apply the plugin to the [target] project.
     *
     * @see [Plugin.apply]
     */
    override fun apply(target: Project) {
        target.logger.info(CREATOR_CAT_CLI_HELLO)
        target.task(DUMMY_TASK_NAME).apply {
            description = "dummy test task doing nothing"
            group = HelpTasksPlugin.HELP_GROUP
            doFirst { println("dummy executed") }
        }

        target.task(FAILING_TASK_NAME).apply {
            description = "failing test task"
            group = HelpTasksPlugin.HELP_GROUP
            doFirst {
                val list = listOf(1, 2, 3)
                println("failing now...")
                println(list[5])
            }
        }
    }

    companion object {
        /** The plugin ID - used when applying the plugin in a build script */
        const val ID: String = "de.creatorcat.kotiya.gradle.func-test-dummy"

        /** Name of a dummy task added by this plugin. */
        const val DUMMY_TASK_NAME: String = "dummyTestTask"

        /** Name of a task that will fail with an [ArrayIndexOutOfBoundsException]. */
        const val FAILING_TASK_NAME: String = "failingTestTask"
    }
}
