# Module kotiya-core

The core module of Kotiya.
It provides general purpose utility functions and types.

State: *Beta*
* The collection of code is rather miscellaneous
* API might change in details 

Dependencies:
* `kotlin-reflect` on JVM

# Package de.creatorcat.kotiya.core.math

Basic mathematical utilities.

Integer maths:
* A [pow operator](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.math/kotlin.-int/pow.html) for several integer types
* [Extensions of the IntRange](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.math/kotlin.ranges.-int-range) to generate all ordered pairs 

# Package de.creatorcat.kotiya.core.predicates

Introduces the [Predicate<T>](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.predicates/-predicate.html) type
which actually is an alias for a `(T) -> Boolean` function.
The type is used to define some extension functions e.g. to check if a predicate
[appliesToAll](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.predicates/kotlin.-function1/applies-to-all.html) arguments.

# Package de.creatorcat.kotiya.core.reflect

Utilities concerning reflection.

Delegation:
* The [PropertyDelegate](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.reflect/other-property.html) useful for value or variable delegation:
  `var x by otherProperty(::y)`

# Package de.creatorcat.kotiya.core.structures

Structural data types and associated utilities.

Data structures:
* [Queue](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-queue) - a First-In-First-Out (FIFO) collection
* Tree representation via [TreeNode](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-tree-node)
* [Wrap<T>](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-wrap) - simply wrap any object e.g. to get indirect write access
* `data class` like standard method implementation support via
  [PropertyBasedDataDefinition](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.structures/-property-based-data-definition)

Functionality:
* Some extensions to standard collections and iterables
* Tree walking based on `TreeNode`

# Package de.creatorcat.kotiya.core.text

This package is concerned with strings and other text representing objects.
