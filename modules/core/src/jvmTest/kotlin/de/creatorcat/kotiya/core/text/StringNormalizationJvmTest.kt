/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class StringNormalizationJvmTest {

    @Test fun `SYSTEM_LINE_SEPARATOR - jvm`() {
        assertEquals(System.lineSeparator(), SYSTEM_LINE_SEPARATOR)
    }
}
