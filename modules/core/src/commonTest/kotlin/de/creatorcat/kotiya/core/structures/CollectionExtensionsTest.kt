/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class CollectionExtensionsTest {

    @Test fun `containsAll - delegates to instance method`() {
        val testee = object : Collection<Int> {
            override fun containsAll(elements: Collection<Int>): Boolean = withCallObserver(this::containsAll) {
                true
            }

            override val size get() = mustNotBeCalled()
            override fun contains(element: Int) = mustNotBeCalled()
            override fun isEmpty() = mustNotBeCalled()
            override fun iterator() = mustNotBeCalled()
        }

        testee.containsAll(1, 2, 3)
        testee.containsAll(4, 5)
        assertEquals(2, testee::containsAll.countedCalls)
    }

    @Test fun `collection setAll - iterable`() {
        val list = mutableListOf(1, 2, 3)
        val newElements = listOf(4, 5)
        list.setAll(newElements)
        assertEquals(newElements, list)
    }

    @Test fun `collection setAll - vararg`() {
        val list = mutableListOf(1, 2, 3)
        val newElements = arrayOf(4, 5)
        list.setAll(*newElements)
        assertThat(list, hasElementsEqual(*newElements))
    }

    @Test fun `collection addAll vararg - standard usage`() {
        val list = mutableListOf(1, 2, 3)
        list.addAll(4, 5)
        assertThat(list, hasElementsEqual(1, 2, 3, 4, 5))
    }

    @Test fun `collection addAll vararg - no elements`() {
        val list = mutableListOf(1, 2, 3)
        list.addAll()
        assertThat(list, hasElementsEqual(1, 2, 3))
    }

    @Test fun `collection size unsigned`() {
        assertEquals(0u, emptyList<Any>().uSize)
        assertEquals(4u, listOf(1, 2, 3, 4).uSize)
    }

    @Test fun `array size unsigned`() {
        assertEquals(0u, emptyArray<Any>().uSize)
        assertEquals(4u, arrayOf(1, 2, 3, 4).uSize)
    }
}

