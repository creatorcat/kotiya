/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.test.core.stubbing.countedCallsForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TreesWithDataTest {

    @Test fun `creation - of a leaf`() {
        val node = SimpleDataNode.of(3)
        node.assertLeafData(3)
    }

    @Test fun `creation - with list`() {
        val children = (0..2).map { SimpleDataNode.of(it) }
        val node = SimpleDataNode.of(3, children)

        assertEquals(3, node.data)
        assertEquals(children, node.children)
    }

    @Test fun `creation - with vararg nodes`() {
        val children = (0..2).map { SimpleDataNode.of(it) }
        val node = SimpleDataNode.of(3, children[0], children[1], children[2])

        assertEquals(3, node.data)
        assertEquals(children, node.children)
    }

    @Test fun `creation - with child data list`() {
        val testNode = SimpleDataNode.ofChildrenWithData(3, listOf(0, 1, 2))

        assertEquals(3, testNode.data)
        testNode.children.forEachIndexed { index, node -> node.assertLeafData(index) }
    }

    @Test fun `creation - with vararg child data `() {
        val testNode = SimpleDataNode.ofChildrenWithData(3, 0, 1, 2)

        assertEquals(3, testNode.data)
        testNode.children.forEachIndexed { index, node -> node.assertLeafData(index) }
    }

    private fun SimpleDataNode<Int>.assertLeafData(data: Int) {
        assertEquals(data, this.data)
        assertTrue(isLeaf)
    }

    @Test fun `creation - with list producer`() {
        val children = (0..2).map { SimpleDataNode.of(it) }
        val node = SimpleDataNode.of(3) {
            withCallCounterForTag(children) {
                children
            }
        }

        assertEquals(3, node.data)
        repeat(3) { assertEquals(children, node.children) }
        assertEquals(1, countedCallsForTag(children))
    }
}
