/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import kotlin.test.Test
import kotlin.test.assertEquals

class DepthCountingTreeWalksTest {

    private val tree = SimpleDataNode.of(0) {
        listOf(
            SimpleDataNode.of(1) {
                listOf(SimpleDataNode.of(2), SimpleDataNode.of(3))
            },
            SimpleDataNode.of(4) {
                listOf(SimpleDataNode.of(5))
            }
        )
    }

    @Test fun `walk and count depth first`() {
        val result = tree.walkAndCountDepthFirst().joinToString(", ") { (node, count) -> "$count-${node.data}" }
        assertEquals("0-0, 1-1, 2-2, 2-3, 1-4, 2-5", result)
    }
}
