/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import kotlin.test.Test
import kotlin.test.assertEquals

class AppendableExtensionsTest {

    @Test fun `appendln - usage`() {
        val appendable = StringBuilder()
        appendable.appendln("line1")
        appendable.appendln()
        appendable.appendln("line2")
        assertEquals(
            "line1${SYSTEM_LINE_SEPARATOR}${SYSTEM_LINE_SEPARATOR}line2${SYSTEM_LINE_SEPARATOR}",
            appendable.toString()
        )
    }
}
