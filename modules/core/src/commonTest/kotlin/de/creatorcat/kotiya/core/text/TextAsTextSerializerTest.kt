/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertEquals

class TextAsTextSerializerTest {
    private val testee = TextAsTextSerializer()

    @Test fun `text to safe entry conversion`() {
        with(testee) {
            assertEquals("abcde", textToSafeEntry("abcde"))
            assertEquals("", textToSafeEntry(""))
            assertEquals("abc\$\$efg", textToSafeEntry("abc\$efg"))
            assertEquals("abc\$#e\$#fg", textToSafeEntry("abc'e'fg"))
            assertEquals("abc\$\$\$#\$\$\$#e\$\$\$#fg", textToSafeEntry("abc\$'\$'e\$'fg"))
            assertEquals(
                """
                abc$#
                def
                """.trimIndent(),
                textToSafeEntry(
                    """
                    abc'
                    def
                    """.trimIndent()
                )
            )
        }
    }

    @Test fun `safe entry to text conversion`() {
        with(testee) {
            assertEquals("abcde", safeEntryToText("abcde"))
            assertEquals("", safeEntryToText(""))
            assertEquals("abc\$efg", safeEntryToText("abc\$\$efg"))
            assertEquals("abc'e'fg", safeEntryToText("abc\$#e\$#fg"))
            assertEquals("abc\$'\$'e\$'fg", safeEntryToText("abc\$\$\$#\$\$\$#e\$\$\$#fg"))
            assertEquals(
                """
                abc'
                def
                """.trimIndent(),
                safeEntryToText(
                    """
                    abc$#
                    def
                    """.trimIndent()
                )
            )
        }
    }

    @Test fun `write list`() {
        val buffer = StringBuilder()
        testee.write(buffer, listOf("abc", "", "a'b\$c"))
        assertEquals("'abc', '', 'a\$#b\$\$c'", buffer.toString())
    }

    @Test fun `write single text`() {
        val buffer = StringBuilder()
        testee.write(buffer, "a'b\$c")
        assertEquals("'a\$#b\$\$c'", buffer.toString())
    }

    @Test fun `read list`() {
        val list = testee.read("'abc', '', 'a\$#b\$\$c', 'def'")
        assertThat(list, hasElementsEqual("abc", "", "a'b\$c", "def"))
    }

    @Test fun `read broken list`() {
        val list = testee.read("'abc', asdfjasdf 'xdf")
        assertThat(list, hasElementsEqual("abc"))
    }

    @Test fun `write and read list`() {
        val originalList = listOf("abc", "", "a'b\$c", "def", "hello 'world' \$abc")
        val buffer = StringBuilder()
        testee.write(buffer, originalList)
        val readList = testee.read(buffer.toString())
        assertEquals(originalList, readList)
    }
}
