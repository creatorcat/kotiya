/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class LinkedItemQueueTest {

    private val testee = LinkedItemQueue<Int>()

    @Test fun `size and emptiness of empty queue`() {
        assertTrue(testee.isEmpty())
        assertEquals(0, testee.size)
    }

    @Test fun `enqueue and dequeue elements`() {
        testee.enqueue(17)
        assertEquals(17, testee.dequeue())

        testee.enqueue(1, 2, 3)
        assertEquals(1, testee.dequeue())
        assertEquals(2, testee.dequeue())
        assertEquals(3, testee.dequeue())

        testee.enqueue(1, 2, 3)
        assertEquals(1, testee.dequeue())
        assertEquals(2, testee.dequeue())
        testee.enqueue(4, 5)
        assertEquals(3, testee.dequeue())
        assertEquals(4, testee.dequeue())
        assertEquals(5, testee.dequeue())
    }

    @Test fun `size and emptiness of non-empty queue`() {
        testee.enqueue(1, 2, 3)
        assertFalse(testee.isEmpty())
        assertEquals(3, testee.size)

        testee.enqueue(4, 5, 6)
        assertFalse(testee.isEmpty())
        assertEquals(6, testee.size)
    }

    @Test fun `dequeue empty queue throws`() {
        assertThrows<NoSuchElementException> { testee.dequeue() }
    }

    @Test fun `iterator of empty queue`() {
        assertFalse(testee.iterator().hasNext())
        assertThrows<NoSuchElementException> { testee.iterator().next() }
    }

    @Test fun `iteration over non-empty queue`() {
        testee.enqueue(1, 2, 3)
        var queueAsString = ""
        for (number in testee) {
            queueAsString += "$number,"
        }
        assertEquals("1,2,3,", queueAsString)
    }

    @Test fun `contains and containsAll with empty queue`() {
        assertFalse(testee.contains(1))
        assertFalse(testee.containsAll(1, 2, 3))
    }

    @Test fun `contains and containsAll with non-empty queue`() {
        testee.enqueue(1, 2, 3, 4, 5)

        assertTrue(testee.contains(1))
        assertTrue(testee.contains(2))
        assertFalse(testee.contains(6))

        assertTrue(testee.containsAll(1, 2, 3, 4, 5))
        assertTrue(testee.containsAll(1, 3, 5))
        assertFalse(testee.containsAll(1, 6))
    }
}
