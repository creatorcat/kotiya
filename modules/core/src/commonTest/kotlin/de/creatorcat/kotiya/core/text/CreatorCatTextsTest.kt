/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNotBlank
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class CreatorCatTextsTest {

    @Test fun `cat logo prints nice`() {
        assertThat(CREATOR_CAT_LOGO, isNotBlank)
        println(CREATOR_CAT_LOGO)
    }

    @Test fun `cli hello contains cat logo`() {
        assertThat(CREATOR_CAT_CLI_HELLO, contains(CREATOR_CAT_LOGO))
        println(CREATOR_CAT_CLI_HELLO)
    }
}
