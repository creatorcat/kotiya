/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.math.pow
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class IterableExtensionsTest {

    @Test fun `mapWhile - maps all`() {
        val r1 = listOf(1, 2, 3, 4, 5, 6).mapWhile({ it > 0 }) { it * 2 }
        val r2 = listOf(1, 2, 3, 4, 5, 6).mapWhile({ it > 0 }, true) { it * 2 }
        assertEquals(listOf(2, 4, 6, 8, 10, 12), r1)
        assertEquals(r1, r2)
    }

    @Test fun `mapWhile - with failing element mapping result`() {
        val r1 = listOf(1, 2, 3, 4, 5, 6).mapWhile({ it < 10 }) { it * 2 }
        val r2 = listOf(1, 2, 3, 4, 5, 6).mapWhile({ it < 10 }, true) { it * 2 }
        assertEquals(listOf(2, 4, 6, 8), r1)
        assertEquals(listOf(2, 4, 6, 8, 10), r2)
    }

    @Test fun `pairs - variation with repetition`() {
        assertThat(
            (0..3).allPairs(),
            hasElementsEqual(
                0 to 0, 0 to 1, 0 to 2, 0 to 3,
                1 to 0, 1 to 1, 1 to 2, 1 to 3,
                2 to 0, 2 to 1, 2 to 2, 2 to 3,
                3 to 0, 3 to 1, 3 to 2, 3 to 3
            )
        )
        assertThat(
            (4..5).allPairs(),
            hasElementsEqual(
                4 to 4, 4 to 5,
                5 to 4, 5 to 5
            )
        )
        assertThat((4..4).allPairs(), hasElementsEqual(4 to 4))
        assertThat(emptyList<String>().allPairs(), isEmpty)

        assertEquals(12 pow 2u, (1..12).allPairs().count())
    }

    @Test fun `triples - variation with repetition`() {
        fun t(x: Int, y: Int, z: Int) = Triple(x, y, z)

        assertThat(
            (0..2).allTriples(),
            hasElementsEqual(
                t(0, 0, 0), t(0, 0, 1), t(0, 0, 2),
                t(0, 1, 0), t(0, 1, 1), t(0, 1, 2),
                t(0, 2, 0), t(0, 2, 1), t(0, 2, 2),
                t(1, 0, 0), t(1, 0, 1), t(1, 0, 2),
                t(1, 1, 0), t(1, 1, 1), t(1, 1, 2),
                t(1, 2, 0), t(1, 2, 1), t(1, 2, 2),
                t(2, 0, 0), t(2, 0, 1), t(2, 0, 2),
                t(2, 1, 0), t(2, 1, 1), t(2, 1, 2),
                t(2, 2, 0), t(2, 2, 1), t(2, 2, 2)
            )
        )
        assertThat(
            (4..5).allTriples(),
            hasElementsEqual(
                t(4, 4, 4), t(4, 4, 5),
                t(4, 5, 4), t(4, 5, 5),
                t(5, 4, 4), t(5, 4, 5),
                t(5, 5, 4), t(5, 5, 5)
            )
        )
        assertThat((4..4).allTriples(), hasElementsEqual(t(4, 4, 4)))
        assertThat(emptyList<String>().allTriples(), isEmpty)

        assertEquals(6 pow 3u, (2..7).allTriples().count())
    }
}
