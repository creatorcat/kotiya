/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.reflect.asKFunction
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.reflect.KFunction
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class QueueTest {

    @Test fun `enqueue iterable of elements`() {

        val testee = object : Queue<Int> {

            val enqueueSingleElement: KFunction<Unit> = asKFunction<Unit, (Int) -> Unit>(this::enqueue)

            override fun enqueue(element: Int): Unit = withCallObserver(enqueueSingleElement) {}

            override fun dequeue() = mustNotBeCalled()
            override val size get() = mustNotBeCalled()
            override fun contains(element: Int) = mustNotBeCalled()
            override fun containsAll(elements: Collection<Int>) = mustNotBeCalled()
            override fun isEmpty() = mustNotBeCalled()
            override fun iterator() = mustNotBeCalled()
        }

        testee.enqueue(listOf(1, 2, 3))
        assertEquals(3, testee.enqueueSingleElement.countedCalls)
        testee.enqueue(listOf(1, 2))
        assertEquals(2, testee.enqueueSingleElement.countedCalls)
    }

    @Test fun `enqueue vararg elements`() {

        val testee = object : Queue<Int> {

            val enqueueIterable: KFunction<Unit> = asKFunction<Unit, (Iterable<Int>) -> Unit>(this::enqueue)

            override fun enqueue(elements: Iterable<Int>): Unit = withCallObserver(enqueueIterable) {}

            override fun enqueue(element: Int) = mustNotBeCalled()
            override fun dequeue() = mustNotBeCalled()
            override val size get() = mustNotBeCalled()
            override fun contains(element: Int) = mustNotBeCalled()
            override fun containsAll(elements: Collection<Int>) = mustNotBeCalled()
            override fun isEmpty() = mustNotBeCalled()
            override fun iterator() = mustNotBeCalled()
        }

        testee.enqueue(1, 2, 3)
        assertEquals(1, testee.enqueueIterable.countedCalls)
        testee.enqueue(1, 2)
        assertEquals(1, testee.enqueueIterable.countedCalls)
    }

    @Test fun `create queue with elements`() {
        val testee = Queue.create(1, 2, 3)
        assertTrue(testee is LinkedItemQueue)
        assertTrue(testee.containsAll(1, 2, 3))
        assertEquals(3, testee.size)
    }

    @Test fun `create empty queue`() {
        val testee = Queue.create<Int>()
        assertTrue(testee.isEmpty())
        assertTrue(testee is LinkedItemQueue)
    }
}
