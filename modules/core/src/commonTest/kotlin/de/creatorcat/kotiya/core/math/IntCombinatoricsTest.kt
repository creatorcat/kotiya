/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.math

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class IntCombinatoricsTest {

    @Test fun `ordered distinct pairs - combination without repetition`() {
        assertThat(
            (0..3).orderedDistinctPairs(),
            hasElementsEqual(
                0 to 1, 0 to 2, 0 to 3,
                1 to 2, 1 to 3,
                2 to 3
            )
        )
        assertThat((4..5).orderedDistinctPairs(), hasElementsEqual(4 to 5))
        assertThat((4..4).orderedDistinctPairs(), isEmpty)
        @Suppress("EmptyRange")
        assertThat((4..3).orderedDistinctPairs(), isEmpty)

        // number of 2-combinations is the binomial coefficient (n 2) = (n^2 - n) / 2
        assertEquals(((12 pow 2u) - 12) / 2, (1..12).orderedDistinctPairs().count())
    }

    @Test fun `ordered pairs - combination with repetition`() {
        assertThat(
            (0..3).orderedPairs(),
            hasElementsEqual(
                0 to 0, 0 to 1, 0 to 2, 0 to 3,
                1 to 1, 1 to 2, 1 to 3,
                2 to 2, 2 to 3,
                3 to 3
            )
        )
        assertThat(
            (4..5).orderedPairs(),
            hasElementsEqual(
                4 to 4, 4 to 5,
                5 to 5
            )
        )
        assertThat((4..4).orderedPairs(), hasElementsEqual(4 to 4))
        @Suppress("EmptyRange")
        assertThat((4..3).orderedPairs(), isEmpty)

        // number of 2-multi-combinations is the multi-set-coefficient ((n 2)) = (n^2 + n) / 2
        assertEquals(((12 pow 2u) + 12) / 2, (1..12).orderedPairs().count())
    }
}
