/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.predicates

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PredicateTest {

    private val below5: Predicate<Int> = { it < 5 }

    @Test fun `predicate applies to value`() {
        assertTrue(below5 appliesTo 4)
        assertFalse(below5 appliesTo 5)
    }

    @Test fun `value satisfies predicate`() {
        assertFalse(5 satisfies below5)
        assertTrue(3 satisfies below5)
    }

    @Test fun `predicate applies to all values`() {
        assertTrue(below5.appliesToAll(1, 2, 3, 4, 0))
        assertFalse(below5.appliesToAll(1, 2, 3, 4, 5))

        assertTrue(appliesToAll(1, 2, 3, 4, 5) { it > 0 })
        assertFalse(appliesToAll(1, 2, 3, 4, 5) { it * 2 < 10 })

        val isString: Predicate<Any?> = { it is String }
        assertFalse(appliesToAll<Int>(1, 7, 23, predicate = isString))
        assertFalse(isString.appliesToAll("a", "b", "c", 1))
    }

    @Test fun `predicate applies to none of the values`() {
        assertTrue(below5.appliesToNone(5, 6, 7, 8, 9))
        assertFalse(below5.appliesToNone(8, 7, 6, 5, 4))

        assertTrue(appliesToNone(1, {}, 3, 4, 5) { obj: Any -> obj is String })
        assertFalse(appliesToNone<Any>("a", "b", 3) { it is String })
    }

    @Test fun `predicate applies to any value`() {
        assertTrue(below5.appliesToAny(5, 6, 7, 8, 9, 4))
        assertFalse(below5.appliesToAny(8, 7, 6, 5, 10))

        assertTrue(appliesToAny(1, 2, 3, 4, 5) { it > 4 })
        assertFalse(appliesToAny<Any>("a", {}, "c") { it is Int })
    }
}
