/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class TreesTest {

    private class NumberedNode(
        val number: Int,
        vararg childNodes: NumberedNode
    ) : TreeNode<NumberedNode> {
        override val children = childNodes.asList()
    }

    @Test fun `tree iteration - depth first`() {
        val root = NumberedNode(
            0,
            NumberedNode(
                1,
                NumberedNode(2),
                NumberedNode(3)
            ),
            NumberedNode(
                4,
                NumberedNode(
                    5,
                    NumberedNode(6)
                )
            )
        )

        val leaf = NumberedNode(7)

        assertNodeNumbers(0..6, root.walkDepthFirst().asIterable())
        assertSame(root, root.walkDepthFirst().root)
        assertNodeNumbers(7..7, leaf.walkDepthFirst().asIterable())
        assertSame(leaf, leaf.walkDepthFirst().root)
    }

    @Test fun `tree iteration - breadth first`() {
        val root = NumberedNode(
            0,
            NumberedNode(
                1,
                NumberedNode(3),
                NumberedNode(
                    4,
                    NumberedNode(6)
                )
            ),
            NumberedNode(
                2,
                NumberedNode(
                    5,
                    NumberedNode(7)
                )
            )
        )

        val leaf = NumberedNode(8)

        assertNodeNumbers(0..7, root.walkBreadthFirst().asIterable())
        assertSame(root, root.walkBreadthFirst().root)
        assertNodeNumbers(8..8, leaf.walkBreadthFirst().asIterable())
        assertSame(leaf, leaf.walkBreadthFirst().root)
    }

    @Test fun `node without children is leaf`() {
        val leaf = NumberedNode(3)
        assertTrue(leaf.isLeaf)

        val noLeaf = NumberedNode(1, NumberedNode(2))
        assertFalse(noLeaf.isLeaf)
    }

    private fun assertNodeNumbers(expected: Iterable<Int>, nodes: Iterable<NumberedNode>) {
        val actualNumbers = nodes
            .map(NumberedNode::number)
            .toList()
        assertEquals(expected.toList(), actualNumbers)
    }
}
