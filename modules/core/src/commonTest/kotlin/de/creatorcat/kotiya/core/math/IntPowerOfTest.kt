/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.math

import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class IntPowerOfTest {

    @Test fun `int power unsigned int`() {
        assertEquals(1, 34 pow 0u)
        assertEquals(17, 17 pow 1u)

        assertEquals(16, 4 pow 2u)
        assertEquals(256, 2 pow 8u)
        assertEquals(1024, 2 pow 10u)
        assertEquals(Int.MIN_VALUE, 2 pow 31u)
        assertEquals(81, 3 pow 4u)
    }

    @Test fun `unsigned int power unsigned int`() {
        assertEquals(1u, 234u pow 0u)
        assertEquals(33u, 33u pow 1u)

        assertEquals(64u, 4u pow 3u)
        assertEquals(2147483648u, 2u pow 31u)
        assertEquals(0u, 2u pow 32u)
    }

    @Test fun `long power unsigned int`() {
        assertEquals(1L, 234L pow 0u)
        assertEquals(33L, 33L pow 1u)

        assertEquals(16L, 4L pow 2u)
        assertEquals(4398046511104, 2L pow 42u)
        assertEquals(Long.MIN_VALUE, 2L pow 63u)
    }

    @Test fun `unsigned long power unsigned int`() {
        assertEquals(1UL, 234UL pow 0u)
        assertEquals(33UL, 33UL pow 1u)

        assertEquals(16UL, 4UL pow 2u)
        assertEquals(9223372036854775808UL, 2UL pow 63u)
        assertEquals(0UL, 2UL pow 64u)
    }
}
