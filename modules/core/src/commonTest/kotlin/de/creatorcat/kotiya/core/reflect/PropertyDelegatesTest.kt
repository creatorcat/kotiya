/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals

class PropertyDelegatesTest {

    private class IntWrap(var value: Int)

    @Test fun delegationVarToVar() {
        val wrap = IntWrap(5)
        var y: Int by otherProperty(wrap::value)

        assertEquals(5, y)
        assertEquals(5, wrap.value)
        y = 17
        assertEquals(17, y)
        assertEquals(17, wrap.value)
        wrap.value = 23
        assertEquals(23, y)
        assertEquals(23, wrap.value)
    }

    @Test fun delegationValToVar() {
        val wrap = IntWrap(5)
        val y: Int by otherProperty(wrap::value)

        assertEquals(5, y)
        assertEquals(5, wrap.value)
        wrap.value = 23
        assertEquals(23, y)
        assertEquals(23, wrap.value)
    }

    @Test fun delegationValToVal() {
        val word = "hello"
        val y: Int by otherProperty(word::length)

        assertEquals(word.length, y)
    }

    @Test fun `target property getter is called each time`() {
        val dummy = object {
            val target: Int get() = withCallObserver(::target) { 23 }
        }
        val delegatedVal: Int by otherProperty(dummy::target)

        assertEquals(23, delegatedVal)
        assertEquals(23, delegatedVal)
        assertEquals(2, dummy::target.countedCalls)
    }
}
