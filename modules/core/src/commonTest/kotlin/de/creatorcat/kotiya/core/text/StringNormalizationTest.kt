/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import kotlin.test.Test
import kotlin.test.assertEquals

class StringNormalizationTest {

    @Test fun normalizeNewlineToLinux() {
        assertEquals("\nOne\nTwo\nThree\n", "\r\nOne\r\nTwo\nThree\r".normalizeNewlineToLinux())
    }

    @Test fun normalizeNewlineToWindows() {
        assertEquals("\r\nOne\r\nTwo\r\nThree\r\n", "\nOne\r\nTwo\nThree\r".normalizeNewlineToWindows())
    }

    @Test fun replaceNewlineWith_atStart() {
        assertEquals("XOne", "\r\nOne".replaceNewlineWith("X"))
        assertEquals("XOne", "\rOne".replaceNewlineWith("X"))
        assertEquals("XOne", "\nOne".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_atEnd() {
        assertEquals("OneX", "One\r\n".replaceNewlineWith("X"))
        assertEquals("OneX", "One\r".replaceNewlineWith("X"))
        assertEquals("OneX", "One\n".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_inTheMiddle() {
        assertEquals("OneXTwo", "One\r\nTwo".replaceNewlineWith("X"))
        assertEquals("OneXTwo", "One\rTwo".replaceNewlineWith("X"))
        assertEquals("OneXTwo", "One\nTwo".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_repeatedAtStart() {
        assertEquals("XXXOne", "\r\n\r\n\r\nOne".replaceNewlineWith("X"))
        assertEquals("XXXOne", "\r\r\rOne".replaceNewlineWith("X"))
        assertEquals("XXXOne", "\n\n\nOne".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_repeatedAtEnd() {
        assertEquals("OneXXX", "One\r\n\r\n\r\n".replaceNewlineWith("X"))
        assertEquals("OneXXX", "One\r\r\r".replaceNewlineWith("X"))
        assertEquals("OneXXX", "One\n\n\n".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_repeatedInTheMiddle() {
        assertEquals("OneXXXTwo", "One\r\n\r\n\r\nTwo".replaceNewlineWith("X"))
        assertEquals("OneXXXTwo", "One\r\r\rTwo".replaceNewlineWith("X"))
        assertEquals("OneXXXTwo", "One\n\n\nTwo".replaceNewlineWith("X"))
    }

    @Test fun replaceNewlineWith_mixed() {
        assertEquals("XOneXTwoXThreeX", "\rOne\r\nTwo\nThree\n".replaceNewlineWith("X"))
    }

    @Test fun `normalizeNewlineToSystem - usage`() {
        val nl = SYSTEM_LINE_SEPARATOR
        assertEquals("${nl}One${nl}Two$nl", "\rOne\r\nTwo\n".normalizeNewlineToSystem())
    }

    @Test fun `sloshToSlash - usage`() {
        assertEquals("c:/my/win/dir///with/some//stuff", "c:\\my\\win\\dir/\\\\with/some\\\\stuff".sloshToSlash())
    }
}
