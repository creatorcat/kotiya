/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import kotlin.reflect.KFunction
import kotlin.test.Test
import kotlin.test.assertEquals

class FunctionUtilsTest {

    @Test fun `cast member function to KFunction`() {
        class A {
            fun f1(@Suppress("UNUSED_PARAMETER") arg: Int): Int = 1
            fun f1(@Suppress("UNUSED_PARAMETER") arg: String): Int = 2
        }

        val a = A()
        val f1IntAsK: KFunction<Int> = asKFunction<Int, (Int) -> Int>(a::f1)
        val f1StringAsK: KFunction<Int> = asKFunction<Int, (String) -> Int>(a::f1)
        f1IntAsK.parameters.forEach { assertEquals(Int::class, it.type.classifier) }
        f1StringAsK.parameters.forEach { assertEquals(String::class, it.type.classifier) }
    }

    @Test fun `cast global function to KFunction`() {

        val f2IntAsK: KFunction<String> = asKFunction<String, (Int) -> String>(::f2)
        val f2StringAsK: KFunction<String> = asKFunction<String, (String) -> String>(::f2)
        f2IntAsK.parameters.forEach { assertEquals(Int::class, it.type.classifier) }
        f2StringAsK.parameters.forEach { assertEquals(String::class, it.type.classifier) }
    }

    @Test fun `cast local function to KFunction`() {
        fun f3(@Suppress("UNUSED_PARAMETER") arg: Int): String = "1"
        fun f3(@Suppress("UNUSED_PARAMETER") arg: String): String = "2"

        val f2IntAsK: KFunction<String> = asKFunction<String, (Int) -> String>(::f3)
        val f2StringAsK: KFunction<String> = asKFunction<String, (String) -> String>(::f3)

        // local functions cannot be inspected via reflection but the KFunction objects may still be used
        // in a list or with extensions
        val funcList: List<KFunction<*>> = listOf(f2IntAsK, f2StringAsK)
        assertEquals(5, f2IntAsK.dummy)
        assertEquals(2, funcList.size)
    }

    @Suppress("unused")
    private val KFunction<*>.dummy: Int
        get() = 5
}

fun f2(@Suppress("UNUSED_PARAMETER") arg: Int): String = "1"
fun f2(@Suppress("UNUSED_PARAMETER") arg: String): String = "2"
