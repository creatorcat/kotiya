/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class WrapTest {

    @Test fun `standard methods`() {
        val x = "data"
        val w = Wrap(x)
        with(Wrap(x)) {
            assertSame(x, data)
            assertEquals(w, this)
            assertEquals(w.hashCode(), hashCode())
            assertEquals(w.toString(), toString())
        }
    }
}
