/*
 * Copyright 2017 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ReplaceDollarTokensTest {

    @Test fun replacementRegEx_basics() {
        assertTrue(dollarTokenReplacementRegex.matches("\$\$"))

        assertFalse(dollarTokenReplacementRegex.matches("noDollar"))
        assertNotMatching("badBraces(sd,f")
        assertNotMatching("badBraces()")
        assertNotMatching("badBraces(){}")
        assertNotMatching("badBraces{")
        assertNotMatching("badBraces{asdf")
        assertNotMatching("badBraces}")
    }

    @Test fun replacementRegEx_tokenName() {
        assertParsedGroups("simpleToken", "simpleToken")
        assertParsedGroups("grouped.token", "grouped.token")
        assertParsedGroups("Underlined_TOKEN", "Underlined_TOKEN")
        assertParsedGroups("123", "123")

        assertNotMatching("bad-token")
        assertNotMatching("bad#token")
    }

    @Test fun replacementRegEx_args() {
        assertParsedGroups("token(a)", "token", "a")
        assertParsedGroups("token(a,b,3,4)", "token", "a,b,3,4")
        assertParsedGroups("token( a , b c ,3 + 4,  #?{}bla_.d   )", "token", " a , b c ,3 + 4,  #?{}bla_.d   ")

        assertNotMatching("token(a, b), c)")
    }

    @Test fun replacementRegEx_body() {
        assertParsedGroups("token{}", "token", null, "")
        assertParsedGroups("token{asbc .sdf894asdf _?=1234 () }", "token", null, "asbc .sdf894asdf _?=1234 () ")
        assertParsedGroups(
            """
            token{
                line1
                line2
            }
            """.trimIndent(),
            "token", null,
            """

            |    line1
            |    line2

            """.trimMargin()
        )

        assertNotMatching("token{ a}b }")
    }

    @Test fun replacementRegEx_argsAndBody() {
        assertParsedGroups(
            "token(a,b, 3, 4){ This is my body! }",
            "token", "a,b, 3, 4", " This is my body! "
        )
    }

    private fun assertParsedGroups(input: String, name: String, args: String? = null, body: String? = null) {
        val res = dollarTokenReplacementRegex.matchEntire("\$$input")
            ?: throw AssertionError("Did not match: $input")
        assertEquals(name, res.groups[NAME_GROUP]?.value)
        assertEquals(args, res.groups[ARGS_GROUP]?.value)
        assertEquals(body, res.groups[BODY_GROUP]?.value)
    }

    private fun assertNotMatching(input: String) = assertFalse(dollarTokenReplacementRegex.matches("\$$input"))

    @Test fun useReplaceDollarTokens() {
        assertEquals("This formula is easy: 1+2=3. Some more data: a=' Bla Blub! '" +
            "This is a \$sign. This will not be replaced: \$unknown",
            replaceDollarTokens(
                "This \$thing is easy: \$sum( 1 , 2   )." +
                    " Some more data: \$valOf( a ){ Bla Blub! }" +
                    "This is a \$\$sign. This will not be replaced: \$unknown"
            ) { name, args, body ->
                when (name) {
                    "thing" -> {
                        assertThat(args, isEmpty)
                        assertThat(body, isEmpty)
                        "formula"
                    }
                    "sum" -> {
                        assertThat(body, isEmpty)
                        "${args[0]}+${args[1]}=${args[0].toInt() + args[1].toInt()}"
                    }
                    "valOf" -> "${args[0]}='$body'"
                    else -> null
                }
            })
    }

    @Test fun replaceDollarTokens_ordered() {
        var count = 1
        assertEquals(
            "1 + 2 = 3",
            replaceDollarTokens("\$a + \$b = \$c") { _, _, _ -> "${count++}" }
        )
    }
}
