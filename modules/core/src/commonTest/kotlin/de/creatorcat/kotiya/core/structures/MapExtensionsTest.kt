/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class MapExtensionsTest {

    @Test fun `putIfAbsent - element already present`() {
        val map = mutableMapOf(3 to "c")
        assertEquals("c", map.putIfAbsent(3, "d"))
        assertEquals("c", map[3])
        assertEquals(1, map.size)
    }

    @Test fun `putIfAbsent - element not present`() {
        val map = mutableMapOf(3 to "c")
        assertNull(map.putIfAbsent(4, "d"))
        assertEquals("c", map[3])
        assertEquals("d", map[4])
        assertEquals(2, map.size)
    }

    @Test fun `putAllIfAbsent - with present and absent elements`() {
        val map = mutableMapOf(3 to "c")
        val other = mutableMapOf(1 to "a", 3 to "d")
        map.putAllIfAbsent(other)
        assertEquals(2, map.size)
        assertEquals("c", map[3])
        assertEquals("a", map[1])
    }

    @Test fun `merge - all cases`() {
        val map = mutableMapOf(1 to 2)
        // new value
        assertEquals(
            3,
            map.merge(2, 3) { _, _ -> mustNotBeCalled() }
        )
        assertEquals(3, map[2])
        // merge value
        assertEquals(
            5,
            map.merge(1, 3) { a, b -> a + b }
        )
        assertEquals(5, map[1])
        // remove value
        assertNull(map.merge(2, 2) { _, _ -> null })
        assertNull(map[2])
    }

    @Test fun `mergeAll - standard usage`() {
        val map1 = mutableMapOf(1 to 2, 2 to 4, 3 to 6)
        val map2 = mapOf(2 to 3, 3 to 4, 4 to 5)
        map1.mergeAll(map2) { a, b -> a + b }
        val expected = mapOf(1 to 2, 2 to 7, 3 to 10, 4 to 5)
        assertEquals(expected, map1)
    }
}
