/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotSame

@ExperimentalUnsignedTypes
class ListExtensionsTest {

    @Test fun `makeMutationSafe - with mutable list`() {
        val original = mutableListOf(1, 2, 3)
        val safe = original.makeMutationSafe()
        assertEquals(original, safe)
        assertNotSame(original, safe)

        original[0] = 5
        assertNotEquals(original, safe)
    }

    @Test fun `pairs with indices`() {
        val src = listOf(0, 1, 2, 3, 4, 5)
        val indices = listOf(0 to 1, 2 to 4, 3 to 5)
        assertThat(
            src.pairsWithIndices(indices),
            hasElementsEqual(indices)
        )
    }

    @Test fun `indexed get unsigned`() {
        val list = listOf(1, 2, 3, 4)
        assertEquals(1, list[0u])
        assertEquals(4, list[3u])
        assertThrows<IndexOutOfBoundsException> { list[4u] }
    }

    @Test fun `indexed set unsigned`() {
        val list = mutableListOf(1, 2, 3, 4)
        list[0u] = 6
        list[3u] = 6
        assertThat(list, hasElementsEqual(6, 2, 3, 6))
        assertThrows<IndexOutOfBoundsException> { list[4u] = 5 }
    }
}
