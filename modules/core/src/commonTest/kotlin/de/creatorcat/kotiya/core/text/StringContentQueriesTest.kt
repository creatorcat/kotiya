/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class StringContentQueriesTest {

    @Test fun containsAll() {
        val testee = "Ant Bear Cat Dog"
        assertTrue(testee.containsAll("Ant", "Bear", "Cat", "Dog"))
        assertTrue(testee.containsAll("aNt", "CAT", "dog", ignoreCase = true))
        assertFalse(testee.containsAll("aNt", "CAT", "dog"))
        assertFalse(testee.containsAll("Ant", "Bear", "Cat", "Dog", "Eel"))
    }
}
