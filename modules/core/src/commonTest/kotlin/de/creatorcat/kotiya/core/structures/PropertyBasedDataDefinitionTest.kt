/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.math.pow
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.reflect.assertEqualsContractFor
import de.creatorcat.kotiya.test.core.reflect.assertHashCodeContractFor
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

@ExperimentalUnsignedTypes
class PropertyBasedDataDefinitionTest {

    @Test fun `combinedHashCodeOf - vararg`() {
        assertEquals(0, combinedHashCodeOf())
        assertEquals(0, combinedHashCodeOf(null))
        assertEquals(1, combinedHashCodeOf(1))
        assertEquals(17 + 2, combinedHashCodeOf(1, 2))
        assertEquals((17 pow 3u) + 2 * (17 pow 2u) + 0 + 3, combinedHashCodeOf(1, 2, null, 3))
    }

    @Test fun `combinedHashCodeOf - iterable`() {
        assertEquals(0, combinedHashCodeOf(emptyList()))
        assertEquals(0, combinedHashCodeOf(listOf(null)))
        assertEquals(1, combinedHashCodeOf(listOf(1)))
        assertEquals(17 + 2, combinedHashCodeOf(listOf(1, 2)))
        assertEquals((17 pow 3u) + 2 * (17 pow 2u) + 0 + 3, combinedHashCodeOf(listOf(1, 2, null, 3)))
    }

    @Test fun `creation via of`() {
        val p1 = MyClass::prop1
        val p2 = MyClass::prop2
        val dataDef = PropertyBasedDataDefinition.of(p1, p2)
        assertEquals(MyClass::class, dataDef.type)
        assertThat(dataDef.properties, hasElementsEqual<Any>(p1, p2))
    }

    @Test fun `computeHashCode - contract`() {
        MyClass::hashCode.assertHashCodeContractFor(testObjects)
    }

    @Test fun `computeHashCode - samples`() {
        assertEquals("a".hashCode() * 17 + 5, MyClass("a", 5).hashCode())
    }

    @Test fun `computeEquals - contract`() {
        MyClass::equals.assertEqualsContractFor(testObjects)
    }

    @Test fun `computeEquals - samples`() {
        assertEquals(MyClass("a", 5), MyClass("a", 15))
        assertEquals(MyClass("a", 33), MyClass("a", 23))
        assertNotEquals(MyClass("a", 1), MyClass("a", 2))
        assertNotEquals(MyClass("a", 1), MyClass("b", 1))
        assertNotEquals(MyClass("a", 1), MyClass("v", 2))
    }

    @Test fun `generateString - samples`() {
        assertThat(MyClass("abc", 17).toString(), contains("abc") and contains("7"))
        assertThat(MyClass("PROP", 123).toString(), contains("PROP") and contains("3"))
    }

    private val testObjects = listOf(
        // some equals
        MyClass("a", 5), MyClass("a", 15), MyClass("a", 25),
        // some non-equal
        MyClass("a", 3), MyClass("b", 5), MyClass("c", 3)
    )

    private class MyClass(val prop1: String, prop2raw: Int) {

        val prop2: Int = prop2raw % 10

        override fun equals(other: Any?): Boolean = dataDefinition.computeEquals(this, other)
        override fun hashCode(): Int = dataDefinition.computeHashCode(this)
        override fun toString(): String = dataDefinition.generateString(this)

        companion object {
            private val dataDefinition = PropertyBasedDataDefinition.of(
                MyClass::prop1,
                MyClass::prop2
            )
        }
    }
}
