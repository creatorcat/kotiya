/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

class StringCreationsTest {

    @Test fun `or empty if`() {
        assertEquals("abc", "abc".orEmptyIf(false))
        assertEquals("", "abc".orEmptyIf(true))
    }

    @Test fun `shorten - no cut`() {
        assertEquals("1234567890", "1234567890".shorten(10))
        assertEquals("12345", "12345".shorten(10))
        assertEquals("1", "1".shorten(4))
    }

    @Test fun `shorten - must cut`() {
        assertEquals("123456...", "1234567890".shorten(9))
        assertEquals("1...", "12345".shorten(4))
    }

    @Test fun `shorten - throws on bad suffix`() {
        assertThrows<IllegalArgumentException> { "a".shorten(3) }
    }

    @Test fun `withPrefix - simple usage`() {
        assertEquals("abcXXX", "abcXXX".withPrefix("abc"))
        assertEquals("abcbcXXX", "bcXXX".withPrefix("abc"))
        assertEquals("abcXXX", "XXX".withPrefix("abc"))
        assertEquals("", "".withPrefix(""))
        assertEquals("XXX", "XXX".withPrefix(""))
        assertEquals("abc", "".withPrefix("abc"))
    }
}
