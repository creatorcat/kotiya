/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.predicates

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class BooleanOperatorsTest {

    @Test fun `implication operator - boolean function values`() {
        assertTrue(true implies true)
        assertFalse(true implies false)
        assertTrue(false implies true)
        assertTrue(false implies false)
    }

    @Test fun `implication operator - some use cases`() {
        val a = true
        val b = false
        assertTrue(!a implies b)
        assertTrue((b == b) implies a)
        // implies has precedence over == !!!
        @Suppress("ReplaceAssertBooleanWithAssertEquality")
        assertFalse(b == b implies a)
    }
}
