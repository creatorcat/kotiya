/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class TypeUtilsTest {

    private class Dummy

    @Test fun `class name - ordinary types`() {
        assertEquals("String", "x".className)
        assertEquals("Int", 1.className)
        assertEquals("Dummy", Dummy().className)
    }

    @Test fun `class name - unknown types`() {
        val x = object {}
        assertEquals("UNKNOWN CLASS", null.className)
        assertEquals("UNKNOWN CLASS", x.className)
    }

    @Test fun `asNullOrInstanceOf - usage`() {
        val x: CharSequence = "x"
        val s = x.asNullOrInstanceOf(String::class)
        assertNotNull(s)

        assertNull(x.asNullOrInstanceOf(Int::class))
    }
}
