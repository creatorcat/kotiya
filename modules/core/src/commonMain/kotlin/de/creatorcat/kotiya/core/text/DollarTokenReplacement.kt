/*
 * Copyright 2017 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

/**
 * Function type to be used with [replaceDollarTokens] to perform actual token replacement.
 * The parsed token components are handed over as arguments to this function.
 *
 * @param tokenName name component of the token
 * @param arguments list of arguments parsed from the `(...)`-part of the token
 * @param body body component parsed from the `{...}`-part of the token
 * @return the replacement string or null if no replacement is available
 */
// KDoc does not support documenting the params of this function type but it seems just natural so its used here.
@Suppress("KDocUnresolvedReference")
typealias DollarTokenReplacer = (tokenName: String, arguments: List<String>, body: String) -> String?

private const val TOKEN_NAME_PATTERN = "[0-9a-zA-Z][\\w\\.]*"
private const val SINGLE_ARG_PATTERN = "\\s*[^\\s\\),][^\\),]*"
private const val TOKEN_ARGS_PATTERN = "$SINGLE_ARG_PATTERN(,$SINGLE_ARG_PATTERN)*"
private const val TOKEN_BODY_PATTERN = "[^\\}]*"

/**
 * Regular expression for dollar token parsing.
 *
 * @see replaceDollarTokens
 */
internal val dollarTokenReplacementRegex =
    Regex(
        "\\\$" + // start with $
            "(" +
            // either another $ --> $$ = $
            "\\\$|" +
            // or start replaceable token...
            "($TOKEN_NAME_PATTERN)" + // name
            "(\\(($TOKEN_ARGS_PATTERN)\\))?" + // arguments in parenthesis: (a,b,c) or ( a1 a2, c, d ) etc.
            "(\\{($TOKEN_BODY_PATTERN)\\})?" + // body inside curly braces: {<anything but }>}
            ")"
    )

/**
 * Regex group index of token name.
 */
internal const val NAME_GROUP = 2

/**
 * Regex group index of token arguments.
 */
internal const val ARGS_GROUP = 4

/**
 * Regex group index of token body.
 */
// remember: this is 7 because TOKEN_ARGS_PATTERN contains another '('
internal const val BODY_GROUP = 7

/**
 * Replace so called dollar tokens in the [input] string using the [replacer] function and
 * produce a new string containing the replacements instead of the tokens.
 *
 * Tokens have the format `$tokenName(arguments){body}` which enables the use of a function
 * like syntax and semantic for replacements.
 * The special token `$$` will be replaced by a simple '$' sign.
 *
 * Format details:
 * * `tokenName` starts with an alpha-numeric character and is followed by alpha-numeric characters
 *   or '_' or '.' in any number (`regex = [0-9a-zA-Z][\w\.]+]`)
 * * `arguments` are a comma separated list of arbitrary symbols, except ')'.
 *   Leading and trailing whitespace is trimmed, however, whitespace inside is preserved to
 *   support e.g. paths with blanks inside as arguments ("c:\program files\...").
 * * The `body` may be anything. Whitespace is preserved here.
 *
 * If the replacer returns null for a token, no replacement is applied and
 * the original token will be preserved.
 */
fun replaceDollarTokens(input: String, replacer: DollarTokenReplacer): String {
    return dollarTokenReplacementRegex.replace(input) { match ->
        val tokenName = match.groups[NAME_GROUP]?.value ?:
        // no tokenName, must be $$
        return@replace "\$"
        val arguments = match.groups[ARGS_GROUP]?.value
            ?.trim()
            ?.split(Regex("\\s*,\\s*"))
            ?: emptyList()
        val body = match.groups[BODY_GROUP]?.value ?: ""
        replacer.invoke(tokenName, arguments, body) ?: match.value
    }
}
