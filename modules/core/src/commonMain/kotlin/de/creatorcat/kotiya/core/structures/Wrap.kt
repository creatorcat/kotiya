/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * Simple wrapper for any value. Comes in handy from time to time.
 *
 * @param data wrapped object of type [T]
 */
data class Wrap<T>(var data: T)
