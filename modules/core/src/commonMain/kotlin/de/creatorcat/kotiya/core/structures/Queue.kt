/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * A First-In-First-Out (FIFO) collection of elements of type [E].
 * New elements may be [enqueued][enqueue] and existing elements may be [dequeued][dequeue].
 */
interface Queue<E> : Collection<E> {

    /**
     * Enqueue an [element] by appending it to the end of the queue.
     *
     * @see dequeue
     */
    fun enqueue(element: E)

    /**
     * Dequeue and return the head element of the queue.
     * The element will be removed from the queue making its successor the new head
     * or leaving the queue empty.
     *
     * @throws NoSuchElementException if the queue is empty
     */
    fun dequeue(): E

    /**
     * Enqueue the given [elements] by appending them to the end of the queue in the given order.
     *
     * The default implementation just calls the single argument [enqueue] method for each element.
     *
     * @see dequeue
     */
    fun enqueue(elements: Iterable<E>) {
        elements.forEach(this::enqueue)
    }

    companion object {

        /**
         * Create a queue with given [elements] of type [E].
         * The resulting queue will be a [LinkedItemQueue].
         */
        fun <E> create(vararg elements: E): Queue<E> {
            val queue = LinkedItemQueue<E>()
            queue.enqueue(*elements)
            return queue
        }
    }
}

/**
 * Enqueue the given [elements] by appending them to the end of the queue in the given order.
 *
 * @see Queue.enqueue
 * @see Queue.dequeue
 */
fun <E> Queue<E>.enqueue(vararg elements: E): Unit = this.enqueue(elements.asIterable())

