/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * Check if all given [elements] are contained in this collection.
 * Directly delegates to the instance method [Collection.containsAll].
 */
fun <E> Collection<E>.containsAll(vararg elements: E): Boolean =
    this.containsAll(elements.asList())

/**
 * Replaces the current elements of the collection by the given [newElements].
 */
fun <E> MutableCollection<E>.setAll(vararg newElements: E) {
    clear()
    addAll(newElements)
}

/**
 * Replaces the current elements of the collection by the given [newElements].
 */
fun <E> MutableCollection<E>.setAll(newElements: Iterable<E>) {
    clear()
    addAll(newElements)
}

/**
 * Adds all given [elements] to this collection.
 */
fun <E> MutableCollection<E>.addAll(vararg elements: E): Boolean =
    addAll(elements)

/**
 * Returns the size of the collection as unsigned integer.
 */
@ExperimentalUnsignedTypes
inline val Collection<*>.uSize: UInt
    get() = size.toUInt()

/**
 * Returns the size of the array as unsigned integer.
 */
@ExperimentalUnsignedTypes
inline val Array<*>.uSize: UInt
    get() = size.toUInt()
