/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * Returns a version of this list that is safe against mutations from outside.
 * Intended to be used to achieve immutability of list arguments.
 *
 * The result may be this list itself if it proves to be immutable.
 * Otherwise a copy will be created.
 * The resulting list is **not** guaranteed to be immutable,
 * but it will not be affected by possible mutation of the original list.
 */
fun <E> List<E>.makeMutationSafe(): List<E> = toList()

/**
 * Returns a lazy [Iterable] of the pairs of elements in this list that match the given [indexPairs].
 */
fun <T> List<T>.pairsWithIndices(indexPairs: Iterable<Pair<Int, Int>>): Iterable<Pair<T, T>> =
    object : Iterable<Pair<T, T>> {
        override fun iterator() = iterator {
            for ((i1, i2) in indexPairs) {
                yield(Pair(get(i1), get(i2)))
            }
        }
    }

/**
 * Read access operator based on an unsigned integer index.
 * Works be converting the index into an ordinary integer.
 */
@ExperimentalUnsignedTypes
operator fun <E> List<E>.get(index: UInt): E = get(index.toInt())

/**
 * Write access operator based on an unsigned integer index.
 * Works be converting the index into an ordinary integer.
 *
 * @return the element previously at the specified position
 */
@ExperimentalUnsignedTypes
operator fun <E> MutableList<E>.set(index: UInt, value: E): E = set(index.toInt(), value)
