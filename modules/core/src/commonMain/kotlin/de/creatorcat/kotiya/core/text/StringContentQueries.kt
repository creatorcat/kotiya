/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

/**
 * Returns `true` if this sequence [contains][CharSequence.contains] all of the given [subStrings].
 *
 * @param ignoreCase `true` to ignore character case when comparing strings - defaults to `false`
 */
fun CharSequence.containsAll(vararg subStrings: CharSequence, ignoreCase: Boolean = false): Boolean {
    for (sub in subStrings) {
        if (!this.contains(sub, ignoreCase)) return false
    }
    return true
}
