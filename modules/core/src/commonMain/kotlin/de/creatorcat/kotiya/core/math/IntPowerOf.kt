/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.math

/**
 * Raises this value to the integer power [n].
 * Calculation is done by log n multiplications so the value may over flow.
 *
 * @param n exponent >= 0
 */
@ExperimentalUnsignedTypes
infix fun Int.pow(n: UInt): Int =
    calcPowRecursively(n, Int::pow, Int::times, 1)

/**
 * Raises this value to the integer power [n].
 * Calculation is done by log n multiplications so the value may over flow.
 *
 * @param n exponent >= 0
 */
@ExperimentalUnsignedTypes
infix fun UInt.pow(n: UInt): UInt =
    calcPowRecursively(n, UInt::pow, UInt::times, 1U)

/**
 * Raises this value to the integer power [n].
 * Calculation is done by log n multiplications so the value may over flow.
 *
 * @param n exponent >= 0
 */
@ExperimentalUnsignedTypes
infix fun Long.pow(n: UInt): Long =
    calcPowRecursively(n, Long::pow, Long::times, 1L)

/**
 * Raises this value to the integer power [n].
 * Calculation is done by log n multiplications so the value may over flow.
 *
 * @param n exponent >= 0
 */
@ExperimentalUnsignedTypes
infix fun ULong.pow(n: UInt): ULong =
    calcPowRecursively(n, ULong::pow, { u1, u2 -> u1 * u2 }, 1UL)

@ExperimentalUnsignedTypes
private inline fun <N> N.calcPowRecursively(n: UInt, powFunc: N.(UInt) -> N, mulOp: (N, N) -> N, one: N): N =
    when {
        n == 0u -> one
        n == 1u -> this
        n > 1u -> {
            val half = n / 2u
            val powHalf = powFunc(half)
            val powHalfSquare = mulOp(powHalf, powHalf)
            if (2u * half == n) // n is even
                powHalfSquare
            else // n is odd
                mulOp(powHalfSquare, this)
        }
        else -> throw IllegalArgumentException("invalid exponent: $n")
    }
