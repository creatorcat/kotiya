/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

// TODO use interface like TreeWalk
private class DepthCountingDepthFirstTreeWalk<out N : TreeNode<N>>(
    val root: N, private val depth: Int
) : Sequence<Pair<N, Int>> {

    override operator fun iterator(): Iterator<Pair<N, Int>> =
        iterator {
            yield(Pair(root, depth))
            for (childNode in root.children) {
                yieldAll(DepthCountingDepthFirstTreeWalk(childNode, depth + 1))
            }
        }
}

/**
 * Depth-first iteration over a tree of nodes along with the current depth count i.e. the distance to the root.
 */
fun <N : TreeNode<N>> N.walkAndCountDepthFirst(): Sequence<Pair<N, Int>> = DepthCountingDepthFirstTreeWalk(this, 0)
