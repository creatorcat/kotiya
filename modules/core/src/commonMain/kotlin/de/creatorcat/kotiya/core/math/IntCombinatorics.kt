/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.math

/**
 * Returns a lazy [Iterable] of all ordered and distinct pairs of numbers in this range
 * `(start, start+1)`, `(start, start+2)` ... `(last-1, last)`
 *
 * This represents a 2-combination without repetition.
 *
 * @see IntRange.start
 * @see IntRange.last
 */
fun IntRange.orderedDistinctPairs(): Iterable<Pair<Int, Int>> = object : Iterable<Pair<Int, Int>> {
    override fun iterator() = iterator {
        for (i1 in this@orderedDistinctPairs.start..(this@orderedDistinctPairs.last - 1)) {
            for (i2 in (i1 + 1)..this@orderedDistinctPairs.last) {
                yield(i1 to i2)
            }
        }
    }
}

/**
 * Returns a lazy [Iterable] of all ordered pairs of numbers in this range:
 * `(start, start)`, `(start, start+1)` ... `(last, last)`
 *
 * This represents a 2-combination with repetition or 2-multi-combination.
 *
 * @see IntRange.start
 * @see IntRange.last
 */

fun IntRange.orderedPairs(): Iterable<Pair<Int, Int>> = object : Iterable<Pair<Int, Int>> {
    override fun iterator() = iterator {
        for (i1 in this@orderedPairs) {
            for (i2 in i1..this@orderedPairs.last) {
                yield(i1 to i2)
            }
        }
    }
}
