/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

private val newlineRegex = Regex("\\r\\n?|\\n")

/**
 * Normalize the string's line endings to linux style (linefeed only).
 *
 * @return the normalized string
 * @see replaceNewlineWith
 */
fun String.normalizeNewlineToLinux(): String = replaceNewlineWith("\n")

/**
 * Normalize the string's line endings to windows style (carriage return + linefeed).
 *
 * @return the normalized string
 * @see replaceNewlineWith
 */
fun String.normalizeNewlineToWindows(): String = replaceNewlineWith("\r\n")

/**
 * Replace the string's line endings with the given replacement.
 *
 * Recognized line endings are: "\r\n", "\r" and "\n".
 *
 * @return the altered string
 */
fun String.replaceNewlineWith(replacement: String): String =
    replace(newlineRegex, replacement)

/**
 * Normalize the string's line endings to [SYSTEM_LINE_SEPARATOR].
 *
 * @return the normalized string
 * @see replaceNewlineWith
 */
fun String.normalizeNewlineToSystem(): String = replaceNewlineWith(SYSTEM_LINE_SEPARATOR)

/**
 * Replace *sloshes* (`\`) by *slashes* (`/`).
 * Useful when working with Windows file paths.
 */
fun String.sloshToSlash(): String =
    replace("\\", "/")

/** The system dependent line separator e.g. `\n` on Unix, `\r\n` on Windows. */
expect val SYSTEM_LINE_SEPARATOR: String
