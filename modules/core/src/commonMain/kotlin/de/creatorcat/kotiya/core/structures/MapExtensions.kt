/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * If the specified [key] does not already exist in this map (see [Map.get])
 * it will be added and associated with the given [value].
 *
 * This is aligned to the JVM method of same name.
 *
 * @return The current value associated to the [key] in the map or `null` if the key
 * was not present (in which case it was just put by this operation).
 *
 * @see MutableMap.put
 */
fun <K, V> MutableMap<K, V>.putIfAbsent(key: K, value: V): V? = get(key) ?: put(key, value)

/**
 * Updates this map with key/value pairs from the specified map [from],
 * but only for keys that do not yet exist int this map.
 */
fun <K, V> MutableMap<K, V>.putAllIfAbsent(from: Map<out K, V>) {
    for ((key, value) in from) {
        this.putIfAbsent(key, value)
    }
}

/**
 * If the [key] is not already associated with a value or is associated with null,
 * associates it with the given [value].
 * Otherwise, replaces the associated value with the results of the [mergeCode],
 * or removes it if the result is `null`.
 * This method may be of use when combining multiple mapped values for a key.
 *
 * @return the new value associated with the specified [key], or `null` if no value is associated with the key
 */
fun <K, V> MutableMap<K, V>.merge(key: K, value: V, mergeCode: (V, V) -> V?): V? {
    val oldValue = get(key)
    val newValue =
        if (oldValue == null) value
        else mergeCode(oldValue, value)
    if (newValue == null)
        remove(key)
    else
        put(key, newValue)

    return newValue
}

/**
 * Merges this map with the [other].
 * For all key-value pairs in [other] calls `this.merge(key, value, mergeCode)`.
 *
 * @see merge
 */
fun <K, V> MutableMap<K, V>.mergeAll(other: Map<K, V>, mergeCode: (V, V) -> V?) {
    for ((key, value) in other) {
        merge(key, value, mergeCode)
    }
}

