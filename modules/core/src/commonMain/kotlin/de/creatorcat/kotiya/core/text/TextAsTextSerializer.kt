/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

/**
 * This class enables serialization of text in string format so it can be read again without any loss.
 * It allows to [write] single texts or lists of text objects using an [Appendable].
 * Written text can later be [read] again, resulting in a list of strings.
 *
 * Writing text as text on first glance seems trivial, but if a list of strings shall be written
 * and later restored, each entry must separated from each other in the storage format.
 * Also whitespaces must be preserved.
 * This class handles all mentioned and other situations.
 */
open class TextAsTextSerializer {

    /** Sequence to be used to envelope text values when [writing][write]. */
    open val entryEnclosingChar: String = "'"

    /** Pattern that matches an [entryEnclosingChar] in a regex. */
    open val entryEnclosingCharInRegex: String by lazy { entryEnclosingChar }

    /**
     * Sequence to be used to escape certain special characters in the serialized text,
     * most important the [entryEnclosingChar].
     */
    open val escapeChar: String = "\$"

    /** Pattern that matches an [escapeChar] in a regex. */
    open val escapeCharInRegex: String = "\\\$"

    /** Escaped representation of the [entryEnclosingChar] in the serialized text. */
    open val escapedEntryEnclosingChar: String = "#"

    /** Sequence to separate multiple text values when [writing][write] a list of text values. */
    open val entrySeparator: String = ", "

    private val textToSafeEntryRegex by lazy { Regex("([$escapeCharInRegex$entryEnclosingCharInRegex])") }
    private val safeEntryToTextRegex by lazy { Regex("$escapeCharInRegex(.)") }

    private val escapeReplacement by lazy { escapeChar + escapeChar }
    private val enclosingCharReplacement by lazy { escapeChar + escapedEntryEnclosingChar }

    private val entryInListRegex by lazy {
        Regex("$entryEnclosingCharInRegex([^$entryEnclosingCharInRegex]*)$entryEnclosingCharInRegex")
    }

    /**
     * Appends the given [text] to the [destination] in the safe serialized format.
     * The written text can be [read] by an equivalent serializer.
     *
     * The format is: [entryEnclosingChar]*"escaped text"*[entryEnclosingChar].
     * In the *escaped text* the `entryEnclosingChar` is represented by an [escapeChar] followed by
     * the [escapedEntryEnclosingChar]. The `escapeChar` is represented by a double `escapeChar`.
     */
    fun write(destination: Appendable, text: CharSequence) {
        destination.append(entryEnclosingChar)
        destination.append(textToSafeEntry(text))
        destination.append(entryEnclosingChar)
    }

    /**
     * Appends safely serialized versions of each element in the [textList] to the [destination].
     * The written text can be [read] by an equivalent serializer.
     *
     * The format is: *"serialized text1"*[entrySeparator]*"serialized text2"*...
     * with *serialized textN* being in the same format as if writing a single text value.
     */
    fun write(destination: Appendable, textList: List<CharSequence>) {
        for ((idx, text) in textList.withIndex()) {
            if (idx > 0) destination.append(entrySeparator)
            write(destination, text)
        }
    }

    /**
     * Reads text serialized by an equivalent serializer into a list of strings.
     *
     * The text values must be in the format written by this serializer (see [write]) to be recognized.
     * However, the read process is very tolerant: it ignores any non-matching sequences silently.
     * This leaves room e.g. for comments inside serialized text lists.
     */
    fun read(source: String): List<String> =
        entryInListRegex.findAll(source).map { match ->
            val entry = match.groups[1]?.value ?: throwOnUnexpectedMatchGroup(match)
            safeEntryToText(entry)
        }.toList()

    internal fun textToSafeEntry(text: CharSequence): String =
        textToSafeEntryRegex.replace(text) { match ->
            when (match.groups[1]?.value) {
                escapeChar -> escapeReplacement
                entryEnclosingChar -> enclosingCharReplacement
                else -> throwOnUnexpectedMatchGroup(match)
            }
        }

    internal fun safeEntryToText(entry: String): String =
        safeEntryToTextRegex.replace(entry) { match ->
            when (match.groups[1]?.value) {
                escapeChar -> escapeChar
                escapedEntryEnclosingChar -> entryEnclosingChar
                else -> throwOnUnexpectedMatchGroup(match)
            }
        }

    private fun throwOnUnexpectedMatchGroup(match: MatchResult): Nothing =
        throw AssertionError("unexpected match result group 1: ${match.groups[1]}")
}
