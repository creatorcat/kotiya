/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.reflect.asNullOrInstanceOf
import de.creatorcat.kotiya.core.reflect.className
import kotlin.reflect.KClass

/**
 * Allows the definition of a data object of [type] through a number of [properties] (getters).
 * Based on the `properties`, it provides some standard object functionality
 * like [computeEquals] or [computeHashCode].
 *
 * This class is meant to be used as helper where ever a Kotlin `data class` cannot be used
 * due to the restrictions on data classes.
 * Usage example:
 *
 *     private class MyClass(val prop1: String, prop2raw: Int) {
 *
 *         val prop2: Int = prop2raw % 10
 *
 *         override fun equals(other: Any?): Boolean = dataDefinition.computeEquals(this, other)
 *         override fun hashCode(): Int = dataDefinition.computeHashCode(this)
 *
 *         companion object {
 *             private val dataDefinition = PropertyBasedDataDefinition.of(
 *                 MyClass::prop1,
 *                 MyClass::prop2
 *             )
 *         }
 *     }
 */
class PropertyBasedDataDefinition<T : Any>(
    /** The associated type of this definition whose data is defined by the [properties]. */
    val type: KClass<T>,
    /** The properties (getters) that define the data of the associated [type]. */
    val properties: List<(T) -> Any?>
) {

    /**
     * Computes equality of [thisObject] and [other] based on the [properties].
     * The result is `true` if `other` is an instance of [type]
     * and each corresponding property value pair of both objects is equal.
     * If any property values differ or [other] is not a [type], the result is `false`.
     */
    fun computeEquals(thisObject: T, other: Any?): Boolean {
        if (thisObject === other) return true
        val otherT = other asNullOrInstanceOf type ?: return false
        for (property in properties) {
            if (property(thisObject) != property(otherT)) return false
        }
        return true
    }

    /**
     * Computes a hash of [thisObject] by retrieving the values of the respective [properties]
     * and computing the [combined hash][combinedHashCodeOf].
     */
    fun computeHashCode(thisObject: T): Int =
        combinedHashCodeOf(properties.map { it(thisObject) })

    /**
     * Generates a string representing [thisObject] based on the values of the respective [properties].
     */
    fun generateString(thisObject: T): String =
        properties.map { it(thisObject) }.joinToString(
            prefix = "${thisObject.className}(",
            postfix = ")"
        )

    companion object {
        /**
         * Convenience method to create a data definition with given [properties] and [type] = [T].
         */
        inline fun <reified T : Any> of(vararg properties: (T) -> Any?): PropertyBasedDataDefinition<T> =
            PropertyBasedDataDefinition(T::class, properties.asList())
    }
}

/**
 * Computes a combined hash of all given [objects], based on the individual hash of each object.
 * If [objects] is empty, the combined hash is `0`.
 */
fun combinedHashCodeOf(objects: Iterable<Any?>): Int {
    val hashes = objects.map(Any?::hashCode)
    return if (hashes.isEmpty()) 0 else hashes.reduce { acc, hash -> 17 * acc + hash }
}

/**
 * Computes a combined hash of all given [objects], based on the individual hash of each object.
 * If [objects] is empty, the combined hash is `0`.
 */
fun combinedHashCodeOf(vararg objects: Any?): Int =
    combinedHashCodeOf(objects.asList())
