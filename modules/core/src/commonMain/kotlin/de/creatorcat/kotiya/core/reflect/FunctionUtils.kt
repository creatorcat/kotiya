/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import kotlin.reflect.KFunction

/**
 * Cast the given [function] to a [KFunction] object.
 * This is sometimes necessary especially to avoid the *overload resolution ambiguity* problem
 * when dealing with function references.
 */
@Suppress("UNCHECKED_CAST")
fun <R, T : Function<R>> asKFunction(function: T): KFunction<R> = function as KFunction<R>
