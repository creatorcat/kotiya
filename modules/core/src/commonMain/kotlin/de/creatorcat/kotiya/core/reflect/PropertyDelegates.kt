/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

/**
 * A mutable property delegate that directly passes through get and set access to another `target` property.
 * The recommended way is to use [otherProperty] for creation.
 *
 * @param V value type of the property
 *
 * @constructor Directly create a property delegation.
 * The recommended way is to use [otherProperty] for creation.
 *
 * @param targetProperty the delegation target
 */
class MutablePropertyDelegate<V>(
    private val targetProperty: KMutableProperty0<V>
) : ReadWriteProperty<Any?, V> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): V = targetProperty()

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: V) {
        targetProperty.set(value)
    }
}

/**
 * A property delegate that directly passes through get access to another `target` property.
 * The recommended way is to use [otherProperty] for creation.
 *
 * @param V value type of the property
 *
 * @constructor Create a property delegation.
 * The recommended way is to use [otherProperty] for creation.
 *
 * @param targetProperty the delegation target
 */
class PropertyDelegate<out V>(
    private val targetProperty: KProperty0<V>
) : ReadOnlyProperty<Any?, V> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): V = targetProperty()
}

/**
 * Create a property delegate that directly passes through get access to another `target` property.
 *
 * @param V value type of the property
 *
 * @param targetProperty the delegation target
 * @return the property delegate
 */
fun <V> otherProperty(targetProperty: KProperty0<V>): PropertyDelegate<V> = PropertyDelegate(targetProperty)

/**
 * A mutable property delegate that directly passes through get and set access to another `target` property.
 *
 * @param V value type of the property
 *
 * @param targetProperty the delegation target
 * @return the property delegate
 */
fun <V> otherProperty(targetProperty: KMutableProperty0<V>): MutablePropertyDelegate<V> =
    MutablePropertyDelegate(targetProperty)
