/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

/**
 * Returns this string or the empty string `""` if the [condition] is `true`.
 */
fun String.orEmptyIf(condition: Boolean): String = if (condition) "" else this

/**
 * Returns a shortened version of this string, if it exceeds the length of [n].
 * The [cutIndicationSuffix] will be appended at the end of the shortened string,
 * so the result will have length [n].
 * If this string is already shorter than [n], it will be returned itself.
 */
fun String.shorten(n: Int, cutIndicationSuffix: CharSequence = "..."): String {
    require(cutIndicationSuffix.length < n) {
        "the cutIndicationSuffix must not be longer than the number of chars to preserve"
    }
    return if (length <= n) this else take(n - cutIndicationSuffix.length) + cutIndicationSuffix
}

/**
 * Returns a version of the string with leading [prefix].
 * If this string already has the same prefix it will be returned without changes.
 *
 * @param ignoreCase indicate if the character shall be ignored when checking for the [prefix] presence;
 *   defaults to `false`
 */
fun String.withPrefix(prefix: CharSequence, ignoreCase: Boolean = false): String =
    if (startsWith(prefix, ignoreCase)) this else "${prefix}${this}"
