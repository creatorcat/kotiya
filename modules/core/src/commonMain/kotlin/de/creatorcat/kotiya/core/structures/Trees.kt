/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * A node of a tree graph.
 *
 * Tree nodes have [children] or they are [leaves][isLeaf].
 * They span a tree, that is a connected and acyclic graph.
 * The tree may be *walked* [depth-first][walkDepthFirst] or [breadth-first][walkBreadthFirst].
 *
 * *Note:* this interface does not provide the functionality to assure that the graph spanned
 * by the nodes is actually a tree. Implementations may take care of this or should at least inform the user.
 *
 * @param N actual node type
 */
interface TreeNode<out N : TreeNode<N>> {
    /** The child nodes of this node, maybe empty. */
    val children: List<N>
}

/**
 * `True` if this node is a *leaf* meaning it has no [children][TreeNode.children].
 */
val TreeNode<*>.isLeaf: Boolean get() = children.isEmpty()

/**
 * Returns a [TreeWalk] that iterates the tree nodes *breadth-first* with this node as [root][TreeWalk.root].
 */
fun <N : TreeNode<N>> N.walkDepthFirst(): TreeWalk<N> = DepthFirstTreeWalk(this)

/**
 * Returns a [TreeWalk] that iterates the tree nodes *depth-first* with this node as [root][TreeWalk.root].
 */
fun <N : TreeNode<N>> N.walkBreadthFirst(): TreeWalk<N> = BreadthFirstTreeWalk(this)

/**
 * A [Sequence] to walk the tree that is spanned by the [root], its [children][TreeNode.children] etc.
 *
 * @param N actual node type
 *
 * @see TreeNode.walkDepthFirst
 * @see TreeNode.walkBreadthFirst
 */
interface TreeWalk<out N : TreeNode<N>> : Sequence<N> {
    /** The root node of the tree to be walked. */
    val root: N
}

private class BreadthFirstTreeWalk<out N : TreeNode<N>>(override val root: N) : TreeWalk<N> {

    override operator fun iterator(): Iterator<N> {
        return iterator {
            val unvisitedNodes = Queue.create(root)
            while (unvisitedNodes.isNotEmpty()) {
                val next = unvisitedNodes.dequeue()
                yield(next)
                unvisitedNodes.enqueue(next.children)
            }
        }
    }
}

private class DepthFirstTreeWalk<out N : TreeNode<N>>(override val root: N) : TreeWalk<N> {

    override operator fun iterator(): Iterator<N> =
        iterator {
            yield(root)
            for (childNode in root.children) {
                yieldAll(DepthFirstTreeWalk(childNode))
            }
        }
}
