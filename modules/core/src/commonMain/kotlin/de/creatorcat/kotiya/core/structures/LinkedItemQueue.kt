/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.reflect.otherProperty

/**
 * Implementation of a [Queue] using linked items.
 *
 * The head item holds the head value and a link to its successor which holds another value and link and so on.
 *
 * @param E element type
 *
 * @constructor Creates an empty queue.
 */
open class LinkedItemQueue<E> : Queue<E> {

    private class Item<E>(val data: E, var next: Item<E>? = null)

    private var head: Item<E>? = null
    private var tail: Item<E>? = null

    private var _size: Int = 0
    override val size: Int by otherProperty(::_size)

    override fun enqueue(element: E) {
        _size++
        val newItem = Item(element)
        val currentTail = tail
        if (currentTail != null) {
            currentTail.next = newItem
        } else {
            // if tail is null so must be head
            head = newItem
        }
        tail = newItem
    }

    override fun dequeue(): E {
        val currentHead = head
        if (currentHead != null) {
            _size--
            head = currentHead.next
            if (tail == currentHead) tail = null
            return currentHead.data
        } else {
            throw NoSuchElementException("cannot dequeue empty queue")
        }
    }

    override fun contains(element: E): Boolean = this.indexOf(element) >= 0

    override fun containsAll(elements: Collection<E>): Boolean {
        var foundElements = elements.size
        for (item in this) {
            foundElements -= elements.count { it == item }
            if (foundElements == 0) return true
        }
        return false
    }

    override fun isEmpty(): Boolean = size == 0

    override fun iterator(): Iterator<E> =
        iterator {
            var nextItem = head
            while (nextItem != null) {
                yield(nextItem.data)
                nextItem = nextItem.next
            }
        }
}
