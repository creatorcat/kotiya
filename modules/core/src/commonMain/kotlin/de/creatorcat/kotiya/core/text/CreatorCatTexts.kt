/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.text

/** The creator cat logo as ascii art. */
const val CREATOR_CAT_LOGO: String = """
  /\___/\        _
 / o   o \       \\
 \ ==▀== /_______//
 /  `¯´ /  /  /  /
(,,)(,,)__(,,)(,,)
"""

/**
 * A creator cat hello text for command line output.
 * Includes the [CREATOR_CAT_LOGO].
 */
const val CREATOR_CAT_CLI_HELLO: String = """
.............................
$CREATOR_CAT_LOGO

Thanks for using Creator Cat!
.............................
"""
