/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.predicates

/**
 * Boolean operator for the logical implication function: `a → b = ¬a ∨ b`.
 */
infix fun Boolean.implies(otherExpression: Boolean): Boolean = !this || otherExpression
