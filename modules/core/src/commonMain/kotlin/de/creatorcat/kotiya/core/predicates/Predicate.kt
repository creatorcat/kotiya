/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.predicates

/**
 * Convenience type alias for predicate-like boolean functions.
 * A predicate function usually checks if a value satisfies certain conditions or is in a certain state.
 */
typealias Predicate<T> = (T) -> Boolean

/**
 * Convenience operator to check if this predicate *applies to* a certain value.
 *
 * @see satisfies
 * @see appliesToAll
 * @see appliesToNone
 * @see appliesToAny
 */
infix fun <T> Predicate<T>.appliesTo(value: T): Boolean = this(value)

/**
 * Convenience operator to check if this value *satisfies* the [predicate].
 *
 * @see appliesTo
 */
infix fun <T> T.satisfies(predicate: Predicate<T>): Boolean = predicate(this)

/**
 * Checks if a [predicate] *applies to* all [objects].
 *
 * @see appliesTo
 * @see appliesToNone
 * @see appliesToAny
 */
fun <T> appliesToAll(vararg objects: T, predicate: Predicate<T>): Boolean {
    for (o in objects) {
        if (!predicate(o)) return false
    }
    return true
}

/**
 * Checks if a [predicate] *applies to* none of the given [objects].
 *
 * @see appliesTo
 * @see appliesToAll
 * @see appliesToAny
 */
fun <T> appliesToNone(vararg objects: T, predicate: Predicate<T>): Boolean =
    appliesToAll(*objects) { !predicate(it) }

/**
 * Checks if a [predicate] *applies to* any of the given [objects].
 *
 * @see appliesTo
 * @see appliesToNone
 * @see appliesToAll
 */
fun <T> appliesToAny(vararg objects: T, predicate: Predicate<T>): Boolean {
    for (o in objects) {
        if (predicate(o)) return true
    }
    return false
}

/**
 * Checks if this predicate *applies to* all [objects].
 *
 * @see appliesTo
 * @see appliesToNone
 * @see appliesToAny
 */
fun <T> Predicate<T>.appliesToNone(vararg objects: T): Boolean = appliesToNone(*objects, predicate = this)

/**
 * Checks if this predicate *applies to* none of the given [objects].
 *
 * @see appliesTo
 * @see appliesToAny
 * @see appliesToAll
 */
fun <T> Predicate<T>.appliesToAll(vararg objects: T): Boolean = appliesToAll(*objects, predicate = this)

/**
 * Checks if this predicate *applies to* any of the given [objects].
 *
 * @see appliesTo
 * @see appliesToNone
 * @see appliesToAll
 */
fun <T> Predicate<T>.appliesToAny(vararg objects: T): Boolean = appliesToAny(*objects, predicate = this)
