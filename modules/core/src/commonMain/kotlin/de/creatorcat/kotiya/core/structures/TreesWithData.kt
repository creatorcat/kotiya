/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

/**
 * A [TreeNode] that has some attached [data] and so have its [children].
 *
 * To create simple nodes use the *static* methods of [SimpleDataNode.Companion].
 */
interface TreeNodeWithData<out D, out N : TreeNodeWithData<D, N>> : TreeNode<N> {

    /** Data object attached to this node. */
    val data: D
}

/**
 * Simplification of [TreeNodeWithData] to be used when no specific implementation is necessary.
 *
 * To create simple nodes use the *static* methods of [SimpleDataNode.Companion].
 */
interface SimpleDataNode<out D> : TreeNodeWithData<D, SimpleDataNode<D>> {

    /** The companion object provides methods to easily create nodes. */
    companion object
}

private class SimpleDataNodeImplementation<out D>(
    override val data: D,
    override val children: List<SimpleDataNode<D>>
) : SimpleDataNode<D>

private class LazyChildrenSimpleDataNode<out D>(
    override val data: D,
    lazyChildren: () -> List<SimpleDataNode<D>>
) : SimpleDataNode<D> {

    override val children: List<SimpleDataNode<D>> by lazy(lazyChildren)
}

/**
 * Creates a [leaf node][TreeNode.isLeaf] with attached [data].
 */
fun <D> SimpleDataNode.Companion.of(data: D): SimpleDataNode<D> =
    SimpleDataNodeImplementation(data, emptyList())

/**
 * Creates a node with attached [data] and given [children].
 */
fun <D> SimpleDataNode.Companion.of(
    data: D, children: List<SimpleDataNode<D>>
): SimpleDataNode<D> =
    SimpleDataNodeImplementation(data, children.makeMutationSafe())

/**
 * Creates a node with attached [data] and given [children].
 */
fun <D> SimpleDataNode.Companion.of(
    data: D, vararg children: SimpleDataNode<D>
): SimpleDataNode<D> =
    SimpleDataNodeImplementation(data, children.asList())

/**
 * Creates a node with attached [data] and a list of [children][SimpleDataNode.children] created
 * by mapping each [childData] to a new leaf node.
 * The data to leaf mapping is evaluated lazily once the `children` are requested for the first time.
 */
fun <D> SimpleDataNode.Companion.ofChildrenWithData(
    data: D, vararg childData: D
): SimpleDataNode<D> =
    LazyChildrenSimpleDataNode(data) { childData.map { SimpleDataNode.of(it) } }

/**
 * Creates a node with attached [data] and a list of [children][SimpleDataNode.children] created
 * by mapping each [childData] to a new leaf node.
 * The data to leaf mapping is evaluated lazily once the `children` are requested for the first time.
 */
fun <D> SimpleDataNode.Companion.ofChildrenWithData(
    data: D, childData: List<D>
): SimpleDataNode<D> =
    LazyChildrenSimpleDataNode(data) { childData.map { SimpleDataNode.of(it) } }

/**
 * Creates a node with attached [data].
 *
 * @param lazyChildren lazily evaluated value of [TreeNodeWithData.children]
 */
fun <D> SimpleDataNode.Companion.of(
    data: D, lazyChildren: () -> List<SimpleDataNode<D>>
): SimpleDataNode<D> =
    LazyChildrenSimpleDataNode(data) { lazyChildren().makeMutationSafe() }
