/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.reflect

import kotlin.reflect.KClass

/**
 * Retrieve the class name of this object if possible.
 *
 * Returns [KClass.simpleName] if this object is not null and the class name is available.
 * Otherwise `"UNKNOWN CLASS"` is returned.
 */
val Any?.className: String
    get() = this?.let { it::class.simpleName } ?: "UNKNOWN CLASS"

/**
 * Returns this object as [T] if it is an instance of [type].
 * Otherwise, returns `null`.
 */
@Suppress("UNCHECKED_CAST")
infix fun <T : Any> Any?.asNullOrInstanceOf(type: KClass<T>): T? =
    if (type.isInstance(this)) this as T else null
