/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.core.structures

import de.creatorcat.kotiya.core.predicates.Predicate
import de.creatorcat.kotiya.core.predicates.appliesTo

/**
 * Apply the [mapping] on the elements of this iterable as long as the produced result satisfies the [predicate].
 *
 * @param includeFirstFail If the mapping process stops due to a mapping result not satisfying the predicate,
 *   this argument indicates whether the mapping result should be dropped or included into the list.
 * @return a list of all mapping results produced
 */
fun <E, R> Iterable<E>.mapWhile(
    predicate: Predicate<R>, includeFirstFail: Boolean = false,
    mapping: (E) -> R
): List<R> {
    val iterator = iterator()
    val results = mutableListOf<R>()
    var condition = true
    while (condition && iterator.hasNext()) {
        val current = mapping(iterator.next())
        condition = predicate appliesTo current
        if (condition || includeFirstFail) results += current
    }
    return results
}

/**
 * Returns a lazy [Iterable] of all pairs of elements in this iterable:
 * `(first, first)`, `(first, second)` ... `(last, last)`
 *
 * This represents a variation with repetition (or permutation with repetition).
 */
fun <T> Iterable<T>.allPairs(): Iterable<Pair<T, T>> = object : Iterable<Pair<T, T>> {
    override fun iterator() = iterator {
        for (i1 in this@allPairs) {
            for (i2 in this@allPairs) {
                yield(i1 to i2)
            }
        }
    }
}

/**
 * Returns a lazy [Iterable] of all triples of elements in this iterable:
 * `(first, first, first)`, `(first, first, second)` ... `(first, second, first)` ... `(last, last, last)`
 *
 * This represents a variation with repetition (or permutation with repetition).
 */
fun <T> Iterable<T>.allTriples(): Iterable<Triple<T, T, T>> = object : Iterable<Triple<T, T, T>> {
    override fun iterator() = iterator {
        for (i1 in this@allTriples) {
            for (i2 in this@allTriples) {
                for (i3 in this@allTriples) {
                    yield(Triple(i1, i2, i3))
                }
            }
        }
    }
}

