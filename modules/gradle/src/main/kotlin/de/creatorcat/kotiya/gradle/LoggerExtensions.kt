/*
 * Copyright 2020 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import org.gradle.api.logging.Logger

/**
 * Uses this `logger` to print a java-style error message
 * containing the [errorMsg] and the [errorReason] as string.
 */
fun Logger.javaStyleError(errorMsg: String, errorReason: Any?) {
    error("e: {}: {}", errorMsg, errorReason)
}

/**
 * Uses this `logger` to print a java-style warning message
 * containing the [warningMsg] and the [warningReason] as string.
 */
fun Logger.javaStyleWarning(warningMsg: String, warningReason: Any?) {
    warn("w: {}: {}", warningMsg, warningReason)
}
