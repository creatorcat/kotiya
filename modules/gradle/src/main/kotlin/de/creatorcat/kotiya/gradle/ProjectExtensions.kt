/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import org.gradle.api.Project
import java.io.File
import java.io.IOException

/**
 * Convenience method to resolve output files and make sure their parent directories exist.
 *
 * Resolve a file [path] using [Project.file] and create all non-existing parent directories.
 */
fun Project.outputFile(path: Any): File {
    val file = file(path)
    if (!file.parentFile.exists() &&
        !(file.parentFile?.mkdirs()
            ?: throw IOException("The file has no valid parent directory: $file"))
    ) {
        throw IOException("Failed to create all parent directories of $file")
    }
    return file
}
