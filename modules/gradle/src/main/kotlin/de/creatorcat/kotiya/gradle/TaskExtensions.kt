/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import org.gradle.api.Buildable
import org.gradle.api.Task
import org.gradle.api.plugins.HelpTasksPlugin
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.publish.plugins.PublishingPlugin
import org.gradle.api.tasks.TaskDependency
import org.gradle.api.tasks.TaskReference
import org.gradle.language.base.plugins.LifecycleBasePlugin

/**
 * The [obj] is considered a *direct* task dependency when retrieving the respective tasks to depend on
 * is as trivial as calling a getter method or accessing a property.
 * This is true if [obj] has one of the following types:
 * [Task], [TaskDependency], [TaskReference], [Buildable]
 */
fun isDirectTaskDependency(obj: Any?): Boolean =
    when (obj) {
        is Task,
        is Buildable,
        is TaskDependency,
        is TaskReference -> true
        else -> false
    }

/**
 * Provides all Gradle standard [task.group][Task.getGroup] names at one single location.
 * Unfortunately Gradle has these names scattered in different locations, so this is a summary.
 */
object TaskGroup {
    /** Standard build task group name */
    const val BUILD: String = LifecycleBasePlugin.BUILD_GROUP

    /** Standard verification task group name */
    const val VERIFICATION: String = LifecycleBasePlugin.VERIFICATION_GROUP

    /** Standard help task group name */
    const val HELP: String = HelpTasksPlugin.HELP_GROUP

    /** Standard documentation task group name */
    const val DOCUMENTATION: String = JavaBasePlugin.DOCUMENTATION_GROUP

    /** Standard publish task group name */
    const val PUBLISH: String = PublishingPlugin.PUBLISH_TASK_GROUP
}
