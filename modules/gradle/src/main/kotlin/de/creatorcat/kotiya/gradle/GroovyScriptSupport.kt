/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import groovy.lang.Closure

/**
 * Convenience method to call a [closure] with this object as [delegate][Closure.delegate],
 * thus *applying* the closure to this object.
 * Useful for Groovy script DSL block methods.
 *
 * @return the result of [closure.call()][Closure.call]
 */
fun <T, R> T.apply(closure: Closure<R>): R {
    closure.delegate = this
    return closure.call()
}
