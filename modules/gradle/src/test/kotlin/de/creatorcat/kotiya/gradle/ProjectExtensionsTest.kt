/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import de.creatorcat.kotiya.test.core.io.FileCreatingTestBase
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ProjectExtensionsTest : FileCreatingTestBase {

    // project is created lazy so the deletion of the testOutDir can happen before
    private val project by lazy {
        ProjectBuilder.builder()
            .withProjectDir(testOutFile("projectDir"))
            .build()
    }

    @Test fun outputFile_missingParent() {
        val path = "not-existing-dir/outfile"
        val file = testOutFile("projectDir/$path")
        assertFalse(file.parentFile.exists())
        val newFile = project.outputFile(path)
        assertTrue(newFile.parentFile.exists())
        assertFalse(newFile.exists())
        assertEquals(file.absoluteFile, newFile.absoluteFile)
    }

    @Test fun outputFile_existingParent() {
        val path = "existing-dir/outfile"
        val file = testOutFile("projectDir/$path")
        file.parentFile.mkdirs()
        assertTrue(file.parentFile.exists())
        val newFile = project.outputFile(path)
        assertTrue(newFile.parentFile.exists())
        assertFalse(newFile.exists())
        assertEquals(file.absoluteFile, newFile.absoluteFile)
    }
}
