/*
 * Copyright 2020 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.gradle.api.logging.Logger
import kotlin.test.Test

class LoggerExtensionsTest {

    private val testee = mock<Logger>()

    @Test fun `java style logs`() {
        testee.javaStyleError("msg", 23)
        verify(testee).error("e: {}: {}", "msg", 23)
        testee.javaStyleWarning("warning", 17)
        verify(testee).warn("w: {}: {}", "warning", 17)
    }
}
