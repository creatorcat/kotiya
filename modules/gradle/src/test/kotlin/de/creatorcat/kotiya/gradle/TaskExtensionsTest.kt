/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.kotiya.matchers.common.isNotBlank
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.api.Buildable
import org.gradle.api.Task
import org.gradle.api.tasks.TaskDependency
import org.gradle.api.tasks.TaskReference
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class TaskExtensionsTest {

    @Test fun `isDirectTaskDependency - all cases`() {
        for (obj in listOf(mock<Task>(), mock<TaskDependency>(), mock<TaskReference>(), mock<Buildable>())) {
            assertTrue(isDirectTaskDependency(obj))
        }
        for (obj in listOf(null, 1, "task", listOf(mock<Task>()))) {
            assertFalse(isDirectTaskDependency(obj))
        }
    }

    @Test fun `standard task group names`() {
        with(TaskGroup) {
            for (name in listOf(BUILD, VERIFICATION, HELP, DOCUMENTATION, PUBLISH)) {
                assertThat(name, isNotBlank)
            }
        }
    }
}
