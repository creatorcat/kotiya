/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.gradle

import de.creatorcat.kotiya.core.structures.Wrap
import de.creatorcat.kotiya.test.gradle.DummyClosure
import kotlin.test.Test
import kotlin.test.assertEquals

class GroovyScriptSupportTest {

    @Test fun `apply closure - usage`() {
        val closure = DummyClosure.of<Int, Wrap<Int>> {
            data++
        }

        val wrap = Wrap(0)
        val result = wrap.apply(closure)

        assertEquals(0, result)
        assertEquals(1, wrap.data)
    }
}
