# Module kotiya-test-core

This module provides general purpose test utilities.

Features:
* The key concept is the use of [kotiya-matchers](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers) via the
  [assertThat](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core/assert-that.html) or
  [T.assert](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core/assert.html) method
* Test exceptions using [assertThrows](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core/assert-throws.html)
* Stub and observe methods or properties in common code
  where [kotlin-mockito](https://github.com/nhaarman/mockito-kotlin/wiki) is not available
  via [the stubbing package](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core.stubbing)
* Validate that implementations of standard methods like `equals` or `compare` comply to the general contract
  via reflective function assertion methods like
  [assertEqualsContractFor](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core.reflect/assert-equals-contract-for.html)

State: *Beta*
* The collection of code is rather miscellaneous
* API might change in details

Dependencies:
* [kotiya-core](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core)
* Basic test assertion functionality is based on
  [kotlin.test](https://kotlinlang.org/api/latest/kotlin.test/index.html)
  and [JUnit5](https://junit.org/junit5/)
* Advanced test assertion functionality is mostly based on [kotiya-matchers](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers)

# Package de.creatorcat.kotiya.test.core

Contains basic test utilities for general usage.

# Package de.creatorcat.kotiya.test.core.reflect

Contains methods to test general properties of functions like symmetry or consistency.
These can be combined to the general contract of standard methods like `equals` or `compare`.

# Package de.creatorcat.kotiya.test.core.stubbing

Contains methods to stub implementations of test dummies like
[mustNotBeCalled](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core.stubbing/must-not-be-called.html)
and to observe function or property calls like
[withCallObserver](https://creatorcat.gitlab.io/kotiya/docs/test-core/html/kotiya-test-core/de.creatorcat.kotiya.test.core.stubbing/with-call-observer.html).

# Package de.creatorcat.kotiya.test.core.io

This package contains test utilities concerning files and IO.
