/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.io

import org.junit.jupiter.api.Test
import java.io.File
import kotlin.reflect.jvm.jvmName
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class FileCreatingTestBaseTest : FileCreatingTestBase {

    private class DummyTest1 : FileCreatingTestBase
    private class DummyTest2 : FileCreatingTestBase
    private class DummyTest3 : FileCreatingTestBase

    @Test fun outputDirName_containsClassName() {
        assertTrue(DummyTest1().outDirName.contains(DummyTest1::class.jvmName))
        assertNotEquals(DummyTest1().outDirName, DummyTest2().outDirName)
    }

    @Test fun testOutDir_wasCreatedAndIsValid() {
        val dir = testOutDir
        assertTrue(dir.exists())
        assertTrue(dir.isDirectory)
        assertTrue(dir.list().isEmpty())
        assertEquals(mainTestOutputDir.toPath(), dir.toPath().parent)
    }

    @Test fun testOutFile_hasRightPath() {
        val expected = testOutDir.toPath().resolve("dummy")
        val actual = testOutFile("dummy").toPath()
        assertEquals(expected, actual)
    }

    @Test fun setup_createsDir() {
        val test = DummyTest1()
        assertRecursiveDeletionIfExists(test.testOutDir)
        assertFalse(test.testOutDir.exists())
        test.setup()
        assertTrue(test.testOutDir.exists())
    }

    @Test fun setup_cleansUpDir() {
        val test = DummyTest2()
        test.testOutDir.mkdirs()
        File(test.testOutDir, "dummy").createNewFile()
        assertFalse(test.testOutDir.list().isEmpty())
        test.setup()
        assertTrue(test.testOutDir.list().isEmpty())
    }

    @Test fun setup_cleansUpDirOnlyOnce() {
        val test = DummyTest3()
        test.setup()
        val file = File(test.testOutDir, "dummy")
        assertFalse(file.exists())
        file.createNewFile()
        assertTrue(file.exists())
        test.setup()
        assertTrue(file.exists())
        test.setup()
        assertTrue(file.exists())
    }
}
