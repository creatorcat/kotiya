/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.io

import de.creatorcat.kotiya.test.core.assertThrows
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.text.RegexOption.DOT_MATCHES_ALL

class FileAssertionsTest {

    // we do not use FileCreatingTestBase here to avoid a circular dependency
    private val testOutDir = File(mainTestOutputDir, "FileAssertionsTest")

    @Test fun assertRecursiveDeletionIfExists_notExisting() {
        val notExistingFile = File(testOutDir, "NOT_EXISTING")
        assertFalse(notExistingFile.exists())
        assertRecursiveDeletionIfExists(notExistingFile)
    }

    @Test fun assertRecursiveDeletionIfExists_file() {
        val file = File(testOutDir, "dummy")
        file.parentFile.mkdirs()
        file.createNewFile()
        assertTrue(file.exists() && file.isFile)
        assertRecursiveDeletionIfExists(file)
        assertFalse(file.exists())
    }

    @Test fun assertRecursiveDeletionIfExists_directory() {
        val dir = File(testOutDir, "dummyDir")
        dir.mkdirs()
        val file = File(dir, "dummyFile")
        file.createNewFile()
        assertTrue(file.exists() && file.isFile)
        assertRecursiveDeletionIfExists(dir)
        assertFalse(file.exists())
        assertFalse(dir.exists())
    }

    @Test fun assertRecursiveDeletionIfExists_fails() {
        // TODO there seems to be no way to test this on linux :-(
        if (!System.getProperty("os.name").startsWith("win", ignoreCase = true)) return

        val file = File(testOutDir, "failingDummy")
        file.parentFile.mkdirs()
        file.createNewFile()
        file.bufferedWriter().use {
            // this will make file unerasable on Windows
            it.write("test")
            assertThrows<AssertionError> { assertRecursiveDeletionIfExists(file) }
        }
        // clean up after
        assertRecursiveDeletionIfExists(file)
    }

    private val testResDir = File("src/jvmTest/resources")
    private val testEqDir = File(testResDir, "fileEquality")
    private fun testEqFile(name: String) = File(testEqDir, name)

    @Test fun assertFileTextEquals_withDummyFiles() {
        assertFileTextEquals(testEqFile("dummy.txt"), testEqFile("copyOfDummy.txt"))
        assertThrows<AssertionError> {
            assertFileTextEquals(
                testEqFile("dummy.txt"),
                testEqFile("alteredDummy.txt")
            )
        }
    }

    @Test fun assertFileTextEquals_encodingOptions() {
        assertThrows<AssertionError> {
            assertFileTextEquals(
                testEqFile("dummy.txt"),
                testEqFile("dummyInOtherEncoding.txt")
            )
        }
        assertFileTextEquals(
            testEqFile("dummy.txt"),
            testEqFile("dummyInOtherEncoding.txt"),
            Charsets.UTF_8,
            Charsets.ISO_8859_1
        )
    }

    @Test fun assertFileTextEquals_withIgnorePattern() {
        // trivial case
        assertFileTextEquals(
            testEqFile("dummy.txt"), testEqFile("copyOfDummy.txt"),
            Regex("NOT_FOUND")
        )
        // with ignored difference
        assertFileTextEquals(
            testEqFile("dummy.txt"), testEqFile("alteredDummy.txt"),
            Regex("d.?|f.?")
        )
        // still fails with encoding difference
        assertThrows<AssertionError> {
            assertFileTextEquals(
                testEqFile("dummy.txt"), testEqFile("dummyInOtherEncoding.txt"),
                Regex("d")
            )
        }
        // does not fail if encoding problems are replaced 
        assertFileTextEquals(
            testEqFile("dummy.txt"), testEqFile("dummyInOtherEncoding.txt"),
            Regex("0.*$", DOT_MATCHES_ALL)
        )
    }
}
