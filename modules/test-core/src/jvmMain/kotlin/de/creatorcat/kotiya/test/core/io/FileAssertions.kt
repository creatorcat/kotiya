/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.io

import java.io.File
import java.nio.charset.Charset
import kotlin.test.assertEquals

/**
 * Parent directory of the output directories for tests that create files.
 * The path is `build/test-output` relative to the project directory.
 *
 * @see FileCreatingTestBase
 */
val mainTestOutputDir: File = File("build/test-output")

/**
 * If the file exists, delete it recursively and assert the deletion i.e. throw and assertion if something went wrong.
 *
 * @param file the file to delete
 */
fun assertRecursiveDeletionIfExists(file: File) {
    if (file.exists() && !file.deleteRecursively()) {
        throw AssertionError("Failed to recursively delete file: ${file.absoluteFile}")
    }
}

/**
 * Assert that the content of both files is equal when read as text.
 *
 * Note that this method reads both file contents into strings so it may
 * take too much time and memory for very large files.
 *
 * @param expected file with expected content
 * @param actual file with actual content
 * @param charsetExpected charset of `expected` file - defaults to [Charsets.UTF_8]
 * @param charsetActual charset of `actual` file - default to `charsetExpected`
 */
@JvmOverloads fun assertFileTextEquals(
    expected: File,
    actual: File,
    charsetExpected: Charset = Charsets.UTF_8,
    charsetActual: Charset = charsetExpected
) {
    val expectedText = expected.readText(charsetExpected)
    val actualText = actual.readText(charsetActual)
    assertEquals(expectedText, actualText)
}

/**
 * Assert that the content of both files is equal when read as text
 * and ignoring all parts that are matched by the [ignoreRegex].
 * In fact, all matched parts will be replaced by an `<IGNORED>` token
 * before the texts are compared to each other.
 *
 * This functionality is very handy when comparing files that contain some ever changing generated parts
 * like time stamps or unique IDs.
 *
 * Note that this method reads both file contents into strings so it may
 * take too much time and memory for very large files.
 *
 * @param expected file with expected content
 * @param actual file with actual content
 * @param ignoreRegex parts of the file that match this regex will be ignored when checking equality
 * @param charsetExpected charset of `expected` file - defaults to [Charsets.UTF_8]
 * @param charsetActual charset of `actual` file - default to `charsetExpected`
 */
@JvmOverloads fun assertFileTextEquals(
    expected: File,
    actual: File,
    ignoreRegex: Regex,
    charsetExpected: Charset = Charsets.UTF_8,
    charsetActual: Charset = charsetExpected
) {
    val expectedText = expected
        .readText(charsetExpected)
        .replace(ignoreRegex, "<IGNORED>")
    val actualText = actual
        .readText(charsetActual)
        .replace(ignoreRegex, "<IGNORED>")
    assertEquals(expectedText, actualText)
}
