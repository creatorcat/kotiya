/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.io

import de.creatorcat.kotiya.matchers.common.exists
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import org.junit.jupiter.api.BeforeEach
import java.io.File
import java.util.concurrent.ConcurrentSkipListSet
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

/**
 * Interface for tests that create files and should do this in a well defined temporary location.
 *
 * Before any test is run (see [BeforeEach]),
 * the [setup] method prepares a clean output folder for the implementing test class.
 * The folder will not be removed before the next test run is started, so the results may be perused.
 *
 * The actual test methods should use on of the provided methods to create and access files in the folder.
 */
interface FileCreatingTestBase {

    /**
     * Output directory for files generated in the tests.
     * The path is `$[mainTestOutputDir]/$[outDirName]` relative to the project directory.
     */
    val testOutDir: File get() = File(mainTestOutputDir, outDirName)

    /**
     * Name of the output directory for this test instance.
     * The name is the actual [JVM class name][KClass.jvmName] to guarantee unique folders for each class.
     */
    val outDirName: String get() = this::class.jvmName

    /**
     * Generate a file of given name inside the [testOutDir].
     *
     * @param name the file name
     * @return the file
     */
    fun testOutFile(name: String): File = File(testOutDir, name)

    /**
     * Prepare the [testOutDir] on first call for the actual test class.
     */
    @BeforeEach fun setup() {
        if (classesWithTestDir.add(this::class)) {
            assertRecursiveDeletionIfExists(testOutDir)
            assertThat(testOutDir, !exists)
            testOutDir.mkdirs()
        }
        assertThat(testOutDir, exists)
    }

    private companion object {
        private val classesWithTestDir = ConcurrentSkipListSet<KClass<*>> { o1, o2 ->
            o1.jvmName.compareTo(o2.jvmName)
        }
    }
}
