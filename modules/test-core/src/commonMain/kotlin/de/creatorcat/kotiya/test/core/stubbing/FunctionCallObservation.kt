/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.byDelegationTo
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEqual
import de.creatorcat.kotiya.matchers.describedWith
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.of
import kotlin.reflect.KCallable
import kotlin.test.assertEquals

private val functionCallCount: MutableMap<KCallable<*>, Int> = HashMap()
private val functionCallArgStore: MutableMap<KCallable<*>, MutableList<List<Any?>>> = HashMap()

/**
 * Wraps a function, method or property getter (the [callable]) with a call observer.
 * Each time the callable is called, a [counter][countedCalls] is incremented,
 * the [arguments] of the call are stored and the [body] is executed to produce the result.
 *
 * **Attention**: storing observation data does not work when creating an object with call counters
 * inside some function. See the examples below.
 * In such cases, the [*tag*-based version][withCallCounterForTag] of this function should be used.
 *
 * Example:
 *
 *     // the function may be a member method of a class ...
 *     class DummyClass : MyInterface {
 *         fun foo(): Int = withCallObserver(this::foo) { 17 }
 *     }
 *
 *     // ... or of an object
 *     val dummyObject = object : MyInterface {
 *         fun foo(): Int = withCallObserver(this::foo) { 23 }
 *     }
 *
 *     // but not an object created inside a function
 *     fun dummy(): MyInterface = object : MyInterface {
 *         fun foo(): Int = withCallObserver(this::foo) { 5 }
 *     }
 *
 *     @Test fun verify() {
 *         val dc = DummyClass()
 *         dc.foo()
 *         assertEquals(1, dc::foo.countedCalls)
 *
 *         dummyObject.foo()
 *         assertEquals(1, dummyObject::foo.countedCalls)
 *
 *         val df = dummy()
 *         df.foo()
 *         // this will fail !!!
 *         assertEquals(1, dummyObject::foo.countedCalls)
 *     }
 */
fun <R> withCallObserver(callable: KCallable<R>, vararg arguments: Any?, body: () -> R): R {
    val numOfCalls = functionCallCount[callable]
    functionCallCount[callable] = numOfCalls?.inc() ?: 1
    val callArgList = functionCallArgStore.getOrPut(callable, ::mutableListOf)
    callArgList.add(arguments.toList())
    return body()
}

/**
 * Get the number of calls that where counted for this callable if it is wrapped by [withCallObserver]
 * or zero if it is not wrapped.
 *
 * Note: The counter (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce a zero.
 */
val KCallable<*>.countedCalls: Int
    get() {
        val result = functionCallCount[this] ?: 0
        resetObservationData()
        return result
    }

/**
 * Get the list of observed argument tuples for this callable if it is wrapped by [withCallObserver],
 * or an empty list if it is not wrapped.
 *
 * Note: The argument list (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce an empty list.
 */
val KCallable<*>.observedArguments: List<List<Any?>>
    get() {
        val result = functionCallArgStore[this] ?: emptyList<List<Any?>>()
        resetObservationData()
        return result
    }

/**
 * Reset all data collected during the observation of this callable:
 * * [countedCalls] `= 0`
 * * [observedArguments] `= emptyList`
 *
 * @see withCallObserver
 */
fun KCallable<*>.resetObservationData() {
    functionCallCount.remove(this)
    functionCallArgStore.remove(this)
}

/**
 * Creates a matcher that checks if the [call count][countedCalls] of a callable is
 * equal to [expectedCount].
 *
 * Note: The counter (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce a zero.
 */
fun hasCallCount(expectedCount: Int): AtomicMatcher<KCallable<*>> =
    Matcher.of(descriptionOfAtomicCondition("have $expectedCount counted calls")) {
        val actual = it.countedCalls
        Pair(actual == expectedCount, "has $actual counted calls")
    }

/**
 * Creates a matcher that checks if the [observedArguments] of a callable are
 * equal to the [expectedArgumentTuples].
 *
 * Note: The argument list (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce an empty list.
 */
fun wasCalledWith(expectedArgumentTuples: List<List<Any?>>): AtomicMatcher<KCallable<*>> =
    (Matcher.byDelegationTo<KCallable<*>, List<List<Any?>>>(hasElementsEqual(expectedArgumentTuples)) {
        it.observedArguments
    }).describedWith({
        descriptionOfCombinedCondition("have been called with the following arguments:\n$expectedArgumentTuples")
            .withEnding()
    }) { _, result ->
        { "was called with an argument list that ${result.actualState}" }
    }

/**
 * Creates a matcher that checks if the [observedArguments] of a callable with only a single argument are
 * equal to the [expectedArguments].
 *
 * Note: The argument list (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce an empty list.
 */
fun wasCalledWith(vararg expectedArguments: Any?): AtomicMatcher<KCallable<*>> =
    wasCalledWith(expectedArguments.map { listOf(it) })

/**
 * Creates a matcher that checks if the last tuple of [observedArguments] of a callable
 * are equal to the [expectedArgumentsOfLastCall].
 *
 * Note: The argument list (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce an empty list.
 */
fun wasLastCalledWith(expectedArgumentsOfLastCall: List<Any?>): AtomicMatcher<KCallable<*>> =
    (Matcher.byDelegationTo<KCallable<*>, List<Any?>>(isEqual(expectedArgumentsOfLastCall)) {
        it.observedArguments.last()
    }).describedWith({
        descriptionOfCombinedCondition(
            "have been called with the following arguments on its last call:\n$expectedArgumentsOfLastCall"
        ).withEnding()
    }) { _, result ->
        { "has a last call argument list that ${result.actualState}" }
    }

/**
 * Creates a matcher that checks if the last tuple of [observedArguments] of a callable
 * are equal to the [expectedArgumentsOfLastCall].
 *
 * Note: The argument list (and any other observed data) will be [reset][resetObservationData]
 * each time this value is requested, so directly subsequent calls will produce an empty list.
 */
fun wasLastCalledWith(vararg expectedArgumentsOfLastCall: Any?): AtomicMatcher<KCallable<*>> =
    wasLastCalledWith(expectedArgumentsOfLastCall.toList())

/**
 * Assert that this callable is lazily evaluated by the given [block] of code,
 * meaning its [call count][countedCalls] is 0 before executing the block
 * and 1 after executing the block multiple times (at least 2).
 */
fun KCallable<*>.assertIsLazilyCalledBy(block: () -> Unit) {
    assertEquals(0, this.countedCalls)
    repeat(3) { block() }
    assertEquals(1, this.countedCalls)
}
