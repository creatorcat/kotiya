/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

/**
 * Stub implementation helper throwing an [UnsupportedOperationException] on each call.
 *
 * Use this to stub methods and properties not unnecessary for the specific test implementation.
 *
 * Example:
 *
 *     class Dummy : MyInterface {
 *         fun foo() = 17
 *         fun bar() = mustNotBeCalled()
 *     }
 */
fun mustNotBeCalled(): Nothing = throw UnsupportedOperationException("method must not be called on this test object")
