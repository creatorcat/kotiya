/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

import de.creatorcat.kotiya.core.reflect.className
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.describeExpectations
import de.creatorcat.kotiya.matchers.describeWhy
import de.creatorcat.kotiya.matchers.isMismatch

/**
 * Assert that the [value] satisfies the given [matcher].
 *
 * @throws AssertionError if the [evaluation][Matcher.evaluate] results in a
 *   [mismatch][de.creatorcat.kotiya.matchers.MatcherEvaluationNode.isMismatch]
 */
fun <T> assertThat(value: T, matcher: Matcher<T>): Unit = assertThat("the ${value.className}", value, matcher)

/**
 * Assert that the [value] satisfies the given [matcher].
 *
 * @param valueTitle title used for the value when generating a failure message
 * @throws AssertionError if the [evaluation][Matcher.evaluate] results in a
 *   [mismatch][de.creatorcat.kotiya.matchers.MatcherEvaluationNode.isMismatch]
 */
fun <T> assertThat(valueTitle: String, value: T, matcher: Matcher<T>) {
    val result = matcher.evaluate(value)
    if (result.isMismatch) throw AssertionError(
        matcher.describeExpectations(
            valueTitle,
            "is expected to"
        ) + "\nInstead, ${result.describeWhy("it")}.\n" +
            "Evaluated value:\n$value"
    )
}

/**
 * Assert that this object satisfies the given [matcher].
 *
 * @throws AssertionError if the [evaluation][Matcher.evaluate] results in a
 *   [mismatch][de.creatorcat.kotiya.matchers.MatcherEvaluationNode.isMismatch]
 */
fun <T> T.assert(matcher: Matcher<T>): Unit = assertThat(this, matcher)
