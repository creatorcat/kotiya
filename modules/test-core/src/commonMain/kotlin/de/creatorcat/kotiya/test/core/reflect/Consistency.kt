/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.core.math.orderedPairs
import de.creatorcat.kotiya.core.predicates.implies
import de.creatorcat.kotiya.core.structures.allPairs
import de.creatorcat.kotiya.core.structures.pairsWithIndices
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.matchers.describedWith
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.reflect.KFunction

/**
 * Asserts that this function is *consistent* for all given [testObjects].
 *
 * A function is consistent for an argument if multiple consecutive calls with the unchanged object
 * produce an equal result.
 *
 * Uses the [isConsistentArgumentIn] matcher.
 */
fun <T, F> F.assertArgumentConsistencyFor(testObjects: List<T>)
    where F : (T) -> Any?,
          F : KFunction<Any?> {

    require("number of test objects", testObjects.size, isGreaterEqThan(1))
    assertThat("the list of test objects", testObjects, each(isConsistentArgumentIn(this, this.name)))
}

/**
 * Creates a matcher that checks if an object of type [T] is a consistent argument
 * in the domain of the given [function].
 *
 * A function is consistent for an argument if multiple consecutive calls with the unchanged object
 * produce an equal result.
 *
 * @param functionName name of the function used in matcher and evaluation result description tests;
 *   defaults to `"unnamed function"`
 * @see assertArgumentConsistencyFor
 */
fun <T> isConsistentArgumentIn(function: (T) -> Any?, functionName: String = "unnamed function"): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("consistent argument in $functionName")) { value ->
        val firstResult = function(value)
        for (numOfCalls in 2..5) {
            val currentResult = function(value)
            if (firstResult != currentResult)
                return@of Pair(
                    false, "is not consistent in $functionName:" +
                        " 1. result = $firstResult while $numOfCalls. result = $currentResult"
                )
        }
        Pair(true, "is consistent in $functionName")
    }

/**
 * Asserts that this function is *consistent* for [all pairs][allPairs] of given [testObjects].
 *
 * A function is consistent for an argument pair if multiple consecutive calls with the unchanged objects
 * produce an equal result.
 *
 * Uses the [isConsistentPairIn] matcher.
 */
fun <T, F> F.assertArgumentPairConsistencyFor(testObjects: List<T>)
    where F : (T, T) -> Any?,
          F : KFunction<Any?> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of all pairs of test objects",
        testObjects.allPairs(),
        each(isConsistentPairIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a pair of objects of type [T] is a consistent argument
 * in the domain of the given [function].
 *
 * A function is consistent for an argument pair if multiple consecutive calls with the unchanged objects
 * produce an equal result.
 *
 * @param functionName name of the function used in matcher and evaluation result description tests;
 *   defaults to `"unnamed function"`
 * @see assertArgumentPairConsistencyFor
 */
fun <T> isConsistentPairIn(
    function: (T, T) -> Any?, functionName: String = "unnamed function"
): AtomicMatcher<Pair<T, T>> =
    isConsistentArgumentIn<Pair<T, T>>({ (x, y) -> function(x, y) }, functionName)
        .describedWith(descriptionOfBeing("consistent argument pair in $functionName"))

/**
 * Asserts that this function is *consistent* with equals for all [ordered pairs][orderedPairs]
 * of the given [testObjects].
 * It sufficient to test the ordered pairs since equals must be symmetric.
 *
 * A function is consistent with equals for an argument pair if argument equality implies result equality:
 * `x == y implies f(x) == f(y)`.
 *
 * Uses the [isConsistentWithEqualsIn] matcher.
 */
fun <T, F> F.assertConsistencyWithEqualsFor(testObjects: List<T>)
    where F : (T) -> Any?,
          F : KFunction<Any?> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of all ordered pairs of test objects",
        testObjects.pairsWithIndices(testObjects.indices.orderedPairs()),
        each(isConsistentWithEqualsIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a pair of objects of type [T] has arguments
 * in the domain of the given [function] that are consistent with equals.
 *
 * A function is consistent with equals for an argument pair if argument equality implies result equality:
 * `x == y implies f(x) == f(y)`.
 *
 * @param functionName name of the function used in matcher and evaluation result description tests;
 *   defaults to `"unnamed function"`
 * @see assertArgumentPairConsistencyFor
 */
fun <T> isConsistentWithEqualsIn(
    function: (T) -> Any?, functionName: String = "unnamed function"
): AtomicMatcher<Pair<T, T>> =
    Matcher.of(descriptionOfBeing("argument pair consistent with equals in $functionName")) { (x, y) ->
        val consistent = (x == y) implies (function(x) == function(y))
        Pair(consistent, "is ${"not ".orEmptyIf(consistent)}consistent with equals in $functionName")
    }
