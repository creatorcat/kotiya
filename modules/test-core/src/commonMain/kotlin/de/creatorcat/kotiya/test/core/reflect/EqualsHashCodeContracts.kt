/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import kotlin.reflect.KFunction

/**
 * Asserts that this function satisfies all conditions of the *equals-contract* for the given [testObjects]:
 * * [reflexivity][assertReflexivityFor]
 * * [symmetry][assertSymmetryFor]
 * * [transitivity][assertTransitivityFor]
 * * [consistency][assertArgumentPairConsistencyFor]
 * * [no equivalence to null][assertNoRelationToNullFor]
 */
fun <T, F> F.assertEqualsContractFor(testObjects: List<T>)
    where F : (T, T?) -> Boolean,
          F : KFunction<Boolean> {

    assertReflexivityFor(testObjects)
    assertSymmetryFor(testObjects)
    assertTransitivityFor(testObjects)
    assertArgumentPairConsistencyFor(testObjects)
    assertNoRelationToNullFor(testObjects)
}

/**
 * Asserts that this function satisfies all conditions of the *hashCode-contract* for the given [testObjects]:
 * * [consistency][assertArgumentConsistencyFor]
 * * [consistency with equals][assertConsistencyWithEqualsFor]
 */
fun <T : Any, F> F.assertHashCodeContractFor(testObjects: List<T>)
    where F : (T) -> Int,
          F : KFunction<Int> {

    assertArgumentConsistencyFor(testObjects)
    assertConsistencyWithEqualsFor(testObjects)
}
