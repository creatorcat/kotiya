/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.core.predicates.implies
import de.creatorcat.kotiya.core.structures.allTriples
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.math.sign
import kotlin.reflect.KFunction

/**
 * Asserts that this function satisfies all conditions of the *compareTo-contract* for the given [testObjects]:
 * * [symmetry][assertSymmetryFor] of `f(x,y) == 0`
 * * [transitivity][assertTransitivityFor] of `f(x,y) > 0`
 * * [asymmetry][assertAsymmetryFor] of `f(x,y) > 0`
 * * [compareTo-equivalence-property][assertCompareToEquivalenceProperty]
 *
 * Note that [consistency with equals][assertCompareToConsistencyWithEquals] is not part of the
 * default contract and must be checked additionally if desired.
 */
fun <T : Comparable<T>, F> F.assertCompareToContractFor(testObjects: List<T>)
    where F : (T, T) -> Int,
          F : KFunction<Int> {

    assertCompareToEquivalenceProperty(testObjects)
    run {
        fun compareTo(x: T, y: T): Boolean = this(x, y) > 0
        ::compareTo.assertTransitivityFor(testObjects)
        ::compareTo.assertAsymmetryFor(testObjects)
    }
    run {
        fun compareTo(x: T, y: T): Boolean = this(x, y) == 0
        ::compareTo.assertSymmetryFor(testObjects)
    }
}

/**
 * Asserts that this function satisfies the *compareTo-equivalence-contract*
 * for [all triples][allTriples] of given [testObjects].
 * This means any triple `(x,y,z)` of objects of type [T] represents
 * arguments in the domain of this function that satisfy
 * `f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z))` where `sgn` is the usual sign function.
 *
 * Uses the [satisfiesCompareToEquivalenceProperty] matcher.
 */
fun <T : Comparable<T>, F> F.assertCompareToEquivalenceProperty(testObjects: List<T>)
    where F : (T, T) -> Int,
          F : KFunction<Int> {

    require("number of test objects", testObjects.size, isGreaterEqThan(3))
    assertThat(
        "the set of all triples of test objects",
        testObjects.allTriples(),
        each(satisfiesCompareToEquivalenceProperty(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a triple `(x,y,z)` of objects of type [T] represents
 * arguments in the domain of the given [function] that satisfy the following contract:
 * `f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z))` where `sgn` is the usual sign function.
 *
 * @param functionName name of the function used in matcher and evaluation result description tests;
 *   defaults to `"unnamed function"`
 * @see assertCompareToEquivalenceProperty
 */
fun <T> satisfiesCompareToEquivalenceProperty(
    function: (T, T) -> Int, functionName: String = "unnamed function"
): AtomicMatcher<Triple<T, T, T>> =
    Matcher.of(
        descriptionOfAtomicCondition(
            "satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being $functionName"
        )
    ) { (x, y, z) ->
        val satisfied = (function(x, y) == 0) implies (function(x, z).sign == function(y, z).sign)
        Pair(
            satisfied,
            "does ${"not ".orEmptyIf(satisfied)}satisfy" +
                " f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being $functionName"
        )
    }

/**
 * Asserts that this comparison function is consistent with equals for all given [testObjects].
 *
 * The function is consistent with equals if the relation `f(x,y) == 0` is
 * [equivalent to equals][assertEquivalenceToEqualsFor]:
 * `(f(x,y) == 0) == (x == y)`.
 */
fun <T : Comparable<T>, F> F.assertCompareToConsistencyWithEquals(testObjects: List<T>)
    where F : (T, T) -> Int,
          F : KFunction<Int> {

    fun compareToEquals0(x: T, y: T): Boolean = this(x, y) == 0
    ::compareToEquals0.assertEquivalenceToEqualsFor(testObjects)
}
