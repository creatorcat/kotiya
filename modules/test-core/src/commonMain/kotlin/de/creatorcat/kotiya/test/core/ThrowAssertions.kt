/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

/**
 * Assert that a [block] of code throws an expected exception of type [E].
 *
 * @return the expected and caught exception for further usage
 */
inline fun <reified E : Throwable> assertThrows(block: () -> Unit): E {
    try {
        block()
    } catch (e: Throwable) {
        return e as? E
            ?: throw AssertionError(
                "Unexpected exception: $e of type ${e::class} - expected type ${E::class}"
            )
    }
    // if it gets here, no exception was thrown at all
    throw AssertionError("Missing exception: ${E::class}")
}
