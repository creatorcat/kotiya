/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.core.math.orderedDistinctPairs
import de.creatorcat.kotiya.core.structures.pairsWithIndices
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.reflect.KFunction

/**
 * Asserts that this function is commutative for all [ordered distinct pairs][orderedDistinctPairs]
 * of given [testObjects].
 *
 * A function is commutative for an argument pair if the order of both arguments is irrelevant:
 * `f(x,y) == f(y,x)`.
 *
 * Uses the [isCommutativeIn] matcher.
 */
fun <T, F> F.assertCommutativityFor(testObjects: List<T>)
    where F : (T, T) -> Any?,
          F : KFunction<Any?> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of distinct ordered test object pairs",
        testObjects.pairsWithIndices(testObjects.indices.orderedDistinctPairs()),
        each(isCommutativeIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a pair of objects of type [T] is a commutative argument
 * in the domain of the given [function].
 *
 * A function is commutative for an argument pair if the order of both arguments is irrelevant:
 * `f(x,y) == f(y,x)`.
 *
 * @param functionName name of the function used in matcher and evaluation result description tests;
 *   defaults to `"unnamed function"`
 * @see assertCommutativityFor
 */
fun <T> isCommutativeIn(
    function: (T, T) -> Any?, functionName: String = "unnamed function"
): AtomicMatcher<Pair<T, T>> =
    Matcher.of(descriptionOfBeing("commutative pair in $functionName")) { (x, y) ->
        val commutative = function(x, y) == function(y, x)
        Pair(commutative, "is ${"not ".orEmptyIf(commutative)}commutative in $functionName")
    }


