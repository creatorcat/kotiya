/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.introspection.hasContents

/**
 * Combination of [assertSuccess] and [assertFailure].
 */
fun <T> AtomicMatcher<T>.assertSuccessAndFailure(
    matchingValue: T, matchingActualState: String,
    mismatchingValue: T, mismatchingActualState: String
) {
    assertSuccess(matchingValue, matchingActualState)
    assertFailure(mismatchingValue, mismatchingActualState)
}

/**
 * Assert that this matcher successfully matches the [matchingValue] and
 * produces the [matchingActualState] message.
 */
fun <T> AtomicMatcher<T>.assertSuccess(matchingValue: T, matchingActualState: String) {
    evaluate(matchingValue).assert(hasContents(true, this, matchingActualState))
}

/**
 * Assert that this matcher successfully matches the [matchingValue] and
 * produces the [matchingActualState] message.
 */
fun <T, R : Any> ValueExtractingAtomicMatcher<T, R>.assertSuccess(
    matchingValue: T, matchingActualState: String, extractedValue: R
) {
    evaluate(matchingValue).assert(hasContents(true, this, matchingActualState, extractedValue))
}

/**
 * Assert that this matcher fails to match the [mismatchingValue] and
 * produces the [mismatchingActualState] message.
 */
fun <T> AtomicMatcher<T>.assertFailure(mismatchingValue: T, mismatchingActualState: String) {
    evaluate(mismatchingValue).assert(hasContents(false, this, mismatchingActualState))
}

/**
 * Assert that this matcher fails to match the [mismatchingValue] and
 * produces the [mismatchingActualState] message.
 */
fun <T, R : Any> ValueExtractingAtomicMatcher<T, R>.assertFailure(
    mismatchingValue: T, mismatchingActualState: String
) {
    evaluate(mismatchingValue).assert(hasContents<R>(false, this, mismatchingActualState, null))
}
