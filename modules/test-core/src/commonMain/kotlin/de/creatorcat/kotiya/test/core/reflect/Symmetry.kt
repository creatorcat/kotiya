/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.core.math.orderedDistinctPairs
import de.creatorcat.kotiya.core.predicates.implies
import de.creatorcat.kotiya.core.structures.allPairs
import de.creatorcat.kotiya.core.structures.pairsWithIndices
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.matchers.describedWith
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.reflect.KFunction

/**
 * Asserts that this relation is symmetric for all [ordered distinct pairs][orderedDistinctPairs]
 * of given [testObjects].
 *
 * A relation is symmetric for an argument pair if the order of both arguments is irrelevant:
 * `f(x,y) == f(y,x)`.
 *
 * Uses the [isSymmetricIn] matcher.
 */
fun <T, F> F.assertSymmetryFor(testObjects: List<T>)
    where F : (T, T) -> Any?,
          F : KFunction<Any?> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of distinct ordered test object pairs",
        testObjects.pairsWithIndices(testObjects.indices.orderedDistinctPairs()),
        each(isSymmetricIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a pair of objects of type [T] is a symmetric argument
 * in the given [relation].
 *
 * A relation is symmetric for an argument pair if the order of both arguments is irrelevant:
 * `f(x,y) == f(y,x)`.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertSymmetryFor
 */
fun <T> isSymmetricIn(
    relation: (T, T) -> Any?, relationName: String = "unnamed relation"
): AtomicMatcher<Pair<T, T>> =
    isCommutativeIn(relation, relationName).describedWith(
        descriptionOfBeing("symmetric pair in $relationName")
    ) { _, node ->
        node.actualState.replace("commutative", "symmetric")
    }

/**
 * Asserts that this relation is asymmetric for all [allPairs]
 * of given [testObjects].
 *
 * A relation is asymmetric for an argument pair if:
 * `relation(x,y) implies !relation(y,x)`.
 *
 * Uses the [isAsymmetricIn] matcher.
 */
fun <T, F> F.assertAsymmetryFor(testObjects: List<T>)
    where F : (T, T) -> Boolean,
          F : KFunction<Boolean> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of all test object pairs",
        testObjects.allPairs(),
        each(isAsymmetricIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if a pair of objects of type [T] is an asymmetric element
 * in the given [relation].
 *
 * A relation is asymmetric for an argument pair if:
 * `relation(x,y) implies !relation(y,x)`.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertAsymmetryFor
 */
fun <T> isAsymmetricIn(
    relation: (T, T) -> Boolean, relationName: String = "unnamed relation"
): AtomicMatcher<Pair<T, T>> =
    Matcher.of(descriptionOfBeing("asymmetric pair in $relationName")) { (x, y) ->
        val asymmetric = relation(x, y) implies !relation(y, x)
        Pair(asymmetric, "is ${"not ".orEmptyIf(asymmetric)}asymmetric in $relationName")
    }
