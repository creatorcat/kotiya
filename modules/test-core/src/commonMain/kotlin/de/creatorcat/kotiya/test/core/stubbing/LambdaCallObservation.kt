/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of
import kotlin.test.assertEquals

private val globalTaggedCallCount: MutableMap<Any, MutableMap<Any, Int>> = HashMap()
private val Any.taggedCallCount: MutableMap<Any, Int>
    get() = globalTaggedCallCount.getOrPut(this) { HashMap() }

/**
 * Wraps a lambda (the [body]) with a call counter.
 * Each time the lambda is called, the counter identified with the given [tag] object is incremented and
 * the `body` is executed to produce the result.
 * The number of calls may then be retrieved via [countedCallsForTag].
 *
 * The count for the given [tag] is also bound to the *receiver* of this method (usually the test class),
 * to avoid tag clashes with other tests.
 *
 * For functions, methods and property getters the *callable*-based version of this function should be used.
 *
 * Example:
 *
 *     class CodeRunner<R>(val code: () -> R) {
 *         fun run(): R = code()
 *     }
 *
 *     @Test fun verify() {
 *         val testee = CodeRunner { withCallCounterForTag("myTag") { 17 } }
 *         testee.run()
 *
 *         assertEquals(1, countedCallsForTag("myTag"))
 *     }
 */
fun <R> Any.withCallCounterForTag(tag: Any, body: () -> R): R {
    val numOfCalls = taggedCallCount[tag]
    taggedCallCount[tag] = numOfCalls?.inc() ?: 1
    return body()
}

/**
 * Get the number of calls that where counted for this [tag] if it corresponds to a lambda
 * wrapped by [withCallCounterForTag] or zero if it is not wrapped.
 *
 * The count for the given [tag] is also bound to the *receiver* of this method (usually the test class),
 * to avoid tag clashes with other tests.
 *
 * Note: The counter will be reset each time this value is requested,
 * so directly subsequent calls will produce a zero.
 */
fun Any.countedCallsForTag(tag: Any): Int = taggedCallCount.remove(tag) ?: 0

/**
 * Reset the number of calls that where counted for this [tag] to 0.
 *
 * @see withCallCounterForTag
 * @see countedCallsForTag
 */
fun Any.resetCountedCallsForTag(tag: Any) {
    countedCallsForTag(tag)
}

/**
 * Creates a matcher that checks if the [call count for a tag][countedCallsForTag] is
 * equal to [expectedCount].
 *
 * The matcher is also bound to the *receiver* of this method (usually the test class),
 * to avoid tag clashes with other tests.
 *
 * Note: The counter will be reset each time the count is requested, so also if the matcher is evaluated.
 */
fun Any.hasCallCountAsTag(expectedCount: Int): AtomicMatcher<Any> =
    Matcher.of(descriptionOfAtomicCondition("have $expectedCount counted calls as tag")) {
        val actual = countedCallsForTag(it)
        Pair(actual == expectedCount, "has $actual counted calls as tag")
    }

/**
 * Assert that a callable associated to the given [tag] is lazily evaluated by the given [block] of code,
 * meaning the tags [call count][countedCallsForTag] is 0 before executing the block
 * and 1 after executing the block multiple times (at least 2).
 */
fun Any.assertTagIsLazilyCalledBy(tag: Any, block: () -> Unit) {
    assertEquals(0, countedCallsForTag(tag))
    repeat(3) { block() }
    assertEquals(1, countedCallsForTag(tag))
}
