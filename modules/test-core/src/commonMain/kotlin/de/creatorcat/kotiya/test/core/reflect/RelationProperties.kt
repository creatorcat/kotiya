/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.core.math.orderedPairs
import de.creatorcat.kotiya.core.predicates.implies
import de.creatorcat.kotiya.core.structures.allTriples
import de.creatorcat.kotiya.core.structures.pairsWithIndices
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.descriptionOfNotBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.each
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.reflect.KFunction

/**
 * Asserts that this relation is reflexive for all given [testObjects].
 *
 * A relation is reflexive for an argument if the argument is related to itself:
 * `relation(x,x) == true`.
 *
 * Uses the [isReflexiveIn] matcher.
 */
fun <T, F> F.assertReflexivityFor(testObjects: List<T>)
    where F : (T, T) -> Boolean,
          F : KFunction<Boolean> {

    require("number of test objects", testObjects.size, isGreaterEqThan(1))
    assertThat(
        "the list of test objects",
        testObjects,
        each(isReflexiveIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if an object of type [T] is a reflexive
 * element in the given [relation].
 *
 * A relation is reflexive for an argument if the argument is related to itself:
 * `relation(x,x) == true`.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertReflexivityFor
 */
fun <T> isReflexiveIn(relation: (T, T) -> Boolean, relationName: String = "unnamed relation"): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("reflexive element in $relationName")) {
        val reflexive = relation(it, it)
        Pair(reflexive, "is ${"not ".orEmptyIf(reflexive)}reflexive in $relationName")
    }

/**
 * Asserts that this relation is transitive for [all triples][allTriples] of the given [testObjects].
 *
 * A relation is transitive for an argument triple if:
 * `relation(x,y) && relation(y,z) implies relation(x,z)`.
 *
 * Uses the [isTransitiveIn] matcher.
 */
fun <T, F> F.assertTransitivityFor(testObjects: List<T>)
    where F : (T, T) -> Boolean,
          F : KFunction<Boolean> {

    require("number of test objects", testObjects.size, isGreaterEqThan(3))
    assertThat(
        "the set of all test object triples",
        testObjects.allTriples(),
        each(isTransitiveIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if an object triple of type [T] represents transitive elements
 * in the given [relation].
 *
 * A relation is transitive for an argument triple if:
 * `relation(x,y) && relation(y,z) implies relation(x,z)`.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertTransitivityFor
 */
fun <T> isTransitiveIn(
    relation: (T, T) -> Boolean, relationName: String = "unnamed relation"
): AtomicMatcher<Triple<T, T, T>> =
    Matcher.of(descriptionOfBeing("transitive triple in $relationName")) { (o1, o2, o3) ->
        val transitive = (relation(o1, o2) && relation(o2, o3)) implies relation(o1, o3)
        Pair(transitive, "is ${"not ".orEmptyIf(transitive)}transitive in $relationName")
    }

/**
 * Asserts that this relation is equivalent to equals for all [ordered pairs][orderedPairs]
 * of the given [testObjects].
 *
 * A relation is equivalent to equals for an argument pair if
 * the pair is element in the relation only if both of its values are equal:
 * `(x == y) == relation(x,y)`.
 *
 * Uses the [isInRelationOnlyIfEqual] matcher.
 */
fun <T, F> F.assertEquivalenceToEqualsFor(testObjects: List<T>)
    where F : (T, T) -> Boolean,
          F : KFunction<Boolean> {

    require("number of test objects", testObjects.size, isGreaterEqThan(2))
    assertThat(
        "the set of all ordered test object pairs",
        testObjects.pairsWithIndices(testObjects.indices.orderedPairs()),
        each(isInRelationOnlyIfEqual(this, this.name))
    )
}

/**
 * Creates a matcher that checks if an object pair of type [T] is element in the given [relation]
 * only if both of its values are equal: `(x == y) == relation(x,y)`.
 * This also implies symmetry of the relation for this pair.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertEquivalenceToEqualsFor
 */
fun <T> isInRelationOnlyIfEqual(
    relation: (T, T) -> Boolean, relationName: String = "unnamed relation"
): AtomicMatcher<Pair<T, T>> =
    Matcher.of(descriptionOfBeing("in $relationName if and only if both values are equal")) { (x, y) ->
        // remember: symmetry is implied
        val leftResult = relation(x, y)
        val rightResult = relation(y, x)
        if (leftResult != rightResult)
            return@of Pair(false, "is not symmetric in $relationName")
        val equal = x == y
        if (equal && !leftResult)
            Pair(false, "has equal values but is not in $relationName")
        else if (!equal && leftResult)
            Pair(false, "has unequal values but is in $relationName")
        else if (equal)
            Pair(true, "has equal values and is in $relationName")
        else
            Pair(true, "has unequal values and is not in $relationName")
    }

/**
 * Asserts that this relation has no relation to `null` for all given [testObjects].
 *
 * An object is related to `null` if: `relation(x, null) == true`.
 *
 * Uses the [isNotRelatedToNullIn] matcher.
 */
fun <T, F> F.assertNoRelationToNullFor(testObjects: List<T>)
    where F : (T, T?) -> Boolean,
          F : KFunction<Boolean> {

    require("number of test objects", testObjects.size, isGreaterEqThan(1))
    assertThat(
        "the list of test objects",
        testObjects,
        each(isNotRelatedToNullIn(this, this.name))
    )
}

/**
 * Creates a matcher that checks if an object of type [T] is related to null in the given [relation].
 *
 * An object is related to `null` if: `relation(x, null) == true`.
 *
 * @param relationName name of the relation used in matcher and evaluation result description tests;
 *   defaults to `"unnamed relation"`
 * @see assertNoRelationToNullFor
 */
fun <T> isNotRelatedToNullIn(
    relation: (T, T?) -> Boolean, relationName: String = "unnamed relation"
): AtomicMatcher<T> =
    Matcher.of(descriptionOfNotBeing("related to null in $relationName")) {
        val notRelatedToNull = !relation(it, null)
        Pair(notRelatedToNull, "is ${"not ".orEmptyIf(!notRelatedToNull)}related to null in $relationName")
    }

