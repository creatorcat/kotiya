/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class CommutativityTest {

    @Test fun `commutativity - assertion`() {
        fun add(a: Int, b: Int) = a + b
        fun subX(a: Int, b: Int) = if (a == 1 || b == 1) 23 else a - b
        ::add.assertCommutativityFor(listOf(1, 2, 3, 4, 5))
        val exc =
            assertThrows<AssertionError> { ::subX.assertCommutativityFor(listOf(1, 2, 3)) }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element (2, 3) at index 2 that is not commutative in subX")
        )
    }

    @Test fun `commutativity - matcher`() {
        fun add(a: Int, b: Int) = a + b
        fun sub(a: Int, b: Int) = a - b

        with(isCommutativeIn(::add)) {
            expectedState.assert(hasContents("be commutative pair in unnamed function", isAtomic = true))
            evaluate(1 to 1).assert(hasContents(true, this, "is commutative in unnamed function"))
            evaluate(1 to 3).assert(hasContents(true, this, "is commutative in unnamed function"))
        }

        with(isCommutativeIn(::sub, "subtraction")) {
            expectedState.assert(hasContents("be commutative pair in subtraction", isAtomic = true))
            evaluate(1 to 1).assert(hasContents(true, this, "is commutative in subtraction"))
            evaluate(1 to 2).assert(hasContents(false, this, "is not commutative in subtraction"))
        }
    }

}
