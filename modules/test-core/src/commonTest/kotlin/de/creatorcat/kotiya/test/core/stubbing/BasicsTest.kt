/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class BasicsTest {

    @Test fun `mustNotBeCalled - always throws`() {
        assertThrows<UnsupportedOperationException> { mustNotBeCalled() }
    }
}
