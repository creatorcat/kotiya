/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class SymmetryTest {

    @Test fun `symmetry - assertion`() {
        fun add(a: Int, b: Int) = a + b
        fun subX(a: Int, b: Int) = if (a == 1 || b == 1) 23 else a - b
        ::add.assertSymmetryFor(listOf(1, 2, 3, 4, 5))
        val exc =
            assertThrows<AssertionError> { ::subX.assertSymmetryFor(listOf(1, 2, 3)) }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element (2, 3) at index 2 that is not symmetric in subX")
        )
    }

    @Test fun `symmetry - matcher`() {
        fun add(a: Int, b: Int) = a + b
        fun sub(a: Int, b: Int) = a - b

        with(isSymmetricIn(::add)) {
            expectedState.assert(hasContents("be symmetric pair in unnamed relation", isAtomic = true))
            evaluate(1 to 1).assert(hasContents(true, this, "is symmetric in unnamed relation"))
            evaluate(1 to 3).assert(hasContents(true, this, "is symmetric in unnamed relation"))
        }

        with(isSymmetricIn(::sub, "subtraction")) {
            expectedState.assert(hasContents("be symmetric pair in subtraction", isAtomic = true))
            evaluate(1 to 1).assert(hasContents(true, this, "is symmetric in subtraction"))
            evaluate(1 to 2).assert(hasContents(false, this, "is not symmetric in subtraction"))
        }
    }

    @Test fun `asymmetry - assertion`() {
        fun lt(a: Int, b: Int) = a < b
        fun ltEq(a: Int, b: Int) = a <= b

        ::lt.assertAsymmetryFor(listOf(1, 2, 3, 4, 5, 1))
        val exc =
            assertThrows<AssertionError> { ::ltEq.assertAsymmetryFor(listOf(1, 2)) }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element (1, 1) at index 0 that is not asymmetric in ltEq")
        )
    }

    @Test fun `asymmetry - matcher`() {
        fun lt(a: Int, b: Int) = a < b
        fun ltEq(a: Int, b: Int) = a <= b

        with(isAsymmetricIn(::lt)) {
            expectedState.assert(hasContents("be asymmetric pair in unnamed relation", isAtomic = true))
            evaluate(1 to 1).assert(hasContents(true, this, "is asymmetric in unnamed relation"))
            evaluate(1 to 3).assert(hasContents(true, this, "is asymmetric in unnamed relation"))
            evaluate(3 to 1).assert(hasContents(true, this, "is asymmetric in unnamed relation"))
        }

        with(isAsymmetricIn(::ltEq, "less-or-equal")) {
            expectedState.assert(hasContents("be asymmetric pair in less-or-equal", isAtomic = true))
            evaluate(1 to 2).assert(hasContents(true, this, "is asymmetric in less-or-equal"))
            evaluate(2 to 1).assert(hasContents(true, this, "is asymmetric in less-or-equal"))
            evaluate(1 to 1).assert(hasContents(false, this, "is not asymmetric in less-or-equal"))
        }
    }
}
