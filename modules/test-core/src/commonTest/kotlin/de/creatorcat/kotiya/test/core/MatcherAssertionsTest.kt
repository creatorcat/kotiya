/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

import de.creatorcat.kotiya.matchers.common.isEqual
import kotlin.test.Test
import kotlin.test.assertEquals

class MatcherAssertionsTest {

    @Test fun `assertThat - with custom title`() {
        assertThat("the number", 3, isEqual(3))
        val e = assertThrows<AssertionError> { assertThat("the number", 2, isEqual(3)) }
        assertEquals(
            "The number is expected to be equal 3.\nInstead, it is 2.\n" +
                "Evaluated value:\n2", e.message
        )
    }

    @Test fun `assertThat - with default title`() {
        assertThat(3, isEqual(3))
        val e = assertThrows<AssertionError> { assertThat(2, isEqual(3)) }
        assertEquals(
            "The Int is expected to be equal 3.\nInstead, it is 2.\n" +
                "Evaluated value:\n2", e.message
        )
    }

    @Test fun `object assert - simple usage`() {
        3.assert(isEqual(3))
        val e = assertThrows<AssertionError> { 2.assert(isEqual(3)) }
        val delegationTargetException = assertThrows<AssertionError> { assertThat(2, isEqual(3)) }
        assertEquals(delegationTargetException.message, e.message)
    }
}
