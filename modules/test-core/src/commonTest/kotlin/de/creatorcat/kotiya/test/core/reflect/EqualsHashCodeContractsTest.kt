/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class EqualsHashCodeContractsTest {

    @Test fun `equals contract`() {
        String::equals.assertEqualsContractFor(listOf("", "a", "A", "a", "b", "c", "c", "c"))
        Int::equals.assertEqualsContractFor(listOf(1, 1, 2, 2, 3, 3, 3, 4, 5, 6, 7, 8, 23412))

        val ex = assertThrows<AssertionError> {
            UncommonEquality::equals.assertEqualsContractFor(
                listOf(UncommonEquality(6), UncommonEquality(3))
            )
        }
        assertThat(
            ex.message,
            isNoneNull<String>() which contains("not symmetric in equals")
        )
    }

    @Test fun `hashCode contract`() {
        String::hashCode.assertHashCodeContractFor(listOf("", "a", "A", "a", "b", "c", "c", "c"))
        Int::hashCode.assertHashCodeContractFor(listOf(1, 1, 2, 2, 3, 3, 3, 4, 5, 6, 7, 8, 23412))

        val ex = assertThrows<AssertionError> {
            UncommonEquality::hashCode.assertHashCodeContractFor(
                listOf(UncommonEquality(6), UncommonEquality(3))
            )
        }
        assertThat(
            ex.message,
            isNoneNull<String>() which contains("not consistent with equals")
        )
    }

    private class UncommonEquality(val x: Int) {

        override fun equals(other: Any?): Boolean =
            if (other is UncommonEquality) x % other.x == 0 else false

        override fun hashCode(): Int = x
    }
}
