/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

import de.creatorcat.kotiya.matchers.common.isEqual
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import kotlin.test.Test

class MatcherEvaluationAssertionsTest {

    @Test fun `assert successful evaluation`() {
        val matcher = isEqual(23)
        matcher.assertSuccess(23, "is 23")

        assertThrows<AssertionError> { matcher.assertSuccess(23, "is 24") }
        assertThrows<AssertionError> { matcher.assertSuccess(24, "is 24") }
    }

    @Test fun `assert successful evaluation with value extraction`() {
        val matcher = isInstanceOf<Int>()
        matcher.assertSuccess(23, "is of type Int", 23)

        assertThrows<AssertionError> { matcher.assertSuccess("x", "is of type String", 17) }
        assertThrows<AssertionError> { matcher.assertSuccess(17, "is of type Int", 23) }
        assertThrows<AssertionError> { matcher.assertSuccess(24, "is of type Unknown", 24) }
    }

    @Test fun `assert failing evaluation`() {
        val matcher = isEqual(23)
        matcher.assertFailure(24, "is 24")

        assertThrows<AssertionError> { matcher.assertFailure(23, "is 23") }
        assertThrows<AssertionError> { matcher.assertFailure(24, "is 23") }
    }

    @Test fun `assert failing evaluation with value extraction`() {
        val matcher = isInstanceOf<Int>()
        matcher.assertFailure(24.5, "is of type Double")

        assertThrows<AssertionError> { matcher.assertFailure(23, "is of type Int") }
        assertThrows<AssertionError> { matcher.assertFailure("x", "is of type Int") }
    }

    @Test fun `assert successful and failing evaluation`() {
        val matcher = isEqual(23)
        matcher.assertSuccessAndFailure(23, "is 23", 24, "is 24")

        assertThrows<AssertionError> {
            matcher.assertSuccessAndFailure(23, "is 23", 23, "is 23")
        }
        assertThrows<AssertionError> {
            matcher.assertSuccessAndFailure(24, "is 24", 24, "is 24")
        }
    }
}
