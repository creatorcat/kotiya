/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class RelationPropertiesTest {

    @Test fun `reflexivity - assertion`() {
        fun eq(a: Any?, b: Any?) = a == b
        fun ltOr1(a: Int, b: Int) = a == 1 || a < b
        ::eq.assertReflexivityFor(listOf(1, "2", false))
        val exc = assertThrows<AssertionError> { ::ltOr1.assertReflexivityFor(listOf(1, 2)) }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element 2 at index 1 that is not reflexive in ltOr1")
        )
    }

    @Test fun `reflexivity - matcher`() {
        fun eq(a: Any?, b: Any?) = a == b
        fun lt(a: Int, b: Int) = a < b

        with(isReflexiveIn(::eq)) {
            expectedState.assert(hasContents("be reflexive element in unnamed relation", isAtomic = true))
            evaluate(1).assert(hasContents(true, this, "is reflexive in unnamed relation"))
        }

        with(isReflexiveIn(::lt, "less-than")) {
            expectedState.assert(hasContents("be reflexive element in less-than", isAtomic = true))
            evaluate(1).assert(hasContents(false, this, "is not reflexive in less-than"))
        }
    }

    @Test fun `transitivity - assertion`() {
        fun lt(a: Int, b: Int) = a < b
        fun abGT10(a: Int, b: Int) = a + b > 10

        ::lt.assertTransitivityFor(listOf(1, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::abGT10.assertTransitivityFor(listOf(2, 9, 3))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains(
                "element (2, 9, 2) at index 3 that is not transitive in abGT10"
            )
        )
    }

    @Test fun `transitivity - matcher`() {
        fun lt(a: Int, b: Int) = a < b
        fun abGT10(a: Int, b: Int) = a + b > 10

        with(isTransitiveIn(::lt)) {
            expectedState.assert(hasContents("be transitive triple in unnamed relation", isAtomic = true))
            evaluate(Triple(1, 2, 3)).assert(hasContents(true, this, "is transitive in unnamed relation"))
            evaluate(Triple(1, 1, 2)).assert(hasContents(true, this, "is transitive in unnamed relation"))
            evaluate(Triple(1, 1, 1)).assert(hasContents(true, this, "is transitive in unnamed relation"))
        }

        with(isTransitiveIn(::abGT10, "a + b > 10")) {
            expectedState.assert(hasContents("be transitive triple in a + b > 10", isAtomic = true))
            evaluate(Triple(5, 6, 7)).assert(hasContents(true, this, "is transitive in a + b > 10"))
            evaluate(Triple(2, 9, 3)).assert(hasContents(false, this, "is not transitive in a + b > 10"))
        }
    }

    @Test fun `equivalence to equals - assertion`() {
        fun subIs0(a: Int, b: Int) = a - b == 0
        fun addIs6(a: Int, b: Int) = a + b == 6

        ::subIs0.assertEquivalenceToEqualsFor(listOf(1, 1, 2, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::addIs6.assertEquivalenceToEqualsFor(listOf(3, 2))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains(
                "element (2, 2) at index 2 that has equal values but is not in addIs6"
            )
        )
    }

    @Test fun `equivalence to equals - matcher`() {
        fun subIs0(a: Int, b: Int) = a - b == 0
        fun addIs6(a: Int, b: Int) = a + b == 6

        with(isInRelationOnlyIfEqual(::subIs0)) {
            expectedState.assert(
                hasContents(
                    "be in unnamed relation if and only if both values are equal",
                    isAtomic = true
                )
            )
            evaluate(1 to 1).assert(hasContents(true, this, "has equal values and is in unnamed relation"))
            evaluate(1 to 3).assert(hasContents(true, this, "has unequal values and is not in unnamed relation"))
            evaluate(4 to 2).assert(hasContents(true, this, "has unequal values and is not in unnamed relation"))
            evaluate(4 to 4).assert(hasContents(true, this, "has equal values and is in unnamed relation"))
        }

        with(isInRelationOnlyIfEqual(::addIs6, "adds-to-6")) {
            expectedState.assert(hasContents("be in adds-to-6 if and only if both values are equal", isAtomic = true))
            evaluate(3 to 3).assert(hasContents(true, this, "has equal values and is in adds-to-6"))
            evaluate(3 to 4).assert(hasContents(true, this, "has unequal values and is not in adds-to-6"))
            evaluate(4 to 4).assert(hasContents(false, this, "has equal values but is not in adds-to-6"))
            evaluate(2 to 4).assert(hasContents(false, this, "has unequal values but is in adds-to-6"))
        }
    }

    @Test fun `no null relation - assertion`() {
        fun eq(a: Any?, b: Any?) = a == b
        fun nullEqOdd(a: Int, b: Int?) = a % 2 == 1 && b == null

        ::eq.assertNoRelationToNullFor(listOf(1, "2", false))
        val exc = assertThrows<AssertionError> { ::nullEqOdd.assertNoRelationToNullFor(listOf(2, 4, 3)) }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element 3 at index 2 that is related to null in nullEqOdd")
        )
    }

    @Test fun `no null relation - matcher`() {
        fun eq(a: Any?, b: Any?) = a == b
        fun nullEqOdd(a: Int, b: Int?) = a % 2 == 1 && b == null

        with(isNotRelatedToNullIn(::eq)) {
            expectedState.assert(hasContents("not be related to null in unnamed relation", isAtomic = true))
            evaluate(1).assert(hasContents(true, this, "is not related to null in unnamed relation"))
            evaluate(2).assert(hasContents(true, this, "is not related to null in unnamed relation"))
        }

        with(isNotRelatedToNullIn(::nullEqOdd, "strange-relation")) {
            expectedState.assert(hasContents("not be related to null in strange-relation", isAtomic = true))
            evaluate(2).assert(hasContents(true, this, "is not related to null in strange-relation"))
            evaluate(1).assert(hasContents(false, this, "is related to null in strange-relation"))
        }
    }
}
