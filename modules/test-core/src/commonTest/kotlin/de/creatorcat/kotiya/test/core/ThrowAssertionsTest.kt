/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class ThrowAssertionsTest {

    @Test fun `assertThrows - fail when nothing is thrown`() {
        try {
            assertThrows<Throwable> { }
        } catch (e: AssertionError) {
            return
        }
        fail()
    }

    @Test fun `assertThrows - fail if wrong exception type`() {
        try {
            assertThrows<IllegalArgumentException> { throw IndexOutOfBoundsException() }
        } catch (e: AssertionError) {
            return
        }
        fail()
    }

    @Test fun `assertThrows - succeed if right type is thrown`() {
        val exception = assertThrows<IllegalArgumentException> { throw IllegalArgumentException() }
        assertEquals(IllegalArgumentException::class, exception::class)
    }
}
