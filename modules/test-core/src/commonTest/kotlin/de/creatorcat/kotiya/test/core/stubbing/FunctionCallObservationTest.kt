/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

class FunctionCallObservationTest {

    @Test fun `call count - with multiple callables and calls`() {
        fun func1(): Int = withCallObserver(::func1) { 17 }
        fun func2(): Int = withCallObserver(::func2) { 23 }
        val dummy = object {
            val prop: Int get() = withCallObserver(::prop) { 5 }
        }

        repeat(3) { assertEquals(17, func1()) }

        repeat(2) { assertEquals(23, func2()) }

        assertEquals(5, dummy.prop)

        assertEquals(3, ::func1.countedCalls)
        assertEquals(2, ::func2.countedCalls)
        assertEquals(1, dummy::prop.countedCalls)
    }

    @Test fun `call count - is reset after getting count`() {
        fun func1(): Int = withCallObserver(::func1) { 17 }

        repeat(3) { assertEquals(17, func1()) }

        assertEquals(3, ::func1.countedCalls)
        assertEquals(0, ::func1.countedCalls)

        assertEquals(17, func1())
        assertEquals(1, ::func1.countedCalls)
        assertEquals(0, ::func1.countedCalls)
    }

    @Test fun `call count - is 0 for unknown callable`() {
        fun funcX(): Int = 23
        val dummy = object {
            val propX: Int = 5
        }

        assertEquals(0, ::funcX.countedCalls)
        assertEquals(0, dummy::propX.countedCalls)
    }

    @Test fun `call count - matcher`() {
        fun foo(): Int = withCallObserver(::foo) { 3 }
        repeat(3) { foo() }
        assertThat(::foo, hasCallCount(3))
        assertThat(::foo, hasCallCount(0))
        assertThat(::foo, !hasCallCount(1))
    }

    @Test fun `reset data`() {
        fun func3(): Int = withCallObserver(::func3, "dummy arg") { 17 }

        // check that count & args are stored
        repeat(3) { assertEquals(17, func3()) }
        assertEquals(3, ::func3.countedCalls)
        repeat(3) { assertEquals(17, func3()) }
        assertEquals(3, ::func3.observedArguments.size)

        // now check that they are reset
        repeat(3) { assertEquals(17, func3()) }
        ::func3.resetObservationData()
        assertEquals(0, ::func3.countedCalls)
        repeat(3) { assertEquals(17, func3()) }
        ::func3.resetObservationData()
        assertThat(::func3.observedArguments, isEmpty)
    }

    @Test fun `call count - assert lazily called`() {
        fun func4(): Int = withCallObserver(::func4) { 17 }
        val x by lazy(::func4)

        // fails if already called
        func4()
        assertThrows<AssertionError> {
            ::func4.assertIsLazilyCalledBy {}
        }

        // fails if not called at all
        ::func4.resetObservationData()
        assertThrows<AssertionError> {
            ::func4.assertIsLazilyCalledBy {}
        }

        // fails if counted more than once
        ::func4.resetObservationData()
        assertThrows<AssertionError> {
            ::func4.assertIsLazilyCalledBy { func4() }
        }

        // succeeds with lazy val
        ::func4.resetObservationData()
        ::func4.assertIsLazilyCalledBy { x + 1 } // +1 avoids unused warning :-)
    }

    @Test fun `arguments - are stored`() {
        fun funcWithArgs(arg1: Int, arg2: String): Int = withCallObserver(::funcWithArgs, arg1, arg2) { arg1 }

        repeat(3) { assertEquals(it, funcWithArgs(it, it.toString())) }

        assertThat(
            ::funcWithArgs.observedArguments, hasElementsEqual(
                listOf<Any?>(0, "0"), listOf(1, "1"), listOf(2, "2")
            )
        )
    }

    @Test fun `arguments - are reset after get`() {
        fun argFun1(): Int = withCallObserver(::argFun1, 1, 2) { 3 }

        repeat(3) { argFun1() }
        assertEquals(3, ::argFun1.observedArguments.size)
        assertThat(::argFun1.observedArguments, isEmpty)
    }

    @Test fun `arguments - is empty for unknown callable`() {
        fun argFuncX(): Int = 23
        val argDummy = object {
            val propX: Int = 5
        }

        assertThat(::argFuncX.observedArguments, isEmpty)
        assertThat(argDummy::propX.observedArguments, isEmpty)
    }

    @Test fun `arguments - matcher for all`() {
        fun argFun2(arg1: Int): Int = withCallObserver(::argFun2, arg1) { arg1 }

        repeat(3) { assertEquals(it, argFun2(it)) }

        with(wasCalledWith(0, 1, 2)) {
            expectedState.assert(
                hasContents(
                    "have been called with the following arguments:\n[[0], [1], [2]].",
                    endsPhrase = true, isAtomic = false
                )
            )
            evaluate(::argFun2).assert(
                hasAtomicContents(
                    true, this,
                    "was called with an argument list that has elements equal [[0], [1], [2]]"
                )
            )
        }
    }

    @Test fun `arguments - matcher for last call`() {
        fun argFun3(arg1: Int, arg2: String): Int = withCallObserver(::argFun3, arg1, arg2) { arg1 }

        repeat(3) { assertEquals(it, argFun3(it, it.toString())) }

        with(wasLastCalledWith(2, "2")) {
            expectedState.assert(
                hasContents(
                    "have been called with the following arguments on its last call:\n[2, 2].",
                    endsPhrase = true, isAtomic = false
                )
            )
            evaluate(::argFun3).assert(
                hasAtomicContents(
                    true, this,
                    "has a last call argument list that has elements equal [2, 2]"
                )
            )
        }
    }
}
