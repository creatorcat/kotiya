/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class CompareToContractTest {

    @Test fun `compareTo equivalence property - assertion`() {
        fun sub(a: Int, b: Int) = a - b
        fun subX(a: Int, b: Int) = if (a == -b) 0 else a - b

        ::sub.assertCompareToEquivalenceProperty(listOf(1, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::subX.assertCompareToEquivalenceProperty(listOf(5, -5, 3))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains(
                "element (5, -5, 3) at index 5 that does not satisfy f(x,y) == 0 implies"
            )
        )
    }

    @Test fun `compareTo equivalence property - matcher`() {
        fun sub(a: Int, b: Int) = a - b
        fun subX(a: Int, b: Int) = if (a == -b) 0 else a - b

        with(satisfiesCompareToEquivalenceProperty(::sub)) {
            expectedState.assert(
                hasContents(
                    "satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being unnamed function",
                    isAtomic = true
                )
            )
            evaluate(Triple(1, 2, 3)).assert(
                hasContents(
                    true, this,
                    "does satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being unnamed function"
                )
            )
            evaluate(Triple(1, 1, 3)).assert(
                hasContents(
                    true, this,
                    "does satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being unnamed function"
                )
            )
        }

        with(satisfiesCompareToEquivalenceProperty(::subX, "subX")) {
            expectedState.assert(
                hasContents(
                    "satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being subX",
                    isAtomic = true
                )
            )
            evaluate(Triple(1, 2, 3)).assert(
                hasContents(
                    true, this,
                    "does satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being subX"
                )
            )
            evaluate(Triple(5, -5, 3)).assert(
                hasContents(
                    false, this,
                    "does not satisfy f(x,y) == 0 implies sgn(f(x,z)) == sgn(f(y,z)) with f being subX"
                )
            )
        }
    }

    @Test fun `compareTo consistency with equals - assertion`() {
        fun subIs0(a: Int, b: Int) = a - b
        fun addX(a: Int, b: Int) = if (a == b) 0 else a + b

        ::subIs0.assertCompareToConsistencyWithEquals(listOf(1, 1, 2, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::addX.assertCompareToConsistencyWithEquals(listOf(3, 2, -3))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains(
                "element (3, -3) at index 2 that has unequal values but is in compareToEquals0"
            )
        )
    }

    @Test fun `compareTo contract`() {
        String::compareTo.assertCompareToContractFor(listOf("", "a", "A", "a", "b", "c", "c", "c"))
        String::compareTo.assertCompareToConsistencyWithEquals(listOf("", "a", "A", "a", "b", "c", "c", "c"))
        fun int_compareTo(x: Int, y: Int) = x.compareTo(y)
        ::int_compareTo.assertCompareToContractFor(listOf(0, 1, 2, 3, -1, -2, -3, 1, 1, 2, 2, 2, 3, 4, 56))
        ::int_compareTo.assertCompareToConsistencyWithEquals(listOf(0, 1, 2, 3, -1, -2, -3, 1, 1, 2, 2, 2, 3, 4, 56))

        fun subX(a: Int, b: Int) = if (a == -b) 0 else a - b
        val ex = assertThrows<AssertionError> {
            ::subX.assertCompareToContractFor(listOf(5, -5, 3))
        }
        assertThat(
            ex.message,
            isNoneNull<String>() which contains("element (5, -5, 3) at index 5 that does not satisfy")
        )
    }
}
