/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.stubbing

import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

class LambdaCallObservationTest {
    @Test fun `tagged call count - simple usage`() {
        val testee = CodeRunner { withCallCounterForTag(1) { 17 } }
        repeat(3) { testee.run() }

        assertEquals(3, countedCallsForTag(1))
    }

    @Test fun `tagged call count - is reset after getting count`() {
        val testee = CodeRunner { withCallCounterForTag(2) { 17 } }
        repeat(3) { testee.run() }
        assertEquals(3, countedCallsForTag(2))
        assertEquals(0, countedCallsForTag(2))

        repeat(2) { testee.run() }
        assertEquals(2, countedCallsForTag(2))
    }

    @Test fun `tagged call count - is 0 for unknown tag`() {
        assertEquals(0, countedCallsForTag("UNKNOWN"))
    }

    @Test fun `tagged call count - is bound to receiver`() {
        val other = "other"
        val t1 = CodeRunner { withCallCounterForTag(1) { 17 } }
        val t2 = CodeRunner { other.withCallCounterForTag(1) { 17 } }
        t1.run()
        t2.run()
        assertEquals(1, countedCallsForTag(1))
        assertEquals(1, other.countedCallsForTag(1))
    }

    @Test fun `tagged call count - matcher`() {
        val runner = CodeRunner { withCallCounterForTag("RUNNER") { 3 } }
        repeat(3) { runner.run() }
        assertThat("RUNNER", hasCallCountAsTag(3))
        assertThat("RUNNER", hasCallCountAsTag(0))
        assertThat("RUNNER", !hasCallCountAsTag(1))
    }

    @Test fun `tagged call count - reset`() {
        val testee = CodeRunner { withCallCounterForTag(3) { 17 } }
        repeat(3) { testee.run() }
        // first check if counting works
        assertEquals(3, countedCallsForTag(3))

        repeat(3) { testee.run() }
        resetCountedCallsForTag(3)
        assertEquals(0, countedCallsForTag(2))
    }

    @Test fun `tagged call count - assert lazily called`() {
        fun func5(): Int = withCallCounterForTag(5) { 17 }
        val x by lazy(::func5)

        // fails if already called
        func5()
        assertThrows<AssertionError> {
            assertTagIsLazilyCalledBy(5) {}
        }

        // fails if not called at all
        resetCountedCallsForTag(5)
        assertThrows<AssertionError> {
            assertTagIsLazilyCalledBy(5) {}
        }

        // fails if counted more than once
        resetCountedCallsForTag(5)
        assertThrows<AssertionError> {
            assertTagIsLazilyCalledBy(5) { func5() }
        }

        // succeeds with lazy val
        resetCountedCallsForTag(5)
        assertTagIsLazilyCalledBy(5) { x + 1 } // +1 avoids unused warning :-)
    }

    private class CodeRunner<R>(val code: () -> R) {
        fun run(): R = code()
    }
}
