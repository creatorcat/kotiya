/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.test.core.reflect

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.common.isNoneNull
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class ConsistencyTest {

    @Test fun `consistent argument - assertion`() {
        var c = 0
        fun id(a: Int) = a
        fun accumulator(a: Int): Int {
            c += a
            return c
        }

        ::id.assertArgumentConsistencyFor(listOf(1, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::accumulator.assertArgumentConsistencyFor(listOf(1))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element 1 at index 0 that is not consistent in accumulator")
        )
    }

    @Test fun `consistent argument - matcher`() {
        var c = 0
        fun id(a: Int) = a
        fun accumulator(a: Int): Int {
            c += a
            return c
        }

        with(isConsistentArgumentIn(::id)) {
            expectedState.assert(hasContents("be consistent argument in unnamed function", isAtomic = true))
            evaluate(1).assert(hasContents(true, this, "is consistent in unnamed function"))
            evaluate(17).assert(hasContents(true, this, "is consistent in unnamed function"))
        }

        with(isConsistentArgumentIn(::accumulator, "accumulator")) {
            expectedState.assert(hasContents("be consistent argument in accumulator", isAtomic = true))
            evaluate(5).assert(
                hasContents(
                    false, this,
                    "is not consistent in accumulator: 1. result = 5 while 2. result = 10"
                )
            )
        }
    }

    @Test fun `consistent pair - assertion`() {
        var c = 0
        fun add(a: Int, b: Int) = a + b
        fun accumulator(a: Int, b: Int): Int {
            c += a + b
            return c
        }

        ::add.assertArgumentPairConsistencyFor(listOf(1, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::accumulator.assertArgumentPairConsistencyFor(listOf(1, 2))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains("element (1, 1) at index 0 that is not consistent in accumulator")
        )
    }

    @Test fun `consistent pair - matcher`() {
        var c = 0
        fun add(a: Int, b: Int) = a + b
        fun accumulator(a: Int, b: Int): Int {
            c += a + b
            return c
        }

        with(isConsistentPairIn(::add)) {
            expectedState.assert(hasContents("be consistent argument pair in unnamed function", isAtomic = true))
            evaluate(1 to 2).assert(hasContents(true, this, "is consistent in unnamed function"))
            evaluate(17 to 23).assert(hasContents(true, this, "is consistent in unnamed function"))
        }

        with(isConsistentPairIn(::accumulator, "accumulator")) {
            expectedState.assert(hasContents("be consistent argument pair in accumulator", isAtomic = true))
            evaluate(5 to 1).assert(
                hasContents(
                    false, this,
                    "is not consistent in accumulator: 1. result = 6 while 2. result = 12"
                )
            )
        }
    }

    @Test fun `consistent with equals - assertion`() {
        fun square(a: Int) = a * a
        fun valOfM(a: Mod2EqualInt) = a.value

        ::square.assertConsistencyWithEqualsFor(listOf(1, 2, 3, 4, 5))
        val exc = assertThrows<AssertionError> {
            ::valOfM.assertConsistencyWithEqualsFor(listOf(Mod2EqualInt(2), Mod2EqualInt(3), Mod2EqualInt(4)))
        }
        assertThat(
            exc.message,
            isNoneNull<String>() which contains(
                "element (2, 4) at index 2 that is not consistent with equals in valOfM"
            )
        )
    }

    @Test fun `consistent with equals - matcher`() {
        fun square(a: Int) = a * a
        fun valOfM(a: Mod2EqualInt) = a.value

        with(isConsistentWithEqualsIn(::square)) {
            expectedState.assert(
                hasContents(
                    "be argument pair consistent with equals in unnamed function", isAtomic = true
                )
            )
            evaluate(2 to 2).assert(hasContents(true, this, "is consistent with equals in unnamed function"))
            evaluate(17 to 23).assert(hasContents(true, this, "is consistent with equals in unnamed function"))
        }

        with(isConsistentWithEqualsIn(::valOfM, "strange-func")) {
            expectedState.assert(
                hasContents(
                    "be argument pair consistent with equals in strange-func", isAtomic = true
                )
            )
            evaluate(Mod2EqualInt(2) to Mod2EqualInt(2))
                .assert(hasContents(true, this, "is consistent with equals in strange-func"))
            evaluate(Mod2EqualInt(17) to Mod2EqualInt(23))
                .assert(hasContents(false, this, "is not consistent with equals in strange-func"))
        }
    }

    private class Mod2EqualInt(val value: Int) {
        override fun equals(other: Any?): Boolean =
            if (other is Mod2EqualInt)
                other.value % 2 == value % 2
            else
                false

        override fun hashCode(): Int = value % 2

        override fun toString(): String = value.toString()
    }
}
