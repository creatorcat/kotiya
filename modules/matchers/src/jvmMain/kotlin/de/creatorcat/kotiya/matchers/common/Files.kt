/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of
import java.io.File

/**
 * A matcher that checks if a file exists.
 */
val exists: AtomicMatcher<File> = Matcher.of(descriptionOfAtomicCondition("exist", "exists")) {
    val exists = it.exists()
    Pair(exists, if (exists) "exists" else "does not exist")
}
