/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assertThat
import java.io.File
import kotlin.test.Test

class FilesTest {

    private val existingFile = File("src/jvmTest/resources/dummy")
    private val unknownFile = File("src/jvmTest/resources/unknown")

    @Test fun `exists - simple usage`() {
        assertThat(existingFile, exists)
        assertThat(unknownFile, !exists)
    }
}
