/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.introspection

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.InvertibleValueStateDescription
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.of

/**
 * Creates a matcher for [ValueStateDescription]s that matches:
 * * [infiniteForm] to [description.infiniteForm][ValueStateDescription.infiniteForm]
 * * [isAtomic] to [description.isAtomic][ValueStateDescription.isAtomic] (defaults to true)
 * * [startsPhrase] to [description.startsPhrase][ValueStateDescription.startsPhrase] (defaults to false)
 * * [endsPhrase] to [description.endsPhrase][ValueStateDescription.endsPhrase] (defaults to false)
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    infiniteForm: String,
    isAtomic: Boolean = true,
    startsPhrase: Boolean = false, endsPhrase: Boolean = false
): AtomicMatcher<ValueStateDescription> =
    Matcher.of({
        descriptionOfCombinedCondition(
            "have infiniteForm=$infiniteForm, isAtomic=$isAtomic, startsPhrase=$startsPhrase, endsPhrase=$endsPhrase"
        )
    }) { value ->
        val matches = infiniteForm == value.infiniteForm &&
            isAtomic == value.isAtomic &&
            startsPhrase == value.startsPhrase &&
            endsPhrase == value.endsPhrase
        Pair(
            matches,
            {
                "has infiniteForm=${value.infiniteForm}, isAtomic=${value.isAtomic}," +
                    " startsPhrase=${value.startsPhrase}, endsPhrase=${value.endsPhrase}"
            }
        )
    }

/**
 * Creates a matcher for [ConjugableValueStateDescription]s that matches:
 * * [infiniteForm] to [description.infiniteForm][ConjugableValueStateDescription.infiniteForm]
 * * [thirdPersonForm] to [description.thirdPersonForm][ConjugableValueStateDescription.thirdPersonForm]
 * * [isAtomic] to [description.isAtomic][ConjugableValueStateDescription.isAtomic] (defaults to false)
 * * [startsPhrase] to [description.startsPhrase][ConjugableValueStateDescription.startsPhrase] (defaults to false)
 * * [endsPhrase] to [description.endsPhrase][ConjugableValueStateDescription.endsPhrase] (defaults to false)
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    infiniteForm: String,
    thirdPersonForm: String,
    isAtomic: Boolean = true,
    startsPhrase: Boolean = false, endsPhrase: Boolean = false
): CombinedMatcher<ConjugableValueStateDescription> =
    Matcher.of<ConjugableValueStateDescription>({
        descriptionOfAtomicCondition("have thirdPersonForm=$thirdPersonForm")
    }) { value ->
        Pair(thirdPersonForm == value.thirdPersonForm, { "has thirdPersonForm=${value.thirdPersonForm}" })
    } and hasContents(infiniteForm, isAtomic, startsPhrase, endsPhrase)

/**
 * Creates a matcher for [InvertibleValueStateDescription]s that matches:
 * * [infiniteForm] to [description.infiniteForm][InvertibleValueStateDescription.infiniteForm]
 * * [thirdPersonForm] to [description.thirdPersonForm][InvertibleValueStateDescription.thirdPersonForm]
 * * [infiniteNegation] to [description.negation.infiniteForm][InvertibleValueStateDescription.negation]
 * * [thirdPersonNegation] to [description.negation.thirdPersonForm][InvertibleValueStateDescription.negation]
 * * [isAtomic] to [description.isAtomic][InvertibleValueStateDescription.isAtomic] (defaults to true)
 * * [startsPhrase] to [description.startsPhrase][InvertibleValueStateDescription.startsPhrase] (defaults to false)
 * * [endsPhrase] to [description.endsPhrase][InvertibleValueStateDescription.endsPhrase] (defaults to false)
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    infiniteForm: String,
    thirdPersonForm: String,
    infiniteNegation: String,
    thirdPersonNegation: String,
    isAtomic: Boolean = true,
    startsPhrase: Boolean = false, endsPhrase: Boolean = false
): CombinedMatcher<InvertibleValueStateDescription> =
    Matcher.of<InvertibleValueStateDescription>({
        descriptionOfCombinedCondition(
            "have a negation with infiniteForm=$infiniteNegation thirdPersonForm=$thirdPersonNegation"
        )
    }) {
        val ng = it.negation
        val matches = infiniteNegation == ng.infiniteForm && thirdPersonNegation == ng.thirdPersonForm
        Pair(matches, { "has negation with infiniteForm=${ng.infiniteForm} thirdPersonForm=${ng.thirdPersonForm}" })
    } and hasContents(infiniteForm, thirdPersonForm, isAtomic, startsPhrase, endsPhrase)
