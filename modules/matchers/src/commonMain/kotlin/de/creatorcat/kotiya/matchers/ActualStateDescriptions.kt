/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Returns a [Sequence] of all evaluated nodes that actually
 * [contribute][CombinedMatcherEvaluationNode.resultContributors] to the match result of this node
 * ([atomic nodes][AtomicMatcherEvaluationNode] have no children and thus only contribute themselves).
 * The tree is walked *depth-first*, so this node itself will be the first element in the sequence.
 * Then follows the first contributing [child][MatcherEvaluationNode.children] and
 * its contributing children recursively etc.
 */
fun MatcherEvaluationNode.walkResultContributors(): Sequence<MatcherEvaluationNode> =
    object : Sequence<MatcherEvaluationNode> {
        override operator fun iterator(): Iterator<MatcherEvaluationNode> =
            iterator {
                yield(this@walkResultContributors)
                if (this@walkResultContributors is CombinedMatcherEvaluationNode) {
                    for (contributor in resultContributors) {
                        yieldAll(contributor.walkResultContributors())
                    }
                }
            }
    }

/**
 * Generates a human readable text explaining *"why this result"* was produced.
 * The text starts with the [valueTitle] and lists all states or conditions of the evaluated value,
 * that made the matcher fail or succeed
 * (based on all [contributing evaluation nodes][walkResultContributors]).
 * The text does neither start nor end a sentence and will thus not be capitalized.
 * This allows further prefixing / suffixing.
 *
 * @param valueTitle title of the evaluated value in the text; defaults to `"the value"`
 */
fun MatcherEvaluationNode.describeWhy(valueTitle: String = "the value"): String =
    walkResultContributors()
        .filterIsInstance<AtomicMatcherEvaluationNode>()
        .map { it.actualState }
        .toSet()
        .joinToString(prefix = "$valueTitle ", separator = " and ") { it }
