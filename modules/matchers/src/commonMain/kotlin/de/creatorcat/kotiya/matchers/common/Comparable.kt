/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of

/**
 * Creates a matcher that checks if a value is less than [max].
 */
fun <T : Comparable<T>> isLessThan(max: T): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("less than $max")) {
        Pair(it < max, "is $it")
    }

/**
 * Creates a matcher that checks if a value is less than or equal to [max].
 */
fun <T : Comparable<T>> isLessEqThan(max: T): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("less than or equal to $max")) {
        Pair(it <= max, "is $it")
    }

/**
 * Creates a matcher that checks if a value is greater than [min].
 */
fun <T : Comparable<T>> isGreaterThan(min: T): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("greater than $min")) {
        Pair(it > min, "is $it")
    }

/**
 * Creates a matcher that checks if a value is greater than or equal to [min].
 */
fun <T : Comparable<T>> isGreaterEqThan(min: T): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("greater than or equal to $min")) {
        Pair(it >= min, "is $it")
    }

/**
 * Creates a matcher that checks if a value is inside the [range].
 */
fun <T : Comparable<T>> isInside(range: ClosedRange<T>): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("inside $range")) {
        Pair(it in range, "is $it")
    }

