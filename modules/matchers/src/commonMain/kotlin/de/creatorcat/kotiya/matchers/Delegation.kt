/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

private class AtomicDelegationMatcher<in O, in T>(
    private val target: AtomicMatcher<T>,
    private val transformInput: (O) -> T
) : AtomicMatcher<O> {
    override val expectedState: ValueStateDescription by lazy(target::expectedState)
    override fun evaluate(value: O): AtomicMatcherEvaluationNode {
        val result = target.evaluate(transformInput(value))
        return AtomicMatcherEvaluationNode.of(result.isMatch, this, result::actualState)
    }
}

/**
 * Creates an [AtomicMatcher] that delegates its [expectedState][AtomicMatcher.expectedState]
 * and [evaluation][AtomicMatcher.evaluate] to the [target]
 * after [transformation] of a target compliant value from the original input.
 *
 * Be aware that the [expectedState][AtomicMatcher.expectedState] of the new matcher
 * and the [actualState][AtomicMatcherEvaluationNode.actualState] of any of its evaluation results
 * will contain the texts of the [target] matcher.
 * To define new descriptions use [describedWith].
 */
fun <O, T> Matcher.Companion.byDelegationTo(
    target: AtomicMatcher<T>, transformation: (O) -> T
): AtomicMatcher<O> =
    AtomicDelegationMatcher(target, transformation)

