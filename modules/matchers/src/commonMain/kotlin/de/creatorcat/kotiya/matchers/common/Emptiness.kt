/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.core.reflect.className
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.ofLazyActualState

/**
 * Check if the given object is *empty* in the sense defined by its type.
 * Supported types are:
 * * [Collection], [Map] and [Array]
 * * [Iterable] and [Sequence]
 * * [CharSequence]
 *
 * @throws IllegalArgumentException if the type is not supported
 *
 * @see isEmptyIterable
 * @see isEmptyString
 * @see isEmptySequence
 * @see isEmptyMap
 * @see isEmptyCollection
 * @see isEmptyArray
 */
val isEmpty: AtomicMatcher<Any> =
    Matcher.ofLazyActualState(descriptionOfBeing("empty")) { value ->
        val result = when (value) {
            is Collection<*> -> isEmptyCollection.evaluate(value)
            is Map<*, *> -> isEmptyMap.evaluate(value)
            is Array<*> -> isEmptyArray.evaluate(value)
            is CharSequence -> isEmptyString.evaluate(value)
            is Sequence<*> -> isEmptySequence.evaluate(value)
            // iterable must be last to avoid collection being handled here
            is Iterable<*> -> isEmptyIterable.evaluate(value)
            else -> throw IllegalArgumentException(
                "emptiness cannot be determined for $value of type ${value.className}"
            )
        }
        result.isMatch to result::actualState
    }

/**
 * Checks if an [Iterable] is empty i.e. contains no elements.
 */
val isEmptyIterable: AtomicMatcher<Iterable<*>> = createEmptinessMatcher {
    this.firstOrNull() == null
}

/**
 * Checks if a [Sequence] is empty i.e. contains no elements.
 */
val isEmptySequence: AtomicMatcher<Sequence<*>> = createEmptinessMatcher {
    this.firstOrNull() == null
}

/**
 * Checks if a collection [is empty][Collection.isEmpty].
 */
val isEmptyCollection: AtomicMatcher<Collection<*>> = createEmptinessMatcher(Collection<*>::isEmpty)

/**
 * Checks if a map [is empty][Map.isEmpty].
 */
val isEmptyMap: AtomicMatcher<Map<*, *>> = createEmptinessMatcher(Map<*, *>::isEmpty)

/**
 * Checks if an array [is empty][Array.isEmpty].
 */
val isEmptyArray: AtomicMatcher<Array<*>> = createEmptinessMatcher(Array<*>::isEmpty)

/**
 * Checks if a string (actually a char sequence) [is empty][CharSequence.isEmpty].
 */
val isEmptyString: AtomicMatcher<CharSequence> = createEmptinessMatcher(CharSequence::isEmpty)

private fun <T> createEmptinessMatcher(isEmpty: T.() -> Boolean): AtomicMatcher<T> =
    Matcher.of(descriptionOfBeing("empty")) {
        val empty = it.isEmpty()
        Pair(empty, "is ${"not ".orEmptyIf(empty)}empty")
    }    
