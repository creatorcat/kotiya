/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.predicates.Predicate
import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.of

/**
 * A matcher checks if a value satisfies certain conditions or is in a specific state (see [evaluate]).
 * Furthermore, it provides the means to generate human readable descriptions of what is expected
 * of a value to match (see [generateExpectationTree]) and why a certain value did not match
 * (via the [evaluation nodes][MatcherEvaluationNode] in the tree produced by [evaluate]).
 *
 * Thus, a matcher matcher is an extension of [Predicate]
 * and is likewise usable as simple boolean function.
 *
 * ### Creation and Combination
 *
 * Every matcher implementation should only be concerned with one single aspect of an object.
 * This means, for different conditions there should be different matchers.
 * These are called *atomic* matchers and should implement [AtomicMatcher].
 *
 * To create atomic matchers use one of the "static" methods provided via the [Matcher.Companion].
 * If you just need a custom description for an existing matcher, use one of the
 * [describedWith] methods.
 *
 * Arbitrary matchers may be combined (usually by logical functions) to form a [CombinedMatcher].
 * Basic boolean operators may be used easily via the operator functions [and], [or] and [not].
 * Such combinations create a structure which corresponds to a tree graph.
 * The inner nodes of this tree represent the combination operations and its leaves are the atomic matchers.
 * This tree structure is accessible via [generateExpectationTree].
 *
 * @param T type of values this matcher can evaluate
 */
interface Matcher<in T> : Predicate<T> {

    /**
     * Generates a tree of [ValueStateDescription]s which defines,
     * what is expected of a value to satisfy this matcher.
     *
     * A matcher may be atomic or created by combining several matchers via operators (see [CombinedMatcher]).
     * Thus, the description of what is expected from a value to match
     * should also be a combination of the descriptions of all the child matchers.
     *
     * For [AtomicMatcher]s, the tree will consist of one node which contains the respective
     * [expectedState][AtomicMatcher.expectedState].
     * For [CombinedMatcher]s, the resulting tree should be generated using the
     * expectation trees of the [child matchers][CombinedMatcher.childMatchers].
     * However, the matcher has full control of how to embed the child descriptions,
     * so several atomic descriptions may also be composed to a single combined description object.
     *
     * ### Example of an expectation tree
     *
     * Tree of a matcher that checks if an integer is `(odd && <10) || (even && >20)`.
     *
     * Matcher combination tree:
     *
     *               (OR)
     *              /    \
     *          (AND)    (AND)
     *         /   \     /    \
     *     (odd) (<10) (even) (>20)
     *
     *
     * Matcher description tree:
     *
     *                      "satisfy any of the following conditions:"
     *                        /                           \
     *          "It must be odd and < 10."    "It must be even and > 20."
     *
     * @param valueTitle The title to be used when denoting the value that will be evaluated by the matcher
     *   in the description text. However, implementations are free to replace the title by a pronoun (*it*).
     *   Defaults to `"the value"`.
     *   Capitalization of the title string must be left to the description generation functions.
     * @param verb The verb initiating the expectation phrase, e.g. "must", "should" or "is expected to".
     */
    fun generateExpectationTree(
        valueTitle: String = "the value", verb: String = "must"
    ): SimpleDataNode<ValueStateDescription>

    /**
     * Evaluates if the [value] satisfies the expected conditions.
     * Produces an evaluated matcher node tree, that is either a [match][MatcherEvaluationNode.isMatch]
     * or a [mismatch][MatcherEvaluationNode.isMismatch].
     *
     * A matcher may be atomic or created by combining several matchers via operators (see [CombinedMatcher]).
     * Thus, the evaluation of a value through all necessary child matchers (which may only be some)
     * also spans a tree structure.
     *
     * The *evaluation tree* resembles the [expectation tree][generateExpectationTree],
     * but it only contains those nodes that were actually visited in the evaluation process.
     * For example, when 2 matchers are combined by logical `AND`,
     * it may suffice to only evaluate the first child matcher to render the result (if it is a mismatch).
     *
     * Furthermore, only a subset of these nodes is actually
     * [contributing][CombinedMatcherEvaluationNode.resultContributors] to the result.
     * For example, when 2 matchers are combined by logical `AND`, with the first producing a match
     * and the second producing a mismatch, then only the second contributes to the resulting mismatch.
     *
     * ### Examples
     *
     * Evaluation trees for a matcher that checks if an integer is `(odd && <10) || (even && >20)`.
     * The decoration symbols indicate whether the matcher produced a match (✓) or a mismatch (❌).
     * Matchers without symbols have not been evaluated and will not be included in the generated tree.
     * They are just displayed here for better understanding.
     * A trailing '*' denotes the contributing matchers.
     *
     * For input 3:
     *
     *                ✓(OR)
     *              /       \
     *         ✓(AND)*      (AND)
     *          /   \       /    \
     *    ✓(odd)* ✓(<10)* (even) (>20)
     *
     * For input 4:
     *
     *                ❌(OR)
     *              /       \
     *        ❌(AND)*      ❌(AND)*
     *         /   \        /     \
     *    ❌(odd)* (<10)  ✓(even) ❌(>20)*
     */
    fun evaluate(value: T): MatcherEvaluationNode

    /**
     * Convenience invocation operator to check if a matcher *applies to* a certain value [p1]
     * i.e. if [matcher.evaluate(p1)][Matcher.evaluate] produces a [match][MatcherEvaluationNode.isMatch].
     *
     * This makes the matcher usable as simple [Predicate].
     */
    override fun invoke(p1: T): Boolean = evaluate(p1).isMatch

    /** The companion object provides methods to easily create matchers. */
    companion object
}

/**
 * A [Matcher] that is known to be atomic, i.e. not a combination of other matchers.
 * Atomic matchers have an atomic [expectation tree][generateExpectationTree]
 * and will always produce an atomic evaluation tree when [evaluated][evaluate].
 */
interface AtomicMatcher<in T> : Matcher<T> {
    /**
     * Human readable description of what state a value must be in to satisfy this matcher.
     *
     * @see generateExpectationTree
     */
    val expectedState: ValueStateDescription

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> =
        SimpleDataNode.of(expectedState)

    override fun evaluate(value: T): AtomicMatcherEvaluationNode
}

/**
 * A [Matcher] that is known to be some combination of [childMatchers].
 * Combined matchers always have a non-trivial [expectation tree][generateExpectationTree]
 * and will always produce a similar constructed evaluation tree when [evaluated][evaluate].
 */
interface CombinedMatcher<in T> : Matcher<T> {

    /**
     * Human readable short name of the combination operation that is represented by this matcher,
     * for example "and", "or" or "not".
     */
    val combinationOperatorName: String

    /**
     * List of matchers that are part of this combination.
     * The list will respect the argument order of the respective combination operation
     * if it is of any importance, e.g. `val children = (antecedent, consequent)` for an "implication".
     */
    // TODO design info:
    // the matchers cannot be typed since combinations of different types are possible in a tree
    // and thus the type cannot be determined in general (e.g. type narrowing matchers)
    val childMatchers: List<Matcher<*>>

    override fun evaluate(value: T): CombinedMatcherEvaluationNode
}
