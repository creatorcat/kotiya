/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.of

/**
 * Creates the inverse (or negation) of this matcher.
 *
 * The resulting matcher uses this matchers [evaluation function][Matcher.evaluate]
 * and then negates the result.
 * If the original matcher returns a [match][MatcherEvaluationNode.isMatch],
 * the inverse will produce a [mismatch][MatcherEvaluationNode.isMismatch] and vice versa.
 *
 * The [evaluation tree][Matcher.evaluate] of the negation will be a
 * [CombinedMatcherEvaluationNode] with the original tree as child node.
 *
 * The operator is involutory, meaning if this matcher was itself created using the `not` operator,
 * the result will be the original matcher again so that `!!matcher === matcher` holds.
 */
operator fun <T> Matcher<T>.not(): Matcher<T> =
    (this as? NegatedMatcher)?.originalMatcher // double negation --> back to original
        ?: NegatedMatcher(this)

private class NegatedMatcher<in T>(
    val originalMatcher: Matcher<T>
) : CombinedMatcherBase<T>("not") {
    override val childMatchers: List<Matcher<T>> by lazy { listOf(originalMatcher) }

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val originalExpectationTree = originalMatcher.generateExpectationTree(valueTitle, verb)
        val originalDescription = originalExpectationTree.data
        return if (originalDescription is InvertibleValueStateDescription)
            SimpleDataNode.of(originalDescription.negation)
        else
            SimpleDataNode.of(
                descriptionOfCombinedCondition("not satisfy the following condition").withEnding(":"),
                originalExpectationTree.withCompletedDescription("it", verb)
            )
    }

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val originalResult = originalMatcher.evaluate(value)
        return CombinedMatcherEvaluationNode.of(originalResult.isMismatch, originalResult)
    }
}

