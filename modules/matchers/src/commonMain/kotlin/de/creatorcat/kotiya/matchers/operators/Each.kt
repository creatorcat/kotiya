/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.mapWhile
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherBase
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.withPrefixedActualState

/**
 * Creates a matcher that checks if each element of an [Iterable] satisfies the given [elementMatcher].
 * Note, that the matcher also produces a match for empty iterables.
 *
 * The [childMatchers][CombinedMatcher.childMatchers] are [isEmpty] and the [elementMatcher].
 * The evaluation tree will consist of a the evaluation node of [isEmpty] if the evaluated
 * iterable is empty or of as many [elementMatcher]-evaluation-nodes as the matcher was applied.
 */
fun <T> each(elementMatcher: Matcher<T>): CombinedMatcher<Iterable<T>> =
    object : CombinedMatcherBase<Iterable<T>>("for-each") {
        override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
            return SimpleDataNode.of(
                descriptionOfCombinedCondition(
                    "be empty or each of its elements must satisfy the following condition"
                ).withEnding(":")
            ) {
                listOf(
                    elementMatcher.generateExpectationTree("The element", verb)
                        .withCompletedDescription("The element", verb)
                )
            }
        }

        override val childMatchers: List<Matcher<*>> by lazy { listOf(isEmpty, elementMatcher) }

        override fun evaluate(value: Iterable<T>): CombinedMatcherEvaluationNode {
            val results = value.withIndex().mapWhile(MatcherEvaluationNode::isMatch, includeFirstFail = true) {
                elementMatcher.evaluate(it.value)
                    .withPrefixedActualState { "has element ${it.value} at index ${it.index} that " }
            }

            if (results.isEmpty())
                return CombinedMatcherEvaluationNode.of(true, isEmpty.evaluate(value))

            val lastResult = results.last()
            return if (lastResult.isMatch)
                CombinedMatcherEvaluationNode.of(true, results)
            else
                CombinedMatcherEvaluationNode.of(false, results, listOf(lastResult))
        }

    }
