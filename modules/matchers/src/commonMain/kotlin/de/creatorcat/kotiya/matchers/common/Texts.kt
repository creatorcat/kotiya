/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.descriptionOfNotBeing
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.ofLazyActualState

/**
 * Checks if a string is [blank][CharSequence.isBlank].
 */
val isBlank: AtomicMatcher<CharSequence> =
    Matcher.of(descriptionOfBeing("blank")) {
        val blank = it.isBlank()
        Pair(blank, "is ${"not ".orEmptyIf(blank)}blank")
    }

/**
 * Checks if a string is [not blank][CharSequence.isNotBlank].
 */
val isNotBlank: AtomicMatcher<CharSequence> =
    Matcher.of(descriptionOfNotBeing("blank")) {
        val notBlank = it.isNotBlank()
        Pair(notBlank, "is ${"not ".orEmptyIf(!notBlank)}blank")
    }

/**
 * Creates a matcher that checks if a string [contains][CharSequence.contains] a [subString].
 *
 * @param ignoreCase indicates if the character case should be ignored; defaults to `false`
 */
fun contains(subString: CharSequence, ignoreCase: Boolean = false): AtomicMatcher<CharSequence> =
    Matcher.of(
        descriptionOfAtomicCondition(
            "contain '$subString'${" (ignoring case)".orEmptyIf(!ignoreCase)}",
            "contains '$subString'${" (ignoring case)".orEmptyIf(!ignoreCase)}"
        )
    ) {
        val contains = it.contains(subString, ignoreCase)
        Pair(
            contains,
            "contains ${"no ".orEmptyIf(contains)}'$subString'${" (ignoring case)".orEmptyIf(!ignoreCase)}"
        )
    }

/**
 * Creates a matcher that checks if a string [contains][CharSequence.contains]
 * at least one match of the given [regex].
 */
fun contains(regex: Regex): AtomicMatcher<CharSequence> =
    Matcher.of(
        descriptionOfAtomicCondition(
            "contain at least one match of regex '${regex.pattern}'",
            "contains at least one match of regex '${regex.pattern}'"
        )
    ) {
        val contains = it.contains(regex)
        Pair(
            contains,
            "contains ${if (contains) "a" else "no"} match of regex '${regex.pattern}'"
        )
    }

/**
 * Creates a matcher that checks if a string [endsWith][CharSequence.endsWith] a [suffix].
 *
 * @param ignoreCase indicates if the character case should be ignored; defaults to `false`
 */
fun endsWith(suffix: CharSequence, ignoreCase: Boolean = false): AtomicMatcher<CharSequence> =
    Matcher.ofLazyActualState(
        descriptionOfAtomicCondition(
            "end with '$suffix'${" (ignoring case)".orEmptyIf(!ignoreCase)}",
            "ends with '$suffix'${" (ignoring case)".orEmptyIf(!ignoreCase)}"
        )
    ) {
        val endsWith = it.endsWith(suffix, ignoreCase)
        Pair(endsWith) {
            if (endsWith)
                "ends with '$suffix'${" (ignoring case)".orEmptyIf(!ignoreCase)}"
            else {
                val commonSuffix = it.commonSuffixWith(suffix, ignoreCase)
                val firstDifferentChar = it.elementAtOrNull(it.length - commonSuffix.length - 1) ?: ""
                "differs after ${commonSuffix.length} characters ('[$firstDifferentChar]$commonSuffix'" +
                    "${" - ignoring case".orEmptyIf(!ignoreCase)})"
            }
        }
    }
