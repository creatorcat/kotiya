/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Extension of the [CombinedMatcher] with a fixed type [T].
 * The [childMatchers] of this combination are all matchers of type [T].
 */
interface FixedTypeCombinedMatcher<in T> : CombinedMatcher<T> {
    override val childMatchers: List<Matcher<T>>
}

/**
 * Check if there are at least 2 [children] and optimize them based on associativity.
 * The result will always be a new list so there will be no problems with mutation from outside.
 */
internal inline fun <T, reified M : FixedTypeCombinedMatcher<T>> checkAndOptimizeCombinedMatcherChildren(
    children: List<Matcher<T>>
): List<Matcher<T>> {
    require(children.size >= 2) { "this combined matcher needs at least 2 arguments" }
    // TODO optimization tests with more than 2 children
    val newChildren = mutableListOf<Matcher<T>>()
    for (m in children) {
        // wrong inspection, see https://youtrack.jetbrains.com/issue/KT-21520
        if (m is M)
            newChildren += m.childMatchers
        else
            newChildren += m
    }
    return newChildren
}

