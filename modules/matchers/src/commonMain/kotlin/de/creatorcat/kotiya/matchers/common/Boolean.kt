/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of

/**
 * A matcher that checks if a boolean is `false`.
 */
val isFalse: AtomicMatcher<Boolean> =
    Matcher.of(descriptionOfBeing("false")) {
        Pair(!it, "is $it")
    }

/**
 * A matcher that checks if a boolean is `true`.
 */
val isTrue: AtomicMatcher<Boolean> =
    Matcher.of(descriptionOfBeing("true")) {
        Pair(it, "is $it")
    }
