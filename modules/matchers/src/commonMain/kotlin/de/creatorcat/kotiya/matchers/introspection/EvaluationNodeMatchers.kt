/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.introspection

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.AtomicMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.AtomicMatcherEvaluationNodeWithValue
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.operators.which

/**
 * Creates a matcher for a [MatcherEvaluationNode] that matches:
 * * [isMatch] to [node.isMatch][MatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][MatcherEvaluationNode.matcher] (using equals)
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    isMatch: Boolean, matcher: Matcher<*>
): AtomicMatcher<MatcherEvaluationNode> =
    Matcher.of({ descriptionOfCombinedCondition("have isMatch=$isMatch, matcher=$matcher") }) { value ->
        val matches = isMatch == value.isMatch && matcher == value.matcher
        Pair(matches, { "has isMatch=${value.isMatch}, matcher=${value.matcher}" })
    }

/**
 * Creates a matcher for an [AtomicMatcherEvaluationNode] that matches:
 * * [isMatch] to [node.isMatch][AtomicMatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][AtomicMatcherEvaluationNode.matcher] (using equals)
 * * [actualState] to [node.actualState][AtomicMatcherEvaluationNode.actualState]
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    isMatch: Boolean, matcher: AtomicMatcher<*>,
    actualState: String
): CombinedMatcher<AtomicMatcherEvaluationNode> =
    Matcher.of<AtomicMatcherEvaluationNode>({
        descriptionOfAtomicCondition("have actualState=$actualState")
    }) { value ->
        Pair(actualState == value.actualState, { "has actualState=${value.actualState}" })
    } and hasContents(isMatch, matcher)

/**
 * Creates a matcher for an [AtomicMatcherEvaluationNodeWithValue] that matches:
 * * [isMatch] to [node.isMatch][AtomicMatcherEvaluationNodeWithValue.isMatch]
 * * [matcher] to [node.matcher][AtomicMatcherEvaluationNodeWithValue.matcher] (using equals)
 * * [actualState] to [node.actualState][AtomicMatcherEvaluationNodeWithValue.actualState]
 * * [extractedValue] to [node.extractedValue][AtomicMatcherEvaluationNodeWithValue.extractedValue]
 *
 * This matcher is meant for testing a matcher itself.
 */
fun <V : Any> hasContents(
    isMatch: Boolean, matcher: AtomicMatcher<*>,
    actualState: String, extractedValue: V?
): CombinedMatcher<AtomicMatcherEvaluationNodeWithValue<V>> =
    Matcher.of<AtomicMatcherEvaluationNodeWithValue<V>>({
        descriptionOfAtomicCondition("have extractedValue=$extractedValue")
    }) { value ->
        Pair(extractedValue == value.extractedValue, { "has extractedValue=${value.extractedValue}" })
    } and hasContents(isMatch, matcher, actualState)

/**
 * Creates a matcher that first checks if the node is an [AtomicMatcherEvaluationNode] and then matches:
 * * [isMatch] to [node.isMatch][AtomicMatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][AtomicMatcherEvaluationNode.matcher] (using equals)
 * * [actualState] to [node.actualState][AtomicMatcherEvaluationNode.actualState]
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasAtomicContents(
    isMatch: Boolean, matcher: AtomicMatcher<*>,
    actualState: String
): CombinedMatcher<MatcherEvaluationNode> =
    isInstanceOf<AtomicMatcherEvaluationNode>() which hasContents(isMatch, matcher, actualState)

/**
 * Creates a matcher for a [CombinedMatcherEvaluationNode] that matches:
 * * [isMatch] to [node.isMatch][CombinedMatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][CombinedMatcherEvaluationNode.matcher] (using equals)
 * * [children] to [node.children][CombinedMatcherEvaluationNode.children]
 * * [contributingChildren] to [node.resultContributors][CombinedMatcherEvaluationNode.resultContributors]
 *   (which defaults to [children])
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    isMatch: Boolean, matcher: CombinedMatcher<*>,
    children: List<MatcherEvaluationNode>,
    contributingChildren: List<MatcherEvaluationNode> = children
): CombinedMatcher<CombinedMatcherEvaluationNode> =
    Matcher.of<CombinedMatcherEvaluationNode>({
        descriptionOfCombinedCondition("have children=$children, resultContributors=$contributingChildren")
    }) { value ->
        val matches = children == value.children && contributingChildren == value.resultContributors
        Pair(matches, { "has children=${value.children}, resultContributors=${value.resultContributors}" })
    } and hasContents(isMatch, matcher)

/**
 * Creates a matcher for a [CombinedMatcherEvaluationNode] that matches:
 * * [isMatch] to [node.isMatch][CombinedMatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][CombinedMatcherEvaluationNode.matcher] (using equals)
 * * [childrenSize] to the size of [node.children][CombinedMatcherEvaluationNode.children]
 * * [contributorSize] to the size of [node.resultContributors][CombinedMatcherEvaluationNode.resultContributors]
 *   (which defaults to [childrenSize])
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasContents(
    isMatch: Boolean, matcher: CombinedMatcher<*>,
    childrenSize: Int, contributorSize: Int = childrenSize
): CombinedMatcher<CombinedMatcherEvaluationNode> =
    Matcher.of<CombinedMatcherEvaluationNode>({
        descriptionOfCombinedCondition(
            "have children.size=$childrenSize, resultContributors.size=$contributorSize"
        )
    }) { value ->
        val matches = childrenSize == value.children.size &&
            contributorSize == value.resultContributors.size &&
            value.children.containsAll(value.resultContributors)
        Pair(matches, {
            "has children=${value.children}, resultContributors.size=${value.resultContributors}"
        })
    } and hasContents(isMatch, matcher)

/**
 * Creates a matcher that first checks if the node is a [CombinedMatcherEvaluationNode] and then matches:
 * * [isMatch] to [node.isMatch][CombinedMatcherEvaluationNode.isMatch]
 * * [matcher] to [node.matcher][CombinedMatcherEvaluationNode.matcher] (using equals)
 * * [childrenSize] to the size of [node.children][CombinedMatcherEvaluationNode.children]
 * * [contributorSize] to the size of [node.resultContributors][CombinedMatcherEvaluationNode.resultContributors]
 *   (which defaults to [childrenSize])
 *
 * This matcher is meant for testing a matcher itself.
 */
fun hasCombinedContents(
    isMatch: Boolean, matcher: CombinedMatcher<*>,
    childrenSize: Int, contributorSize: Int = childrenSize
): CombinedMatcher<MatcherEvaluationNode> =
    isInstanceOf<CombinedMatcherEvaluationNode>() which hasContents(isMatch, matcher, childrenSize, contributorSize)
