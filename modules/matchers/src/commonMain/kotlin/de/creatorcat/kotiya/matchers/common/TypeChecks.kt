/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.core.reflect.className
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.descriptionOfNotBeing
import de.creatorcat.kotiya.matchers.of
import kotlin.reflect.KClass

/**
 * Checks for nullness.
 */
val isNull: AtomicMatcher<Any?> =
    Matcher.of(descriptionOfBeing("null")) {
        Pair(it == null, "is ${"not ".orEmptyIf(it == null)}null")
    }

/**
 * Checks for non-nullness and extracts a non-null value if matching.
 */
val isNotNull: ValueExtractingAtomicMatcher<Any?, Any> =
    ValueExtractingAtomicMatcher.of(descriptionOfNotBeing("null")) {
        Pair(it, "is ${"not ".orEmptyIf(it == null)}null")
    }

/**
 * Created a matcher that checks for non-nullness and extracts a value of type [T] if not null.
 */
inline fun <reified T : Any> isNoneNull(): ValueExtractingAtomicMatcher<T?, T> =
    isInstanceOf(T::class)

/**
 * Creates a matcher that checks if a value is an instance of type [T]
 * and extracts that value then.
 */
inline fun <reified T : Any> isInstanceOf(): ValueExtractingAtomicMatcher<Any?, T> =
    isInstanceOf(T::class)

/**
 * Creates a matcher that checks if a value is an instance of [type]
 * and extracts that value then.
 */
fun <T : Any> isInstanceOf(type: KClass<T>): ValueExtractingAtomicMatcher<Any?, T> =
    ValueExtractingAtomicMatcher.of(
        descriptionOfBeing("of type ${type.simpleName}"),
        type.simpleName ?: type.toString()
    ) {
        if (type.isInstance(it)) {
            @Suppress("UNCHECKED_CAST")
            val value = it as T
            Pair(value, "is of type ${it.className}")
        } else
            Pair(null, "is of type ${it.className}")
    }
