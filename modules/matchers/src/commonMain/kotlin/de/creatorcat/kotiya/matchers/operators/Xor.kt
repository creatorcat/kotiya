/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.mapWhile
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherBase
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.FixedTypeCombinedMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.checkAndOptimizeCombinedMatcherChildren
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.isMismatch

/**
 * Creates a matcher that applies if exactly one of the given [matchers] applies to the evaluated value.
 * Given only two child matchers, this represents the logical *xor* function (see [xor]).
 *
 * Actually creates a [OneOfMatcher] with the given matchers as [OneOfMatcher.childMatchers].
 */
fun <T> oneOf(matchers: List<Matcher<T>>): OneOfMatcher<T> = OneOfMatcher(matchers)

/**
 * Creates a matcher that applies if exactly one of the given [matchers] applies to the evaluated value.
 * Given only two child matchers, this represents the logical *xor* function (see [xor]).
 *
 * Actually creates a [OneOfMatcher] with the given matchers as [OneOfMatcher.childMatchers].
 */
fun <T> oneOf(vararg matchers: Matcher<T>): OneOfMatcher<T> = oneOf(matchers.toList())

/**
 * Creates a matcher that represents the exclusive disjunction (*xor*).
 * It applies if either this matcher or the [other] applies but not both at the same time.
 * The operator is associative, so if *xor* is used multiple times it results in a [oneOf]-matcher:
 * `a XOR b XOR c == oneOf(a, b, c)`.
 *
 * Actually creates a [OneOfMatcher] with the this and the [other] matchers as [OneOfMatcher.childMatchers].
 */
infix fun <T> Matcher<T>.xor(other: Matcher<T>): OneOfMatcher<T> = OneOfMatcher(listOf(this, other))

/**
 * A matcher that applies if exactly one of its [childMatchers] applies to the evaluated value.
 * Given only two child matchers, this represents the logical *xor* function (see [xor]).
 *
 * The resulting matcher [evaluates][evaluate] its [childMatchers] in the order they appear in the list.
 * If two of them produce a [match][MatcherEvaluationNode.isMatch],
 * evaluation fails immediately and the results of both matcher will be the only
 * [contributing results][CombinedMatcherEvaluationNode.resultContributors].
 * If all matchers produce a [mismatch][MatcherEvaluationNode.isMismatch],
 * the final result will also be a mismatch and all child results are contributing.
 * A match is only produced if just a single child matchers applies.
 * The respective result will then be the only contributing.
 *
 * The creation is optimized based on the operator associativity.
 * If any of the [childMatchers] is already a `OneOfMatcher`,
 * the children of this matcher will be added directly to the new disjunction instead of the matcher itself.
 * Thus, associativity holds: `oneOf(a, oneOf(b, c)) == oneOf(oneOf(a, b), c) == oneOf(a, b, c)` .
 *
 * @param childMatchers the [child matchers][CombinedMatcher.childMatchers] of the combination,
 *   must be at least 2
 */
open class OneOfMatcher<in T>(
    childMatchers: List<Matcher<T>>
) : CombinedMatcherBase<T>("xor"), FixedTypeCombinedMatcher<T> {
    override val childMatchers: List<Matcher<T>> =
        checkAndOptimizeCombinedMatcherChildren<T, OneOfMatcher<T>>(childMatchers)

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val childDescriptions = childMatchers.map { it.generateExpectationTree(valueTitle, verb) }
        if (childDescriptions.all { it.data.isAtomic }) {
            val suffix =
                if (childDescriptions.size == 2) "but not both"
                else "but not more than one of them"
            return childDescriptions.combineUsingOperatorAndSuffix(suffix)
        }
        return SimpleDataNode.of(
            descriptionOfCombinedCondition("satisfy exactly one of the following conditions").withEnding(":")
        ) {
            childDescriptions.map {
                it.withCompletedDescription(valueTitle, verb)
            }
        }
    }

    private fun List<SimpleDataNode<ValueStateDescription>>.combineUsingOperatorAndSuffix(
        suffix: String
    ): SimpleDataNode<ValueStateDescription> {
        val base = combineUsingOperatorName("or")
        val withSuffix = if (base is ConjugableValueStateDescription)
            descriptionOfCombinedCondition(
                "either ${base.infiniteForm} $suffix",
                "either ${base.thirdPersonForm} $suffix"
            )
        else
            descriptionOfCombinedCondition("either ${base.infiniteForm} $suffix")
        return SimpleDataNode.of(withSuffix)
    }

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        var firstMatch: MatcherEvaluationNode? = null
        val results = childMatchers.mapWhile(
            { firstMatch === null || it === firstMatch || it.isMismatch }, includeFirstFail = true
        ) {
            val result = it.evaluate(value)
            if (result.isMatch && firstMatch == null) firstMatch = result
            result
        }
        val lastResult = results.last()
        // fix first match to allow smart cast
        val firstResult = firstMatch
        return when {
            firstResult !== null && (lastResult === firstResult || lastResult.isMismatch) ->
                CombinedMatcherEvaluationNode.of(true, results, listOf(firstResult))
            firstResult != null && lastResult.isMatch ->
                CombinedMatcherEvaluationNode.of(false, results, listOf(firstResult, lastResult))
            else -> // no match found
                CombinedMatcherEvaluationNode.of(false, results)
        }
    }
}
