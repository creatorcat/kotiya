/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherBase
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.withPrefixedActualState

/**
 * Creates a fail-fast conjunction of this matcher and the [valueMatcher]
 * only that the [valueMatcher] is applied to the [extracted value][ValueExtractingAtomicMatcher.evaluate]
 * of this matcher's result (if it is a match).
 * All other aspects work similar to the [ConjunctionMatcher][de.creatorcat.kotiya.matchers.ConjunctionMatcher].
 *
 * This allows type-narrowing with matchers in many forms.
 * The most common is using an [isInstanceOf][de.creatorcat.kotiya.matchers.common.isInstanceOf] matcher.
 *
 * Example:
 *
 *     val isIntAndPrime = isInstanceOf<Int>() which isPrime
 *
 * @see whichThen
 */
infix fun <T, V : Any> ValueExtractingAtomicMatcher<T, V>.which(valueMatcher: Matcher<V>): CombinedMatcher<T> =
    ValueExtractingConjunction(this, valueMatcher)

private class ValueExtractingConjunction<in T, out V : Any>(
    private val extractingMatcher: ValueExtractingAtomicMatcher<T, V>,
    private val valueMatcher: Matcher<V>
) : CombinedMatcherBase<T>("extracted-value-and") {

    override val childMatchers: List<Matcher<*>> by lazy { listOf(extractingMatcher, valueMatcher) }

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val extractorNode = extractingMatcher.generateExpectationTree(valueTitle, verb)
        val valueNode = valueMatcher.generateExpectationTree(valueTitle, verb)
        check(extractorNode.data.isAtomic) {
            "value extracting matcher did not produce atomic description"
        }
        return if (valueNode.isLeaf && !valueNode.data.startsPhrase)
            SimpleDataNode.of(
                descriptionOfCombinedCondition(
                    "${extractorNode.data.infiniteForm} and the ${extractingMatcher.extractedValueTypeName}" +
                        " $verb ${valueNode.data.infiniteForm}"
                )
            )
        else
            SimpleDataNode.of(
                descriptionOfCombinedCondition(
                    "${extractorNode.data.infiniteForm} and the ${extractingMatcher.extractedValueTypeName}" +
                        " $verb satisfy the following condition"
                ).withEnding(":"),
                valueNode.withCompletedDescription("it", verb)
            )
    }

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val leftResult = extractingMatcher.evaluate(value)
        return if (leftResult.isMatch) {
            val extractedValue = checkNotNull(leftResult.extractedValue) {
                "value extracting matcher did not extract value on match"
            }
            val rightResult = valueMatcher.evaluate(extractedValue)
                .withPrefixedActualState {
                    "led to extracted ${extractingMatcher.extractedValueTypeName} which "
                }
            if (rightResult.isMatch)
                CombinedMatcherEvaluationNode.of(true, leftResult, rightResult)
            else
                CombinedMatcherEvaluationNode.of(false, listOf(leftResult, rightResult), listOf(rightResult))
        } else {
            CombinedMatcherEvaluationNode.of(false, leftResult)
        }
    }
}

