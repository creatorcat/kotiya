/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.makeMutationSafe
import de.creatorcat.kotiya.core.structures.mapWhile
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherBase
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.FixedTypeCombinedMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition

/**
 * Creates a matcher that applies if either all of the given [matchers] apply or none of them at all.
 * Given only two child matchers, this represents the logical *equivalence* function (see [iff]).
 *
 * Actually creates an [EquivalenceMatcher] with the given matchers as [EquivalenceMatcher.childMatchers].
 */
fun <T> isAllOrNoneOf(matchers: List<Matcher<T>>): EquivalenceMatcher<T> = EquivalenceMatcher(matchers)

/**
 * Creates a matcher that applies if either all of the given [matchers] apply or none of them at all.
 * Given only two child matchers, this represents the logical *equivalence* function (see [iff]).
 *
 * Actually creates an [EquivalenceMatcher] with the given matchers as [EquivalenceMatcher.childMatchers].
 */
fun <T> isAllOrNoneOf(vararg matchers: Matcher<T>): EquivalenceMatcher<T> = isAllOrNoneOf(matchers.toList())

/**
 * Creates a matcher that represents the logical equivalence (or biconditional) function.
 *
 * Actually creates an [EquivalenceMatcher] with this and the [other] matcher as [EquivalenceMatcher.childMatchers].
 */
infix fun <T> Matcher<T>.iff(other: Matcher<T>): EquivalenceMatcher<T> = EquivalenceMatcher(listOf(this, other))

/**
 * A matcher that applies if either all of its [childMatchers] apply or none of them at all.
 * Given only two child matchers, this represents the logical *equivalence* function (see [iff]).
 *
 * The resulting matcher [evaluates][evaluate] its [childMatchers] in the order they appear in the list.
 * If two of them produce a different result (concerning match/mismatch) evaluation fails immediately.
 * Otherwise all matchers will be evaluated and the result will be a match.
 * In both cases the evaluation results of all evaluated matchers are also
 * the [contributing results][CombinedMatcherEvaluationNode.resultContributors].
 *
 * Note, that there can be *no* associativity optimization for this type,
 * since `isAllOrNoneOf(true, false, false) == false` but `isAllOrNoneOf(true, isAllOrNoneOf(false, false)) == true`.
 *
 * @param childMatchers the [child matchers][CombinedMatcher.childMatchers] of the combination,
 *   must be at least 2
 */
open class EquivalenceMatcher<in T>(
    childMatchers: List<Matcher<T>>
) : CombinedMatcherBase<T>("iff"), FixedTypeCombinedMatcher<T> {
    override val childMatchers: List<Matcher<T>> = childMatchers.apply {
        require(size >= 2) { "equivalence matcher needs at least 2 arguments" }
    }.makeMutationSafe()

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val childDescriptions = childMatchers.map { it.generateExpectationTree(valueTitle, verb) }
        return if (childDescriptions.all { it.data.isAtomic })
            childDescriptions.generateForAtomics()
        else SimpleDataNode.of(
            descriptionOfCombinedCondition("satisfy all or none of the following conditions").withEnding(":")
        ) {
            childDescriptions.map {
                it.withCompletedDescription(valueTitle, verb)
            }
        }
    }

    private fun List<SimpleDataNode<ValueStateDescription>>.generateForAtomics()
        : SimpleDataNode<ValueStateDescription> {

        if (size > 2)
            return combineUsingOperatorAndSuffix("or none of them at all")
        else {
            val a1 = get(0).data
            val a2 = get(1).data
            val conjugable = a2 as? ConjugableValueStateDescription ?: a1 as? ConjugableValueStateDescription
            val other = if (a1 === conjugable) a2 else a1
            return if (conjugable != null) {
                val infiniteForm = "${other.infiniteForm} if and only if it ${conjugable.thirdPersonForm}"
                val description = if (other is ConjugableValueStateDescription) {
                    descriptionOfCombinedCondition(
                        infiniteForm,
                        "${other.thirdPersonForm} if and only if it ${conjugable.thirdPersonForm}"
                    )
                } else {
                    descriptionOfCombinedCondition(infiniteForm)
                }
                SimpleDataNode.of(description)
            } else {
                combineUsingOperatorAndSuffix("or none of both")
            }
        }
    }

    private fun List<SimpleDataNode<ValueStateDescription>>.combineUsingOperatorAndSuffix(
        suffix: String
    ): SimpleDataNode<ValueStateDescription> {
        val base = combineUsingOperatorName("and")
        val withSuffix = if (base is ConjugableValueStateDescription)
            descriptionOfCombinedCondition(
                "${base.infiniteForm} $suffix",
                "${base.thirdPersonForm} $suffix"
            )
        else
            descriptionOfCombinedCondition("${base.infiniteForm} $suffix")
        return SimpleDataNode.of(withSuffix)
    }

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val firstResult = childMatchers[0].evaluate(value)
        val results = childMatchers.mapWhile({ it.isMatch == firstResult.isMatch }, includeFirstFail = true) {
            if (it === childMatchers[0]) firstResult else it.evaluate(value)
        }
        return CombinedMatcherEvaluationNode.of(
            results.last().isMatch == firstResult.isMatch,
            results
        )
    }
}
