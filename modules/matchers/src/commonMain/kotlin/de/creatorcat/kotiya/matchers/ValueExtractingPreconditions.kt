/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Matcher based variant of the [kotlin.check] method that also extracts a value on match.
 *
 * Throws an [IllegalStateException] if the [value] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy].
 *
 * @return the value extracted from the argument on successful match
 */
fun <T, R : Any> check(value: T, matcher: ValueExtractingAtomicMatcher<T, R>): R =
    check("the value", value, matcher)

/**
 * Matcher based variant of the [kotlin.check] method that also extracts a value on match.
 *
 * Throws an [IllegalStateException] if the [value] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy], with respect to the [valueTitle].
 *
 * @return the value extracted from the argument on successful match
 */
fun <T, R : Any> check(valueTitle: String, value: T, matcher: ValueExtractingAtomicMatcher<T, R>): R {
    val result = matcher.evaluate(value)
    check(result.isMatch) {
        "${matcher.describeExpectations(valueTitle)}\nInstead, ${result.describeWhy(valueTitle)}."
    }
    return checkNotNull(result.extractedValue) { "extracting matcher did not extract value on match" }
}

/**
 * Matcher based variant of the [kotlin.require] method that also extracts a value on match.
 *
 * Throws an [IllegalArgumentException] if the [argument] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy].
 *
 * @return the value extracted from the argument on successful match
 */
fun <T, R : Any> require(argument: T, matcher: ValueExtractingAtomicMatcher<T, R>): R =
    require("the argument", argument, matcher)

/**
 * Matcher based variant of the [kotlin.require] method that also extracts a value on match.
 *
 * Throws an [IllegalArgumentException] if the [argument] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy], with respect to the [argumentTitle].
 *
 * @return the value extracted from the argument on successful match
 */
fun <T, R : Any> require(argumentTitle: String, argument: T, matcher: ValueExtractingAtomicMatcher<T, R>): R {
    val result = matcher.evaluate(argument)
    require(result.isMatch) {
        "${matcher.describeExpectations(argumentTitle)}\nInstead, ${result.describeWhy(argumentTitle)}."
    }
    return requireNotNull(result.extractedValue) { "extracting matcher did not extract value on match" }
}
