/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

private class DelegatingMatcher<in T>(
    private val target: AtomicMatcher<T>,
    lazyExpectedState: () -> ValueStateDescription,
    private val transformResult: (T, AtomicMatcherEvaluationNode) -> Pair<Boolean, () -> String>
) : AtomicMatcher<T> {
    override val expectedState: ValueStateDescription by lazy(lazyExpectedState)
    override fun evaluate(value: T): AtomicMatcherEvaluationNode {
        val (isMatch, lazyActualState) = transformResult(value, target.evaluate(value))
        return AtomicMatcherEvaluationNode.of(isMatch, this, lazyActualState)
    }
}

/**
 * Creates a new matcher based on this but with a new [expectedState][AtomicMatcher.expectedState]
 * and a new [actualState][AtomicMatcherEvaluationNode.actualState]
 * for its [evaluation result][AtomicMatcher.evaluate].
 *
 * @param newExpectedState new expected state description
 * @param newActualState Generator for the new value of `evaluate(x).actualState`.
 *   This is a function of `(evaluatedValue, originalResult) -> newActualState`.
 *   Defaults to the `originalResult.actualState`.
 */
fun <T> AtomicMatcher<T>.describedWith(
    newExpectedState: ValueStateDescription,
    newActualState: (T, AtomicMatcherEvaluationNode) -> String = { _, result -> result.actualState }
): AtomicMatcher<T> =
    DelegatingMatcher(this, { newExpectedState }) { value, result ->
        result.isMatch to { newActualState(value, result) }
    }

/**
 * Creates a new matcher based on this but with a new [expectedState][AtomicMatcher.expectedState]
 * and a new [actualState][AtomicMatcherEvaluationNode.actualState]
 * for its [evaluation result][AtomicMatcher.evaluate].
 *
 * @param newLazyExpectedState lazily evaluated new expected state description
 * @param newLazyActualState Generator for the new lazily evaluated value of `evaluate(x).actualState`.
 *   This is a function of `(evaluatedValue, originalResult) -> () -> newActualState`.
 */
fun <T> AtomicMatcher<T>.describedWith(
    newLazyExpectedState: () -> ValueStateDescription,
    newLazyActualState: (T, AtomicMatcherEvaluationNode) -> () -> String
): AtomicMatcher<T> =
    DelegatingMatcher(this, newLazyExpectedState) { value, result ->
        result.isMatch to newLazyActualState(value, result)
    }

/**
 * Creates a new matcher based on this but with a new [expectedState][AtomicMatcher.expectedState].
 *
 * @param newLazyExpectedState lazily evaluated new expected state description
 */
fun <T> AtomicMatcher<T>.describedWith(
    newLazyExpectedState: () -> ValueStateDescription
): AtomicMatcher<T> =
    DelegatingMatcher(this, newLazyExpectedState) { _, result ->
        result.isMatch to result::actualState
    }

/**
 * Creates a copy of this node whose [actualState][AtomicMatcherEvaluationNode.actualState] is prefixed with [prefix].
 */
fun AtomicMatcherEvaluationNode.withPrefixedActualState(prefix: String): AtomicMatcherEvaluationNode =
    AtomicMatcherEvaluationNode.of(isMatch, matcher) { "$prefix$actualState" }

/**
 * Creates a copy of this node whose [actualState][AtomicMatcherEvaluationNode.actualState] is prefixed
 * with the result of [lazyPrefix] once it is generated.
 */
fun AtomicMatcherEvaluationNode.withPrefixedActualState(lazyPrefix: () -> String): AtomicMatcherEvaluationNode =
    AtomicMatcherEvaluationNode.of(isMatch, matcher) { "${lazyPrefix()}$actualState" }

/**
 * Creates a copy of this node whose atomic children, grand-children etc. have their
 * [actualState][AtomicMatcherEvaluationNode.actualState] prefixed with [prefix].
 * All other properties of all children stay the same.
 */
fun CombinedMatcherEvaluationNode.withPrefixedActualState(prefix: String): CombinedMatcherEvaluationNode =
    withPrefixedActualState { prefix }

/**
 * Creates a copy of this node whose atomic children, grand-children etc. have their
 * [actualState][AtomicMatcherEvaluationNode.actualState] prefixed with the result of [lazyPrefix] once it is generated.
 * All other properties of all children stay the same.
 */
fun CombinedMatcherEvaluationNode.withPrefixedActualState(lazyPrefix: () -> String): CombinedMatcherEvaluationNode {
    val newChildren = mutableListOf<MatcherEvaluationNode>()
    val newContributors = mutableListOf<MatcherEvaluationNode>()
    for (child in children) {
        val newChild = child.withPrefixedActualState(lazyPrefix)
        newChildren += newChild
        if (resultContributors.contains(child)) newContributors += newChild
    }
    return CombinedMatcherEvaluationNode.of(isMatch, matcher, newChildren, newContributors)
}

/**
 * If this is an atomic node, creates a copy of it whose
 * [actualState][AtomicMatcherEvaluationNode.actualState] is prefixed with [prefix].
 *
 * If this is a combined node, creates a copy of this node whose atomic children, grand-children etc. have their
 * [actualState][AtomicMatcherEvaluationNode.actualState] prefixed with [prefix].
 * All other properties of all children stay the same.
 */
fun MatcherEvaluationNode.withPrefixedActualState(prefix: String): MatcherEvaluationNode =
    when (this) {
        is AtomicMatcherEvaluationNode -> withPrefixedActualState(prefix)
        is CombinedMatcherEvaluationNode -> withPrefixedActualState(prefix)
    }

/**
 * If this is an atomic node, creates a copy of it whose
 * [actualState][AtomicMatcherEvaluationNode.actualState] is prefixed
 * with the result of [lazyPrefix] once it is generated.
 *
 * If this is a combined node, creates a copy of this node whose atomic children, grand-children etc. have their
 * [actualState][AtomicMatcherEvaluationNode.actualState] prefixed with the result of [lazyPrefix] once it is generated.
 * All other properties of all children stay the same.
 */
fun MatcherEvaluationNode.withPrefixedActualState(lazyPrefix: () -> String): MatcherEvaluationNode =
    when (this) {
        is AtomicMatcherEvaluationNode -> withPrefixedActualState(lazyPrefix)
        is CombinedMatcherEvaluationNode -> withPrefixedActualState(lazyPrefix)
    }
