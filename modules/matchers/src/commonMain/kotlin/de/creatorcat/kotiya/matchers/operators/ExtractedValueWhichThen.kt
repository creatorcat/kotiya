/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.reflect.otherProperty
import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.withPrefixedActualState

/**
 * Combines this matcher and the [valueMatcher] resembling the material implication (*->*).
 * This matcher represents the **antecedent** while the other is the **consequent**.
 * However, the [valueMatcher] is only applied to the [extracted value][ValueExtractingAtomicMatcher.evaluate]
 * of this matcher's result (if it is a match).
 * All other aspects work similar to the [ordinary implication][implies].
 *
 * This allows optional type-narrowing with matchers in many forms.
 * The most common is using an [isInstanceOf][de.creatorcat.kotiya.matchers.common.isInstanceOf] matcher.
 *
 * Example:
 *
 *     val ifIntThenPrime = isInstanceOf<Int>() whichThen isPrime
 *
 * @see which
 */
infix fun <T, V : Any> ValueExtractingAtomicMatcher<T, V>.whichThen(valueMatcher: Matcher<V>): CombinedMatcher<T> =
    ValueExtractingImplication(this, valueMatcher)

private class ValueExtractingImplication<in T, out V : Any>(
    private val extractingMatcher: ValueExtractingAtomicMatcher<T, V>,
    private val valueMatcher: Matcher<V>
) : ImplicationMatcherBase<T>("extracted-value-if-then") {
    override val antecedent: Matcher<*> by otherProperty(::extractingMatcher)
    override val consequent: Matcher<*> by otherProperty(::valueMatcher)

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val leftResult = extractingMatcher.evaluate(value)
        return if (leftResult.isMatch) {
            val extractedValue = checkNotNull(leftResult.extractedValue) {
                "value extracting matcher did not extract value on match"
            }
            val rightResult = valueMatcher.evaluate(extractedValue)
                .withPrefixedActualState {
                    "led to extracted ${extractingMatcher.extractedValueTypeName} which "
                }
            if (rightResult.isMatch)
                CombinedMatcherEvaluationNode.of(true, listOf(leftResult, rightResult), listOf(rightResult))
            else
                CombinedMatcherEvaluationNode.of(false, leftResult, rightResult)
        } else {
            CombinedMatcherEvaluationNode.of(true, leftResult)
        }
    }

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val superDescriptionTree = super.generateExpectationTree(valueTitle, verb)
        val textAdjustedDescription = descriptionOfCombinedCondition(
            "lead to extracted ${extractingMatcher.extractedValueTypeName} " +
                "that $verb ${superDescriptionTree.data.infiniteForm}"
        )
        val newDescription = if (superDescriptionTree.data.endsPhrase)
        // if the super description already ends a phrase, there is already a colon but the end-state must be set
            textAdjustedDescription.withEnding("")
        else
            textAdjustedDescription

        return SimpleDataNode.of(
            newDescription,
            superDescriptionTree::children
        )
    }
}
