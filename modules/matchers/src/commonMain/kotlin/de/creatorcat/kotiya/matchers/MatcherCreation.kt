/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Base class for [AtomicMatcher] implementations with a fixed [expectedState].
 *
 * It is recommended and more convenient to use on of the *static* creation methods of the [Matcher.Companion]
 * instead of subclassing this.
 */
abstract class SimpleAtomicMatcherBase<in T>(
    override val expectedState: ValueStateDescription
) : AtomicMatcher<T>

/**
 * Base class for [AtomicMatcher] implementations with a lazily generated [expectedState].
 *
 * It is recommended and more convenient to use on of the *static* creation methods of the [Matcher.Companion]
 * instead of subclassing this.
 */
abstract class LazyAtomicMatcherBase<in T>(
    lazyExpectedState: () -> ValueStateDescription
) : AtomicMatcher<T> {
    override val expectedState: ValueStateDescription by lazy(lazyExpectedState)
}

/**
 * Creates an [AtomicMatcher] based on an inlined [body]-lambda.
 * The `body` must produce a pair of `(isMatch, actualStateProducer)`.
 * The result of [Matcher.evaluate] will be generated as atomic node from this pair as follows:
 * * `isMatch`: Indicates whether the result is a [match][MatcherEvaluationNode.isMatch]
 * * `actualStateProducer`: Used to lazily create the [AtomicMatcherEvaluationNode.actualState]
 *
 * @param lazyExpectedState used to lazily create the [AtomicMatcher.expectedState]
 * @param body inlined lambda producing a pair of `(isMatch, actualStateProducer)`
 */
inline fun <T> Matcher.Companion.of(
    noinline lazyExpectedState: () -> ValueStateDescription,
    crossinline body: (T) -> Pair<Boolean, () -> String>
): AtomicMatcher<T> =
    object : LazyAtomicMatcherBase<T>(lazyExpectedState) {
        override fun evaluate(value: T): AtomicMatcherEvaluationNode {
            val (isMatch, lazyActualState) = body(value)
            return AtomicMatcherEvaluationNode.of(isMatch, this, lazyActualState)
        }
    }

/**
 * Creates an [AtomicMatcher] based on an inlined [body]-lambda.
 * The `body` must produce a pair of `(isMatch, actualState)`.
 * The result of [Matcher.evaluate] will be generated as atomic node from this pair as follows:
 * * `isMatch`: Indicates whether the result is a [match][MatcherEvaluationNode.isMatch]
 * * `actualState`: Value of [AtomicMatcherEvaluationNode.actualState]
 *
 * @param expectedState the [AtomicMatcher.expectedState]
 * @param body inlined lambda producing a pair of `(isMatch, actualState)`
 */
inline fun <T> Matcher.Companion.of(
    expectedState: ValueStateDescription,
    crossinline body: (T) -> Pair<Boolean, String>
): AtomicMatcher<T> =
    object : SimpleAtomicMatcherBase<T>(expectedState) {
        override fun evaluate(value: T): AtomicMatcherEvaluationNode {
            val (isMatch, actualState) = body(value)
            return AtomicMatcherEvaluationNode.of(isMatch, this, actualState)
        }
    }

/**
 * Creates an [AtomicMatcher] based on an inlined [body]-lambda.
 * The `body` must produce a pair of `(isMatch, actualStateProducer)`.
 * The result of [Matcher.evaluate] will be generated as atomic node from this pair as follows:
 * * `isMatch`: Indicates whether the result is a [match][MatcherEvaluationNode.isMatch]
 * * `actualStateProducer`: Used to lazily create the [AtomicMatcherEvaluationNode.actualState]
 *
 * @param expectedState the [AtomicMatcher.expectedState]
 * @param body inlined lambda producing a pair of `(isMatch, actualStateProducer)`
 */
inline fun <T> Matcher.Companion.ofLazyActualState(
    expectedState: ValueStateDescription,
    crossinline body: (T) -> Pair<Boolean, () -> String>
): AtomicMatcher<T> =
    object : SimpleAtomicMatcherBase<T>(expectedState) {
        override fun evaluate(value: T): AtomicMatcherEvaluationNode {
            val (isMatch, lazyActualState) = body(value)
            return AtomicMatcherEvaluationNode.of(isMatch, this, lazyActualState)
        }
    }

