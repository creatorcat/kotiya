/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.of

/**
 * Creates a matcher that checks if an iterable contains only [allowedElements] i.e.
 * for each element `e` in the evaluated iterable `allowedElements.contains(e)` must be `true`.
 *
 * Note, that equal elements may appear multiple times and not all allowed elements must be contained.
 * The matcher is fail-fast: it fails at the first found disallowed element.
 *
 * @see containsAll
 * @see containsAllAndOnly
 */
fun <E> containsOnly(allowedElements: Iterable<E>): AtomicMatcher<Iterable<E>> =
    Matcher.of({ descriptionOfAtomicCondition("only contain elements of ${allowedElements.toListString()}") }) {
        val actualIterator = it.iterator()
        var idx = 0
        var problem: (() -> String)? = null
        while (problem == null && actualIterator.hasNext()) {
            val actualVal = actualIterator.next()
            if (!allowedElements.contains(actualVal))
                problem = { "has forbidden element $actualVal at index $idx" }
            else
                idx++
        }
        Pair(problem == null, problem ?: { "only contains elements of ${allowedElements.toListString()}" })
    }

/**
 * Creates a matcher that checks if an iterable contains all [requiredElements] i.e.
 * for each required element `e` the evaluated iterable `it`, the expression `it.contains(e)` must be `true`.
 *
 * Note, that the iterable may contain more and other elements than the required.
 * The matcher is fail-fast: it fails at the first missing required element.
 *
 * @see containsOnly
 * @see containsAllAndOnly
 */
fun <E> containsAll(requiredElements: Iterable<E>): AtomicMatcher<Iterable<E>> =
    Matcher.of({ descriptionOfAtomicCondition("contain all elements of ${requiredElements.toListString()}") }) {
        val reqIterator = requiredElements.iterator()
        var problem: (() -> String)? = null
        while (problem == null && reqIterator.hasNext()) {
            val reqVal = reqIterator.next()
            if (!it.contains(reqVal))
                problem = { "does not contain required element $reqVal" }
        }
        Pair(problem == null, problem ?: { "contains all elements of ${requiredElements.toListString()}" })
    }

/**
 * Creates a conjugation of [containsAll] and [containsOnly] with the given [elements]
 * being both, required and allowed.
 * This actually means, that the evaluated iterable is equal to [elements] disregarding
 * element order and multiplicity.
 */
fun <E> containsAllAndOnly(elements: Iterable<E>): CombinedMatcher<Iterable<E>> =
    containsAll(elements) and containsOnly(elements)


