/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.of

/**
 * Matcher that checks if an integer is positive and thus can be converted to an unsigned integer without problems.
 * On successful match the argument is extracted as [UInt].
 */
@ExperimentalUnsignedTypes
val isPositive: ValueExtractingAtomicMatcher<Int, UInt> =
    ValueExtractingAtomicMatcher.of(descriptionOfBeing("positive (>= 0)")) {
        Pair(if (it >= 0) it.toUInt() else null, "is $it")
    }


