/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */


package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.descriptionOfNotBeing
import de.creatorcat.kotiya.matchers.of

/**
 * Creates a matcher that checks if a value is equal to the [expected].
 */
fun <T> isEqual(expected: T): AtomicMatcher<T> =
    Matcher.of({ descriptionOfBeing("equal $expected") }) {
        Pair(it == expected, { "is $it" })
    }

/**
 * Creates a matcher that checks if a value is not equal to the [unexpected].
 */
fun <T> isNotEqual(unexpected: T): AtomicMatcher<T> =
    Matcher.of({ descriptionOfNotBeing("equal $unexpected") }) {
        Pair(it != unexpected, { "is $it" })
    }

/**
 * Creates a matcher that checks if a value is identical to the [expected] (`===`).
 */
fun <T> isSameAs(expected: T): AtomicMatcher<T> =
    Matcher.of({ descriptionOfBeing("identical to $expected") }) {
        Pair(it === expected, { "is ${"not ".orEmptyIf(it === expected)}identical to $expected" })
    }

/**
 * Creates a matcher that checks if a value is not identical to the [unexpected] (`!==`).
 */
fun <T> isNotSameAs(unexpected: T): AtomicMatcher<T> =
    Matcher.of({ descriptionOfNotBeing("identical to $unexpected") }) {
        Pair(it !== unexpected, { "is ${"not ".orEmptyIf(it === unexpected)}identical to $unexpected" })
    }
