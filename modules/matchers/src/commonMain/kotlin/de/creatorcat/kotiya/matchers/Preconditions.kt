/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Matcher based variant of the [kotlin.check] method.
 *
 * Throws an [IllegalStateException] if the [value] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy].
 *
 * @return the checked value for further safe usage
 */
fun <T> check(value: T, matcher: Matcher<T>): T = check("the value", value, matcher)

/**
 * Matcher based variant of the [kotlin.check] method.
 *
 * Throws an [IllegalStateException] if the [value] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy], with respect to the [valueTitle].
 *
 * @return the checked value for further safe usage
 */
fun <T> check(valueTitle: String, value: T, matcher: Matcher<T>): T {
    val result = matcher.evaluate(value)
    check(result.isMatch) {
        "${matcher.describeExpectations(valueTitle)}\nInstead, ${result.describeWhy(valueTitle)}."
    }
    return value
}

/**
 * Matcher based variant of the [kotlin.require] method.
 *
 * Throws an [IllegalArgumentException] if the [argument] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy].
 *
 * @return the checked argument for further safe usage
 */
fun <T> require(argument: T, matcher: Matcher<T>): T = require("the argument", argument, matcher)

/**
 * Matcher based variant of the [kotlin.require] method.
 *
 * Throws an [IllegalArgumentException] if the [argument] does not satisfy the [matcher].
 * The exception message is generated using the [expectation description][Matcher.describeExpectations]
 * and the [failure description][MatcherEvaluationNode.describeWhy], with respect to the [argumentTitle].
 *
 * @return the checked argument for further safe usage
 */
fun <T> require(argumentTitle: String, argument: T, matcher: Matcher<T>): T {
    val result = matcher.evaluate(argument)
    require(result.isMatch) {
        "${matcher.describeExpectations(argumentTitle)}\nInstead, ${result.describeWhy(argumentTitle)}."
    }
    return argument
}
