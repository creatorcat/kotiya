/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Extension of the [AtomicMatcherEvaluationNode] that holds an [extractedValue] that was
 * produced during the evaluation of a [ValueExtractingAtomicMatcher].
 */
abstract class AtomicMatcherEvaluationNodeWithValue<out V : Any> : AtomicMatcherEvaluationNode() {
    /**
     * Value extracted on [matcher evaluation][ValueExtractingAtomicMatcher.evaluate].
     * Is `null` if evaluation fails and must be non-null otherwise.
     */
    abstract val extractedValue: V?

    /** The companion object provides methods to easily create nodes. */
    companion object
}

private class SimpleNodeWithValue<out V : Any>(
    override val isMatch: Boolean,
    override val matcher: AtomicMatcher<*>,
    override val actualState: String,
    override val extractedValue: V?
) : AtomicMatcherEvaluationNodeWithValue<V>()

private class LazyStateNodeWithValue<out V : Any>(
    override val isMatch: Boolean,
    override val matcher: AtomicMatcher<*>,
    lazyActualState: () -> String,
    override val extractedValue: V?
) : AtomicMatcherEvaluationNodeWithValue<V>() {
    override val actualState by lazy(lazyActualState)
}

/**
 * Creates an [AtomicMatcherEvaluationNodeWithValue] with given properties.
 *
 * @param isMatch indicates the [match result][AtomicMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][AtomicMatcherEvaluationNode.matcher]
 * @param extractedValue value extracted during evaluation (see [AtomicMatcherEvaluationNodeWithValue.extractedValue])
 * @param actualState description of the [actual state][AtomicMatcherEvaluationNode.actualState] of the evaluated value
 */
fun <V : Any> AtomicMatcherEvaluationNodeWithValue.Companion.of(
    isMatch: Boolean,
    matcher: AtomicMatcher<*>,
    extractedValue: V?,
    actualState: String
): AtomicMatcherEvaluationNodeWithValue<V> =
    SimpleNodeWithValue(isMatch, matcher, actualState, extractedValue)

/**
 * Creates an [AtomicMatcherEvaluationNodeWithValue] with given properties and a lazily evaluated actual state.
 *
 * @param isMatch indicates the [match result][AtomicMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][AtomicMatcherEvaluationNode.matcher]
 * @param extractedValue value extracted during evaluation (see [AtomicMatcherEvaluationNodeWithValue.extractedValue])
 * @param lazyActualState lazily evaluated description of the
 *   [actual state][AtomicMatcherEvaluationNode.actualState] of the evaluated value
 */
fun <V : Any> AtomicMatcherEvaluationNodeWithValue.Companion.of(
    isMatch: Boolean,
    matcher: AtomicMatcher<*>,
    extractedValue: V?,
    lazyActualState: () -> String
): AtomicMatcherEvaluationNodeWithValue<V> =
    LazyStateNodeWithValue(isMatch, matcher, lazyActualState, extractedValue)
