/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.matchers.ConjunctionMatcher
import de.creatorcat.kotiya.matchers.DisjunctionMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.not

/**
 * Creates a matcher that applies iff all [matchers] apply.
 *
 * Actually creates a [ConjunctionMatcher] with the matchers as its children.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isAllOf(matchers: List<Matcher<T>>): ConjunctionMatcher<T> =
    ConjunctionMatcher(matchers)

/**
 * Creates a matcher that applies iff all [matchers] apply.
 *
 * Actually creates a [ConjunctionMatcher] with the matchers as its children.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isAllOf(vararg matchers: Matcher<T>): ConjunctionMatcher<T> =
    isAllOf(matchers.toList())

/**
 * Creates a matcher that applies iff any of the [matchers] applies.
 *
 * Actually creates a [DisjunctionMatcher] with the matchers as its children.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isAnyOf(matchers: List<Matcher<T>>): DisjunctionMatcher<T> =
    DisjunctionMatcher(matchers)

/**
 * Creates a matcher that applies iff any of the [matchers] applies.
 *
 * Actually creates a [DisjunctionMatcher] with the matchers as its children.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isAnyOf(vararg matchers: Matcher<T>): DisjunctionMatcher<T> =
    isAnyOf(matchers.toList())

/**
 * Creates a matcher that applies iff none of the [matchers] applies.
 *
 * Shortcut to `!isAnyOf(...)`.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isNoneOf(matchers: List<Matcher<T>>): Matcher<T> =
    !DisjunctionMatcher(matchers)

/**
 * Creates a matcher that applies iff none of the [matchers] applies.
 *
 * Shortcut to `!isAnyOf(...)`.
 *
 * @param T input value type of all matchers and the resulting combined matcher
 */
fun <T> isNoneOf(vararg matchers: Matcher<T>): Matcher<T> =
    isNoneOf(matchers.toList())
