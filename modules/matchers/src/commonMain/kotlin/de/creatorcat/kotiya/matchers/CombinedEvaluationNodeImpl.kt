/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.makeMutationSafe

private class SimpleCombinedEvaluationNode(
    override val isMatch: Boolean,
    override val matcher: CombinedMatcher<*>,
    override val children: List<MatcherEvaluationNode>,
    override val resultContributors: List<MatcherEvaluationNode>
) : CombinedMatcherEvaluationNode()

/**
 * Creates an [CombinedMatcherEvaluationNode] with all children contributing to the result
 * (see [Matcher.evaluate]).
 *
 * @param isMatch indicates the [match result][CombinedMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][CombinedMatcherEvaluationNode.matcher]
 * @param contributingChildren the [children][CombinedMatcherEvaluationNode.children] of this node,
 *   which are also the [contributing children][CombinedMatcherEvaluationNode.resultContributors]
 *   (must not be empty)
 */
fun CombinedMatcherEvaluationNode.Companion.of(
    isMatch: Boolean,
    matcher: CombinedMatcher<*>,
    vararg contributingChildren: MatcherEvaluationNode
): CombinedMatcherEvaluationNode {
    val children = contributingChildren.asList().apply {
        if (isEmpty()) throw IllegalArgumentException("combined node has no children")
    }
    return SimpleCombinedEvaluationNode(isMatch, matcher, children, children)
}

/**
 * Creates an [CombinedMatcherEvaluationNode] with only a subset of contributing children
 * (see [Matcher.evaluate]).
 *
 * @param isMatch indicates the [match result][CombinedMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][CombinedMatcherEvaluationNode.matcher]
 * @param allChildren list of [children][CombinedMatcherEvaluationNode.children] (must not be empty)
 * @param contributingChildren list of [contributing children][CombinedMatcherEvaluationNode.resultContributors],
 *   must be a subset of [allChildren] and not be empty
 */
fun CombinedMatcherEvaluationNode.Companion.of(
    isMatch: Boolean,
    matcher: CombinedMatcher<*>,
    allChildren: List<MatcherEvaluationNode>,
    contributingChildren: List<MatcherEvaluationNode> = allChildren
): CombinedMatcherEvaluationNode {
    if (allChildren.isEmpty()) throw IllegalArgumentException("combined node has no children")
    if (contributingChildren.isEmpty() || !allChildren.containsAll(contributingChildren))
        throw IllegalArgumentException("contributing children are not non-empty subset of children")

    return SimpleCombinedEvaluationNode(
        isMatch, matcher,
        allChildren.makeMutationSafe(), contributingChildren.makeMutationSafe()
    )
}
