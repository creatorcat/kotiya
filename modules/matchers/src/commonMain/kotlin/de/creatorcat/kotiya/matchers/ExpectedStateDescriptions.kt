/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.structures.walkAndCountDepthFirst

/**
 * Generates a human readable text that describes what is expected of a value to satisfy this matcher.
 *
 * Based on the matcher's [expectation tree][Matcher.generateExpectationTree],
 * a hierarchical list structure is created, containing one or many description entries (lines).
 * The entries reflect the underlying atomic matchers and the operators that where used to form this matcher.
 *
 * TODO more doc
 *
 * @param valueTitle title to be used when denoting the evaluated value in the description;
 *   defaults to `"the value"`
 * @param verb verb initiating the expectation phrase, e.g. "must", "should" or "is expected to";
 *   defaults to `"must"`
 * @param bullet bullet string starting each list entry (possibly multiplied); defaults to `"*"`
 */
fun Matcher<*>.describeExpectations(
    valueTitle: String = "the value", verb: String = "must", bullet: String = "*"
): String {
    val exTree = generateExpectationTree(valueTitle, verb)
    val firstPhrase = exTree.data.withCompletion(valueTitle, verb).infiniteForm
    return if (exTree.isLeaf) firstPhrase else
        firstPhrase + exTree.walkAndCountDepthFirst().drop(1)
            .joinToString(prefix = "\n", separator = "\n") { (node, depth) ->
                val prefix = "${bullet.repeat(depth)} "
                "$prefix${node.data.infiniteForm}"
            }
}
