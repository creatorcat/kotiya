/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import kotlin.reflect.KClass

/**
 * Extension of the [AtomicMatcher] that extracts a value of type [V] during the [evaluation][evaluate]
 * if the evaluated value is matched successfully.
 * Such matchers allow further usage of the match result e.g. to apply special matchers on the extracted value.
 */
interface ValueExtractingAtomicMatcher<in T, out V : Any> : AtomicMatcher<T> {

    /**
     * Extends the [evaluation][AtomicMatcher.evaluate] by extracting a value of type [V]
     * if the evaluated value is matched successfully.
     */
    override fun evaluate(value: T): AtomicMatcherEvaluationNodeWithValue<V>

    /**
     * Human readable type name of the extracted value.
     * To be used when [generating descriptions][generateExpectationTree].
     */
    val extractedValueTypeName: String

    /** The companion object provides methods to easily create matchers. */
    companion object {

        /**
         * Generates a best-guess type name description for this class to be used as [extractedValueTypeName].
         * If this is an ordinary class, [KClass.simpleName] is returned.
         * If no simple name can be generated the type is named `"Object"`.
         */
        val KClass<*>.defaultTypeName: String
            get() =
                if (this == Any::class) "Object"
                else this.simpleName ?: "Object"
    }
}

/**
 * Creates a [ValueExtractingAtomicMatcher] based on an inlined [body]-lambda.
 * The `body` must produce a pair of `(extractedValue, actualState)`.
 * The result of [evaluate][ValueExtractingAtomicMatcher.evaluate] will be generated as atomic node
 * from this pair as follows:
 * * `extractedValue`: defines the result's [extractedValue][AtomicMatcherEvaluationNodeWithValue.extractedValue]
 *   and if it is `null` the result will be a mismatch, otherwise a match
 * * `actualState`: the result's [actualState][AtomicMatcherEvaluationNodeWithValue.actualState]
 *
 * The matcher's [extractedValueTypeName][ValueExtractingAtomicMatcher.extractedValueTypeName]
 * will be the [defaultTypeName][ValueExtractingAtomicMatcher.Companion.defaultTypeName] of type [V].
 *
 * @param expectedState the matcher's [expectedState][ValueExtractingAtomicMatcher.expectedState]
 * @param body inlined lambda producing a pair of `(extractedValue, actualState)`
 */
inline fun <T, reified V : Any> ValueExtractingAtomicMatcher.Companion.of(
    expectedState: ValueStateDescription,
    noinline body: (T) -> Pair<V?, String>
): ValueExtractingAtomicMatcher<T, V> {
    return of(expectedState, V::class.defaultTypeName, body)
}

/**
 * Creates a [ValueExtractingAtomicMatcher] based on an inlined [body]-lambda.
 * The `body` must produce a pair of `(extractedValue, actualState)`.
 * The result of [evaluate][ValueExtractingAtomicMatcher.evaluate] will be generated as atomic node
 * from this pair as follows:
 * * `extractedValue`: defines the result's [extractedValue][AtomicMatcherEvaluationNodeWithValue.extractedValue]
 *   and if it is `null` the result will be a mismatch, otherwise a match
 * * `actualState`: the result's [actualState][AtomicMatcherEvaluationNodeWithValue.actualState]
 *
 * @param expectedState the matcher's [expectedState][ValueExtractingAtomicMatcher.expectedState]
 * @param extractedValueTypeName the matcher's
 *   [extractedValueTypeName][ValueExtractingAtomicMatcher.extractedValueTypeName]
 * @param body inlined lambda producing a pair of `(extractedValue, actualState)`
 */
fun <T, V : Any> ValueExtractingAtomicMatcher.Companion.of(
    expectedState: ValueStateDescription,
    extractedValueTypeName: String,
    body: (T) -> Pair<V?, String>
): ValueExtractingAtomicMatcher<T, V> =
    object : SimpleAtomicMatcherBase<T>(expectedState), ValueExtractingAtomicMatcher<T, V> {
        override val extractedValueTypeName: String = extractedValueTypeName

        override fun evaluate(value: T): AtomicMatcherEvaluationNodeWithValue<V> {
            val (extractedValue, actualState) = body(value)
            return AtomicMatcherEvaluationNodeWithValue.of(extractedValue != null, this, extractedValue, actualState)
        }
    }
