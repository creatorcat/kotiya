/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.mapWhile
import de.creatorcat.kotiya.core.structures.of

/**
 * Creates a fail-fast conjunction of this matcher and the [other].
 *
 * Actually creates a [ConjunctionMatcher] with this matcher and the [other] as its children.
 */
infix fun <T> Matcher<T>.and(other: Matcher<T>): ConjunctionMatcher<T> =
    ConjunctionMatcher(listOf(this, other))

/**
 * Represents a fail-fast conjunction (logical "and") of the given [childMatchers].
 *
 * The resulting matcher [evaluates][evaluate] its [childMatchers] in the order they appear in the list.
 * If any of them produces a [mismatch][MatcherEvaluationNode.isMismatch],
 * evaluation fails immediately and the result of this matcher will be the only
 * [contributing result][CombinedMatcherEvaluationNode.resultContributors].
 * If all matchers produce a [match][MatcherEvaluationNode.isMatch],
 * the final result will also be a match and all child results are contributing.
 *
 * The creation is optimized based on the operator associativity.
 * If any of the [childMatchers] is already a `ConjunctionMatcher`,
 * the children of this matcher will be added directly to the new conjunction instead of the matcher itself.
 * Thus, associativity holds: `(a and b) and c == a and (b and c) == a and b and c`.
 *
 * @param childMatchers the [child matchers][CombinedMatcher.childMatchers] of the combination,
 *   must be at least 2
 */
open class ConjunctionMatcher<in T>(
    childMatchers: List<Matcher<T>>
) : XJunctionMatcherBase<T>("and", checkAndOptimizeCombinedMatcherChildren<T, ConjunctionMatcher<T>>(childMatchers)) {
    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> =
        generateXJunctionTree("satisfy all of the following conditions", "it", verb)

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val results = childMatchers.mapWhile(MatcherEvaluationNode::isMatch, includeFirstFail = true) {
            it.evaluate(value)
        }
        val lastResult = results.last()
        return if (lastResult.isMatch)
            CombinedMatcherEvaluationNode.of(true, results)
        else
            CombinedMatcherEvaluationNode.of(false, results, listOf(lastResult))
    }
}

/**
 * Creates a succeed-fast disjunction of this matcher and the [other].
 *
 * Actually creates a [DisjunctionMatcher] with this matcher and the [other] as its children.
 */
infix fun <T> Matcher<T>.or(other: Matcher<T>): DisjunctionMatcher<T> =
    DisjunctionMatcher(listOf(this, other))

/**
 * Represents a succeed-fast disjunction (logical "or") of the given [childMatchers].
 *
 * The resulting matcher [evaluates][evaluate] its [childMatchers] in the order they appear in the list.
 * If any of them produces a [match][MatcherEvaluationNode.isMatch],
 * evaluation succeeds immediately and the result of this matcher will be the only
 * [contributing result][CombinedMatcherEvaluationNode.resultContributors].
 * If all matchers produce a [mismatch][MatcherEvaluationNode.isMismatch],
 * the final result will also be a mismatch and all child results are contributing.
 *
 * The creation is optimized based on the operator associativity.
 * If any of the [childMatchers] is already a `DisjunctionMatcher`,
 * the children of this matcher will be added directly to the new disjunction instead of the matcher itself.
 * Thus, associativity holds: `(a or b) or c == a or (b or c) == a or b or c` .
 *
 * @param childMatchers the [child matchers][CombinedMatcher.childMatchers] of the combination,
 *   must be at least 2
 */
open class DisjunctionMatcher<in T>(
    childMatchers: List<Matcher<T>>
) : XJunctionMatcherBase<T>("or", checkAndOptimizeCombinedMatcherChildren<T, DisjunctionMatcher<T>>(childMatchers)) {
    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> =
        generateXJunctionTree("satisfy any of the following conditions", "it", verb)

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val results = childMatchers.mapWhile(MatcherEvaluationNode::isMismatch, includeFirstFail = true) {
            it.evaluate(value)
        }
        val lastResult = results.last()
        return if (lastResult.isMismatch)
            CombinedMatcherEvaluationNode.of(false, results)
        else
            CombinedMatcherEvaluationNode.of(true, results, listOf(lastResult))
    }
}

/**
 * Base class for the matcher combinations [ConjunctionMatcher] and [DisjunctionMatcher].
 * Not to be instantiated from outside the module.
 *
 * @constructor For internal usage only.
 * @param combinationOperatorName short name of the associated operator like "and" or "or",
 *   also used as [CombinedMatcher.combinationOperatorName]
 * @param childMatchers the [child matchers][CombinedMatcher.childMatchers] of the combination,
 *   must be checked and optimized before, see [checkAndOptimizeCombinedMatcherChildren]
 */
abstract class XJunctionMatcherBase<in T> internal constructor(
    combinationOperatorName: String,
    override val childMatchers: List<Matcher<T>>
) : CombinedMatcherBase<T>(combinationOperatorName), FixedTypeCombinedMatcher<T> {

    /**
     * Generates a proper description tree.
     * If all child descriptions are atomic, they are [combined][combineUsingOperatorName]
     * using the [combineUsingOperatorName].
     * Otherwise, the [introPhrase] is used to create a new description node (with ending),
     * whose children are the [completed][ValueStateDescription.withCompletion] descriptions of all [childMatchers].
     *
     * The [valueTitle] and [verb] will be applied on description generation and completion if necessary.
     */
    internal fun generateXJunctionTree(
        introPhrase: String,
        valueTitle: String,
        verb: String
    ): SimpleDataNode<ValueStateDescription> {
        val childDescriptions = childMatchers.map { it.generateExpectationTree(valueTitle, verb) }
        if (childDescriptions.all { it.data.isAtomic }) {
            return SimpleDataNode.of(childDescriptions.combineUsingOperatorName(combinationOperatorName))
        }
        return SimpleDataNode.of(descriptionOfCombinedCondition(introPhrase).withEnding(":")) {
            childDescriptions.map {
                it.withCompletedDescription(valueTitle, verb)
            }
        }
    }
}
