/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.TreeNode

/**
 * A node in the [matcher evaluation tree][Matcher.evaluate],
 * that corresponds to a matcher that was actually applied on the evaluated value to render the result.
 *
 * A matcher may be atomic or created by combining several matchers.
 * The resulting structure is best described by a tree where each matcher represents one node.
 * The [evaluation][Matcher.evaluate] of a value can similarly be understood as searching
 * a path of matchers that apply to the value in the tree, from root to leaves.
 * This leads to the *evaluation tree*.
 *
 * Thus, each node represents either an atomic matcher and thus a leaf of the tree ([AtomicMatcherEvaluationNode])
 * or a combined matcher ([CombinedMatcherEvaluationNode]).
 *
 * To create atomic nodes use the "static" methods provided via the [AtomicMatcherEvaluationNode.Companion].
 * For combined nodes use one of the "static" methods provided via [CombinedMatcherEvaluationNode.Companion].
 */
sealed class MatcherEvaluationNode : TreeNode<MatcherEvaluationNode> {
    /**
     * `True` iff the evaluation did succeed and produced a *match* at this point in the tree.
     * If the node is atomic, it means the corresponding [matcher] did match.
     * If this is a combined node, it means the combination of the results
     * of the [children] produces a match and the node corresponds to a [CombinedMatcher].
     */
    abstract val isMatch: Boolean

    /**
     * The nodes corresponding [Matcher].
     * This matcher was applied to produce this node and especially the [match result][isMatch].
     */
    // TODO design info:
    // the matcher cannot be typed since combinations of different types are possible in a tree
    // and thus the type cannot be determined in general
    abstract val matcher: Matcher<*>

    /**
     * Creates a string including the string representation of the [isMatch] state.
     */
    abstract override fun toString(): String
}

/**
 * Alternative to [!isMatch][MatcherEvaluationNode.isMatch].
 */
val MatcherEvaluationNode.isMismatch: Boolean get() = !isMatch

/**
 * Base class of combined evaluation result nodes.
 *
 * To create nodes use one of the "static" methods provided via the [CombinedMatcherEvaluationNode.Companion].
 */
abstract class CombinedMatcherEvaluationNode : MatcherEvaluationNode() {

    abstract override val matcher: CombinedMatcher<*>

    /**
     * Subset of the [children] that actually contributes to the result (see [Matcher.evaluate]).
     */
    abstract val resultContributors: List<MatcherEvaluationNode>

    /**
     * Creates a string including the string representation of the [isMatch] state,
     * the [corresponding matcher's][matcher] [combination operator][CombinedMatcher.combinationOperatorName]
     * and all [children].
     */
    override fun toString(): String =
        "${generateMatchString()}: ${matcher.combinationOperatorName}(${children.joinToString(", ")})\n"

    /** The companion object provides methods to easily create nodes. */
    companion object
}

/**
 * Base class of an atomic evaluation result node.
 *
 * Being the leaves of the evaluation tree, atomic evaluated nodes describe the [actualState] of
 * the evaluated value during the [corresponding matcher's][matcher] evaluation call.
 *
 * To create atomic nodes use the "static" methods provided via the [AtomicMatcherEvaluationNode.Companion].
 */
abstract class AtomicMatcherEvaluationNode : MatcherEvaluationNode() {

    abstract override val matcher: AtomicMatcher<*>

    /** Atomic nodes have no children so this is always empty. */
    final override val children: List<MatcherEvaluationNode> get() = emptyList()

    /**
     * Human readable description of the actual state of the evaluated value during the
     * [Matcher.evaluate] call. This corresponds to the [expected state][AtomicMatcher.expectedState]
     * and should give the end user information about what exactly went wrong or right.
     *
     * The text must be the completing part of a sentence that starts with a phrase like
     * *"The value should be equal 17 but it"* or *"The value must start with 'abc' but it"*.
     * For example: `"is 13"` or ```"starts with ab[d]"```.
     *
     * The full description may then be generated to something like
     * *"The value should be equal 17 but it is 13"*.
     */
    abstract val actualState: String

    override fun toString(): String = "${generateMatchString()}: atomic"

    /** The companion object provides methods to easily create nodes. */
    companion object
}

private fun MatcherEvaluationNode.generateMatchString() = if (isMatch) "match" else "mismatch"
    
