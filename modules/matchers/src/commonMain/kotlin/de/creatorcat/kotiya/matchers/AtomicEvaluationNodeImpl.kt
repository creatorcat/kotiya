/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

private class SimpleAtomicEvaluationNode(
    override val isMatch: Boolean,
    override val matcher: AtomicMatcher<*>,
    override val actualState: String
) : AtomicMatcherEvaluationNode()

private class LazyAtomicEvaluationNode(
    override val isMatch: Boolean,
    override val matcher: AtomicMatcher<*>,
    lazyActualState: () -> String
) : AtomicMatcherEvaluationNode() {
    override val actualState: String by lazy(lazyActualState)
}

/**
 * Creates an [AtomicMatcherEvaluationNode] with given properties.
 *
 * @param isMatch indicates the [match result][AtomicMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][AtomicMatcherEvaluationNode.matcher]
 * @param actualState description of the [actual state][AtomicMatcherEvaluationNode.actualState] of the evaluated value
 */
fun AtomicMatcherEvaluationNode.Companion.of(
    isMatch: Boolean,
    matcher: AtomicMatcher<*>,
    actualState: String
): AtomicMatcherEvaluationNode =
    SimpleAtomicEvaluationNode(isMatch, matcher, actualState)

/**
 * Creates an [AtomicMatcherEvaluationNode] with given properties and a lazily evaluated actual state.
 *
 * @param isMatch indicates the [match result][AtomicMatcherEvaluationNode.isMatch]
 * @param matcher the associated [matcher][AtomicMatcherEvaluationNode.matcher]
 * @param lazyActualState lazily evaluated description of the
 *   [actual state][AtomicMatcherEvaluationNode.actualState] of the evaluated value
 */
fun AtomicMatcherEvaluationNode.Companion.of(
    isMatch: Boolean,
    matcher: AtomicMatcher<*>,
    lazyActualState: () -> String
): AtomicMatcherEvaluationNode =
    LazyAtomicEvaluationNode(isMatch, matcher, lazyActualState)
