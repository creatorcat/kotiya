/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

/**
 * Human readable description of what state a value must be in
 * or what conditions must apply to it, to satisfy a [Matcher].
 *
 * The description must be usable as part of a sentence.
 * See the documentation of [infiniteForm] for more information.
 *
 * To create descriptions use one of the `descriptionOf*` methods.
 */
interface ValueStateDescription {
    /**
     * Expected state description with infinite verb form.
     *
     * The text must be a part of a sentence that starts with a phrase like
     * *"The value must"* or *"The value is expected to"*.
     * For example: `"be equal 3"`, `"be less than 17"` or `"start with A"`.
     *
     * The full description may then be generated to something like *"The value must be equal 3."*
     */
    val infiniteForm: String

    /**
     * Indicates whether the textual phrase of this description is atomic, i.e.
     * describes only a single condition or state.
     * Atomic descriptions can be combined with other description phrases
     * to a combined phrase that describes a combination of conditions or states.
     *
     * Example:
     * The phrases "be less than 10" and "be even" may be combined to "be less than 10 and be even".
     * The resulting phrase "be less than 10 and be even" is no longer atomic and combinations may
     * produce ambiguity: "be less than 10 and be even or be greater than 20" - which operator has priority?
     *
     * To avoid ambiguity, only atomic phrases should be combined in the same description.
     */
    val isAtomic: Boolean

    /**
     * Whether this descriptions contains the start of a sentence, e.g. "The value must be equal 3...".
     *
     * @see endsPhrase
     * @see isComplete
     */
    val startsPhrase: Boolean

    /**
     * Whether this descriptions contains the end of a sentence, usually a period, e.g. "... be equal 3.".
     *
     * @see startsPhrase
     * @see isComplete
     */
    val endsPhrase: Boolean

    /**
     * Returns a new description based on this but with the given [phraseEnd].
     * The new description will not be [atomic][isAtomic].
     * If this description already [ends a phrase][endsPhrase],
     * nothing will be done and the result will be this description.
     *
     * Subclasses should override to return a proper subclass result.
     */
    fun withEnding(phraseEnd: String = "."): ValueStateDescription =
        if (endsPhrase) this else
            SimpleDescription("$infiniteForm$phraseEnd", false, startsPhrase, true)

    /**
     * Returns a new description based on this but with a starting phrase made
     * by concatenation of [valueTitle] and [verb] (see [Matcher.generateExpectationTree]).
     * The new description will not be [atomic][isAtomic].
     * If this description does already [start a phrase][startsPhrase],
     * nothing will be done and the result will be this description.
     *
     * Subclasses should override to return a proper subclass result.
     */
    fun withStart(valueTitle: String, verb: String): ValueStateDescription =
        if (startsPhrase) this else
            SimpleDescription("${valueTitle.capitalize()} $verb $infiniteForm", false, true, endsPhrase)
}

/**
 * Check whether this description is complete i.e. it [starts a phrase][ValueStateDescription.startsPhrase]
 * and [ends a phrase][ValueStateDescription.endsPhrase].
 */
val ValueStateDescription.isComplete: Boolean get() = startsPhrase && endsPhrase

/**
 * Returns a new description based on this but with a starting and an ending phrase.
 * This is just a convenience call to [ValueStateDescription.withStart] and
 * [ValueStateDescription.withEnding] consecutively.
 */
fun ValueStateDescription.withCompletion(
    valueTitle: String, verb: String, phraseEnd: String = "."
): ValueStateDescription =
    withStart(valueTitle, verb).withEnding(phraseEnd)

/**
 * Extension of the [ValueStateDescription] that supports different grammatical (conjugated) forms of the description.
 * See the documentation of each property for more information.
 *
 * To create descriptions use one of the `descriptionFor*` methods.
 */
interface ConjugableValueStateDescription : ValueStateDescription {

    /**
     * Expected state description with verb in third person.
     *
     * The text must be the completing part of a sentence that starts with a phrase like
     * *"If the value"* or just *"The value"*.
     * For example: `"is equal 3"`, `"is less than 17"` or `"starts with A"`.
     *
     * The full description may then be generated to something like
     * *"If the value is less than 17, it must be equal 3."*
     */
    val thirdPersonForm: String

    override fun withEnding(phraseEnd: String): ConjugableValueStateDescription =
        if (endsPhrase) this else
            SimpleConjugableDescription(
                "$infiniteForm$phraseEnd", "$thirdPersonForm$phraseEnd",
                false, startsPhrase, true
            )

    override fun withStart(valueTitle: String, verb: String): ConjugableValueStateDescription =
        if (startsPhrase) this else
            SimpleConjugableDescription(
                "${valueTitle.capitalize()} $verb $infiniteForm",
                "${valueTitle.capitalize()} $thirdPersonForm",
                false, true, endsPhrase
            )
}

/**
 * Returns a new description based on this but with a starting and an ending phrase.
 * This is just a convenience call to [ConjugableValueStateDescription.withStart] and
 * [ConjugableValueStateDescription.withEnding] consecutively.
 */
fun ConjugableValueStateDescription.withCompletion(
    valueTitle: String, verb: String, phraseEnd: String = "."
): ConjugableValueStateDescription =
    withStart(valueTitle, verb).withEnding(phraseEnd)

/**
 * Extension of the [ConjugableValueStateDescription] that also provides inverted description texts.
 * This [negation] may be used when creating matchers via the [not]-operator.
 *
 * To create descriptions use one of the `descriptionFor*` methods.
 */
interface InvertibleValueStateDescription : ConjugableValueStateDescription {

    /**
     * Human readable description of the negation of the state that is described by this object.
     * This will be used when [negating a matcher][not].
     *
     * The negation has different [infiniteForm] and [thirdPersonForm]
     * but [isAtomic], [startsPhrase] and [endsPhrase] must match the respective properties of the original.
     */
    val negation: InvertibleValueStateDescription

    override fun withEnding(phraseEnd: String): InvertibleValueStateDescription =
        if (endsPhrase) this else
            LazyNegationDescription(
                "$infiniteForm$phraseEnd", "$thirdPersonForm$phraseEnd",
                "${negation.infiniteForm}$phraseEnd", "${negation.thirdPersonForm}$phraseEnd",
                false, startsPhrase, true
            )

    override fun withStart(valueTitle: String, verb: String): InvertibleValueStateDescription =
        if (startsPhrase) this else
            LazyNegationDescription(
                "${valueTitle.capitalize()} $verb $infiniteForm",
                "${valueTitle.capitalize()} $thirdPersonForm",
                "${valueTitle.capitalize()} $verb ${negation.infiniteForm}",
                "${valueTitle.capitalize()} ${negation.thirdPersonForm}",
                false, true, endsPhrase
            )
}

/**
 * Returns a new description based on this but with a starting and an ending phrase.
 * This is just a convenience call to [InvertibleValueStateDescription.withStart] and
 * [InvertibleValueStateDescription.withEnding] consecutively.
 */
fun InvertibleValueStateDescription.withCompletion(
    valueTitle: String, verb: String, phraseEnd: String = "."
): InvertibleValueStateDescription =
    withStart(valueTitle, verb).withEnding(phraseEnd)

private class LazyNegationDescription(
    override val infiniteForm: String,
    override val thirdPersonForm: String,
    infiniteNegation: String,
    thirdPersonNegation: String,
    override val isAtomic: Boolean,
    override val startsPhrase: Boolean = false,
    override val endsPhrase: Boolean = false
) : InvertibleValueStateDescription {
    override val negation: InvertibleValueStateDescription by lazy {
        FixedNegationDescription(infiniteNegation, thirdPersonNegation, this, isAtomic, startsPhrase, endsPhrase)
    }
}

private open class SimpleDescription(
    override val infiniteForm: String,
    override val isAtomic: Boolean,
    override val startsPhrase: Boolean = false,
    override val endsPhrase: Boolean = false
) : ValueStateDescription

private open class SimpleConjugableDescription(
    infiniteForm: String,
    override val thirdPersonForm: String,
    isAtomic: Boolean,
    startsPhrase: Boolean = false,
    endsPhrase: Boolean = false
) : SimpleDescription(infiniteForm, isAtomic, startsPhrase, endsPhrase),
    ConjugableValueStateDescription

private class FixedNegationDescription(
    infiniteForm: String,
    thirdPersonForm: String,
    override val negation: InvertibleValueStateDescription,
    isAtomic: Boolean,
    startsPhrase: Boolean,
    endsPhrase: Boolean
) : SimpleConjugableDescription(infiniteForm, thirdPersonForm, isAtomic, startsPhrase, endsPhrase),
    InvertibleValueStateDescription

/**
 * Creates an [InvertibleValueStateDescription] that describes a certain condition a value must satisfy,
 * that is **not** a state of being (see [descriptionOfBeing]).
 * The description will be [atomic][ValueStateDescription.isAtomic] and have
 * no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [infiniteForm]: `contain "abc"`
 * * [thirdPersonForm]: `contains "abc"`
 * * [infiniteNegation]: `contain no "abc"`
 * * [thirdPersonNegation]: `contains no "abc"`
 *
 * @param infiniteForm value of [InvertibleValueStateDescription.infiniteForm]
 * @param thirdPersonForm value of [InvertibleValueStateDescription.thirdPersonForm];
 *   defaults to `does $infiniteForm`
 * @param infiniteNegation `infiniteForm` of the [InvertibleValueStateDescription.negation];
 *   defaults to `not $infiniteForm`
 * @param thirdPersonNegation `thirdPersonForm` of the [InvertibleValueStateDescription.negation]
 *   defaults to `does $infiniteNegation`
 *
 * @see descriptionOfBeing
 * @see descriptionOfCombinedCondition
 */
fun descriptionOfAtomicCondition(
    infiniteForm: String,
    thirdPersonForm: String = "does $infiniteForm",
    infiniteNegation: String = "not $infiniteForm",
    thirdPersonNegation: String = "does $infiniteNegation"
): InvertibleValueStateDescription =
    LazyNegationDescription(infiniteForm, thirdPersonForm, infiniteNegation, thirdPersonNegation, true)

/**
 * Creates a [ConjugableValueStateDescription] that describes a combination of conditions a value must satisfy.
 * This description is not [atomic][ValueStateDescription.isAtomic] and should thus not be further combined.
 * It will have no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [infiniteForm]: `contain "abc" and "xyz`
 * * [thirdPersonForm]: `contains "abc" and "xyz"`
 *
 * @param infiniteForm value of [ConjugableValueStateDescription.infiniteForm]
 * @param thirdPersonForm value of [ConjugableValueStateDescription.thirdPersonForm]
 *
 * @see descriptionOfBeing
 * @see descriptionOfAtomicCondition
 */
fun descriptionOfCombinedCondition(
    infiniteForm: String,
    thirdPersonForm: String
): ConjugableValueStateDescription =
    SimpleConjugableDescription(infiniteForm, thirdPersonForm, false)

/**
 * Creates a [ValueStateDescription] that describes a combination of conditions a value must satisfy.
 * This description is not [atomic][ValueStateDescription.isAtomic] and should thus not be further combined.
 * It will have no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [infiniteForm]: `contain "abc" and "xyz`
 *
 * @param infiniteForm value of [ValueStateDescription.infiniteForm]
 *
 * @see descriptionOfBeing
 * @see descriptionOfAtomicCondition
 */
fun descriptionOfCombinedCondition(
    infiniteForm: String
): ValueStateDescription =
    SimpleDescription(infiniteForm, false)

/**
 * Creates a [InvertibleValueStateDescription] that describes a state of being of a value.
 * From the [stateOfBeing] all description forms are derived as follows in the example.
 * The description will be [atomic][ValueStateDescription.isAtomic] and have
 * no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [stateOfBeing]: `equal 17`
 * * [InvertibleValueStateDescription.infiniteForm]: `be equal 17`
 * * [InvertibleValueStateDescription.thirdPersonForm]: `is equal 17`
 * * `infiniteForm` of the [InvertibleValueStateDescription.negation]: `not be equal 17`
 * * `thirdPersonForm` of the [InvertibleValueStateDescription.negation]: `is not equal 17`
 *
 * @see descriptionOfNotBeing
 * @see descriptionOfAtomicCondition
 */
fun descriptionOfBeing(
    stateOfBeing: String
): InvertibleValueStateDescription =
    LazyNegationDescription(
        "be $stateOfBeing", "is $stateOfBeing",
        "not be $stateOfBeing", "is not $stateOfBeing",
        true
    )

/**
 * Creates a [InvertibleValueStateDescription] that describes a state of being of a value.
 * From the [stateOfBeing] and the [negatedStateOfBeing] all description forms are derived as follows in the example.
 * The description will be [atomic][ValueStateDescription.isAtomic] and have
 * no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [stateOfBeing]: `even`
 * * [negatedStateOfBeing]: `odd`
 * * [InvertibleValueStateDescription.infiniteForm]: `be even`
 * * [InvertibleValueStateDescription.thirdPersonForm]: `is even`
 * * `infiniteForm` of the [InvertibleValueStateDescription.negation]: `be odd`
 * * `thirdPersonForm` of the [InvertibleValueStateDescription.negation]: `is odd`
 *
 * @see descriptionOfNotBeing
 * @see descriptionOfAtomicCondition
 */
fun descriptionOfBeing(
    stateOfBeing: String,
    negatedStateOfBeing: String
): InvertibleValueStateDescription =
    LazyNegationDescription(
        "be $stateOfBeing", "is $stateOfBeing",
        "be $negatedStateOfBeing", "is $negatedStateOfBeing",
        true
    )

/**
 * Creates a [InvertibleValueStateDescription] that describes a state of **not** being of a value.
 * This is like the negation of [descriptionOfBeing].
 * From the [stateOfBeing] all description forms are derived as follows in the example.
 * The description will be [atomic][ValueStateDescription.isAtomic] and have
 * no [start][ValueStateDescription.startsPhrase] and no [ending][ValueStateDescription.endsPhrase].
 *
 * Example:
 * * [stateOfBeing]: `equal 17`
 * * [InvertibleValueStateDescription.infiniteForm]: `not be equal 17`
 * * [InvertibleValueStateDescription.thirdPersonForm]: `is not equal 17`
 * * `infiniteForm` of the [InvertibleValueStateDescription.negation]: `be equal 17`
 * * `thirdPersonForm` of the [InvertibleValueStateDescription.negation]: `is equal 17`
 *
 * @see descriptionOfBeing
 * @see descriptionOfAtomicCondition
 */
fun descriptionOfNotBeing(
    stateOfBeing: String
): InvertibleValueStateDescription =
    LazyNegationDescription(
        "not be $stateOfBeing", "is not $stateOfBeing",
        "be $stateOfBeing", "is $stateOfBeing",
        true
    )
