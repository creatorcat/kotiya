/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.predicates.Predicate
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.AtomicMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.ofLazyActualState

/**
 * Creates an ad-hoc matcher for [T] objects that checks if the given [predicate] is satisfied.
 * This is meant to be used in places where:
 * * a special matcher is needed, that cannot be created by operator combination of existing matchers
 *   and will not be used anywhere else,
 * * the [expected][AtomicMatcher.expectedState] and [actual][AtomicMatcherEvaluationNode.actualState]
 *   state descriptions are not so important
 *
 * Example:
 *
 *    assertThat(listOfStrings, each(matches { it.startsWith("ab") }))
 *
 * @param conditionDescription An optional description of the condition to be satisfied by the object to match.
 *   This will be used as the infinite form of a [descriptionOfAtomicCondition].
 *   Defaults to `"satisfy some undescribed condition"`.
 */
fun <T> matches(
    conditionDescription: String = "satisfy some undescribed condition",
    predicate: Predicate<T>
): AtomicMatcher<T> =
    Matcher.ofLazyActualState(descriptionOfAtomicCondition(conditionDescription)) {
        Pair(predicate(it), { "is $it" })
    }
