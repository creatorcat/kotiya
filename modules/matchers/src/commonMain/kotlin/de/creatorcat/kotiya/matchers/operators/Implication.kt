/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherBase
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition

/**
 * Combines this matcher and the other resembling the material implication (*->*).
 * This matcher represents the **antecedent** while the other is the [consequent].
 *
 * The resulting matcher will succeed fast, meaning it uses this matchers
 * [evaluation function][Matcher.evaluate] first.
 * If it produces a *mismatch*, evaluation succeeds immediately.
 * Otherwise, the [consequent]-matcher will be applied on the value.
 * The final result is then only dependent on the result of the `consequent`-matcher's evaluation.
 *
 * @param T input value type of this matcher and the resulting combined matcher
 */
infix fun <T> Matcher<T>.implies(consequent: Matcher<T>): CombinedMatcher<T> =
    ImplicationMatcher(this, consequent)

private class ImplicationMatcher<in T>(
    override val antecedent: Matcher<T>,
    override val consequent: Matcher<T>
) : ImplicationMatcherBase<T>("if-then") {

    override fun evaluate(value: T): CombinedMatcherEvaluationNode {
        val leftResult = antecedent.evaluate(value)
        return if (leftResult.isMatch) {
            val rightResult = consequent.evaluate(value)
            if (rightResult.isMatch)
                CombinedMatcherEvaluationNode.of(true, listOf(leftResult, rightResult), listOf(rightResult))
            else
                CombinedMatcherEvaluationNode.of(false, leftResult, rightResult)
        } else {
            CombinedMatcherEvaluationNode.of(true, leftResult)
        }
    }
}

/**
 * Base class for implication matcher types.
 * Implements child matcher handling ([antecedent] and [consequent])
 * and a proper [generateExpectationTree] method.
 *
 * @param combinationOperatorName value of [CombinedMatcherBase.combineUsingOperatorName]
 */
internal abstract class ImplicationMatcherBase<in T>(
    combinationOperatorName: String
) : CombinedMatcherBase<T>(combinationOperatorName) {

    protected abstract val antecedent: Matcher<*>
    protected abstract val consequent: Matcher<*>

    final override val childMatchers by lazy { listOf(antecedent, consequent) }

    /**
     * Generates a proper description tree.
     *
     * If [antecedent] and [consequent] descriptions are [atomic][ValueStateDescription.isAtomic]
     * and antecedent is also [conjugable][ConjugableValueStateDescription],
     * they are combined to one description of type "consequent if is antecedent".
     *
     * If only consequent is atomic, it will be used to create a new description node
     * of type "consequent if it satisfies:",
     * whose child is the [completion][CombinedMatcherBase.withCompletedDescription] of the antecedent.
     *
     * If none is atomic, a new description is created of type "must satisfy second if first holds:",
     * whose children are the completed descriptions of all antecedent (first) and consequent (second).
     *
     * The [valueTitle] and [verb] will be applied on description generation and completion if necessary.
     */
    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> {
        val antecedentNode = antecedent.generateExpectationTree(valueTitle, verb)
        val consequentNode = consequent.generateExpectationTree(valueTitle, verb)
        // use temp var for antecedentNode.data to enable smart casting 
        val antecedentData = antecedentNode.data
        return when {
            antecedentData.isAtomic && antecedentData is ConjugableValueStateDescription
                && consequentNode.data.isAtomic -> {

                SimpleDataNode.of(
                    descriptionOfCombinedCondition(
                        "${consequentNode.data.infiniteForm} if it ${antecedentData.thirdPersonForm}"
                    )
                )
            }
            consequentNode.data.isAtomic -> {
                SimpleDataNode.of(
                    descriptionOfCombinedCondition(
                        "${consequentNode.data.infiniteForm} if it satisfies the following condition"
                    ).withEnding(":"),
                    antecedentNode.withCompletedDescription("it", verb)
                )
            }
            else -> {
                SimpleDataNode.of(
                    descriptionOfCombinedCondition(
                        "satisfy the second condition if it satisfies the first condition"
                    ).withEnding(":"),
                    antecedentNode.withCompletedDescription("it", verb),
                    consequentNode.withCompletedDescription("it", verb)
                )
            }
        }
    }
}
