/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.byDelegationTo
import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.isMismatch
import de.creatorcat.kotiya.matchers.of

/**
 * Creates a matcher that checks if an [Iterable] has equal elements as the [expected], which means:
 * * both have equal number of elements
 * * and each element at index `i` (retrieved by `i` [next][Iterator.next] calls)
 *   is equal to the respective element of the other
 *
 * When matching elements the standard equality method is used (`==`),
 * so there is no recursive matching e.g. for lists of lists.
 */
fun <E> hasElementsEqual(expected: Iterable<E>): AtomicMatcher<Iterable<E>> =
    Matcher.of({ descriptionOfAtomicCondition("have elements equal ${expected.toListString()}") }) {
        val expIterator = expected.iterator()
        val actualIterator = it.iterator()
        var idx = 0
        var problem: (() -> String)? = null
        while (problem == null && expIterator.hasNext() && actualIterator.hasNext()) {
            val expVal = expIterator.next()
            val actualVal = actualIterator.next()
            if (expVal != actualVal)
                problem = { "has element $actualVal at index $idx where $expVal was expected" }
            else
                idx++
        }
        if (problem == null) {
            if (expIterator.hasNext()) problem = { "has to few elements ($idx)" }
            if (actualIterator.hasNext()) problem = { "has to many elements (> $idx)" }
        }
        Pair(problem == null, problem ?: { "has elements equal ${expected.toListString()}" })
    }

/**
 * This will guarantee a proper representation of the elements for every iterable
 * regardless of the respective `toString` implementation.
 */
internal fun <E> Iterable<E>.toListString() = joinToString(prefix = "[", separator = ", ", postfix = "]")

/**
 * Creates a matcher that checks if an [Iterable] has elements equal to [expectedElements], which means:
 * * it has as many elements as given
 * * and each element at index `i` (retrieved by `i` [next][Iterator.next] calls)
 *   is equal to the respective expected element
 *
 * When matching elements the standard equality method is used (`==`),
 * so there is no recursive matching e.g. for lists of lists.
 */
fun <E> hasElementsEqual(vararg expectedElements: E): AtomicMatcher<Iterable<E>> =
    hasElementsEqual(expectedElements.asIterable())

/**
 * Creates a matcher that checks if an [Array] has equal elements as the [expected], which means:
 * * both have equal number of elements
 * * and each element at index `i` (retrieved by `i` [next][Iterator.next] calls)
 *   is equal to the respective element of the other
 *
 * Delegates matching to [hasElementsEqual].
 */
fun <E> hasArrayElementsEqual(expected: Iterable<E>): AtomicMatcher<Array<E>> =
    Matcher.byDelegationTo(hasElementsEqual(expected), Array<E>::asIterable)

/**
 * Creates a matcher that checks if an [Array] has elements equal to [expectedElements], which means:
 * * it has as many elements as given
 * * and each element at index `i` (retrieved by `i` [next][Iterator.next] calls)
 *   is equal to the respective expected element
 *
 * Delegates matching to [hasElementsEqual].
 */
fun <E> hasArrayElementsEqual(vararg expectedElements: E): AtomicMatcher<Array<E>> =
    Matcher.byDelegationTo(hasElementsEqual(*expectedElements), Array<E>::asIterable)

/**
 * Creates a matcher that checks if a [List] is equal to the [expected], which means:
 * * it is not null
 * * both have equal number of elements
 * * and each element at index `i` is equal to the respective element of the other
 *
 * Actually delegates to [isNotNull] and [hasElementsEqual].
 */
// TODO design info
// list equality is defined by ordered elements equality so this makes sense 
fun <E> isEqual(expected: List<E>): AtomicMatcher<List<E>?> {
    val hasProperElements = hasElementsEqual(expected)
    return Matcher.of({ descriptionOfBeing("equal $expected") }) {
        val r1 = isNotNull.evaluate(it)
        if (r1.isMismatch) {
            Pair(false, r1::actualState)
        } else {
            val r2 = hasProperElements.evaluate(it as List<E>)
            Pair(r2.isMatch, r2::actualState)
        }
    }
}
