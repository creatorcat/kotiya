/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.predicates.appliesTo
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCallsForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class NegationOperatorTest {

    @Test fun `negates evaluation result`() {
        val is4 = Matcher.of<Int>(descriptionOfBeing("equal 4")) {
            withCallCounterForTag("is4") { Pair(it == 4, "is $it") }
        }
        val isNot4 = !is4

        assertTrue(isNot4 appliesTo 17)
        assertFalse(isNot4 appliesTo 4)
        assertEquals(2, countedCallsForTag("is4"))
    }

    @Test fun `child matcher is original`() {
        val is4 = Matcher.of<Int>(descriptionOfBeing("equal 4")) { Pair(it == 4, "is $it") }
        val isNot4 = !is4 as CombinedMatcher
        assertEquals(1, isNot4.childMatchers.size)
        assertSame(is4, isNot4.childMatchers[0])
    }

    @Test fun `evaluation tree`() {
        val is4 = Matcher.of<Int>(descriptionOfBeing("equal 4")) { Pair(it == 4, "is $it") }
        val isNot4 = !is4 as CombinedMatcher

        val result = isNot4.evaluate(4)
        result.assert(hasContents(false, isNot4, 1))
        val childNode = result.children[0] as AtomicMatcherEvaluationNode
        childNode.assert(hasContents(true, is4, "is 4"))
    }

    @Test fun `expectation tree of invertible`() {
        val originalDescription = descriptionOfBeing("A")
        val isA = Matcher.of<String>(originalDescription) { Pair(it == "A", "is $it") }
        val tree = (!isA).generateExpectationTree()
        assertTrue(tree.isLeaf)
        assertSame(originalDescription.negation, tree.data)
    }

    @Test fun `expectation tree of not invertible`() {
        val originalDescription = descriptionOfCombinedCondition("contain A or B")
        val containsAorB = Matcher.of<String>(originalDescription) {
            Pair(it.contains("A") || it.contains("B"), "is $it")
        }
        val tree = (!containsAorB).generateExpectationTree()
        assertEquals(1, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertEquals("It must contain A or B.", data.infiniteForm)
        }
    }

    @Test fun `usage with type casts`() {
        val isNotInt: Matcher<Number> = !isInt
        assertTrue(isNotInt appliesTo 2.3)
        assertFalse(isNotInt appliesTo 5)
    }

    @Test fun `double negation is original`() {
        val is3 = Matcher.of<Int>(descriptionOfBeing("equal 3")) { Pair(it == 3, "is $it") }
        val isNot3 = !is3
        val is3Again = !isNot3
        assertSame(is3, is3Again)
        assertTrue(is3 appliesTo 3)
        assertFalse(isNot3 appliesTo 3)
    }

}
