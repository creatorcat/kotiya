/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccessAndFailure
import kotlin.test.Test

class EqualityTest {

    @Test fun `isEqual - simple usage`() {
        val matcher = isEqual(Dummy(23))
        matcher.expectedState.assert(hasContents("be equal 23"))
        matcher.assertSuccessAndFailure(Dummy(23), "is 23", Dummy(17), "is 17")
    }

    @Test fun `isEqual - with nullable type`() {
        val matcher: AtomicMatcher<Int?> = isEqual(23)
        matcher.expectedState.assert(hasContents("be equal 23"))
        matcher.assertSuccessAndFailure(23, "is 23", 17, "is 17")
        matcher.assertFailure(null, "is null")
    }

    @Test fun `isNotEqual - simple usage`() {
        val matcher = isNotEqual(Dummy(23))
        matcher.expectedState.assert(hasContents("not be equal 23"))
        matcher.assertSuccessAndFailure(Dummy(17), "is 17", Dummy(23), "is 23")
    }

    @Test fun `isSameAs - simple usage`() {
        val expected = Dummy(23)
        val matcher = isSameAs(expected)
        matcher.expectedState.assert(hasContents("be identical to 23"))
        matcher.assertSuccessAndFailure(expected, "is identical to 23", Dummy(23), "is not identical to 23")
    }

    @Test fun `isNotSameAs - simple usage`() {
        val unexpected = Dummy(23)
        val matcher = isNotSameAs(unexpected)
        matcher.expectedState.assert(hasContents("not be identical to 23"))
        matcher.assertSuccessAndFailure(Dummy(23), "is not identical to 23", unexpected, "is identical to 23")
    }

    data class Dummy(val x: Int) {
        override fun toString(): String = x.toString()
    }
}
