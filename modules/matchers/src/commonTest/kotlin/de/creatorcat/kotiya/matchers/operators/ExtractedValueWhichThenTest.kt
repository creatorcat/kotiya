/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.StateOfBeingMatcher
import de.creatorcat.kotiya.matchers.ValueExtractingAtomicMatcher
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasCombinedContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.isComplete
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isLessThan10
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.or
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.resetObservationData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue

class ExtractedValueWhichThenTest {

    @Test fun `whichThen - evaluation`() {
        with(isInstanceOf<Int>() whichThen isEven) {
            val isInt = childMatchers[0]
            // long match
            with(evaluate(8)) {
                assert(hasContents(true, matcher, 2, 1))
                children[0].assertIsTypeNode(true, isInt)
                children[1].assert(hasAtomicContents(true, isEven, "led to extracted Int which is even"))
                assertSame(children[1], resultContributors[0])
            }
            // mismatch
            with(evaluate(5)) {
                assert(hasCombinedContents(false, matcher, 2))
                children[0].assertIsTypeNode(true, isInt)
                children[1].assert(hasAtomicContents(false, isEven, "led to extracted Int which is odd"))
            }

            // fast match
            isEven::evaluate.resetObservationData()
            with(evaluate("a")) {
                assert(hasCombinedContents(true, matcher, 1))
                children[0].assertIsTypeNode(false, isInt, "String")
            }
            assertEquals(0, isEven::evaluate.countedCalls)
        }
    }

    private fun MatcherEvaluationNode.assertIsTypeNode(
        isMatch: Boolean, matcher: Matcher<*>, actualTypeName: String = "Int"
    ) {
        assert(hasAtomicContents(isMatch, matcher as AtomicMatcher<*>, "is of type $actualTypeName"))
    }

    @Test fun `whichThen - child matchers`() {
        val isInt = isInstanceOf<Int>()
        val matcher = isInt whichThen isEven
        assertEquals(listOf(isInt, isEven), matcher.childMatchers)
    }

    @Test fun `whichThen - expectation tree with both args atomic`() {
        val testee = isInstanceOf<String>() whichThen StateOfBeingMatcher("A")
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(
            hasContents("lead to extracted String that must be A if it is of type String", isAtomic = false)
        )
    }

    @Test fun `whichThen - expectation tree with atomic consequent and combined antecedent`() {
        val complexExtractingMatcher = ValueExtractingAtomicMatcher
            .of<Any, Int>(descriptionOfCombinedCondition("be Int and less than 10")) {
                val num = if (it is Int && it < 10) it else null
                Pair(num, "is ${"no ".orEmptyIf(num != null)}Int < 10")
            }
        val testee = complexExtractingMatcher whichThen isEven
        val tree = testee.generateExpectationTree()

        assertEquals(1, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertEquals(
            "lead to extracted Int that must be even if it satisfies the following condition:",
            tree.data.infiniteForm
        )
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be Int and less than 10."))
        }
    }

    @Test fun `whichThen - expectation tree with both args combined`() {
        val testee = isInstanceOf<Int>() whichThen (isEven or isLessThan10)
        val tree = testee.generateExpectationTree()

        assertEquals(2, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertEquals(
            "lead to extracted Int that must satisfy the second condition if it satisfies the first condition:",
            tree.data.infiniteForm
        )
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be of type Int."))
        }
        with(tree.children[1]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be even or be less than 10."))
        }
    }

}
