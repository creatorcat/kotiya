/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.text.containsAll
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.countedCallsForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class PreconditionsTest {

    @Test fun `check - with given value name`() {
        assertEquals(8, check("X", 8, is8))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalStateException> {
            check("X", 9, is8)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("X", 9)
    }

    @Test fun `check - without value name`() {
        assertEquals(8, check(8, is8))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalStateException> {
            check(9, is8)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("the value", 9)
    }

    @Test fun `require - with given argument name`() {
        assertEquals(8, require("A", 8, is8))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalArgumentException> {
            require("A", 9, is8)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("A", 9)
    }

    @Test fun `require - without argument name`() {
        assertEquals(8, require(8, is8))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalArgumentException> {
            require(9, is8)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("the argument", 9)
    }

    private fun RuntimeException.assertMessage(valueTitle: String, value: Int) {
        val matcherResult = is8.evaluate(value)
        assertTrue(
            assertNotNull(message).containsAll(
                is8.describeExpectations(valueTitle),
                matcherResult.describeWhy(valueTitle)
            )
        )
    }

    private val is8: AtomicMatcher<Int> = Matcher.of(descriptionOfBeing("8")) {
        withCallCounterForTag(this) { Pair(it == 8, "is $it") }
    }
}
