/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.text.containsAll
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EvaluationNodesAPITest {

    @Test fun `atomic nodes - are leaves`() {
        assertTrue(Atomic().isLeaf)
    }

    @Test fun `atomic nodes - string representation`() {
        val trueEval = Atomic(true)
        assertTrue(trueEval.toString().contains("match"))
        assertEquals(1, trueEval::isMatch.countedCalls)
        val falseEval = Atomic(false)
        assertTrue(falseEval.toString().contains("mismatch"))
        assertEquals(1, falseEval::isMatch.countedCalls)
    }

    @Test fun `combined description - string representation`() {
        val a = Atomic()
        val b = Atomic()
        val operatorName = "OP"
        val combined = Combined(true, operatorName, a, b)
        val s = combined.toString()
        assertTrue(s.contains("match"))
        assertEquals(1, combined::isMatch.countedCalls)
        assertTrue(s.containsAll(a.toString(), b.toString(), operatorName))

        val failedCombined = Combined(false, operatorName, a, b)
        assertTrue(failedCombined.toString().contains("mismatch"))
        assertEquals(1, failedCombined::isMatch.countedCalls)
    }

    @Test fun `isMismatch - is negation of isMatch`() {
        val mismatch = Atomic(false)
        val match = Atomic(true)

        assertFalse(match.isMismatch)
        assertEquals(1, match::isMatch.countedCalls)
        assertTrue(mismatch.isMismatch)
        assertEquals(1, mismatch::isMatch.countedCalls)
    }

    private class Combined(
        private val isMatchValue: Boolean,
        operatorName: String,
        vararg children: MatcherEvaluationNode
    ) : CombinedMatcherEvaluationNode() {
        override val matcher = OperatorNameMatcher(operatorName)
        override val isMatch: Boolean get() = withCallObserver(this::isMatch) { isMatchValue }
        override val resultContributors get() = mustNotBeCalled()
        override val children = children.asList()
    }

    private class Atomic(private val isMatchValue: Boolean = true) : AtomicMatcherEvaluationNode() {
        override val matcher get() = mustNotBeCalled()
        override val isMatch: Boolean get() = withCallObserver(this::isMatch) { isMatchValue }
        override val actualState get() = mustNotBeCalled()
    }
}
