/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertSuccessAndFailure
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class EmptinessTest {

    @Test fun `isEmptySequence - simple usage`() {
        isEmptySequence.expectedState.assert(hasContents("be empty"))
        isEmptySequence.assertSuccessAndFailure(sequenceOf(), sequenceOf(1, 2, 3))
    }

    @Test fun `isEmptyIterable - simple usage`() {
        isEmptyIterable.expectedState.assert(hasContents("be empty"))
        isEmptyIterable.assertSuccessAndFailure(emptyList(), listOf(1, 2, 3))
    }

    @Test fun `isEmptyCollection - simple usage`() {
        isEmptyCollection.expectedState.assert(hasContents("be empty"))
        isEmptyCollection.assertSuccessAndFailure(emptyList(), listOf(1, 2, 3))
    }

    @Test fun `isEmptyArray - simple usage`() {
        isEmptyArray.expectedState.assert(hasContents("be empty"))
        isEmptyArray.assertSuccessAndFailure(emptyArray(), arrayOf(1, 2, 3))
    }

    @Test fun `isEmptyMap - simple usage`() {
        isEmptyMap.expectedState.assert(hasContents("be empty"))
        isEmptyMap.assertSuccessAndFailure(emptyMap(), mapOf(1 to 1, 2 to 2, 3 to 3))
    }

    @Test fun `isEmptyString - simple usage`() {
        isEmptyString.expectedState.assert(hasContents("be empty"))
        isEmptyString.assertSuccessAndFailure("", "abc")
    }

    @Test fun `isEmpty - simple usage`() {
        isEmpty.expectedState.assert(hasContents("be empty"))
        isEmpty.assertSuccessAndFailure(sequenceOf(), sequenceOf(1, 2))
        isEmpty.assertSuccessAndFailure(sequenceOf<Int>().asIterable(), sequenceOf(1, 2).asIterable())
        isEmpty.assertSuccessAndFailure(listOf(), listOf(1, 2))
        isEmpty.assertSuccessAndFailure(arrayOf(), arrayOf(1, 2))
        isEmpty.assertSuccessAndFailure(mapOf(), mapOf(1 to 1, 2 to 2))
        isEmpty.assertSuccessAndFailure("", "abc")

        assertThrows<IllegalArgumentException> { isEmpty.evaluate(3) }
    }

    private fun <T> AtomicMatcher<T>.assertSuccessAndFailure(
        matchingValue: T, mismatchingValue: T
    ) =
        assertSuccessAndFailure(matchingValue, "is empty", mismatchingValue, "is not empty")

}
