/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.InvertibleValueStateDescription
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccess
import kotlin.test.Test

class CollectionEqualityTest {

    @Test fun `hasElementsEqual - description`() {
        with(hasElementsEqual(listOf(1, 2, 3)).expectedState as InvertibleValueStateDescription) {
            assert(hasContents("have elements equal [1, 2, 3]", isAtomic = true))
        }
    }

    @Test fun `hasElementsEqual - evaluation with elements in both`() {
        with(hasElementsEqual(listOf(1, 2, 3))) {
            assertSuccess(listOf(1, 2, 3), "has elements equal [1, 2, 3]")
            assertFailure(listOf(1, 3, 3), "has element 3 at index 1 where 2 was expected")
            assertFailure(listOf(1, 2, 3, 4), "has to many elements (> 3)")
            assertFailure(listOf(1, 2), "has to few elements (2)")
        }
    }

    @Test fun `hasElementsEqual - evaluation with empty expected`() {
        with(hasElementsEqual(emptyList<Int>())) {
            assertSuccess(listOf<Int>(), "has elements equal []")
            assertFailure(listOf(1, 2, 3, 4), "has to many elements (> 0)")
        }
    }

    @Test fun `hasElementsEqual - evaluation with empty actual`() {
        with(hasElementsEqual(listOf(1, 2, 3))) {
            assertFailure(listOf<Int>(), "has to few elements (0)")
        }
    }

    @Test fun `hasElementsEqual - evaluation with list elements`() {
        with(hasElementsEqual(listOf(listOf(1, 2), listOf(3, 4)))) {
            assertSuccess(listOf(listOf(1, 2), listOf(3, 4)), "has elements equal [[1, 2], [3, 4]]")
            assertFailure(
                listOf(listOf(1, 2), listOf(3, 5)),
                "has element [3, 5] at index 1 where [3, 4] was expected"
            )
        }
    }

    @Test fun `hasElementsEqual - with var args`() {
        with(hasElementsEqual(1, 2, 3)) {
            assertSuccess(listOf(1, 2, 3), "has elements equal [1, 2, 3]")
        }
    }

    @Test fun `isEqual - for lists delegates to isNotNull and hasElementsEqual`() {
        with(isEqual(listOf(1, 2, 3))) {
            expectedState.assert(hasContents("be equal [1, 2, 3]", isAtomic = true))
            assertSuccess(listOf(1, 2, 3), "has elements equal [1, 2, 3]")
            assertFailure(listOf(1, 2, 3, 4), "has to many elements (> 3)")
            assertFailure(null, "is null")
        }
    }

    @Test fun `hasArrayElementsEqual - with var args`() {
        with(hasArrayElementsEqual(1, 2, 3)) {
            assertSuccess(arrayOf(1, 2, 3), "has elements equal [1, 2, 3]")
            assertFailure(arrayOf(1, 2), "has to few elements (2)")
        }
    }

    @Test fun `hasArrayElementsEqual - with iterable`() {
        with(hasArrayElementsEqual(listOf(1, 2, 3))) {
            assertSuccess(arrayOf(1, 2, 3), "has elements equal [1, 2, 3]")
            assertFailure(arrayOf(1, 2, 3, 4), "has to many elements (> 3)")
        }
    }
}
