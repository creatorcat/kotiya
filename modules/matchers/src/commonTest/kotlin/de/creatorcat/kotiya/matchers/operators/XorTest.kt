/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.predicates.appliesTo
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.StateOfBeingMatcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.describedWith
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.is2or3
import de.creatorcat.kotiya.matchers.isDouble
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isInt
import de.creatorcat.kotiya.matchers.isLessThan10
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class XorTest {

    private val matchers = ('a'..'c').associate { it to contains(it.toString()) }

    @Test fun `one of matcher - evaluation`() {
        val testee = oneOf(matchers.values.toList())

        // match
        with(testee.evaluate("xxxbxxx")) {
            assert(hasContents(true, testee, 3, 1))
            children[0].assertContainsNode('a', false)
            children[1].assertContainsNode('b', true)
            assertSame(children[1], resultContributors[0])
        }

        // fail - none match
        with(testee.evaluate("xyz")) {
            assert(hasContents(false, testee, 3))
            children[0].assertContainsNode('a', false)
            children[1].assertContainsNode('b', false)
            children[2].assertContainsNode('c', false)
        }

        // fail - two match
        with(testee.evaluate("axxxb")) {
            assert(hasContents(false, testee, 2))
            children[0].assertContainsNode('a', true)
            children[1].assertContainsNode('b', true)
        }
    }

    private fun MatcherEvaluationNode.assertContainsNode(key: Char, isMatch: Boolean) {
        assert(
            hasAtomicContents(
                isMatch, matchers[key] ?: throw AssertionError("unknown matcher"),
                "contains ${"no ".orEmptyIf(isMatch)}'$key'"
            )
        )
    }

    @Test fun `xor operator - evaluation`() {
        val testee = contains("a") xor contains("c")
        assertFalse(testee appliesTo "ac")
        assertTrue(testee appliesTo "a")
        assertTrue(testee appliesTo "c")
        assertFalse(testee appliesTo "")
    }

    @Test fun `one of matcher - needs at least 2 children`() {
        assertThrows<IllegalArgumentException> {
            oneOf(emptyList<Matcher<Any>>())
        }
        assertThrows<IllegalArgumentException> {
            oneOf(isEven)
        }
    }

    @Test fun `one of matcher - child matchers`() {
        assertEquals(matchers.values.toList(), oneOf(matchers.values.toList()).childMatchers)
    }

    @Test fun `one of matcher - operator name`() {
        assertTrue(oneOf(isInt, isEven).combinationOperatorName.isNotBlank())
    }

    @Test fun `one of matcher - associativity`() {
        val testee = oneOf(isInt, oneOf(isEven, isLessThan10), isDouble)
        assertEquals(listOf<Matcher<Int>>(isInt, isEven, isLessThan10, isDouble), testee.childMatchers)
    }

    @Test fun `one of matcher - expectation tree with 2 args atomic`() {
        val testee = oneOf(
            StateOfBeingMatcher("A"), StateOfBeingMatcher("B")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        (tree.data as ConjugableValueStateDescription).assert(
            hasContents("either be A or be B but not both", "either is A or is B but not both", isAtomic = false)
        )
    }

    @Test fun `one of matcher - expectation tree with more args atomic`() {
        val testee = oneOf(
            StateOfBeingMatcher("A"), StateOfBeingMatcher("B"), StateOfBeingMatcher("C")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        (tree.data as ConjugableValueStateDescription).assert(
            hasContents(
                "either be A, be B or be C but not more than one of them",
                "either is A, is B or is C but not more than one of them",
                isAtomic = false
            )
        )
    }

    @Test fun `one of matcher - expectation tree with combined args`() {
        val testee = oneOf(is2or3, isEven)
        val tree = testee.generateExpectationTree("X", "shall")

        assertEquals(2, tree.children.size)
        tree.data.assert(
            hasContents(
                "satisfy exactly one of the following conditions:",
                endsPhrase = true, isAtomic = false
            )
        )
        (tree.children[0].data).assert(
            hasContents("X shall be 2 or 3.", endsPhrase = true, startsPhrase = true, isAtomic = false)
        )
        (tree.children[1].data).assert(
            hasContents("X shall be even.", endsPhrase = true, startsPhrase = true, isAtomic = false)
        )
    }

    @Test fun `one of matcher - expectation tree with 2 args atomic not conjugable`() {
        val testee = oneOf(
            StateOfBeingMatcher("A"), StateOfBeingMatcher("B").describedWith(atomicSimpleDescription)
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(hasContents("either be A or be something complex but not both", isAtomic = false))
        assertFalse(tree.data is ConjugableValueStateDescription)
    }

    private val atomicSimpleDescription: ValueStateDescription = object : ValueStateDescription {
        override val infiniteForm = "be something complex"
        override val isAtomic = true
        override val startsPhrase = false
        override val endsPhrase = false
    }
}
