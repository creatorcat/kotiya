/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assertSuccessAndFailure
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class BooleanTest {

    @Test fun `is false`() {
        with(isFalse) {
            assertThat(expectedState, hasContents("be false"))
            assertSuccessAndFailure(false, "is false", true, "is true")
        }
    }

    @Test fun `is true`() {
        with(isTrue) {
            assertThat(expectedState, hasContents("be true"))
            assertSuccessAndFailure(true, "is true", false, "is false")
        }
    }
}
