/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals

class AtomicEvaluationNodeImplTest {

    @Test fun `atomic node - simple actual state`() {
        val node = AtomicMatcherEvaluationNode.of(true, AtomicNothingMatcher, "a-state")
        node.assert(hasContents(true, AtomicNothingMatcher, "a-state"))
    }

    @Test fun `atomic node - lazy actual state`() {
        fun lazyActualState(): String = withCallObserver(::lazyActualState) { "a-state" }

        val node = AtomicMatcherEvaluationNode.of(true, AtomicNothingMatcher, ::lazyActualState)
        repeat(3) { node.assert(hasContents(true, AtomicNothingMatcher, "a-state")) }
        // lazy will only be called once
        assertEquals(1, ::lazyActualState.countedCalls)
    }
}

