/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assertSuccessAndFailure
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class ComparableTest {

    @Test fun `is less than`() {
        with(isLessThan(17)) {
            assertThat(expectedState, hasContents("be less than 17"))
            assertSuccessAndFailure(10, 20)
            assertSuccessAndFailure(16, 17)
        }
    }

    @Test fun `is less than or equal`() {
        with(isLessEqThan(17)) {
            assertThat(expectedState, hasContents("be less than or equal to 17"))
            assertSuccessAndFailure(10, 20)
            assertSuccessAndFailure(17, 18)
        }
    }

    @Test fun `is greater than`() {
        with(isGreaterThan(13)) {
            assertThat(expectedState, hasContents("be greater than 13"))
            assertSuccessAndFailure(20, 10)
            assertSuccessAndFailure(14, 13)
        }
    }

    @Test fun `is greater than or equal`() {
        with(isGreaterEqThan(13)) {
            assertThat(expectedState, hasContents("be greater than or equal to 13"))
            assertSuccessAndFailure(20, 10)
            assertSuccessAndFailure(13, 12)
        }
    }

    @Test fun `is inside range`() {
        with(isInside(17..23)) {
            assertThat(expectedState, hasContents("be inside 17..23"))
            assertSuccessAndFailure(20, 30)
            assertSuccessAndFailure(17, 16)
            assertSuccessAndFailure(23, 24)
        }
        with(isInside('a'..'c')) {
            assertThat(expectedState, hasContents("be inside a..c"))
            assertSuccessAndFailure('b', 'd')
        }
    }

    private fun <T : Comparable<T>> AtomicMatcher<T>.assertSuccessAndFailure(
        matchingValue: T, mismatchingValue: T
    ) = assertSuccessAndFailure(matchingValue, "is $matchingValue", mismatchingValue, "is $mismatchingValue")
}
