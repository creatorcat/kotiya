/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.predicates.appliesTo
import de.creatorcat.kotiya.matchers.common.contains
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AllAnyNoneTest {

    @Test fun `allOf - evaluation`() {
        val testee = isAllOf(contains("a"), contains("b"), contains("c"))
        assertTrue(testee appliesTo "abc")
        assertFalse(testee appliesTo "ab")
        assertFalse(testee appliesTo "bc")
        assertFalse(testee appliesTo "ac")
        assertFalse(testee appliesTo "a")
        assertFalse(testee appliesTo "b")
        assertFalse(testee appliesTo "c")
        assertFalse(testee appliesTo "")
    }

    @Test fun `anyOf - evaluation`() {
        val testee = isAnyOf(contains("a"), contains("b"), contains("c"))
        assertTrue(testee appliesTo "abc")
        assertTrue(testee appliesTo "ab")
        assertTrue(testee appliesTo "bc")
        assertTrue(testee appliesTo "ac")
        assertTrue(testee appliesTo "a")
        assertTrue(testee appliesTo "b")
        assertTrue(testee appliesTo "c")
        assertFalse(testee appliesTo "")
    }

    @Test fun `noneOf - evaluation`() {
        val testee = isNoneOf(contains("a"), contains("b"), contains("c"))
        assertFalse(testee appliesTo "abc")
        assertFalse(testee appliesTo "ab")
        assertFalse(testee appliesTo "bc")
        assertFalse(testee appliesTo "ac")
        assertFalse(testee appliesTo "a")
        assertFalse(testee appliesTo "b")
        assertFalse(testee appliesTo "c")
        assertTrue(testee appliesTo "")
    }
}
