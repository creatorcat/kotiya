/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.text.containsAll
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.countedCallsForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ValueExtractingPreconditionsTest {

    @Test fun `check - with given value name`() {
        assertEquals(8, check("X", "8", isNum))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalStateException> {
            check("X", "a", isNum)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("X", "a")
    }

    @Test fun `check - without value name`() {
        assertEquals(8, check("8", isNum))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalStateException> {
            check("a", isNum)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("the value", "a")
    }

    @Test fun `require - with given argument name`() {
        assertEquals(4, require("A", "4", isNum))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalArgumentException> {
            require("A", "c", isNum)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("A", "c")
    }

    @Test fun `require - without argument name`() {
        assertEquals(7, require("7", isNum))
        assertEquals(1, countedCallsForTag(this))
        val e1 = assertThrows<IllegalArgumentException> {
            require("s", isNum)
        }
        assertEquals(1, countedCallsForTag(this))
        e1.assertMessage("the argument", "s")
    }

    private fun RuntimeException.assertMessage(valueTitle: String, value: String) {
        val matcherResult = isNum.evaluate(value)
        assertTrue(
            assertNotNull(message).containsAll(
                isNum.describeExpectations(valueTitle),
                matcherResult.describeWhy(valueTitle)
            )
        )
    }

    private val isNum: ValueExtractingAtomicMatcher<String, Int> =
        ValueExtractingAtomicMatcher.of(descriptionOfBeing("a number")) {
            withCallCounterForTag(this) {
                Pair(it.toIntOrNull(), "is $it")
            }
        }
}
