/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.assertIsLazilyCalledBy
import de.creatorcat.kotiya.test.core.stubbing.assertTagIsLazilyCalledBy
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertSame

class MatcherCreationTest {

    private val dummyDescription = descriptionOfBeing("a dummy")

    @Test fun `simple base - properties`() {
        val matcher = object : SimpleAtomicMatcherBase<Int>(dummyDescription) {
            override fun evaluate(value: Int) = mustNotBeCalled()
        }

        assertSame(dummyDescription, matcher.expectedState)
    }

    @Test fun `lazy base - properties`() {
        fun expectedState(): ValueStateDescription = withCallObserver(::expectedState) { dummyDescription }
        val matcher = object : LazyAtomicMatcherBase<Int>(::expectedState) {
            override fun evaluate(value: Int) = mustNotBeCalled()
        }

        ::expectedState.assertIsLazilyCalledBy { assertSame(dummyDescription, matcher.expectedState) }
    }

    @Test fun `creation - with lazy expected and actual state`() {
        fun expected(): ValueStateDescription = withCallObserver(::expected) { dummyDescription }

        val matcher = Matcher.of<String>(::expected) {
            Pair(it.contains("23"), {
                withCallCounterForTag("LAZY_ALL") { "contains ${"no ".orEmptyIf(it.contains("23"))}23" }
            })
        }

        ::expected.assertIsLazilyCalledBy { assertSame(dummyDescription, matcher.expectedState) }

        val match = matcher.evaluate("magic number 23")
        assertTagIsLazilyCalledBy("LAZY_ALL") { match.assert(hasContents(true, matcher, "contains 23")) }

        val mismatch = matcher.evaluate("just 17")
        assertTagIsLazilyCalledBy("LAZY_ALL") { mismatch.assert(hasContents(false, matcher, "contains no 23")) }
    }

    @Test fun `creation - with fixed expected and actual state`() {
        val matcher = Matcher.of<String>(dummyDescription) {
            Pair(it.contains("23"), "contains ${"no ".orEmptyIf(it.contains("23"))}23")
        }

        assertSame(dummyDescription, matcher.expectedState)

        val match = matcher.evaluate("magic number 23")
        match.assert(hasContents(true, matcher, "contains 23"))

        val mismatch = matcher.evaluate("just 17")
        mismatch.assert(hasContents(false, matcher, "contains no 23"))
    }

    @Test fun `creation - with fixed expected and lazy actual state`() {

        val matcher = Matcher.ofLazyActualState<String>(dummyDescription) {
            Pair(it.contains("23"), {
                withCallCounterForTag("LAZY_ACTUAL") { "contains ${"no ".orEmptyIf(it.contains("23"))}23" }
            })
        }

        assertSame(dummyDescription, matcher.expectedState)

        val match = matcher.evaluate("magic number 23")
        assertTagIsLazilyCalledBy("LAZY_ACTUAL") { match.assert(hasContents(true, matcher, "contains 23")) }

        val mismatch = matcher.evaluate("just 17")
        assertTagIsLazilyCalledBy("LAZY_ACTUAL") { mismatch.assert(hasContents(false, matcher, "contains no 23")) }
    }
}
