/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.predicates.satisfies
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class MatcherTest {

    @Test fun `predicate invocation - uses evaluate`() {
        val is17 = DummyMatcher<Int>(descriptionOfBeing("17")) { it == 17 }
        assertTrue(is17(17))
        assertFalse(is17(18))
        assertFalse(is17(16))
        assertEquals(3, is17::evaluate.countedCalls)
    }

    @Test fun `matcher - variance of type parameters`() {
        val isInt: Matcher<Any?> = DummyMatcher(descriptionOfBeing("Int")) { it is Int }

        fun matchNumber(matcher: Matcher<Number>, value: Number) = value satisfies matcher

        assertTrue(matchNumber(isInt, 23))
    }

    @Test fun `atomic matcher - generateExpectationTree uses expectedState`() {
        val expectedState = descriptionOfBeing("5")
        val is5 = DummyMatcher<Int>(expectedState) { it == 5 }
        val tree = is5.generateExpectationTree()
        assertTrue(tree.isLeaf)
        assertSame(expectedState, tree.data)
    }

    private class DummyMatcher<T>(
        override val expectedState: ValueStateDescription,
        private val body: (T) -> Boolean
    ) : AtomicMatcher<T> {

        override fun evaluate(value: T): AtomicMatcherEvaluationNode = withCallObserver(this::evaluate) {
            AtomicMatcherEvaluationNode.of(body(value), this, ::mustNotBeCalled)
        }
    }

}
