/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue

class ValueStateDescriptionTest {

    @Test fun `description of atomic condition`() {
        descriptionOfAtomicCondition("contain A").assert(
            hasContents("contain A", "does contain A", "not contain A", "does not contain A")
        )

        descriptionOfAtomicCondition("contain A", "contains A", "miss A", "misses A").assert(
            hasContents("contain A", "contains A", "miss A", "misses A")
        )
    }

    @Test fun `description of being - default negation`() {
        descriptionOfBeing("A").assert(
            hasContents("be A", "is A", "not be A", "is not A")
        )
    }

    @Test fun `description of being - custom negation`() {
        descriptionOfBeing("A", "B").assert(
            hasContents("be A", "is A", "be B", "is B")
        )
    }

    @Test fun `description of not being`() {
        descriptionOfNotBeing("A").assert(
            hasContents("not be A", "is not A", "be A", "is A")
        )
    }

    @Test fun `description of combined condition - infinite`() {
        descriptionOfCombinedCondition("contain a and b").assert(
            hasContents("contain a and b", isAtomic = false)
        )
    }

    @Test fun `description of combined condition - conjugable`() {
        descriptionOfCombinedCondition("contain a and b", "contains a and b").assert(
            hasContents("contain a and b", "contains a and b", isAtomic = false)
        )
    }

    @Test fun `is complete - checks start and ending`() {
        val testee = DummyDescription(starts = true, ends = false)
        assertEquals(false, testee.isComplete)
        assertEquals(1, testee::startsPhrase.countedCalls)
        assertEquals(1, testee::endsPhrase.countedCalls)

        val testee2 = DummyDescription(starts = false, ends = false)
        assertEquals(false, testee2.isComplete)
        assertEquals(1, testee2::startsPhrase.countedCalls)
        assertEquals(0, testee2::endsPhrase.countedCalls)
    }

    @Test fun `with completion - uses with start and with end`() {
        val testee = DummyDescription(starts = false, ends = false)
        testee.withCompletion("x", "must")
        assertEquals(1, testee::withStart.countedCalls)
        assertEquals(1, testee::withEnding.countedCalls)
    }

    private class DummyDescription(private val starts: Boolean, private val ends: Boolean) : ValueStateDescription {
        override val infiniteForm get() = mustNotBeCalled()
        override val isAtomic get() = mustNotBeCalled()
        override val startsPhrase: Boolean
            get() = withCallObserver(this::startsPhrase) { starts }
        override val endsPhrase: Boolean
            get() = withCallObserver(this::endsPhrase) { ends }

        override fun withEnding(phraseEnd: String): ValueStateDescription =
            withCallObserver(this::withEnding) { this }

        override fun withStart(valueTitle: String, verb: String): ValueStateDescription =
            withCallObserver(this::withStart) { this }
    }

    @Test fun `with start - simple description`() {
        val desc = descriptionOfCombinedCondition("contain 1 and 2")
        with(desc.withStart("x", "must")) {
            assert(
                hasContents(
                    "X must contain 1 and 2",
                    isAtomic = false, startsPhrase = true, endsPhrase = false
                )
            )
            assertSame(this, this.withStart("dummy", "dummy"))
        }
    }

    @Test fun `with end - simple description`() {
        val desc = descriptionOfCombinedCondition("contain 1 and 2")
        with(desc.withEnding(":")) {
            assert(
                hasContents(
                    "contain 1 and 2:",
                    isAtomic = false, startsPhrase = false, endsPhrase = true
                )
            )
            assertSame(this, this.withEnding("dummy"))
        }
    }

    @Test fun `with start - conjugable description`() {
        val desc = descriptionOfCombinedCondition("contain 1 and 2", "contains 1 and 2")
        with(desc.withStart("x", "must")) {
            assert(
                hasContents(
                    "X must contain 1 and 2", "X contains 1 and 2",
                    isAtomic = false, startsPhrase = true, endsPhrase = false
                )
            )
            assertSame(this, this.withStart("dummy", "dummy"))
        }
    }

    @Test fun `with end - conjugable description`() {
        val desc = descriptionOfCombinedCondition("contain 1 and 2", "contains 1 and 2")
        with(desc.withEnding(":")) {
            assert(
                hasContents(
                    "contain 1 and 2:", "contains 1 and 2:",
                    isAtomic = false, startsPhrase = false, endsPhrase = true
                )
            )
            assertSame(this, this.withEnding("dummy"))
        }
    }

    @Test fun `with start - invertible description`() {
        val desc = descriptionOfBeing("a")
        with(desc.withStart("x", "must")) {
            assert(
                hasContents(
                    "X must be a", "X is a", "X must not be a", "X is not a",
                    isAtomic = false, startsPhrase = true, endsPhrase = false
                )
            )
            assertSame(this, this.withStart("dummy", "dummy"))
        }
    }

    @Test fun `with end - invertible description`() {
        val desc = descriptionOfBeing("a")
        with(desc.withEnding(":")) {
            assert(
                hasContents(
                    "be a:", "is a:", "not be a:", "is not a:",
                    isAtomic = false, startsPhrase = false, endsPhrase = true
                )
            )
            assertSame(this, this.withEnding("dummy"))
        }
    }

    @Test fun `with completion - overridden for conjugable`() {
        val result: ConjugableValueStateDescription =
            descriptionOfCombinedCondition("be blue", "is blue")
                .withCompletion("X", "must")
        assertTrue(result.isComplete)
    }

    @Test fun `with completion - overridden for invertible`() {
        val result: InvertibleValueStateDescription =
            descriptionOfBeing("blue")
                .withCompletion("X", "must")
        assertTrue(result.isComplete)
    }
}
