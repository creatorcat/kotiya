/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class FixedTypeCombinedMatcherTest {

    @Test fun `optimize and check children - needs at least 2 child matchers`() {
        assertThrows<IllegalArgumentException> {
            checkAndOptimizeCombinedMatcherChildren(listOf(isEven))
        }
        assertThrows<IllegalArgumentException> {
            checkAndOptimizeCombinedMatcherChildren(emptyList<Matcher<Any>>())
        }
    }
}
