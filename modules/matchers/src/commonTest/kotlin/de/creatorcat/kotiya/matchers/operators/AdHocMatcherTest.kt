/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class AdHocMatcherTest {

    @Test fun `ad-hoc creation - usage in assertion`() {
        assertThat("abc", matches { it.startsWith("ab") })
        assertThat(listOf("abc", "abcd"), each(matches { it.startsWith("ab") }))
    }

    @Test fun `ad-hoc creation - description`() {
        val m1 = matches<String> { it.startsWith("ab") }
        assertThat(m1.expectedState, hasContents("satisfy some undescribed condition"))

        val m2 = matches<String>("start with ab") { it.startsWith("ab") }
        assertThat(m2.expectedState, hasContents("start with ab"))
    }

    @Test fun `ad-hoc creation - matching`() {
        val m1 = matches<String> { it.startsWith("ab") }
        assertThat(m1.evaluate("abc"), hasContents(true, m1, "is abc"))
        assertThat(m1.evaluate("cbc"), hasContents(false, m1, "is cbc"))
    }
}
