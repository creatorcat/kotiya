/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test

class CombinedEvaluationNodeImplTest {

    private val matcher = OperatorNameMatcher("OP")

    @Test fun `vararg - all children contribute`() {
        val c = ('a'..'c').generateAtomicNodes(true)
        val node = CombinedMatcherEvaluationNode.of(true, matcher, c[0], c[1], c[2])
        node.assert(hasContents(true, matcher, c))
    }

    @Test fun `vararg - throw on empty children`() {
        assertThrows<IllegalArgumentException> { CombinedMatcherEvaluationNode.of(true, matcher) }
    }

    @Test fun `from lists - only subset contributes`() {
        val children = ('a'..'c').generateAtomicNodes(true)
        val contributors = listOf(children[0], children[2])
        val node = CombinedMatcherEvaluationNode.of(true, matcher, children, contributors)
        node.assert(hasContents(true, matcher, children, contributors))
    }

    @Test fun `from lists - default is all children contribute`() {
        val children = ('a'..'c').generateAtomicNodes(true)
        val node = CombinedMatcherEvaluationNode.of(false, matcher, children)
        node.assert(hasContents(false, matcher, children, children))
    }

    @Test fun `from lists - throw on empty children`() {
        assertThrows<IllegalArgumentException> { CombinedMatcherEvaluationNode.of(true, matcher, emptyList()) }
    }

    @Test fun `from lists - throw on empty contributors`() {
        val children = ('a'..'c').generateAtomicNodes(true)
        assertThrows<IllegalArgumentException> {
            CombinedMatcherEvaluationNode.of(true, matcher, children, emptyList())
        }
    }

    @Test fun `from lists - throw when contributors not a subset`() {
        val children = ('a'..'c').generateAtomicNodes(true)
        val contributors = ('d'..'f').generateAtomicNodes(true)
        assertThrows<IllegalArgumentException> {
            CombinedMatcherEvaluationNode.of(false, matcher, children, contributors)
        }
    }

    private fun CharRange.generateAtomicNodes(isMatch: Boolean = true) =
        map { AtomicMatcherEvaluationNode.of(isMatch, AtomicNothingMatcher, "$it") }
}
