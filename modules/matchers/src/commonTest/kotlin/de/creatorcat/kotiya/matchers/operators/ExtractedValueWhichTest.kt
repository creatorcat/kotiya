/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isLessThan10
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.resetObservationData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue

class ExtractedValueWhichTest {

    @Test fun `which - evaluation`() {
        val matcher: CombinedMatcher<Any?> = isInstanceOf(Int::class) which isLessThan10
        with(matcher) {
            val isInt = childMatchers[0]
            // match
            with(evaluate(8)) {
                assert(hasContents(true, matcher, 2))
                children[0].assertIsTypeNode(true, isInt)
                children[1].assert(hasAtomicContents(true, isLessThan10, "led to extracted Int which is 8"))
            }
            // long mismatch
            with(evaluate(20)) {
                assert(hasContents(false, matcher, 2, 1))
                children[0].assertIsTypeNode(true, isInt)
                children[1].assert(hasAtomicContents(false, isLessThan10, "led to extracted Int which is 20"))
                assertSame(children[1], resultContributors[0])
            }

            // fast mismatch
            isLessThan10::evaluate.resetObservationData()
            with(evaluate("a")) {
                assert(hasContents(false, matcher, 1))
                children[0].assertIsTypeNode(false, isInt, "String")
            }
            assertEquals(0, isLessThan10::evaluate.countedCalls)
        }
    }

    private fun MatcherEvaluationNode.assertIsTypeNode(
        isMatch: Boolean, matcher: Matcher<*>, actualTypeName: String = "Int"
    ) {
        assert(hasAtomicContents(isMatch, matcher as AtomicMatcher<*>, "is of type $actualTypeName"))
    }

    @Test fun `which - child matchers`() {
        val isInt = isInstanceOf<Int>()
        val matcher = isInt which isEven
        assertEquals(listOf(isInt, isEven), matcher.childMatchers)
    }

    @Test fun `which - expectation tree with atomic`() {
        val matcher = isInstanceOf<Int>() which isEven
        with(matcher.generateExpectationTree(verb = "SHALL")) {
            assertTrue(isLeaf)
            data.assert(hasContents("be of type Int and the Int SHALL be even", isAtomic = false))
        }
    }

    @Test fun `which - expectation tree with none-phrase-starting leaf`() {
        val matcher = isInstanceOf<Int>().which(isEven and isLessThan10)

        with(matcher.generateExpectationTree(verb = "SHALL")) {
            assertTrue(isLeaf)
            data.assert(
                hasContents("be of type Int and the Int SHALL be even and be less than 10", isAtomic = false)
            )
        }
    }

    @Test fun `which - expectation tree with combined-node`() {
        val matcher = isInstanceOf<Int>() which !(isEven and isLessThan10)

        with(matcher.generateExpectationTree(verb = "SHALL")) {
            assertEquals(1, children.size)
            data.assert(
                hasContents(
                    "be of type Int and the Int SHALL satisfy the following condition:",
                    endsPhrase = true, isAtomic = false
                )
            )
            with(children[0]) {
                assertEquals(1, children.size)
                data.assert(
                    hasContents(
                        "It SHALL not satisfy the following condition:",
                        startsPhrase = true, endsPhrase = true, isAtomic = false
                    )
                )
                with(children[0]) {
                    data.assert(
                        hasContents(
                            "It SHALL be even and be less than 10.",
                            startsPhrase = true, endsPhrase = true, isAtomic = false
                        )
                    )
                }
            }
        }
    }
}
