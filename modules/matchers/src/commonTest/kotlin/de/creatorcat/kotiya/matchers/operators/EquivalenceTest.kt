/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.predicates.appliesTo
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.ConjugableValueStateDescription
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.StateOfBeingMatcher
import de.creatorcat.kotiya.matchers.ValueStateDescription
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.matchers.describedWith
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.is2or3
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isInt
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EquivalenceTest {

    private val matchers = ('a'..'c').associate { it to contains(it.toString()) }

    @Test fun `equivalence - evaluation`() {
        val testee = isAllOrNoneOf(matchers.values.toList())

        // success - all match
        with(testee.evaluate("axxxbxxxc")) {
            assert(hasContents(true, testee, 3))
            children[0].assertContainsNode('a', true)
            children[1].assertContainsNode('b', true)
            children[2].assertContainsNode('c', true)
        }

        // success - none match
        with(testee.evaluate("xyz")) {
            assert(hasContents(true, testee, 3))
            children[0].assertContainsNode('a', false)
            children[1].assertContainsNode('b', false)
            children[2].assertContainsNode('c', false)
        }

        // failure - long mixed match
        with(testee.evaluate("axxxb")) {
            assert(hasContents(false, testee, 3))
            children[0].assertContainsNode('a', true)
            children[1].assertContainsNode('b', true)
            children[2].assertContainsNode('c', false)
        }

        // failure - short mixed match
        with(testee.evaluate("a")) {
            assert(hasContents(false, testee, 2))
            children[0].assertContainsNode('a', true)
            children[1].assertContainsNode('b', false)
        }
    }

    private fun MatcherEvaluationNode.assertContainsNode(key: Char, isMatch: Boolean) {
        assert(
            hasAtomicContents(
                isMatch, matchers[key] ?: throw AssertionError("unknown matcher"),
                "contains ${"no ".orEmptyIf(isMatch)}'$key'"
            )
        )
    }

    @Test fun `iff operator - evaluation`() {
        val testee = contains("a") iff contains("c")
        assertTrue(testee appliesTo "ac")
        assertFalse(testee appliesTo "a")
        assertFalse(testee appliesTo "c")
        assertTrue(testee appliesTo "")
    }

    @Test fun `equivalence - needs at least 2 children`() {
        assertThrows<IllegalArgumentException> {
            isAllOrNoneOf(emptyList<Matcher<Any>>())
        }
        assertThrows<IllegalArgumentException> {
            isAllOrNoneOf(isEven)
        }
    }

    @Test fun `equivalence - child matchers`() {
        assertEquals(matchers.values.toList(), isAllOrNoneOf(matchers.values.toList()).childMatchers)
    }

    @Test fun `equivalence - operator name`() {
        assertTrue(isAllOrNoneOf(isInt, isEven).combinationOperatorName.isNotBlank())
    }

    @Test fun `equivalence - expectation tree with more args atomic`() {
        val testee = isAllOrNoneOf(
            StateOfBeingMatcher("A"), StateOfBeingMatcher("B"), StateOfBeingMatcher("C")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        (tree.data as ConjugableValueStateDescription).assert(
            hasContents(
                "be A, be B and be C or none of them at all",
                "is A, is B and is C or none of them at all",
                isAtomic = false
            )
        )
    }

    @Test fun `equivalence - expectation tree with combined args`() {
        val testee = isAllOrNoneOf(is2or3, isEven)
        val tree = testee.generateExpectationTree("X", "shall")

        assertEquals(2, tree.children.size)
        tree.data.assert(
            hasContents(
                "satisfy all or none of the following conditions:",
                isAtomic = false, endsPhrase = true
            )
        )
        (tree.children[0].data).assert(
            hasContents("X shall be 2 or 3.", endsPhrase = true, startsPhrase = true, isAtomic = false)
        )
        (tree.children[1].data).assert(
            hasContents("X shall be even.", endsPhrase = true, startsPhrase = true, isAtomic = false)
        )
    }

    @Test fun `equivalence - expectation tree with 2 atomics - conjugable and simple`() {
        val testee = isAllOrNoneOf(
            StateOfBeingMatcher("A"),
            StateOfBeingMatcher("B").describedWith(atomicSimpleDescription)
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(hasContents("be something complex if and only if it is A", isAtomic = false))
        assertFalse(tree.data is ConjugableValueStateDescription)
    }

    @Test fun `equivalence - expectation tree with 2 atomics - simple and conjugable`() {
        val testee = isAllOrNoneOf(
            StateOfBeingMatcher("A").describedWith(atomicSimpleDescription),
            StateOfBeingMatcher("B")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(hasContents("be something complex if and only if it is B", isAtomic = false))
        assertFalse(tree.data is ConjugableValueStateDescription)
    }

    @Test fun `equivalence - expectation tree with 2 atomics - both conjugable`() {
        val testee = isAllOrNoneOf(
            StateOfBeingMatcher("A"),
            StateOfBeingMatcher("B")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        (tree.data as ConjugableValueStateDescription).assert(
            hasContents(
                "be A if and only if it is B",
                "is A if and only if it is B",
                isAtomic = false
            )
        )
    }

    @Test fun `equivalence - expectation tree with 2 atomics - both simple`() {
        val testee = isAllOrNoneOf(
            StateOfBeingMatcher("A").describedWith(atomicSimpleDescription),
            StateOfBeingMatcher("B").describedWith(atomicSimpleDescription)
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(
            hasContents("be something complex and be something complex or none of both", isAtomic = false)
        )
        assertFalse(tree.data is ConjugableValueStateDescription)
    }

    private val atomicSimpleDescription: ValueStateDescription = object : ValueStateDescription {
        override val infiniteForm = "be something complex"
        override val isAtomic = true
        override val startsPhrase = false
        override val endsPhrase = false
    }
}
