/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.introspection

import de.creatorcat.kotiya.matchers.descriptionOfAtomicCondition
import de.creatorcat.kotiya.matchers.descriptionOfBeing
import de.creatorcat.kotiya.matchers.descriptionOfCombinedCondition
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.matchers.withCompletion
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class ValueStateDescriptionMatchersTest {

    @Test fun `hasContents - nonspecific description`() {
        val isTesteeMatch = hasContents(
            "X is infinite.",
            isAtomic = false, startsPhrase = true, endsPhrase = true
        )
        assertThat(descriptionOfCombinedCondition("infinite").withCompletion("X", "is"), isTesteeMatch)
        assertThat(descriptionOfCombinedCondition("Y").withCompletion("X", "is"), !isTesteeMatch)
        assertThat(descriptionOfCombinedCondition("X is infinite").withEnding(), !isTesteeMatch)
        assertThat(descriptionOfCombinedCondition("infinite.").withStart("X", "is"), !isTesteeMatch)

        val isTesteeMatchWithDefaults = hasContents("infinite")
        assertThat(descriptionOfAtomicCondition("infinite"), isTesteeMatchWithDefaults)
        assertThat(descriptionOfCombinedCondition("infinite"), !isTesteeMatchWithDefaults)
    }

    @Test fun `hasContents - conjugable description`() {
        val isTesteeMatch = hasContents(
            "X must be infinite.", "X is infinite.",
            isAtomic = false, startsPhrase = true, endsPhrase = true
        )
        assertThat(descriptionOfBeing("infinite").withCompletion("X", "must"), isTesteeMatch)
        assertThat(descriptionOfBeing("infinite").withCompletion("Y", "must"), !isTesteeMatch)
        assertThat(descriptionOfAtomicCondition("be infinite", "is other").withCompletion("X", "must"), !isTesteeMatch)

        val isTesteeMatchWithDefaults = hasContents("be infinite", "is infinite")
        assertThat(descriptionOfBeing("infinite"), isTesteeMatchWithDefaults)
        assertThat(descriptionOfCombinedCondition("be infinite", "is infinite"), !isTesteeMatchWithDefaults)
    }

    @Test fun `hasContents - invertible description`() {
        val isTesteeMatch = hasContents(
            "X must be infinite.", "X is infinite.",
            "X must not be infinite.", "X is not infinite.",
            isAtomic = false, startsPhrase = true, endsPhrase = true
        )
        assertThat(descriptionOfBeing("infinite").withCompletion("X", "must"), isTesteeMatch)
        assertThat(descriptionOfBeing("infinite").withCompletion("Y", "must"), !isTesteeMatch)
        assertThat(descriptionOfBeing("infinite", "un-infinite").withCompletion("X", "must"), !isTesteeMatch)

        val isTesteeMatchWithDefaults = hasContents("be infinite", "is infinite", "not be infinite", "is not infinite")
        assertThat(descriptionOfBeing("infinite"), isTesteeMatchWithDefaults)
    }
}
