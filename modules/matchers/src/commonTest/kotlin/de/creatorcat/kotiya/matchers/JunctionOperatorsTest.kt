/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.predicates.appliesTo
import de.creatorcat.kotiya.core.predicates.appliesToAll
import de.creatorcat.kotiya.core.predicates.appliesToNone
import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasCombinedContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.resetObservationData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertSame
import kotlin.test.assertTrue

class JunctionOperatorsTest {

    @Test fun `conjunction - evaluation`() {
        val isEvenIntBelow10 = ConjunctionMatcher<Int>(listOf<Matcher<Int>>(isEven, isInt, isLessThan10))

        // match
        with(isEvenIntBelow10.evaluate(2)) {
            assert(hasContents(true, isEvenIntBelow10, 3))
            children[0].assert(hasAtomicContents(true, isEven, "is even"))
            children[1].assert(hasAtomicContents(true, isInt, "is Int"))
            children[2].assert(hasAtomicContents(true, isLessThan10, "is 2"))
        }

        // fast fail
        isLessThan10::evaluate.resetObservationData()
        isInt::evaluate.resetObservationData()
        with(isEvenIntBelow10.evaluate(3)) {
            assert(hasContents(false, isEvenIntBelow10, 1))
            children[0].assert(hasAtomicContents(false, isEven, "is odd"))
        }
        assertEquals(0, isLessThan10::evaluate.countedCalls)
        assertEquals(0, isInt::evaluate.countedCalls)

        // long fail
        with(isEvenIntBelow10.evaluate(12)) {
            assert(hasContents(false, isEvenIntBelow10, 3, 1))
            children[0].assert(hasAtomicContents(true, isEven, "is even"))
            children[1].assert(hasAtomicContents(true, isInt, "is Int"))
            children[2].assert(hasAtomicContents(false, isLessThan10, "is 12"))
            assertSame(children[2], resultContributors[0])
        }
    }

    @Test fun `conjunction - child matchers`() {
        val childMatchers = listOf<AtomicMatcher<Int>>(isEven, isInt, isLessThan10)
        val isEvenIntBelow10 = ConjunctionMatcher<Int>(childMatchers)

        assertEquals(childMatchers, isEvenIntBelow10.childMatchers)

        assertThrows<IllegalArgumentException> { ConjunctionMatcher(listOf(isEven)) }
        assertThrows<IllegalArgumentException> { ConjunctionMatcher<Any>(emptyList()) }
    }

    @Test fun `conjunction - expectation tree`() {
        val isEvenIntBelow10 = ConjunctionMatcher<Int>(listOf<Matcher<Int>>(isEven, isInt, isLessThan10))

        val simpleNode = isEvenIntBelow10.generateExpectationTree()
        assertTrue(simpleNode.isLeaf)
        assertEquals("be even, be Int and be less than 10", simpleNode.data.infiniteForm)

        val isEven2or3 = ConjunctionMatcher(listOf(isEven, is2or3))

        val tree = isEven2or3.generateExpectationTree("NOT USED", "shall")
        assertEquals(2, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertTrue(tree.data.infiniteForm.contains("satisfy all of the following conditions"))
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertEquals("It shall be even.", data.infiniteForm)
        }
        with(tree.children[1]) {
            assertTrue(data.isComplete)
            assertEquals("It shall be 2 or 3.", data.infiniteForm)
        }
    }

    @Test fun `disjunction - evaluation`() {
        val isEvenOrDoubleOrBelow10 = DisjunctionMatcher<Int>(listOf<Matcher<Int>>(isEven, isDouble, isLessThan10))

        // fail
        with(isEvenOrDoubleOrBelow10.evaluate(11)) {
            assert(hasContents(false, isEvenOrDoubleOrBelow10, 3))
            children[0].assert(hasAtomicContents(false, isEven, "is odd"))
            children[1].assert(hasAtomicContents(false, isDouble, "is Int"))
            children[2].assert(hasAtomicContents(false, isLessThan10, "is 11"))
        }

        // fast match
        isLessThan10::evaluate.resetObservationData()
        isDouble::evaluate.resetObservationData()
        with(isEvenOrDoubleOrBelow10.evaluate(12)) {
            assert(hasContents(true, isEvenOrDoubleOrBelow10, 1))
            children[0].assert(hasAtomicContents(true, isEven, "is even"))
        }
        assertEquals(0, isLessThan10::evaluate.countedCalls)
        assertEquals(0, isDouble::evaluate.countedCalls)

        // long match
        with(isEvenOrDoubleOrBelow10.evaluate(9)) {
            assert(hasContents(true, isEvenOrDoubleOrBelow10, 3, 1))
            children[0].assert(hasAtomicContents(false, isEven, "is odd"))
            children[1].assert(hasAtomicContents(false, isDouble, "is Int"))
            children[2].assert(hasAtomicContents(true, isLessThan10, "is 9"))
            assertSame(children[2], resultContributors[0])
        }
    }

    @Test fun `disjunction - child matchers`() {
        val childMatchers = listOf<AtomicMatcher<Int>>(isEven, isDouble, isLessThan10)
        val isEvenOrDoubleOrBelow10 = DisjunctionMatcher<Int>(childMatchers)

        assertEquals(childMatchers, isEvenOrDoubleOrBelow10.childMatchers)

        assertThrows<IllegalArgumentException> { DisjunctionMatcher(listOf(isEven)) }
        assertThrows<IllegalArgumentException> { DisjunctionMatcher<Any>(emptyList()) }
    }

    @Test fun `disjunction - expectation tree`() {
        val isEvenOrDoubleOrBelow10 = DisjunctionMatcher<Int>(listOf<Matcher<Int>>(isEven, isDouble, isLessThan10))

        val simpleNode = isEvenOrDoubleOrBelow10.generateExpectationTree()
        assertTrue(simpleNode.isLeaf)
        assertEquals("be even, be Double or be less than 10", simpleNode.data.infiniteForm)

        val isEvenOr2or3 = DisjunctionMatcher(listOf(isEven, is2or3))

        val tree = isEvenOr2or3.generateExpectationTree("NOT USED", "shall")
        assertEquals(2, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertTrue(tree.data.infiniteForm.contains("satisfy any of the following conditions"))
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertEquals("It shall be even.", data.infiniteForm)
        }
        with(tree.children[1]) {
            assertTrue(data.isComplete)
            assertEquals("It shall be 2 or 3.", data.infiniteForm)
        }
    }

    @Test fun `operator and - usage and optimization`() {
        val isEvenBelow10 = isEven and isLessThan10
        assertTrue(isEvenBelow10 appliesTo 2)
        assertEquals(listOf<Matcher<Int>>(isEven, isLessThan10), isEvenBelow10.childMatchers)

        val isIntEvenBelow10 = isInt and isEvenBelow10
        assertFalse(isIntEvenBelow10 appliesTo 12)
        assertEquals(listOf<Matcher<Int>>(isInt, isEven, isLessThan10), isIntEvenBelow10.childMatchers)

        val isEvenBelow10Int = isEvenBelow10 and isInt
        assertFalse(isEvenBelow10Int appliesTo 3)
        assertEquals(listOf<Matcher<Int>>(isEven, isLessThan10, isInt), isEvenBelow10Int.childMatchers)

        val isEvenBelow10Redundant = isEvenBelow10 and isEvenBelow10
        assertTrue(isEvenBelow10Redundant appliesTo 4)
        assertEquals(
            listOf<Matcher<Int>>(isEven, isLessThan10, isEven, isLessThan10),
            isEvenBelow10Redundant.childMatchers
        )
    }

    @Test fun `operator or - usage and optimization`() {
        val isEvenOrBelow10 = isEven or isLessThan10
        assertTrue(isEvenOrBelow10 appliesTo 3)
        assertEquals(listOf<Matcher<Int>>(isEven, isLessThan10), isEvenOrBelow10.childMatchers)

        val isIntOrEvenOrBelow10 = isInt or isEvenOrBelow10
        assertTrue(isIntOrEvenOrBelow10 appliesTo 233)
        assertEquals(listOf<Matcher<Int>>(isInt, isEven, isLessThan10), isIntOrEvenOrBelow10.childMatchers)

        val isEvenOrBelow10OrInt = isEvenOrBelow10 or isInt
        assertTrue(isEvenOrBelow10OrInt appliesTo -231)
        assertEquals(listOf<Matcher<Int>>(isEven, isLessThan10, isInt), isEvenOrBelow10OrInt.childMatchers)

        val isEvenOrBelow10Redundant = isEvenOrBelow10 or isEvenOrBelow10
        assertTrue(isEvenOrBelow10Redundant appliesTo 12)
        assertEquals(
            listOf<Matcher<Int>>(isEven, isLessThan10, isEven, isLessThan10),
            isEvenOrBelow10Redundant.childMatchers
        )
    }

    // TODO move elsewhere?
    @Test fun `operator multi combination - and or negation`() {
        val isIntGE10OrDouble = (isInt and !isLessThan10) or isDouble
        val isIntGE10 = isIntGE10OrDouble.childMatchers[0] as CombinedMatcher
        val isGE10 = isIntGE10.childMatchers[1] as CombinedMatcher

        // match
        with(isIntGE10OrDouble.evaluate(12.3)) {
            assert(hasContents(true, isIntGE10OrDouble, 2, 1))
            children[1].assert(hasAtomicContents(true, isDouble, "is Double"))
            assertSame(children[1], resultContributors[0])
            with(children[0]) {
                assert(hasCombinedContents(false, isIntGE10, 1))
                children[0].assert(hasAtomicContents(false, isInt, "is Double"))
            }
        }

        // mismatch
        with(isIntGE10OrDouble.evaluate(2)) {
            assert(hasContents(false, isIntGE10OrDouble, 2))
            children[1].assert(hasAtomicContents(false, isDouble, "is Int"))
            with(children[0] as CombinedMatcherEvaluationNode) {
                assert(hasContents(false, isIntGE10, 2, 1))
                assertSame(children[1], resultContributors[0])
                children[0].assert(hasAtomicContents(true, isInt, "is Int"))
                with(children[1]) {
                    assert(hasCombinedContents(false, isGE10, 1))
                    children[0].assert(hasAtomicContents(true, isLessThan10, "is 2"))
                }
            }
        }

        // further checks...
        isIntGE10OrDouble.appliesToAll(10, 1.2, 17)
        isIntGE10OrDouble.appliesToNone(2.3, 3, 4L)
    }

    @Test fun `XJunction - expectation tree of non combinable`() {
        val testee = XJunctionMatcherDummyImpl("OP", listOf(isEven, is2or3))

        val tree = testee.generateXJunctionTree("phrase", "X", "shall")
        assertEquals(2, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertTrue(tree.data.infiniteForm.contains("phrase"))
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertEquals("X shall be even.", data.infiniteForm)
        }
        with(tree.children[1]) {
            assertTrue(data.isComplete)
            assertEquals("X shall be 2 or 3.", data.infiniteForm)
        }
    }

    @Test fun `XJunction - expectation tree of combinable`() {
        val testee = XJunctionMatcherDummyImpl("OP", listOf(isEven, isLessThan10, isInt))

        val tree = testee.generateXJunctionTree("NOT USED", "NOT USED", "NOT USED")
        assertTrue(tree.isLeaf)
        with(tree.data) {
            assertFalse(isAtomic)
            assertFalse(startsPhrase)
            assertFalse(endsPhrase)
            assertEquals("be even, be less than 10 OP be Int", infiniteForm)
        }
    }

    private class XJunctionMatcherDummyImpl<T>(
        combinationOperatorName: String,
        childMatchers: List<Matcher<T>>
    ) : XJunctionMatcherBase<T>(combinationOperatorName, childMatchers) {
        override fun generateExpectationTree(valueTitle: String, verb: String) = mustNotBeCalled()
        override fun evaluate(value: T) = mustNotBeCalled()
    }
}
