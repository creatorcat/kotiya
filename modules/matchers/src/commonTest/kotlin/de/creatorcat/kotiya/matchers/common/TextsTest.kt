/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccess
import de.creatorcat.kotiya.test.core.assertSuccessAndFailure
import kotlin.test.Test

class TextsTest {

    @Test fun `isBlank - simple usage`() {
        isBlank.expectedState.assert(hasContents("be blank"))
        isBlank.assertSuccessAndFailure("  \t", "is blank", "abc", "is not blank")
    }

    @Test fun `isNotBlank - simple usage`() {
        isNotBlank.expectedState.assert(hasContents("not be blank"))
        isNotBlank.assertSuccessAndFailure("a", "is not blank", " \n\t", "is blank")
    }

    @Test fun `contains string - simple usage`() {
        val caseSensitive = contains("abc")
        caseSensitive.expectedState.assert(hasContents("contain 'abc'"))
        caseSensitive.assertSuccessAndFailure("xxxabcxxx", "contains 'abc'", "xxx", "contains no 'abc'")

        val ignoringCase = contains("abc", true)
        ignoringCase.expectedState.assert(hasContents("contain 'abc' (ignoring case)"))
        ignoringCase.assertSuccessAndFailure(
            "xxxabcxxx", "contains 'abc' (ignoring case)",
            "xxx", "contains no 'abc' (ignoring case)"
        )
    }

    @Test fun `contains regex - simple usage`() {
        val containsAorB = contains(Regex("A.*B"))
        containsAorB.expectedState.assert(hasContents("contain at least one match of regex 'A.*B'"))
        containsAorB.assertSuccessAndFailure(
            "xxxAxBxxx", "contains a match of regex 'A.*B'",
            "xxx", "contains no match of regex 'A.*B'"
        )
    }

    @Test fun `endsWith string - case sensitive`() {
        with(endsWith("abc")) {
            expectedState.assert(hasContents("end with 'abc'"))
            assertSuccess("xxxabc", "ends with 'abc'")
            assertFailure("xxxyyy", "differs after 0 characters ('[y]')")
            assertFailure("xxxybc", "differs after 2 characters ('[y]bc')")
            assertFailure("bc", "differs after 2 characters ('[]bc')")
            assertFailure("", "differs after 0 characters ('[]')")
        }
    }

    @Test fun `endsWith string - ignoring case`() {
        with(endsWith("abc", true)) {
            expectedState.assert(hasContents("end with 'abc' (ignoring case)"))
            assertSuccess("xxxaBC", "ends with 'abc' (ignoring case)")
            assertFailure("xxxyyC", "differs after 1 characters ('[y]C' - ignoring case)")
            assertFailure("xxxyBc", "differs after 2 characters ('[y]Bc' - ignoring case)")
        }
    }

    @Test fun `endsWith string - empty suffix`() {
        with(endsWith("")) {
            assertSuccess("abc", "ends with ''")
            assertSuccess("", "ends with ''")
        }
    }
}
