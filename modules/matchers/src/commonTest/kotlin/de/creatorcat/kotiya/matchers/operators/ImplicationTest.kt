/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.core.structures.isLeaf
import de.creatorcat.kotiya.matchers.Matcher
import de.creatorcat.kotiya.matchers.StateOfBeingMatcher
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.is2or3
import de.creatorcat.kotiya.matchers.isComplete
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isInt
import de.creatorcat.kotiya.matchers.isLessThan10
import de.creatorcat.kotiya.matchers.or
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.resetObservationData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame
import kotlin.test.assertTrue

class ImplicationTest {

    @Test fun `implies - evaluation`() {
        val ifIntThenLT10 = isInt implies isLessThan10

        // long match
        with(ifIntThenLT10.evaluate(2)) {
            assert(hasContents(true, ifIntThenLT10, 2, 1))
            children[0].assert(hasAtomicContents(true, isInt, "is Int"))
            children[1].assert(hasAtomicContents(true, isLessThan10, "is 2"))
            assertSame(children[1], resultContributors[0])
        }

        // fast match
        isLessThan10::evaluate.resetObservationData()
        with(ifIntThenLT10.evaluate(3.4)) {
            assert(hasContents(true, ifIntThenLT10, 1))
            children[0].assert(hasAtomicContents(false, isInt, "is Double"))
        }
        assertEquals(0, isLessThan10::evaluate.countedCalls)

        // fail
        with(ifIntThenLT10.evaluate(11)) {
            assert(hasContents(false, ifIntThenLT10, 2))
            children[0].assert(hasAtomicContents(true, isInt, "is Int"))
            children[1].assert(hasAtomicContents(false, isLessThan10, "is 11"))
        }
    }

    @Test fun `implies - operator name`() {
        assertTrue((isInt implies isEven).combinationOperatorName.isNotBlank())
    }

    @Test fun `implication base - expectation tree with both args atomic`() {
        val testee = ImplicationMatcherBaseDummyImpl(
            StateOfBeingMatcher("A"), StateOfBeingMatcher("B")
        )
        val tree = testee.generateExpectationTree()

        assertTrue(tree.isLeaf)
        tree.data.assert(hasContents("be B if it is A", isAtomic = false))
    }

    @Test fun `implication base - expectation tree with atomic consequent and combined antecedent`() {
        val testee = ImplicationMatcherBaseDummyImpl(is2or3, isEven)
        val tree = testee.generateExpectationTree()

        assertEquals(1, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertTrue(tree.data.infiniteForm.contains("be even if it"))
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be 2 or 3."))
        }
    }

    @Test fun `implication base - expectation tree with both args combined`() {
        val testee = ImplicationMatcherBaseDummyImpl(is2or3, isEven or isLessThan10)
        val tree = testee.generateExpectationTree()

        assertEquals(2, tree.children.size)
        assertTrue(tree.data.endsPhrase)
        assertTrue(tree.data.infiniteForm.contains("satisfy the second condition if it satisfies the first"))
        with(tree.children[0]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be 2 or 3."))
        }
        with(tree.children[1]) {
            assertTrue(data.isComplete)
            assertTrue(data.infiniteForm.endsWith("must be even or be less than 10."))
        }
    }

    @Test fun `implication base - child matchers`() {
        val testee = ImplicationMatcherBaseDummyImpl(isEven, isLessThan10)
        assertEquals(listOf(isEven, isLessThan10), testee.childMatchers)
    }

    @Test fun `implication base - operator name`() {
        val testee = object : ImplicationMatcherBase<Any>("OP") {
            override val antecedent get() = mustNotBeCalled()
            override val consequent get() = mustNotBeCalled()
            override fun evaluate(value: Any) = mustNotBeCalled()
        }
        assertEquals("OP", testee.combinationOperatorName)
    }

    private class ImplicationMatcherBaseDummyImpl(
        override val antecedent: Matcher<*>,
        override val consequent: Matcher<*>
    ) : ImplicationMatcherBase<Any>("DUMMY") {
        override fun evaluate(value: Any) = mustNotBeCalled()
    }
}
