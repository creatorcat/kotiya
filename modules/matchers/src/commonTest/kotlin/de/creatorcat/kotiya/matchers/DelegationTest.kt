/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccess
import de.creatorcat.kotiya.test.core.stubbing.countedCalls
import de.creatorcat.kotiya.test.core.stubbing.resetObservationData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class DelegationTest {

    @Test fun `atomic delegation - description`() {
        val hasLengthLT10: AtomicMatcher<String> = Matcher.byDelegationTo(isLessThan10, String::length)
        assertSame(isLessThan10.expectedState, hasLengthLT10.expectedState)
    }

    @Test fun `atomic delegation - evaluation`() {
        isLessThan10::evaluate.resetObservationData()
        val hasLengthLT10: AtomicMatcher<String> = Matcher.byDelegationTo(isLessThan10, String::length)
        hasLengthLT10.assertSuccess("12345", "is 5")
        hasLengthLT10.assertFailure("12345678901", "is 11")
        assertEquals(2, isLessThan10::evaluate.countedCalls)
    }
}
