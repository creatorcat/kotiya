/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.introspection

import de.creatorcat.kotiya.matchers.AtomicMatcher
import de.creatorcat.kotiya.matchers.AtomicMatcherEvaluationNodeWithValue
import de.creatorcat.kotiya.matchers.CombinedMatcher
import de.creatorcat.kotiya.matchers.CombinedMatcherEvaluationNode
import de.creatorcat.kotiya.matchers.MatcherEvaluationNode
import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.isDouble
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.isInt
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.matchers.of
import de.creatorcat.kotiya.matchers.or
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class EvaluationNodeMatchersTest {

    @Test fun `hasContents - result-matcher`() {
        val isTesteeMatch = hasContents(true, isInt)
        assertThat(atomic(true, isInt, "a"), isTesteeMatch)
        assertThat(atomic(false, isInt, "any"), !isTesteeMatch)
        assertThat(atomic(true, isDouble, "x"), !isTesteeMatch)

        val isIntOrDouble = isInt or isDouble
        val isCombinedTesteeMatch = hasContents(true, isIntOrDouble)
        assertThat(combined(true, isIntOrDouble), isCombinedTesteeMatch)
        assertThat(combined(false, isIntOrDouble), !isCombinedTesteeMatch)
        assertThat(combined(true, isInt and isEven), !isCombinedTesteeMatch)
    }

    @Test fun `atomic hasContents - result-matcher-state`() {
        val isTesteeMatch = hasContents(true, isInt, "state")
        assertThat(atomic(true, isInt, "state"), isTesteeMatch)
        assertThat(atomic(true, isInt, "x"), !isTesteeMatch)
    }

    @Test fun `atomic hasContents - result-matcher-state-value`() {
        val isTesteeMatch = hasContents(true, isInt, "state", 3)
        assertThat(atomic(true, isInt, "state", 3), isTesteeMatch)
        assertThat(atomic(true, isInt, "state", 17), !isTesteeMatch)
    }

    @Test fun `hasAtomicContents - result-matcher-state`() {
        val isTesteeMatch = hasAtomicContents(true, isInt, "state")
        assertThat(atomic(true, isInt, "state"), isTesteeMatch)
        assertThat(atomic(true, isInt, "x"), !isTesteeMatch)
        assertThat(combined(true, isInt and isEven), !isTesteeMatch)
    }

    @Test fun `hasContents - result-matcher-children`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val notChildren = listOf(children[0])

        val isTesteeMatch = hasContents(true, isIntOrDouble, children)
        assertThat(combined(true, isIntOrDouble, children), isTesteeMatch)
        assertThat(combined(false, isIntOrDouble, children), !isTesteeMatch)
        assertThat(combined(true, isInt and isDouble, children), !isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, notChildren), !isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children, notChildren), !isTesteeMatch)
    }

    @Test fun `hasContents - result-matcher-children-contributors`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val contributors = listOf(children[0])
        val notContributors = listOf(children[1])

        val isTesteeMatch = hasContents(true, isIntOrDouble, children, contributors)
        assertThat(combined(true, isIntOrDouble, children, contributors), isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children), !isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children, notContributors), !isTesteeMatch)
    }

    @Test fun `hasContents - result-matcher-childrenSize`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val notChildren = listOf(children[0])

        val isTesteeMatch = hasContents(true, isIntOrDouble, 2)
        assertThat(combined(true, isIntOrDouble, children), isTesteeMatch)
        assertThat(combined(false, isIntOrDouble, children), !isTesteeMatch)
        assertThat(combined(true, isInt and isDouble, children), !isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, notChildren), !isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children, notChildren), !isTesteeMatch)
    }

    @Test fun `hasContents - result-matcher-childrenSize-contributorsSize`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val contributors = listOf(children[0])

        val isTesteeMatch = hasContents(true, isIntOrDouble, 2, 1)
        assertThat(combined(true, isIntOrDouble, children, contributors), isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children), !isTesteeMatch)
    }

    @Test fun `hasCombinedContents - result-matcher-childrenSize-contributorsSize`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val contributors = listOf(children[0])

        val isTesteeMatch = hasCombinedContents(true, isIntOrDouble, 2, 1)
        assertThat(combined(true, isIntOrDouble, children, contributors), isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children), !isTesteeMatch)
        assertThat(atomic(true, isInt), !isTesteeMatch)
    }

    @Test fun `hasCombinedContents - result-matcher-childrenSize`() {
        val isIntOrDouble = isInt or isDouble
        val children = listOf(atomic(true, isInt), atomic(false, isDouble))
        val notChildren = listOf(children[0])

        val isTesteeMatch = hasCombinedContents(true, isIntOrDouble, 2)
        assertThat(combined(true, isIntOrDouble, children), isTesteeMatch)
        assertThat(combined(true, isIntOrDouble, children, notChildren), !isTesteeMatch)
        assertThat(atomic(true, isInt), !isTesteeMatch)
    }

    private fun atomic(
        isMatch: Boolean, matcher: AtomicMatcher<*>,
        actualState: String = "",
        value: Int = 23
    ) =
        AtomicMatcherEvaluationNodeWithValue.of(isMatch, matcher, value, actualState)

    private fun combined(
        isMatch: Boolean, matcher: CombinedMatcher<*>,
        allChildren: List<MatcherEvaluationNode> = listOf(atomic(true, isInt)),
        contributingChildren: List<MatcherEvaluationNode> = allChildren
    ) =
        CombinedMatcherEvaluationNode.of(isMatch, matcher, allChildren, contributingChildren)
}
