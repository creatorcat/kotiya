/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import kotlin.test.Test
import kotlin.test.assertEquals

class ExpectedStateDescriptionsTest {

    @Test fun `custom wording on atomic matcher`() {
        assertEquals("X should be even.", isEven.describeExpectations("x", "should"))
    }

    @Test fun `description of complex tree`() {
        assertEquals(
            """
                The value must satisfy all of the following conditions:
                * It must satisfy any of the following conditions:
                ** It must be A and be B.
                ** It must not be C.
                * It must satisfy any of the following conditions:
                ** It must not satisfy the following condition:
                *** It must be E or be F.
                ** It must satisfy all of the following conditions:
                *** It must be G or be H.
                *** It must be I or be J.
            """
                .trimIndent(),

            complexMatcher.describeExpectations()
        )
    }

    private val complexMatcher =
        (
            (StateOfBeingMatcher("A") and StateOfBeingMatcher("B"))
                or
                !StateOfBeingMatcher("C")
            ) and
            (
                !(StateOfBeingMatcher("E") or StateOfBeingMatcher("F"))
                    or
                    (
                        (StateOfBeingMatcher("G") or StateOfBeingMatcher("H"))
                            and
                            (StateOfBeingMatcher("I") or StateOfBeingMatcher("J"))
                        )
                )

}
