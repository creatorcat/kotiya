/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import kotlin.test.Test

class TypeChecksTest {

    @Test fun `isNull object - simple usage`() {
        with(isNull) {
            expectedState.assert(hasContents("be null"))
            evaluate("").assert(hasContents(false, this, "is not null"))
            evaluate(null).assert(hasContents(true, this, "is null"))
        }
    }

    @Test fun `isNotNull object - simple usage`() {
        with(isNotNull) {
            expectedState.assert(hasContents("not be null"))
            evaluate("a").assert(hasContents<Any>(true, this, "is not null", "a"))
            evaluate(null).assert(hasContents<Any>(false, this, "is null", null))
        }
    }

    @Test fun `isNoneNull function - simple usage`() {
        with(isNoneNull<String>()) {
            expectedState.assert(hasContents("be of type String"))
            evaluate("a").assert(hasContents(true, this, "is of type String", "a"))
            evaluate(null).assert(hasContents<String>(false, this, "is of type UNKNOWN CLASS", null))
        }
    }

    @Test fun `isInstanceOf generic function - simple usage`() {
        val matcher = isInstanceOf<String>()
        matcher.expectedState.assert(hasContents("be of type String"))
        matcher.evaluate("a").assert(hasContents(true, matcher, "is of type String", "a"))
        matcher.evaluate(3).assert(hasContents<String>(false, matcher, "is of type Int", null))
        matcher.evaluate(null).assert(hasContents<String>(false, matcher, "is of type UNKNOWN CLASS", null))
    }

    @Test fun `isInstanceOf class object function - simple usage`() {
        val matcher = isInstanceOf(String::class)
        matcher.expectedState.assert(hasContents("be of type String"))
        matcher.evaluate("a").assert(hasContents(true, matcher, "is of type String", "a"))
        matcher.evaluate(3).assert(hasContents<String>(false, matcher, "is of type Int", null))
        matcher.evaluate(null).assert(hasContents<String>(false, matcher, "is of type UNKNOWN CLASS", null))
    }
}
