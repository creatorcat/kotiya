/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class ActualStateDescriptionsTest {

    @Test fun `failure description - single node`() {
        val result = nodeOf("is 3")
        assertEquals("x is 3", result.describeWhy("x"))
    }

    @Test fun `failure description - complex tree`() {
        val cSub = listOf(
            nodeOf("is 3"),
            nodeOf("is 4")
        )

        val cRoot = listOf(
            nodeOf("is odd"),
            CombinedMatcherEvaluationNode.of(
                false, combinedMatcher("NOT_USED"),
                cSub, listOf(cSub[0])
            ),
            nodeOf("is equal or above 3")
        )

        val result = CombinedMatcherEvaluationNode.of(
            false, combinedMatcher("NOT_USED"),
            cRoot, listOf(cRoot[1], cRoot[2])
        )
        assertEquals("x is 3 and is equal or above 3", result.describeWhy("x"))
    }

    @Test fun `walk contributors - complex tree`() {
        val c4_3 = listOf(
            nodeOf("4.3.1"),
            nodeOf("4.3.2")
        )
        val c4 = listOf(
            nodeOf("4.1"),
            nodeOf("4.2"),
            CombinedMatcherEvaluationNode.of(
                true, combinedMatcher("4.3"),
                c4_3, listOf(c4_3[0], c4_3[1])
            )
        )
        val c2 = listOf(
            nodeOf("2.1"),
            nodeOf("2.2")
        )
        val cRoot = listOf(
            nodeOf("1"),
            CombinedMatcherEvaluationNode.of(
                false, combinedMatcher("2"),
                c2, listOf(c2[0])
            ),
            nodeOf("3"),
            CombinedMatcherEvaluationNode.of(
                true, combinedMatcher("4"),
                c4, listOf(c4[0], c4[1])
            )
        )

        val tree = CombinedMatcherEvaluationNode.of(
            false, combinedMatcher("root"),
            cRoot, listOf(cRoot[1], cRoot[2], cRoot[3])
        )
        val visited = tree.walkResultContributors()
            .map {
                when (it) {
                    is CombinedMatcherEvaluationNode -> it.matcher.combinationOperatorName
                    is AtomicMatcherEvaluationNode -> it.actualState
                    else -> mustNotBeCalled()
                }
            }
            .toList()
        assertEquals(listOf("root", "2", "2.1", "3", "4", "4.1", "4.2"), visited)
    }

    @Test fun `walk contributors - single node`() {
        val node = nodeOf("is 3")
        val (visited) = node.walkResultContributors().toList()
        assertSame(node, visited)
    }

    private fun nodeOf(
        actualState: String
    ): AtomicMatcherEvaluationNode =
        AtomicMatcherEvaluationNode.of(true, AtomicNothingMatcher, actualState)

    private fun combinedMatcher(opName: String) = OperatorNameMatcher(opName)
}
