/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.reflect.KClass
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class ValueExtractingMatcherTest {

    private val dummyDescription = descriptionOfBeing("a dummy")

    private fun ValueExtractingAtomicMatcher.Companion.defaultTypeNameOf(clazz: KClass<*>) =
        clazz.defaultTypeName

    @Test fun `default type name`() {
        assertEquals("Int", ValueExtractingAtomicMatcher.defaultTypeNameOf(1::class))
        assertEquals("String", ValueExtractingAtomicMatcher.defaultTypeNameOf("a"::class))
        assertEquals("Number", ValueExtractingAtomicMatcher.defaultTypeNameOf(Number::class))

        assertEquals("Object", ValueExtractingAtomicMatcher.defaultTypeNameOf(Any::class))
        val x = {}
        assertEquals("Object", ValueExtractingAtomicMatcher.defaultTypeNameOf(x::class))
        val a = object : Comparable<Int> {
            override fun compareTo(other: Int) = mustNotBeCalled()
        }
        assertEquals("Object", ValueExtractingAtomicMatcher.defaultTypeNameOf(a::class))
    }

    @Test fun `creation from pair - default type naming`() {
        val matcher = ValueExtractingAtomicMatcher.of<Any?, Int>(dummyDescription) {
            val value = it as? Int
            Pair(value, "is ${"no ".orEmptyIf(it is Int)}Int")
        }

        assertEquals(ValueExtractingAtomicMatcher.defaultTypeNameOf(Int::class), matcher.extractedValueTypeName)

        matcher.assertIntMatcher()
    }

    @Test fun `creation from pair - type name as argument`() {
        val matcher = ValueExtractingAtomicMatcher.of<Any?, Int>(dummyDescription, "Integer") {
            val value = it as? Int
            Pair(value, "is ${"no ".orEmptyIf(it is Int)}Int")
        }

        assertEquals("Integer", matcher.extractedValueTypeName)

        matcher.assertIntMatcher()
    }

    private fun ValueExtractingAtomicMatcher<Any?, Int>.assertIntMatcher() {
        assertSame(dummyDescription, expectedState)

        val match = evaluate(23)
        match.assert(hasContents(true, this, "is Int", 23))

        val mismatch = evaluate("not an int")
        mismatch.assert(hasContents<Int>(false, this, "is no Int", null))
    }
}
