/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.core.structures.of
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CombinedMatcherBaseTest : CombinedMatcherBase<Any>("DUMMY") {

    @Test fun `class properties`() {
        assertEquals("DUMMY", combinationOperatorName)
    }

    @Test fun `combine description with operator name - conjugable`() {
        val descriptions = listOf(
            descriptionOfBeing("a"), descriptionOfBeing("b"), descriptionOfBeing("c")
        )
        val nodes = descriptions.map { SimpleDataNode.of(it) }
        val combined = nodes.combineUsingOperatorName("Q") as ConjugableValueStateDescription

        assertEquals("be a, be b Q be c", combined.infiniteForm)
        assertEquals("is a, is b Q is c", combined.thirdPersonForm)
    }

    @Test fun `combine description with operator name - infinite only`() {
        val descriptions = listOf(
            descriptionOfCombinedCondition("a"),
            descriptionOfCombinedCondition("b"),
            descriptionOfCombinedCondition("c")
        )
        val nodes = descriptions.map { SimpleDataNode.of(it) }
        val combined = nodes.combineUsingOperatorName("O")

        assertEquals("a, b O c", combined.infiniteForm)
    }

    @Test fun `data with completed description`() {
        val children = listOf(descriptionOfBeing("1"), descriptionOfBeing("2")).map {
            SimpleDataNode.of(it)
        }
        val node = SimpleDataNode.of(descriptionOfBeing("17"), children)

        val completedDefaultEndNode = node.withCompletedDescription("X", "must")
        assertEquals(children, completedDefaultEndNode.children)

        val completedCustomEndNode = node.withCompletedDescription("X", "must", " or all is lost.")
        assertEquals(children, completedCustomEndNode.children)

        val completedDefaultEnd = completedDefaultEndNode.data as InvertibleValueStateDescription
        assertTrue(completedDefaultEnd.isComplete)
        assertEquals("X must be 17.", completedDefaultEnd.infiniteForm)
        assertEquals("X is 17.", completedDefaultEnd.thirdPersonForm)

        val completedCustomEnd = completedCustomEndNode.data as InvertibleValueStateDescription
        assertTrue(completedCustomEnd.isComplete)
        assertEquals("X must be 17 or all is lost.", completedCustomEnd.infiniteForm)
        assertEquals("X is 17 or all is lost.", completedCustomEnd.thirdPersonForm)
    }

    @Test fun `combined node creation - varargs`() {
        val c = ('a'..'c').generateAtomicNodes()
        val node = CombinedMatcherEvaluationNode.of(true, c[0], c[1], c[2])

        node.assert(hasContents(true, this, c))
    }

    @Test fun `combined node creation - lists`() {
        val children = ('a'..'c').generateAtomicNodes()
        val contributors = children.subList(0, 1)
        val different = CombinedMatcherEvaluationNode.of(true, children, contributors)
        val same = CombinedMatcherEvaluationNode.of(true, children)

        different.assert(hasContents(true, this, children, contributors))
        same.assert(hasContents(true, this, children, children))
    }

    override fun generateExpectationTree(valueTitle: String, verb: String) = mustNotBeCalled()
    override val childMatchers get() = mustNotBeCalled()
    override fun evaluate(value: Any) = mustNotBeCalled()

    private fun CharRange.generateAtomicNodes(isMatch: Boolean = true) =
        map { AtomicMatcherEvaluationNode.of(isMatch, AtomicNothingMatcher, "$it") }
}
