/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccess
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

@ExperimentalUnsignedTypes
class NumbersTest {

    @Test fun `is positive matcher`() {
        with(isPositive) {
            assertThat(expectedState, hasContents("be positive (>= 0)"))

            assertSuccess(0, "is 0", 0u)
            assertSuccess(Int.MAX_VALUE, "is ${Int.MAX_VALUE}", Int.MAX_VALUE.toUInt())
            assertSuccess(27, "is 27", 27u)

            assertFailure(-1, "is -1")
            assertFailure(Int.MIN_VALUE, "is ${Int.MIN_VALUE}")
        }
    }

}
