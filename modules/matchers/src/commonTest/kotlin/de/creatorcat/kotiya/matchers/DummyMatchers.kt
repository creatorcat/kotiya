/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.reflect.className
import de.creatorcat.kotiya.core.structures.SimpleDataNode
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver

internal object AtomicNothingMatcher : AtomicMatcher<Any> {
    override val expectedState get() = mustNotBeCalled()
    override fun evaluate(value: Any) = mustNotBeCalled()
}

internal class StateOfBeingMatcher(expectedStateOfBeing: String) : AtomicMatcher<Any> {
    override val expectedState = descriptionOfBeing(expectedStateOfBeing)
    override fun evaluate(value: Any) = mustNotBeCalled()
}

internal class OperatorNameMatcher(operatorName: String) : CombinedMatcher<Any> {
    override fun generateExpectationTree(valueTitle: String, verb: String) = mustNotBeCalled()
    override val combinationOperatorName = operatorName
    override val childMatchers get() = mustNotBeCalled()
    override fun evaluate(value: Any) = mustNotBeCalled()
}

internal val isInt = CallObservedAtomicMatcher<Any?>(descriptionOfBeing("Int")) {
    Pair(it is Int, "is ${it.className}")
}

internal val isLessThan10 = CallObservedAtomicMatcher<Number>(descriptionOfBeing("less than 10")) {
    Pair(it.toInt() < 10, "is $it")
}

internal val isDouble = CallObservedAtomicMatcher<Any?>(descriptionOfBeing("Double")) {
    Pair(it is Double, "is ${it.className}")
}

internal val isEven = CallObservedAtomicMatcher<Int>(descriptionOfBeing("even")) {
    val even = it % 2 == 0
    Pair(even, "is ${if (even) "even" else "odd"}")
}

internal val is2or3 = CallObservedAtomicMatcher<Any>(descriptionOfCombinedCondition("be 2 or 3")) {
    Pair(it == 2 || it == 3, "is $it")
}

internal class CallObservedAtomicMatcher<T>(
    private val expectedStateDescription: ValueStateDescription,
    private val body: (T) -> Pair<Boolean, String>
) : AtomicMatcher<T> {

    override val expectedState: ValueStateDescription
        get() = withCallObserver(this::expectedState) { expectedStateDescription }

    override fun evaluate(value: T): AtomicMatcherEvaluationNode = withCallObserver(this::evaluate) {
        val (isMatch, actualState) = body(value)
        AtomicMatcherEvaluationNode.of(isMatch, this, actualState)
    }

    override fun generateExpectationTree(valueTitle: String, verb: String): SimpleDataNode<ValueStateDescription> =
        withCallObserver(this::generateExpectationTree) {
            super.generateExpectationTree(valueTitle, verb)
        }
}
