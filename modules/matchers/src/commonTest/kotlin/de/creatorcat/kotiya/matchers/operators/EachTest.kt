/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.operators

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasCombinedContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.isEven
import de.creatorcat.kotiya.matchers.withCompletion
import de.creatorcat.kotiya.test.core.assert
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class EachTest {

    private val hasEvenElements = each(isEven)

    @Test fun `each element - evaluation with elements`() {
        with(hasEvenElements.evaluate(listOf(2, 4, 6))) {
            assert(hasCombinedContents(true, hasEvenElements, 3))
            children[0].assert(hasAtomicContents(true, isEven, "has element 2 at index 0 that is even"))
            children[1].assert(hasAtomicContents(true, isEven, "has element 4 at index 1 that is even"))
            children[2].assert(hasAtomicContents(true, isEven, "has element 6 at index 2 that is even"))
        }
        with(hasEvenElements.evaluate(listOf(2, 3, 4))) {
            assert(hasCombinedContents(false, hasEvenElements, 2, 1))
            assertSame(children[1], resultContributors[0])
            children[0].assert(hasAtomicContents(true, isEven, "has element 2 at index 0 that is even"))
            children[1].assert(hasAtomicContents(false, isEven, "has element 3 at index 1 that is odd"))
        }
    }

    @Test fun `each element - evaluation of empty`() {
        with(hasEvenElements.evaluate(listOf())) {
            assert(hasCombinedContents(true, hasEvenElements, 1))
            children[0].assert(hasAtomicContents(true, isEmpty, "is empty"))
        }
    }

    @Test fun `each element - child matchers`() {
        assertEquals(listOf(isEmpty, isEven), hasEvenElements.childMatchers)
    }

    @Test fun `each element - description`() {
        with(hasEvenElements.generateExpectationTree("LIST", "SHALL")) {
            data.assert(
                hasContents(
                    "be empty or each of its elements must satisfy the following condition:",
                    isAtomic = false, endsPhrase = true
                )
            )
            assertEquals(1, children.size)
            with(children[0]) {
                data.assert(
                    hasContents(
                        isEven.expectedState.withCompletion("The element", "SHALL").infiniteForm,
                        endsPhrase = true, startsPhrase = true, isAtomic = false
                    )
                )
            }
        }
    }
}
