/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers

import de.creatorcat.kotiya.core.reflect.className
import de.creatorcat.kotiya.matchers.introspection.hasAtomicContents
import de.creatorcat.kotiya.matchers.introspection.hasCombinedContents
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.test.core.assert
import de.creatorcat.kotiya.test.core.stubbing.countedCallsForTag
import de.creatorcat.kotiya.test.core.stubbing.withCallCounterForTag
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class DescriptionSubstitutionsTest {

    private object TAG

    private val matcher: AtomicMatcher<Int> =
        Matcher.of(descriptionOfBeing("3")) {
            withCallCounterForTag(TAG) { Pair(it == 3, "is $it") }
        }

    private val beingInt = descriptionOfBeing("Int")

    @Test fun `matcher - with constant expected state and default actual state`() {
        val testee = matcher.describedWith(beingInt)

        assertSame(beingInt, testee.generateExpectationTree().data)
        testee.assertCallDelegationAndResult()
    }

    @Test fun `matcher - with constant expected state and new actual state`() {
        val testee = matcher.describedWith(beingInt) { value, result ->
            "is ${value.className} and ${result.actualState}"
        }

        assertSame(beingInt, testee.generateExpectationTree().data)
        testee.assertCallDelegationAndResult("is Int and is 3")
    }

    @Test fun `matcher - with lazy expected state and default lazy actual state`() {
        val testee = matcher.describedWith { beingInt }

        assertSame(beingInt, testee.generateExpectationTree().data)
        testee.assertCallDelegationAndResult()
    }

    @Test fun `matcher - with lazy expected state and new lazy actual state`() {
        val testee = matcher.describedWith({ beingInt }) { value, result ->
            {
                "is ${value.className} and ${result.actualState}"
            }
        }

        assertSame(beingInt, testee.generateExpectationTree().data)
        testee.assertCallDelegationAndResult("is Int and is 3")
    }

    private fun AtomicMatcher<Int>.assertCallDelegationAndResult(
        newActualState: String? = null
    ) {
        val r1 = matcher.evaluate(3)
        val r2 = this.evaluate(3)

        assertEquals(2, this@DescriptionSubstitutionsTest.countedCallsForTag(TAG))
        r2.assertContents(r1, this, newActualState ?: r1.actualState)
    }

    private fun AtomicMatcherEvaluationNode.assertContents(
        original: AtomicMatcherEvaluationNode,
        matcher: AtomicMatcher<*>,
        newActualState: String
    ) {
        assert(hasContents(original.isMatch, matcher, newActualState))
    }

    @Test fun `atomic evaluation node - prefix actual state`() {
        // make type of node generic so generic method must be called
        val node: MatcherEvaluationNode = AtomicMatcherEvaluationNode.of(true, matcher, "old")
        val newNode = node.withPrefixedActualState("prefix ")
        newNode.assert(hasAtomicContents(true, matcher, "prefix old"))
    }

    @Test fun `combined evaluation node - prefix actual state`() {
        val combMatcher = matcher and matcher
        val atomMatcher = matcher
        val child1 = AtomicMatcherEvaluationNode.of(true, atomMatcher, "state1")
        val child2_1 = AtomicMatcherEvaluationNode.of(true, atomMatcher, "state2_1")
        val child2_2 = AtomicMatcherEvaluationNode.of(false, atomMatcher, "state2_2")
        val child2 = CombinedMatcherEvaluationNode.of(
            false, combMatcher, listOf(child2_1, child2_2), listOf(child2_2)
        )
        // make type of node generic so generic method must be called
        val node: MatcherEvaluationNode = CombinedMatcherEvaluationNode.of(
            false, combMatcher, listOf(child1, child2), listOf(child2)
        )
        val newNode = node.withPrefixedActualState("prefix ")
        newNode.assert(hasCombinedContents(false, combMatcher, 2, 1))
        newNode.children[0].assert(hasAtomicContents(true, atomMatcher, "prefix state1"))
        with(newNode.children[1]) {
            assert(hasCombinedContents(false, combMatcher, 2, 1))
            children[0].assert(hasAtomicContents(true, atomMatcher, "prefix state2_1"))
            children[1].assert(hasAtomicContents(false, atomMatcher, "prefix state2_2"))
        }
    }
}
