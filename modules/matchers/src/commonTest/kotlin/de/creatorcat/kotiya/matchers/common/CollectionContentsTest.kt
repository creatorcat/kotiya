/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.kotiya.matchers.common

import de.creatorcat.kotiya.matchers.InvertibleValueStateDescription
import de.creatorcat.kotiya.matchers.introspection.hasContents
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertFailure
import de.creatorcat.kotiya.test.core.assertSuccess
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CollectionContentsTest {

    @Test fun `containsOnly - description`() {
        assertThat(
            containsOnly(listOf(1, 2, 3)).expectedState,
            isInstanceOf<InvertibleValueStateDescription>() which
                hasContents("only contain elements of [1, 2, 3]", isAtomic = true)
        )
    }

    @Test fun `containsOnly - success cases`() {
        with(containsOnly(listOf(1, 2, 3))) {
            assertSuccess(listOf(1), "only contains elements of [1, 2, 3]")
            assertSuccess(listOf(1, 1, 3), "only contains elements of [1, 2, 3]")
            assertSuccess(listOf(1, 2, 3), "only contains elements of [1, 2, 3]")
            assertSuccess(listOf(3, 2, 1), "only contains elements of [1, 2, 3]")
            assertSuccess(listOf(3, 1, 3, 2, 2, 3, 1), "only contains elements of [1, 2, 3]")
            assertSuccess(listOf(), "only contains elements of [1, 2, 3]")
        }
    }

    @Test fun `containsOnly - empty expected`() {
        with(containsOnly(listOf<Int>())) {
            assertSuccess(listOf(), "only contains elements of []")
            assertFailure(listOf(1), "has forbidden element 1 at index 0")
        }
    }

    @Test fun `containsOnly - failure cases`() {
        with(containsOnly(listOf(1, 2, 3))) {
            assertFailure(listOf(4), "has forbidden element 4 at index 0")
            assertFailure(listOf(1, 2, 3, 4), "has forbidden element 4 at index 3")
            assertFailure(listOf(1, 1, 7, 1, 8), "has forbidden element 7 at index 2")
        }
    }

    @Test fun `containsAll - description`() {
        assertThat(
            containsAll(listOf(1, 2, 3)).expectedState,
            isInstanceOf<InvertibleValueStateDescription>() which
                hasContents("contain all elements of [1, 2, 3]", isAtomic = true)
        )
    }

    @Test fun `containsAll - success cases`() {
        with(containsAll(listOf(1, 2, 3))) {
            assertSuccess(listOf(1, 2, 3), "contains all elements of [1, 2, 3]")
            assertSuccess(listOf(1, 2, 3, 4, 5), "contains all elements of [1, 2, 3]")
            assertSuccess(listOf(1, 2, 6, 23, 2, 4, 3), "contains all elements of [1, 2, 3]")
        }
    }

    @Test fun `containsAll - empty expected`() {
        with(containsAll(listOf<Int>())) {
            assertSuccess(listOf(), "contains all elements of []")
            assertSuccess(listOf(1, 2, 3), "contains all elements of []")
        }
    }

    @Test fun `containsAll - failure cases`() {
        with(containsAll(listOf(1, 2, 3))) {
            assertFailure(listOf(4), "does not contain required element 1")
            assertFailure(listOf(1, 4, 3, 6), "does not contain required element 2")
            assertFailure(listOf(), "does not contain required element 1")
        }
    }

    @Test fun `containsAllAndOnly - evaluation`() {
        val matches = containsAllAndOnly(listOf(1, 2, 3))
        assertTrue(matches(listOf(1, 2, 3)))
        assertTrue(matches(listOf(3, 2, 1)))
        assertTrue(matches(listOf(3, 2, 2, 1, 1, 3)))
        assertFalse(matches(listOf(1, 2, 3, 4)))
        assertFalse(matches(listOf(1, 2)))
    }
}
