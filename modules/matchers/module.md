# Module kotiya-matchers

Framework of self-explaining condition matchers.

A matcher generally:
* checks if a value satisfies certain conditions or is in a specific state
* enables generation of human readable descriptions of what is expected of a value to match
  and why a certain value did not match

[Kotiya matchers](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/-matcher) furthermore:
* may be constructed hierarchically by combining multiple matchers via logic operations
  ([and](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/and.html), [or](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/or.html),
  [not](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/not.html), [each](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers.operators/each.html),
  [isAnyOf](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers.operators/is-any-of.html) and more)
  which generates a tree-like structure with each matcher being one node 
* provide their [expectation description](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/-matcher/generate-expectation-tree.html) as a tree,
  matching the hierarchical structure of the combined matcher,
  and thus enable generation of proper texts even for complex matcher combinations
  (see [describeExpectations](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/describe-expectations.html))
* enable tracing of the [matcher's evaluation path](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/-matcher-evaluation-node)
  through the matcher tree and identifying those child matcher's that actually contribute to the match result
  (i.e. which constraint was actually violated - see [describeWhy](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/describe-why.html))
* allow extraction of type-cast values from successful matching (e.g. `isInstanceOf<Int>() which isLessThan(17)`)
* are also usable as simple [Predicate](https://creatorcat.gitlab.io/kotiya/docs/core/html/kotiya-core/de.creatorcat.kotiya.core.predicates/-predicate.html) functions (e.g. `if (isNull(x))`)
* are implemented as Kotlin common module and thus platform independent

There are of course alternative matcher libraries, but none of them has all of the above listed features. 

State: *late Beta*
* Concepts and type hierarchy have undergone several evolution and evaluation steps
* API might only change in details

Dependencies:
* `kotiya-core`

# Package de.creatorcat.kotiya.matchers

Contains the [matcher interface](https://creatorcat.gitlab.io/kotiya/docs/matchers/html/kotiya-matchers/de.creatorcat.kotiya.matchers/-matcher) and all associated basic types. 

# Package de.creatorcat.kotiya.matchers.common

Contains matcher definitions for common use cases.

# Package de.creatorcat.kotiya.matchers.introspection

Contains matchers meant for testing a matcher itself.

# Package de.creatorcat.kotiya.matchers.operators

Contains matcher operators for logical or structural combination.
