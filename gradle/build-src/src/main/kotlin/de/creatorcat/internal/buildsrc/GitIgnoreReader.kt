/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.internal.buildsrc

import de.creatorcat.internal.buildsrc.GitIgnoreReader.Companion.read
import de.creatorcat.internal.buildsrc.GitIgnoreReader.EntryType.ABSOLUTE
import de.creatorcat.internal.buildsrc.GitIgnoreReader.EntryType.COMMENT
import de.creatorcat.internal.buildsrc.GitIgnoreReader.EntryType.RELATIVE
import de.creatorcat.internal.buildsrc.GitIgnoreReader.EntryType.WHITESPACE
import java.io.File

/**
 * Utility to read the entries from a .gitignore file as [GitIgnoreGroup]s.
 *
 * The start of a group is identified by a comment block (or by the begin of the file).
 * All following entries, relative or absolute, are considered belonging to this group,
 * until another comment block starts the next group.
 *
 * Comment blocks separated by blank lines start a new group.
 *
 * @see read
 */
class GitIgnoreReader private constructor() {

    companion object {
        /**
         * Reads the entries from [gitIgnoreFile] as [GitIgnoreGroup]s.
         */
        @JvmStatic
        fun read(gitIgnoreFile: File): List<GitIgnoreGroup> =
            GitIgnoreReader().readGroups(gitIgnoreFile)
    }

    private val currentComments = mutableListOf<String>()
    private val currentAbsolutes = mutableListOf<String>()
    private val currentRelatives = mutableListOf<String>()
    private val groups = mutableListOf<GitIgnoreGroup>()

    private fun readGroups(gitIgnoreFile: File): List<GitIgnoreGroup> {
        var currentState = findFirstGroup
        gitIgnoreFile.useLines { lines ->
            for (line in lines) {
                currentState = currentState(line)
            }
        }
        // if any entries were read, than the last group is now still open
        if (currentState != findFirstGroup)
            completeGroup()
        return groups
    }

    private class ReaderState(
        private val consumeLine: ReaderState.(String) -> ReaderState
    ) {
        operator fun invoke(line: String): ReaderState = consumeLine(line)
    }

    private val findFirstGroup = ReaderState { line ->
        when (line.getEntryType()) {
            COMMENT -> readGroupComments(line)
            ABSOLUTE, RELATIVE -> readGroupEntries(line)
            WHITESPACE -> this
        }
    }

    private val readGroupComments = ReaderState { line ->
        if (line.getEntryType() == COMMENT) {
            currentComments += line.drop(1)
            this
        } else
            readGroupEntries(line)
    }

    private val readGroupEntries: ReaderState = ReaderState { line ->
        when (line.getEntryType()) {
            WHITESPACE -> this
            COMMENT -> {
                completeGroup()
                readGroupComments(line)
            }
            ABSOLUTE -> {
                currentAbsolutes += line.drop(1)
                this
            }
            RELATIVE -> {
                currentRelatives += line
                this
            }
        }
    }

    private fun completeGroup() {
        // TODO improve list to read-only copy
        groups += GitIgnoreGroup(currentComments.toList(), currentAbsolutes.toList(), currentRelatives.toList())
        currentComments.clear()
        currentAbsolutes.clear()
        currentRelatives.clear()
    }

    private fun String.getEntryType() = when {
        isBlank() -> WHITESPACE
        startsWith("#") -> COMMENT
        startsWith("/") -> ABSOLUTE
        else -> RELATIVE
    }

    private enum class EntryType {
        WHITESPACE,
        COMMENT,
        ABSOLUTE,
        RELATIVE
    }
}

/**
 * Represent a group of .gitignore file entries.
 */
data class GitIgnoreGroup(
    /** One or more lines of comment associated to this group. */
    val commentBlock: List<String>,
    /** Absolute path entries starting with a '/'. */
    val absoluteEntries: List<String>,
    /** Relative path entries NOT starting with a '/'. */
    val relativeEntries: List<String>,

    /** Check if the group contains any entries, either [absoluteEntries] or [relativeEntries]. */
    val hasEntries: Boolean = absoluteEntries.isNotEmpty() || relativeEntries.isNotEmpty()
)
