/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.internal.buildsrc

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class GitIgnoreReaderTest {

    private val samplesDir = File("src/test/resources/gitignore/")
    private val sampleSingleGroup = File(samplesDir, "singleGroup.txt")
    private val sampleStartWithEntry = File(samplesDir, "startWithEntry.txt")
    private val sampleStartWithWhitespace = File(samplesDir, "startWithWhitespace.txt")
    private val sampleEmpty = File(samplesDir, "empty.txt")
    private val sampleOnlyComments = File(samplesDir, "onlyComments.txt")
    private val sampleMultiGroups = File(samplesDir, "multipleGroups.txt")

    @Test fun `equality of GitIgnoreGroup`() {
        val group1 = GitIgnoreGroup(listOf("a"), listOf("b", "c"), listOf("d", "e", "f"))
        val group2 = GitIgnoreGroup(listOf("a"), listOf("b", "c"), listOf("d", "e", "f"))
        val group3 = GitIgnoreGroup(listOf("a"), listOf("b", "c"), listOf("d", "f", "e"))
        assertEquals(group1, group2)
        assertNotEquals(group1, group3)
        assertNotEquals(group3, group2)
    }

    @Test fun `read empty file`() {
        val groups = GitIgnoreReader.read(sampleEmpty)
        assertThat(groups, isEmpty)
    }

    @Test fun `read file with only blanks and comments`() {
        val groups = GitIgnoreReader.read(sampleOnlyComments)
        assertThat(
            groups, hasElementsEqual(
                GitIgnoreGroup(listOf(" comment"), emptyList(), emptyList()),
                GitIgnoreGroup(listOf("### more comments and blanks", " 123"), emptyList(), emptyList())
            )
        )
    }

    @Test fun `read single group`() {
        val groups = GitIgnoreReader.read(sampleSingleGroup)
        assertThat(
            groups, hasElementsEqual(
                GitIgnoreGroup(
                    listOf(" comment"),
                    listOf("abs/dir"),
                    listOf("some/dir/", "some/other/dir/")
                )
            )
        )
    }

    @Test fun `read multiple groups`() {
        val groups = GitIgnoreReader.read(sampleMultiGroups)
        assertThat(
            groups, hasElementsEqual(
                GitIgnoreGroup(
                    listOf(" group 1 - absolutes"),
                    listOf("some/dir/", "some/other/dir/"),
                    emptyList()
                ),
                GitIgnoreGroup(
                    listOf(" group 2 - only comment", " continue...", " last"),
                    emptyList(),
                    emptyList()
                ),
                GitIgnoreGroup(
                    listOf(" group 3 - relatives"),
                    emptyList(),
                    listOf("relative/file", "another/dir/with/file")
                ),
                GitIgnoreGroup(
                    listOf(" group 4 - mix", " of absolute, relative and blanks"),
                    listOf("abs/file", "another/abs/file"),
                    listOf("rel/file", "some/more/rel/file")
                )
            )
        )
    }

    @Test fun `read group starting with entry`() {
        val groups = GitIgnoreReader.read(sampleStartWithEntry)
        assertThat(
            groups, hasElementsEqual(
                GitIgnoreGroup(
                    emptyList(),
                    listOf("abs/dir"),
                    listOf("some/dir/")
                )
            )
        )
    }

    @Test fun `read group starting with whitespace`() {
        val groups = GitIgnoreReader.read(sampleStartWithWhitespace)
        assertThat(
            groups, hasElementsEqual(
                GitIgnoreGroup(
                    emptyList(),
                    listOf("abs/dir"),
                    listOf("some/dir/")
                )
            )
        )
    }

    @Test fun `hasEntries property`() {
        assertTrue(GitIgnoreGroup(emptyList(), listOf("a"), listOf("b")).hasEntries)
        assertTrue(GitIgnoreGroup(emptyList(), emptyList(), listOf("b")).hasEntries)
        assertTrue(GitIgnoreGroup(emptyList(), listOf("a"), emptyList()).hasEntries)
        assertFalse(GitIgnoreGroup(emptyList(), emptyList(), emptyList()).hasEntries)
    }
}
